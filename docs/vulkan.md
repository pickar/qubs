https://vulkan-tutorial.com/Introduction - full tutorial from nothing to drawing triangle, covers ~80% of vulkan \
https://www.khronos.org/registry/vulkan/specs/1.1-extensions/html/vkspec.html - full vulkan specification with extensions \
https://vulkan.lunarg.com/doc/sdk/1.1.126.0/windows/getting_started.html - specification from lunarg (developer of vulkan sdk) \
https://vulkan.lunarg.com/doc/view/1.1.126.0/windows/tutorial/html/16-vulkan_1_1_changes.html - differences in api between vulkan 1.0.xxx and 1.1.xxx \
https://renderdoc.org/vulkan-in-30-minutes.html - some basic outline on vulkan \