package xyz.qubs.engine.vulkan.vertices;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.vulkan.VkVertexInputAttributeDescription;
import org.lwjgl.vulkan.VkVertexInputBindingDescription;

public class GeneralVertex implements IVertex {
	private final Vector3f pos;
	private final Vector2f texCoord;
	private final byte[] bytes = new byte[24];
	private int data;

	public GeneralVertex() {
		this.pos = new Vector3f();
		this.texCoord = new Vector2f();
		updateBytes();
	}

	public GeneralVertex(Vector3f pos, Vector2f texCoord) {
		this.pos = pos;
		this.texCoord = texCoord;
		updateBytes();
	}

	public GeneralVertex(Vector3f pos, Vector2f texCoord, int data) {
		this.pos = pos;
		this.texCoord = texCoord;
		this.data = data;
		updateBytes();
	}

	public GeneralVertex(float x, float y, float z, float texX, float texY, int data) {
		this.pos = new Vector3f(x, y, z);
		this.texCoord = new Vector2f(texX, texY);
		this.data = data;
		updateBytes();
	}

	private void updateBytes() {
		IVertex.writeFloat(bytes, 0, pos.x);
		IVertex.writeFloat(bytes, 4, pos.y);
		IVertex.writeFloat(bytes, 8, pos.z);
		IVertex.writeFloat(bytes, 12, texCoord.x);
		IVertex.writeFloat(bytes, 16, texCoord.y);
		IVertex.writeInt(bytes, 20, data);
	}

	@Override
	public byte[] getBytes() {
		return bytes;
	}

	public Vector3f getPos() {
		return pos;
	}

	public Vector2f getTexCoord() {
		return texCoord;
	}

	@Override
	public VkVertexInputBindingDescription.Buffer getBindingDescription() {
		return VertexInfo.bindingDescriptions[1];
	}

	@Override
	public VkVertexInputAttributeDescription.Buffer getAttributeDescription() {
		return VertexInfo.attributeDescriptions[1];
	}

	@Override
	public int getSize() {
		return (3 + 2 + 1) * 4;
	}
}
