package xyz.qubs.engine.vulkan.vertices;

import org.lwjgl.vulkan.VkVertexInputAttributeDescription;
import org.lwjgl.vulkan.VkVertexInputBindingDescription;

public class CompressedVertex implements IVertex {
	private final byte[] bytes = new byte[getSize()];

	public CompressedVertex() {
	}

	public CompressedVertex(int x, int y, int z, byte ao, int tx, int ty, int textureId) {
		setData(((x & 31) << 27) |
		        ((y & 31) << 22) |
		        ((z & 31) << 17) |
		        ((ao & 3) << 15) |
		        ((tx & 1) << 14) |
		        ((ty & 1) << 13) |
		        (textureId & 8191), -256);
	}

	public CompressedVertex(int x, int y, int z, byte ao, int tx, int ty, int textureId, int color) {
		setData(((x & 31) << 27) |
		        ((y & 31) << 22) |
		        ((z & 31) << 17) |
		        ((ao & 3) << 15) |
		        ((tx & 1) << 14) |
		        ((ty & 1) << 13) |
		        (textureId & 8191), color);
	}

	public void setData(int data, int color) {
		IVertex.writeInt(bytes, 0, data);
		IVertex.writeInt(bytes, 4, color);
//		IVertex.writeInt(bytes, 8, 41225125);
//		IVertex.writeInt(bytes, 12, 251232451);
	}

	@Override
	public byte[] getBytes() {
		return bytes;
	}

	@Override
	public VkVertexInputBindingDescription.Buffer getBindingDescription() {
		return VertexInfo.bindingDescriptions[3];
	}

	@Override
	public VkVertexInputAttributeDescription.Buffer getAttributeDescription() {
		return VertexInfo.attributeDescriptions[3];
	}

	@Override
	public int getSize() {
		return 16;
	}
}
