package xyz.qubs.engine.vulkan.vertices;

import org.lwjgl.vulkan.VkVertexInputAttributeDescription;
import org.lwjgl.vulkan.VkVertexInputBindingDescription;

import static org.lwjgl.vulkan.VK10.*;

public class VertexInfo {
	public static VkVertexInputBindingDescription.Buffer[] bindingDescriptions = new VkVertexInputBindingDescription.Buffer[4];
	public static VkVertexInputAttributeDescription.Buffer[] attributeDescriptions = new VkVertexInputAttributeDescription.Buffer[4];

	public static void init() {
		bindingDescriptions[0] = VkVertexInputBindingDescription.calloc(1).binding(0).stride((3 + 4) * 4).inputRate(VK_VERTEX_INPUT_RATE_VERTEX);
		bindingDescriptions[1] = VkVertexInputBindingDescription.calloc(1);
		bindingDescriptions[1].get(0).binding(0).stride((3 + 2 + 1) * 4).inputRate(VK_VERTEX_INPUT_RATE_VERTEX);
//		bindingDescriptions[1].get(1).binding(1).stride((3 + 3 + 1) * 4).inputRate(VK_VERTEX_INPUT_RATE_INSTANCE);
		bindingDescriptions[2] = VkVertexInputBindingDescription.calloc(1).binding(0).stride((3 + 4 + 2) * 4).inputRate(VK_VERTEX_INPUT_RATE_VERTEX);

		bindingDescriptions[3] = VkVertexInputBindingDescription.calloc(1).binding(0).stride(16).inputRate(VK_VERTEX_INPUT_RATE_VERTEX);

		attributeDescriptions[0] = VkVertexInputAttributeDescription.calloc(2);
		attributeDescriptions[0].get(0)
				.binding(0)
				.location(0)
				.format(VK_FORMAT_R32G32B32_SFLOAT)
				.offset(0); //offset of pos
		attributeDescriptions[0].get(1)
				.binding(0)
				.location(1)
				.format(VK_FORMAT_R32G32B32A32_SFLOAT)
				.offset(4 * 3); //offset of color

		attributeDescriptions[1] = VkVertexInputAttributeDescription.calloc(3);
		attributeDescriptions[1].get(0)
				.binding(0)
				.location(0)
				.format(VK_FORMAT_R32G32B32_SFLOAT)
				.offset(0); //offset of pos
		attributeDescriptions[1].get(1)
				.binding(0)
				.location(1)
				.format(VK_FORMAT_R32G32_SFLOAT)
				.offset(4 * 3); //offset of texture
		attributeDescriptions[1].get(2)
				.binding(0)
				.location(2)
				.format(VK_FORMAT_R32_SINT)
				.offset(4 * (3 + 2)); //offset of data
//		attributeDescriptions[1].get(2)
//				.binding(1)
//				.location(2)
//				.format(VK_FORMAT_R32G32B32_SFLOAT)
//				.offset(0);
//		attributeDescriptions[1].get(3)
//				.binding(1)
//				.location(3)
//				.format(VK_FORMAT_R32G32B32_SFLOAT)
//				.offset(4 * 3);
//		attributeDescriptions[1].get(4)
//				.binding(1)
//				.location(4)
//				.format(VK_FORMAT_R32_UINT)
//				.offset(4 * 3 + 4 * 3);

		attributeDescriptions[2] = VkVertexInputAttributeDescription.calloc(3);
		attributeDescriptions[2].get(0)
				.binding(0)
				.location(0)
				.format(VK_FORMAT_R32G32B32_SFLOAT)
				.offset(0); //offset of pos
		attributeDescriptions[2].get(1)
				.binding(0)
				.location(1)
				.format(VK_FORMAT_R32G32B32A32_SFLOAT)
				.offset(4 * 3); //offset of color
		attributeDescriptions[2].get(2)
				.binding(0)
				.location(2)
				.format(VK_FORMAT_R32G32_SFLOAT)
				.offset(4 * 3 + 4 * 4); //offset of texture

		attributeDescriptions[3] = VkVertexInputAttributeDescription.calloc(4);
		attributeDescriptions[3].get(0)
				.binding(0)
				.location(0)
				.format(VK_FORMAT_R32_SINT)
				.offset(0);
		attributeDescriptions[3].get(1)
				.binding(0)
				.location(1)
				.format(VK_FORMAT_R32_SINT)
				.offset(4);
		attributeDescriptions[3].get(2)
				.binding(0)
				.location(2)
				.format(VK_FORMAT_R32_SINT)
				.offset(8);
		attributeDescriptions[3].get(3)
				.binding(0)
				.location(3)
				.format(VK_FORMAT_R32_SINT)
				.offset(12);
//              5*3 + 2 + 1 + 1 + 13 = 32 bit
//              xyz   ao  tx  ty  texture

	}

	public static void dispose() {
		for (VkVertexInputBindingDescription.Buffer bindingDescription : bindingDescriptions) {
			bindingDescription.free();
		}
		for (VkVertexInputAttributeDescription.Buffer attributeDescription : attributeDescriptions) {
			attributeDescription.free();
		}
	}
}
