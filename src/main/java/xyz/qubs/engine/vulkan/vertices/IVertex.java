package xyz.qubs.engine.vulkan.vertices;

import org.lwjgl.vulkan.VkVertexInputAttributeDescription;
import org.lwjgl.vulkan.VkVertexInputBindingDescription;

public interface IVertex {
	static void writeFloat(byte[] array, int index, float value) {
		final int intBits = Float.floatToIntBits(value);
		for (int i = index; i < index + 4; i++) {
			array[i] = (byte) (intBits >> i * 8);
		}
	}

	static void writeInt(byte[] array, int index, int value) {
		for (int i = index; i < index + 4; i++) {
			array[i] = (byte) (value >> i * 8);
		}
	}

	VkVertexInputBindingDescription.Buffer getBindingDescription();

	VkVertexInputAttributeDescription.Buffer getAttributeDescription();

	int getSize();

	byte[] getBytes();
}
