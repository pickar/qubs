package xyz.qubs.engine.vulkan.models;

import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.ui.layouts.VulkanLayout;

import static org.lwjgl.util.vma.Vma.vmaDestroyImage;
import static org.lwjgl.vulkan.VK10.vkDestroyImageView;

public class VulkanImage {
	public int width;
	public int height;
	public long image;
	public long imageView;
	public long allocation;
	private boolean disposed = false;

	public void free() {
		if (!disposed) {
			disposed = true;
			vkDestroyImageView(Vulkan.device, imageView, null);
			vmaDestroyImage(Vulkan.vmaAllocator, image, allocation);
		}
	}

	public void markForDisposal() {
		VulkanLayout.disposalQueue.add(this);
	}
}