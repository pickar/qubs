package xyz.qubs.engine.vulkan.models;

import xyz.qubs.engine.Vulkan;

import static org.lwjgl.vulkan.VK10.vkDestroyPipeline;
import static org.lwjgl.vulkan.VK10.vkDestroyPipelineLayout;

public class VulkanPipeline {
	public long layout;
	public long pipeline;

	public void free() {
		vkDestroyPipeline(Vulkan.device, pipeline, null);
		vkDestroyPipelineLayout(Vulkan.device, layout, null);
	}
}
