package xyz.qubs.engine.vulkan.models;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.Window;
import xyz.qubs.engine.ui.models.properties.UIColor;
import xyz.qubs.engine.ui.models.properties.UIVector2u;
import xyz.qubs.engine.vulkan.vertices.GeneralVertex;

@Accessors(fluent = true)
public class VulkanUIRenderObject {
	public static VulkanBuffer indexBuffer;
	public static int[] indices;
	public static VulkanBuffer vertexBuffer;

	static {
		indices = new int[] { 0, 1, 2, 2, 3, 0 };
		indexBuffer = Vulkan.createIndexBuffer(indices);
		vertexBuffer = Vulkan.createVertexBuffer(new GeneralVertex[] {
				new GeneralVertex(1, 1, 0, 1, -1, 0),
				new GeneralVertex(1, 0, 0, 1, 0, 0),
				new GeneralVertex(0, 0, 0, 0, 0, 0),
				new GeneralVertex(0, 1, 0, 0, -1, 0),
		});
	}

	@Setter public VulkanImage texture;
	public int textureId = -1;
	public UIVector2u location;
	@Setter public UIVector2u size;
	public UIVector2u minSize;
	@Setter public UIVector2u maxSize;
	public UIColor color;
	@Setter public int cornerRadius;
	@Setter public int zIndex;
	@Setter public boolean visibility;
	@Getter private final boolean isColor;

	public VulkanUIRenderObject(boolean isColor) {
		this.isColor = isColor;
		if (isColor) {
			color = UIColor.TRANSPARENT;
		} else {
			color = UIColor.WHITE;
		}
	}

	public VulkanUIRenderObject minSize(UIVector2u minSize) {
		this.minSize = new UIVector2u(minSize.x.get(), Window.height - minSize.y.get());
		return this;
	}

	public VulkanUIRenderObject location(UIVector2u location) {
		this.location = new UIVector2u(location.x.get(), Window.height - location.y.get());
		return this;
	}

	public VulkanUIRenderObject color(UIColor color) {
		if (color == null) {
			if (isColor) {
				color = UIColor.TRANSPARENT;
			} else {
				color = UIColor.WHITE;
			}
		}

		this.color = color;
		return this;
	}

	public VulkanUIRenderObject mask(UIColor mask) {
		this.color = mask;
		return this;
	}

	public void dispose() {
		if (texture != null) {
			texture.markForDisposal();
		}
	}
}
