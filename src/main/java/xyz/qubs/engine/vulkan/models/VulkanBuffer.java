package xyz.qubs.engine.vulkan.models;

import xyz.qubs.engine.Vulkan;

import java.util.Objects;

import static org.lwjgl.util.vma.Vma.vmaDestroyBuffer;

public class VulkanBuffer {
	public long buffer;
	public long allocation;

	public boolean disposed;

	public VulkanBuffer(VulkanBuffer oldBuffer) {
		buffer = oldBuffer.buffer;
		allocation = oldBuffer.allocation;
		disposed = false;
	}

	public VulkanBuffer() {

	}

	public void free() {
		if (!disposed) {
			vmaDestroyBuffer(Vulkan.vmaAllocator, buffer, allocation);
		}
		disposed = true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		VulkanBuffer that = (VulkanBuffer) o;
		return buffer == that.buffer &&
		       allocation == that.allocation;
	}

	@Override
	public int hashCode() {
		return Objects.hash(buffer, allocation);
	}
}