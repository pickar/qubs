package xyz.qubs.engine.vulkan.renderers;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector3i;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;
import xyz.qubs.Engine;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.Window;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.model.TexturedQuad;
import xyz.qubs.engine.vulkan.models.VulkanBuffer;
import xyz.qubs.engine.vulkan.models.VulkanImage;
import xyz.qubs.engine.vulkan.models.VulkanPipeline;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorPool;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorSet;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorSetLayout;
import xyz.qubs.engine.vulkan.vertices.GeneralVertex;
import xyz.qubs.engine.vulkan.vertices.IVertex;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.block.TextureRegistry;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.List;

import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.vulkan.VK10.*;
import static xyz.qubs.engine.Vulkan.*;
import static xyz.qubs.engine.vulkan.VulkanUtils.VkCheck;
import static xyz.qubs.game.world.chunk.Chunk.CHUNK_SIZE;

public class AdditionalRenderer implements Renderer {
	public static final AdditionalRenderer INSTANCE = new AdditionalRenderer();
	private final IntBuffer intBuf = memAllocInt(1);
	private final LongBuffer longBuf = memAllocLong(1);
	private final PointerBuffer pointerBuf = memAllocPointer(1);
	private VulkanPipeline[] graphicPipelines;
	private DescriptorPool descriptorPool;
	private DescriptorSetLayout descriptorSetLayout;
	private List<DescriptorSet> descriptorSets;
	private long textureSampler;
	private VulkanBuffer[] uniformBuffers;
	private int uniformBufferSize;
	private boolean dirty;
	private VulkanBuffer vertexBuffer;
	private VulkanBuffer indexBuffer;
	private ByteBuffer uboBuffer;

	private AdditionalRenderer() {
	}

	private DescriptorSetLayout createDescriptorSetLayout() {
		return DescriptorSetLayout.builder()
				.addBinding(0, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT)
				.addBinding(1, 1, VK_DESCRIPTOR_TYPE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
				.addBinding(2, 2, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
				.build();
	}

	private VulkanPipeline[] createPipelines() {
		VulkanPipeline[] pipelines = new VulkanPipeline[2];

		long vertShaderModule = createShaderModule("xyz/qubs/shaders/shader_additional.vert", VK_SHADER_STAGE_VERTEX_BIT);
		long fragShaderModule = createShaderModule("xyz/qubs/shaders/shader_additional.frag", VK_SHADER_STAGE_FRAGMENT_BIT);

		VkPipelineShaderStageCreateInfo.Buffer shaderStages = createShaderStages(new long[] { vertShaderModule }, new long[] { fragShaderModule });

		VkPipelineVertexInputStateCreateInfo vertexInputInfo = createVertexInputInfo(new GeneralVertex());
		VkPipelineInputAssemblyStateCreateInfo inputAssembly = createInputAssembly(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
		VkPipelineViewportStateCreateInfo viewportState = createViewportState();
		VkPipelineRasterizationStateCreateInfo rasterizer = createRasterizer(VK_POLYGON_MODE_FILL, 1.0f, VK_CULL_MODE_NONE);
		VkPipelineMultisampleStateCreateInfo multisampling = createMultisampling();

		VkPipelineColorBlendStateCreateInfo colorBlendingDepth = createColorBlending(VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT);
		VkPipelineColorBlendStateCreateInfo colorBlendingColor = createColorBlending(VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT);

		VkPipelineLayoutCreateInfo pipelineLayout = createPipelineLayout(descriptorSetLayout.getHandle(), null);

		VkPipelineDepthStencilStateCreateInfo depthStencilDepth = createDepthStencil(VK_COMPARE_OP_LESS, true);
		VkPipelineDepthStencilStateCreateInfo depthStencilColor = createDepthStencil(VK_COMPARE_OP_EQUAL, false);

		pipelines[0] = createGraphicsPipeline(shaderStages, vertexInputInfo, inputAssembly, viewportState, rasterizer, multisampling, colorBlendingDepth, pipelineLayout, depthStencilDepth); //depth write pipeline
		pipelines[1] = createGraphicsPipeline(shaderStages, vertexInputInfo, inputAssembly, viewportState, rasterizer, multisampling, colorBlendingColor, pipelineLayout, depthStencilColor); //color write pipeline

		vkDestroyShaderModule(device, vertShaderModule, null);
		vkDestroyShaderModule(device, fragShaderModule, null);

		return pipelines;
	}

	private long createTextureSampler() {
		try (MemoryStack stack = stackPush()) {
			VkSamplerCreateInfo samplerInfo = VkSamplerCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO)
					.magFilter(VK_FILTER_NEAREST)
					.minFilter(VK_FILTER_NEAREST)
					.addressModeU(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.addressModeV(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.addressModeW(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.anisotropyEnable(false)
					.maxAnisotropy(1)
					.borderColor(VK_BORDER_COLOR_INT_OPAQUE_BLACK)
					.unnormalizedCoordinates(false)
					.compareEnable(false)
					.compareOp(VK_COMPARE_OP_ALWAYS)
					.mipLodBias(0)
					.minLod(0)
					.maxLod(0);

			VkCheck(vkCreateSampler(device, samplerInfo, null, longBuf), "Failed to create texture sampler");
			return longBuf.get(0);
		}
	}

	private List<DescriptorSet> createDescriptorSets(VulkanImage[] textures, VulkanBuffer[] uniformBuffers) {
		try (MemoryStack stack = stackPush()) {
			List<DescriptorSet> descriptorSets = descriptorPool.createDescriptorSets(descriptorSetLayout, scImageCount);

			VkDescriptorImageInfo.Buffer samplerInfo = VkDescriptorImageInfo.callocStack(1, stack);
			samplerInfo.sampler(textureSampler);

			VkDescriptorImageInfo.Buffer imageInfo = VkDescriptorImageInfo.calloc(textures.length);
			for (int i = 0; i < textures.length; i++) {
				imageInfo.get(i)
						.imageLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
						.imageView(textures[i].imageView);
			}

			for (int i = 0; i < scImageCount; i++) {
				VkDescriptorBufferInfo.Buffer bufferInfo = VkDescriptorBufferInfo.callocStack(1, stack)
						.buffer(uniformBuffers[i].buffer)
						.offset(0)
						.range(uniformBufferSize);

				descriptorSets.get(i).updateBuilder()
						.addWrite(0, 1, 0).pBufferInfo(bufferInfo).add()
						.addWrite(0, 1, 1).pImageInfo(samplerInfo).add()
						.addWrite(0, textures.length, 2).pImageInfo(imageInfo).add()
						.update();
			}

			imageInfo.free();

			return descriptorSets;
		}
	}

	private DescriptorPool createDescriptorPool() {
		return DescriptorPool.builder()
				.setTypeSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, scImageCount)
				.setTypeSize(VK_DESCRIPTOR_TYPE_SAMPLER, scImageCount)
				.setTypeSize(VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 2 * scImageCount)
				.setMaxSets(scImageCount)
				.build();
	}

	@Override
	public void init() {
		uniformBufferSize = 16 * 4 * 3 + 4;

		textureSampler = createTextureSampler();
		descriptorSetLayout = createDescriptorSetLayout();
		createSwapChain();

		IVertex[] vertices = new GeneralVertex[6 * 4 + 4];
		int[] indices = new int[6 * 6 + 6];
		int indexI = 0;
		int indexV = 0;
		TexturedQuad quad;
		for (int i = 0; i < 6; i++) {
			quad = new TexturedQuad(0, 0, 0, Side.values[i], TextureRegistry.selectionTexture, new byte[] { 0, 0, 0, 0 });
			Side side = Side.values[i];
			int[] quadIndices = TexturedQuad.SidedQuad.indices[i];
			for (int k = 0; k < 4; k++) {
				GeneralVertex vertex = TexturedQuad.SidedQuad.vertices[i][k];
				Vector3f position = new Vector3f(vertex.getPos()).mul(quad.getSize()).add(quad.x, quad.y, quad.z);
				Vector2f texture = new Vector2f(vertex.getTexCoord());
				texture.x *= quad.getSize().get(side.textureX);
				texture.y *= quad.getSize().get(side.textureY);
				int data = (0 & 0x3fffffff) | (quad.getAo()[k] << 30);
				vertices[indexV++] = new GeneralVertex(position, texture, data);
			}
			for (int ind : quadIndices) {
				indices[indexI++] = ind + i * 4;
			}
		}
		quad = new TexturedQuad(0, 0, 0, Side.BACK, TextureRegistry.crossTexture, new byte[] { 0, 0, 0, 0 });
		for (int k = 0; k < 4; k++) {
			GeneralVertex vertex = TexturedQuad.SidedQuad.vertices[quad.getSide().ordinal()][k];
			Vector3f position = new Vector3f(vertex.getPos()).mul(quad.getSize()).add(quad.x, quad.y, quad.z);
			Vector2f texture = new Vector2f(vertex.getTexCoord());
			texture.x *= quad.getSize().get(quad.getSide().textureX);
			texture.y *= quad.getSize().get(quad.getSide().textureY);
			int data = (1 & 0x3fffffff) | (quad.getAo()[k] << 30);
			vertices[indexV++] = new GeneralVertex(position, texture, data);
		}
		for (int ind : TexturedQuad.SidedQuad.indices[quad.getSide().ordinal()]) {
			indices[indexI++] = ind + 6 * 4;
		}

		vertexBuffer = createVertexBuffer(vertices);
		indexBuffer = createIndexBuffer(indices);
		uboBuffer = memAlloc(uniformBufferSize);
	}

	@Override
	public boolean isDirty() {
		return true;
	}

	@Override
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	@Override
	public void prepareCommandBuffer(VkCommandBuffer commandBuffer, int imageIndex) {
		IntBuffer imgId = memCallocInt(1);
		imgId.put(0, 0);
		LongBuffer descSetMat = memCallocLong(1);
		descSetMat.put(0, descriptorSets.get(imageIndex).getHandle());
		IntBuffer alignment = memCallocInt(1);
		LongBuffer vBuf = memCallocLong(1);
		vBuf.put(0, vertexBuffer.buffer);
		LongBuffer instanceBuf = memCallocLong(1);
		LongBuffer zero = memCallocLong(1);

		vkCmdBindVertexBuffers(commandBuffer, 0, vBuf, zero);
		vkCmdBindIndexBuffer(commandBuffer, indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);

		for (VulkanPipeline pipeline : graphicPipelines) {
			vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.pipeline);
			vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, descSetMat, null);

			vkCmdDrawIndexed(commandBuffer, 36, 1, 0, 0, 0);
			vkCmdDrawIndexed(commandBuffer, 6, 1, 36, 0, 1);
		}

		memFree(imgId);
		memFree(descSetMat);
		memFree(alignment);
		memFree(vBuf);
		memFree(instanceBuf);
		memFree(zero);
	}

	private void updateUniformBuffer(int imageIndex) {
		Matrix4f mat = new Matrix4f(ubo.proj);
		mat.mul(ubo.view);
		Matrix4f mat2 = new Matrix4f();
		Vector3i blockPos = Engine.camera.selectedBlockPos;
		int sideId = Engine.camera.selectedBlockSide == null ? -1 : Engine.camera.selectedBlockSide.ordinal();
		if (blockPos != null) {
			BlockType block = Engine.world.getBlockType(blockPos.x, blockPos.y, blockPos.z);
			if (block != null && block.getAABBs().length > 0) {
				blockPos = new Vector3i(blockPos).add(-Engine.world.shift.x * CHUNK_SIZE, -Engine.world.shift.y * CHUNK_SIZE, -Engine.world.shift.z * CHUNK_SIZE);

				mat2.translate(blockPos.x + block.getAABBs()[Engine.camera.selectedBlockAABB].min.x, blockPos.y + block.getAABBs()[Engine.camera.selectedBlockAABB].min.y, blockPos.z + block.getAABBs()[Engine.camera.selectedBlockAABB].min.z);
				mat2.scale(new Vector3f(block.getAABBs()[Engine.camera.selectedBlockAABB].max).sub(block.getAABBs()[Engine.camera.selectedBlockAABB].min));
			}
		}
		mat2.scale(1.02f);
		mat2.translate(-0.01f, -0.01f, -0.01f);

		mat.get(0, uboBuffer);
		mat2.get(16 * 4, uboBuffer);

		Matrix4f model2 = new Matrix4f().scale(32f / Window.width, 32f / Window.height, 1).translate(-.5f, -.5f, 0);
		model2.get(16 * 4 * 2, uboBuffer);

		uboBuffer.putInt(16 * 4 * 3, sideId);

		copyMemory(memAddress(uboBuffer), uniformBufferSize, uniformBuffers[imageIndex].allocation);
	}

	@Override
	public void beforeFrame(int imageIndex) {
		updateUniformBuffer(imageIndex);
	}

	@Override
	public void afterFrame(int imageIndex) {
	}

	@Override
	public void createSwapChain() {
		descriptorPool = createDescriptorPool();
		graphicPipelines = createPipelines();
		uniformBuffers = createUniformBuffers(uniformBufferSize);
		descriptorSets = createDescriptorSets(new VulkanImage[] { TextureRegistry.selectionTexture.vulkanImage, TextureRegistry.crossTexture.vulkanImage }, uniformBuffers);
	}

	@Override
	public void cleanupSwapChain() {
		for (VulkanBuffer uniformBuffer : uniformBuffers) {
			uniformBuffer.free();
		}
		for (VulkanPipeline graphicPipeline : graphicPipelines) {
			graphicPipeline.free();
		}
		descriptorPool.dispose();
	}

	@Override
	public void dispose() {
		vkDestroySampler(Vulkan.device, textureSampler, null);
		descriptorSetLayout.dispose();

		vertexBuffer.free();
		indexBuffer.free();

		memFree(intBuf);
		memFree(longBuf);
		memFree(pointerBuf);
		memFree(uboBuffer);
	}
}
