package xyz.qubs.engine.vulkan.renderers;

import org.lwjgl.vulkan.VkCommandBuffer;

public interface Renderer {
	void init();

	boolean isDirty();

	void setDirty(boolean dirty);

	void prepareCommandBuffer(VkCommandBuffer commandBuffer, int imageIndex);

	void beforeFrame(int imageIndex);

	void afterFrame(int imageIndex);

	void createSwapChain();

	void cleanupSwapChain();

	void dispose();
}
