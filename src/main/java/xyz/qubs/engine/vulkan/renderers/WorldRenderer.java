package xyz.qubs.engine.vulkan.renderers;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import org.joml.Math;
import org.joml.*;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;
import xyz.qubs.Engine;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.vulkan.models.VulkanBuffer;
import xyz.qubs.engine.vulkan.models.VulkanImage;
import xyz.qubs.engine.vulkan.models.VulkanPipeline;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorPool;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorSet;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorSetLayout;
import xyz.qubs.engine.vulkan.vertices.CompressedVertex;
import xyz.qubs.game.block.TextureRegistry;
import xyz.qubs.game.world.chunk.Chunk;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;

import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.vulkan.VK10.*;
import static xyz.qubs.Engine.camera;
import static xyz.qubs.Engine.world;
import static xyz.qubs.engine.Vulkan.*;
import static xyz.qubs.engine.vulkan.VulkanUtils.VkCheck;
import static xyz.qubs.engine.vulkan.VulkanUtils.calculateUniformBufferAlignment;
import static xyz.qubs.game.world.chunk.Chunk.*;

public final class WorldRenderer implements Renderer {
	public static final WorldRenderer INSTANCE = new WorldRenderer();
	public static final Vector3i moved = new Vector3i();
	public static boolean worldMoved = false;
	private final IntBuffer intBuf = memAllocInt(1);
	private final LongBuffer longBuf = memAllocLong(1);
	private final PointerBuffer pointerBuf = memAllocPointer(1);
	private final int TICKS_BETWEEN_FRUSTUM_UPDATE = 2;
	private final int maxChunksInBuffer = 5375; //21*256-1
	private final boolean stopUpdatingFrustum = false;
	public int frustumTicks = 0;
	private int textureArraySize = -1;
	private VulkanPipeline[] solidGraphicPipelines;
	private DescriptorPool descriptorPool;
	private DescriptorSetLayout descriptorSetLayout;
	private DescriptorSet[][] descriptorSets;
	private long textureSampler;
	private VulkanBuffer[] matrixUBO;
	private VulkanBuffer[][] chunksUBOs;
	private int descriptorSetCount = 1;
	private int uniformBufferSize;
	private int uniformBufferAlignment;
	private boolean dirty;
	private ObjectList<Chunk> chunksToRender = new ObjectArrayList<>();
	private boolean frustumUpdate = false;
	private int stackId = 0;
	private int oldStackId = -1;
	private ByteBuffer matrixBuffer;
	private ByteBuffer[] chunksBuffers;

	private WorldRenderer() { }

	public static synchronized void updateFrustum(boolean force) {
		if (INSTANCE.stopUpdatingFrustum) {
			return;
		}
		if (force || (INSTANCE.oldStackId != INSTANCE.stackId && INSTANCE.frustumTicks == 0)) {
			INSTANCE.frustumTicks = INSTANCE.TICKS_BETWEEN_FRUSTUM_UPDATE;
			INSTANCE.frustumUpdate = true;
		}
	}

	private DescriptorSetLayout createDescriptorSetLayout() {
		return DescriptorSetLayout.builder()
				.addBinding(0, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT)
				.addBinding(1, 1, VK_DESCRIPTOR_TYPE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
				.addBinding(2, textureArraySize, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
				.addBinding(3, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, VK_SHADER_STAGE_VERTEX_BIT)
				.build();
	}

	private VulkanPipeline[] createPipelines() {
		VulkanPipeline[] pipelines = new VulkanPipeline[3];
		VkPipelineShaderStageCreateInfo.Buffer shaderStages;

		long vertShaderModule = createShaderModule("xyz/qubs/shaders/world_shader.vert", VK_SHADER_STAGE_VERTEX_BIT);
		long fragShaderModule = createShaderModuleCustom("xyz/qubs/shaders/world_shader.frag", VK_SHADER_STAGE_FRAGMENT_BIT, (str) -> str.replace("texture_count", textureArraySize + ""));

		shaderStages = createShaderStages(new long[] { vertShaderModule }, new long[] { fragShaderModule });

		VkPipelineVertexInputStateCreateInfo vertexInputInfo = createVertexInputInfo(new CompressedVertex());
		VkPipelineInputAssemblyStateCreateInfo inputAssembly = createInputAssembly(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
		VkPipelineViewportStateCreateInfo viewportState = createViewportState();
		VkPipelineRasterizationStateCreateInfo rasterizer = createRasterizer(VK_POLYGON_MODE_FILL, 1.0f, VK_CULL_MODE_BACK_BIT);
		VkPipelineMultisampleStateCreateInfo multisampling = createMultisampling();

		VkPipelineColorBlendStateCreateInfo colorBlendingDepth = createColorBlending(0);
		VkPipelineColorBlendStateCreateInfo colorBlendingColor = createColorBlending(VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT);

		VkPipelineLayoutCreateInfo pipelineLayout = createPipelineLayout(descriptorSetLayout.getHandle(), null);

		VkPipelineDepthStencilStateCreateInfo depthStencilDepth = createDepthStencil(VK_COMPARE_OP_LESS, true);
		VkPipelineDepthStencilStateCreateInfo depthStencilColor = createDepthStencil(VK_COMPARE_OP_EQUAL, false);
		VkPipelineDepthStencilStateCreateInfo depthStencilColorTransparent = createDepthStencil(VK_COMPARE_OP_LESS_OR_EQUAL, false);

		pipelines[0] = createGraphicsPipeline(shaderStages, vertexInputInfo, inputAssembly, viewportState, rasterizer, multisampling, colorBlendingDepth, pipelineLayout, depthStencilDepth); //depth write pipeline
		pipelines[1] = createGraphicsPipeline(shaderStages, vertexInputInfo, inputAssembly, viewportState, rasterizer, multisampling, colorBlendingColor, pipelineLayout, depthStencilColor); //color write pipeline
		pipelines[2] = createGraphicsPipeline(shaderStages, vertexInputInfo, inputAssembly, viewportState, rasterizer, multisampling, colorBlendingColor, pipelineLayout, depthStencilColorTransparent); //color write pipeline

		vkDestroyShaderModule(device, vertShaderModule, null);
		vkDestroyShaderModule(device, fragShaderModule, null);

		return pipelines;
	}

	private long createTextureSampler() {
		try (MemoryStack stack = stackPush()) {
			VkSamplerCreateInfo samplerInfo = VkSamplerCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO)
					.magFilter(VK_FILTER_NEAREST)
					.minFilter(VK_FILTER_NEAREST)
					.addressModeU(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.addressModeV(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.addressModeW(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.anisotropyEnable(false)
					.maxAnisotropy(1)
					.borderColor(VK_BORDER_COLOR_INT_OPAQUE_BLACK)
					.unnormalizedCoordinates(false)
					.compareEnable(false)
					.compareOp(VK_COMPARE_OP_ALWAYS)
					.mipLodBias(0)
					.minLod(0)
					.maxLod(0);

			VkCheck(vkCreateSampler(device, samplerInfo, null, longBuf), "Failed to create texture sampler");
			return longBuf.get(0);
		}
	}

	private DescriptorSet[] createDescriptorSets(VulkanImage[] textures, VulkanBuffer[] matrices, VulkanBuffer[] models) {
		if (textures.length > textureArraySize) {
			throw new IllegalArgumentException("Can't create description sets with " + textures.length + " textures");
		}
		try (MemoryStack stack = stackPush()) {
			DescriptorSet[] descriptorSets = descriptorPool.createDescriptorSets(descriptorSetLayout, scImageCount).toArray(new DescriptorSet[0]);

			VkDescriptorImageInfo.Buffer samplerInfo = VkDescriptorImageInfo.callocStack(1, stack);
			samplerInfo.sampler(textureSampler);

			VkDescriptorImageInfo.Buffer imageInfo = VkDescriptorImageInfo.calloc(textureArraySize);
			for (int i = 0; i < textureArraySize; i++) {
				long imageView;
				if (i >= textures.length) {
					imageView = textures[0].imageView;
				} else {
					imageView = textures[i].imageView;
				}
				imageInfo.get(i)
						.imageLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
						.imageView(imageView);
			}

			for (int i = 0; i < scImageCount; i++) {
				VkDescriptorBufferInfo.Buffer bufferInfo = VkDescriptorBufferInfo.callocStack(1, stack)
						.buffer(matrices[i].buffer)
						.offset(0)
						.range(uniformBufferAlignment);

				VkDescriptorBufferInfo.Buffer bufferInfo2 = VkDescriptorBufferInfo.callocStack(1, stack)
						.buffer(models[i].buffer)
						.offset(0)
						.range(256);

				descriptorSets[i].updateBuilder()
						.addWrite(0, 1, 0).pBufferInfo(bufferInfo).add()
						.addWrite(0, 1, 1).pImageInfo(samplerInfo).add()
						.addWrite(0, textureArraySize, 2).pImageInfo(imageInfo).add()
						.addWrite(0, 1, 3).pBufferInfo(bufferInfo2).add()
						.update();
			}
			imageInfo.free();

			return descriptorSets;
		}
	}

	private DescriptorPool createDescriptorPool() {
		return DescriptorPool.builder()
				.setTypeSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, scImageCount * descriptorSetCount)
				.setTypeSize(VK_DESCRIPTOR_TYPE_SAMPLER, scImageCount * descriptorSetCount)
				.setTypeSize(VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, scImageCount * textureArraySize * descriptorSetCount)
				.setTypeSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, scImageCount * descriptorSetCount)
				.setMaxSets(scImageCount * descriptorSetCount)
				.build();
	}

	@Override
	public void init() {
		uniformBufferAlignment = calculateUniformBufferAlignment(16 * 3 * 4);
		uniformBufferSize = 256 * 256;
		textureArraySize = TextureRegistry.getVulkanImageArray().length;

		matrixBuffer = memAlloc(uniformBufferAlignment);
		chunksBuffers = new ByteBuffer[] { memAlloc(uniformBufferSize) };

		textureSampler = createTextureSampler();
		descriptorSetLayout = createDescriptorSetLayout();
		createSwapChain();

		updateFrustum(true);
		dirty = true;
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	@Override
	public void prepareCommandBuffer(VkCommandBuffer commandBuffer, int imageIndex) {
		IntBuffer imgId = memCallocInt(1);
		imgId.put(0, 0);
		LongBuffer descSetMat = memCallocLong(1);
		descSetMat.put(0, descriptorSets[0][imageIndex].getHandle());
		IntBuffer alignment = memCallocInt(1);
		LongBuffer vBuf = memCallocLong(1);
		LongBuffer instanceBuf = memCallocLong(1);
		LongBuffer zero = memCallocLong(1);
		IntBuffer offset = memCallocInt(1);

		for (int i = 0; i < 2; i++) {
			VulkanPipeline pipeline = solidGraphicPipelines[i];
			vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.pipeline);

			int offsets = 0;
			int index = 0;
			int bufferIndex = 0;
			int chunkCount = 0;
			int lodLevel;
			offset.put(0, offsets);
			descSetMat.put(0, descriptorSets[0][imageIndex].getHandle());
			vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, descSetMat, offset);
			for (Chunk chunk : chunksToRender) {
				if (chunk != null && chunk.ready()) {
					lodLevel = 0;//Maths.clamp((Maths.floor(Math.sqrt(chunk.getDistanceSquaredToCamera())) >> LOG2_CHUNK_SIZE) / 9, 0, MAX_LOD_LEVEL);

					RenderData data = chunk.renderData;
					if (data.indexCount[lodLevel] > 0) {
						vBuf.put(0, data.vertexBuffer.buffer);
						vkCmdBindVertexBuffers(commandBuffer, 0, vBuf, zero);
						vkCmdBindIndexBuffer(commandBuffer, data.indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);
						vkCmdDrawIndexed(commandBuffer, data.indexCount[lodLevel], 1, data.firstIndex[lodLevel], data.vertexOffset[lodLevel], index * 3);
					}
				}
				if (++index == 21) {
					index = 0;
					offsets += 256; //TODO: make it depend on real minimal offset
					offset.put(0, offsets);
					vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, descSetMat, offset);
				}
				if (chunkCount++ == maxChunksInBuffer) {
					chunkCount = 0;
					bufferIndex++;
					index = 0;
					offsets = 0;
					offset.put(0, offsets);
					descSetMat.put(0, descriptorSets[bufferIndex][imageIndex].getHandle());
					vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, descSetMat, offset);
				}
			}

			//render entities (mobs, players, dropped items, custom blocks(?))
		}

		VulkanPipeline pipeline = solidGraphicPipelines[2];
		vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.pipeline);

		int offsets = 0;
		int index = 0;
		int bufferIndex = 0;
		int chunkCount = 0;
		int lodLevel;
		offset.put(0, offsets);
		descSetMat.put(0, descriptorSets[0][imageIndex].getHandle());
		vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, descSetMat, offset);
		for (Chunk chunk : chunksToRender) {
			if (chunk != null && chunk.ready()) {
				lodLevel = 0;//Maths.clamp((Maths.floor(Math.sqrt(chunk.getDistanceSquaredToCamera())) >> LOG2_CHUNK_SIZE) / 9, 0, MAX_LOD_LEVEL);

				RenderData data = chunk.renderData;
				if (data.indexCountTransparent[lodLevel] > 0) {
					vBuf.put(0, data.vertexBufferTransparent.buffer);
					vkCmdBindVertexBuffers(commandBuffer, 0, vBuf, zero);
					vkCmdBindIndexBuffer(commandBuffer, data.indexBufferTransparent.buffer, 0, VK_INDEX_TYPE_UINT32);
					vkCmdDrawIndexed(commandBuffer, data.indexCountTransparent[lodLevel], 1, data.firstIndexTransparent[lodLevel], data.vertexOffsetTransparent[lodLevel], index * 3);
				}
			}
			if (++index == 21) {
				index = 0;
				offsets += 256; //TODO: make it depend on real minimal offset
				offset.put(0, offsets);
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, descSetMat, offset);
			}
			if (chunkCount++ == maxChunksInBuffer) {
				chunkCount = 0;
				bufferIndex++;
				index = 0;
				offsets = 0;
				offset.put(0, offsets);
				descSetMat.put(0, descriptorSets[bufferIndex][imageIndex].getHandle());
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, descSetMat, offset);
			}
		}

		memFree(imgId);
		memFree(descSetMat);
		memFree(alignment);
		memFree(vBuf);
		memFree(instanceBuf);
		memFree(zero);
		memFree(offset);
	}

	private void updateMatrixUniformBuffer(int imageIndex) {
		ubo.proj.get(0, matrixBuffer);
		ubo.view.get(16 * 4, matrixBuffer);

		copyMemory(memAddress(matrixBuffer), uniformBufferAlignment, matrixUBO[imageIndex].allocation);
	}

	private void fillChunksBuffer() {
		int offsets = 0;
		int index = 0;

		int bufferIndex = 0;
		int chunksCount = 0;

		for (Chunk chunk : chunksToRender) {
			chunksBuffers[bufferIndex].putFloat(offsets + index * 12 + 0, (chunk.getChunkPos().x - world.shift.x) << LOG2_CHUNK_SIZE);
			chunksBuffers[bufferIndex].putFloat(offsets + index * 12 + 4, (chunk.getChunkPos().y - world.shift.y) << LOG2_CHUNK_SIZE);
			chunksBuffers[bufferIndex].putFloat(offsets + index * 12 + 8, (chunk.getChunkPos().z - world.shift.z) << LOG2_CHUNK_SIZE);

			if (++index == 21) {
				index = 0;
				offsets += 256; //TODO: make it depend on real minimal offset
			}
			if (chunksCount++ == maxChunksInBuffer) {
				index = 0;
				offsets = 0;
				bufferIndex++;
				chunksCount = 0;
			}
		}
	}

	@Override
	public void beforeFrame(int imageIndex) {
		if (frustumUpdate) {
			disposeBuffers();
			ChunkCuller.doCulling();

			if (chunksToRender.size() > maxChunksInBuffer * descriptorSetCount) {
				descriptorSetCount = (int) Math.ceil((float) chunksToRender.size() / maxChunksInBuffer);
				for (ByteBuffer buffer : chunksBuffers) {
					memFree(buffer);
				}
				chunksBuffers = new ByteBuffer[descriptorSetCount];
				for (int i = 0; i < descriptorSetCount; i++) {
					chunksBuffers[i] = memAlloc(uniformBufferSize);
				}
				cleanupSwapChain();
				createSwapChain();
			}

			fillChunksBuffer(); //TODO: update chunks only when they are changed
		}
		for (int i = 0; i < descriptorSetCount; i++) {
			copyMemory(memAddress(chunksBuffers[i]), uniformBufferSize, chunksUBOs[i][imageIndex].allocation);
		}
		updateMatrixUniformBuffer(imageIndex);
	}

	@Override
	public void afterFrame(int imageIndex) {
		if (dirty) {
			dirty = false;
			stackId++;
		}
	}

	@Override
	public void createSwapChain() {
		descriptorPool = createDescriptorPool();
		solidGraphicPipelines = createPipelines();
		matrixUBO = createUniformBuffers(uniformBufferAlignment);
		chunksUBOs = new VulkanBuffer[descriptorSetCount][];
		descriptorSets = new DescriptorSet[descriptorSetCount][];
		for (int i = 0; i < descriptorSetCount; i++) {
			chunksUBOs[i] = createUniformBuffers(uniformBufferSize);
			descriptorSets[i] = createDescriptorSets(TextureRegistry.getVulkanImageArray(), matrixUBO, chunksUBOs[i]);
		}
	}

	@Override
	public void cleanupSwapChain() {
		for (VulkanBuffer uniformBuffer : matrixUBO) {
			uniformBuffer.free();
		}
		for (VulkanBuffer[] uniformBuffers : chunksUBOs) {
			for (VulkanBuffer uniformBuffer : uniformBuffers) {
				uniformBuffer.free();
			}
		}
		for (VulkanPipeline graphicPipeline : solidGraphicPipelines) {
			graphicPipeline.free();
		}
		descriptorPool.dispose();
	}

	@Override
	public void dispose() {
		vkDestroySampler(device, textureSampler, null);
		descriptorSetLayout.dispose();

		memFree(intBuf);
		memFree(longBuf);
		memFree(pointerBuf);
		memFree(matrixBuffer);
		for (int i = 0; i < descriptorSetCount; i++) {
			memFree(chunksBuffers[i]);
		}
	}

	public static class ChunkCuller {
		public static final CustomFrustumIntersection frustumIntersection = new CustomFrustumIntersection();
		private static final Queue<CullingInfo> queue = new LinkedList<>();
		private static final Vector3f chunkPosMin = new Vector3f();
		private static final Vector3f chunkPosMax = new Vector3f();

		public static void doCulling() {
			INSTANCE.frustumUpdate = false;

			synchronized (world.chunksMap) {
				for (Chunk chunk : world.chunksMap.values()) {
					if (chunk != null && !chunk.disposed) {
						chunk.visited = false;
					}
				}
			}

			if (worldMoved) {
				worldMoved = false;
				Engine.camera.getLocalPosition().add(moved.x << LOG2_CHUNK_SIZE, moved.y << LOG2_CHUNK_SIZE, moved.z << LOG2_CHUNK_SIZE);
				moved.zero();
			}
			fillUBO(); //FIXME

			Matrix4f mat = new Matrix4f(ubo.proj);
			Matrix4f view = new Matrix4f(ubo.view);

			mat.mul(view);
			frustumIntersection.set(mat);
			ObjectList<Chunk> newChunkList = new ObjectArrayList<>();

			Chunk startChunk = camera.getChunk();
			if (startChunk != null) {
				newChunkList.add(startChunk);
				queue.add(new CullingInfo(startChunk, null, new boolean[6]));
				startChunk.visited = true;
			}

			CullingInfo info;
			Chunk chunk;
			while (!queue.isEmpty()) {
				info = queue.poll();
				for (Side side : Side.values) {
					chunk = checkChunk(info, side.x, side.y, side.z, side);
					if (chunk != null) {
						newChunkList.add(chunk);
					}
				}
			}
			newChunkList.removeIf(chunk1 -> chunk1.indexCount == 0);
			newChunkList.sort(Comparator.comparingDouble(Chunk::getDistanceSquaredToCamera));

			INSTANCE.chunksToRender = newChunkList;
			INSTANCE.oldStackId = INSTANCE.stackId;
			INSTANCE.dirty = true;
		}

		private static Chunk checkChunk(CullingInfo info, int ox, int oy, int oz, Side from) {
			if (info.directions[from.opposite().ordinal()]) {
				return null;
			}

			final int x = info.chunk.getChunkPos().x + ox;
			final int y = info.chunk.getChunkPos().y + oy;
			final int z = info.chunk.getChunkPos().z + oz;

			final Chunk chunk = world.getChunk(x, y, z);
			if (chunk == null || !chunk.ready() || chunk.visited) {
				return null;
			}

			if (info.from != null) {
				if (!info.chunk.isConnected(info.from, from)) {
					return null;
				}
			}

			chunkPosMin.set(chunk.getChunkPos().x, chunk.getChunkPos().y, chunk.getChunkPos().z).sub(world.shift.x, world.shift.y, world.shift.z).mul(CHUNK_SIZE);
			chunkPosMax.set(chunkPosMin).add(CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE);
			if (!frustumIntersection.testAab(chunkPosMin, chunkPosMax)) {
				return null;
			}

			final boolean[] newDirs = new boolean[6];
			System.arraycopy(info.directions, 0, newDirs, 0, 6);
			newDirs[from.ordinal()] = true;

			queue.add(new CullingInfo(chunk, from.opposite(), newDirs));

			chunk.visited = true;

			return chunk;
		}

		private static class CullingInfo {
			final Chunk chunk;
			final Side from;
			final boolean[] directions;

			public CullingInfo(Chunk chunk, Side from, boolean[] directions) {
				this.chunk = chunk;
				this.from = from;
				this.directions = directions;
			}
		}

		public static class CustomFrustumIntersection {
			public float nxX, nxY, nxZ, nxW;
			public float pxX, pxY, pxZ, pxW;
			public float nyX, nyY, nyZ, nyW;
			public float pyX, pyY, pyZ, pyW;
			public float nzX, nzY, nzZ, nzW;
			public float pzX, pzY, pzZ, pzW;

			public void set(Matrix4fc m) {
				nxX = m.m03() + m.m00();
				nxY = m.m13() + m.m10();
				nxZ = m.m23() + m.m20();
				nxW = m.m33() + m.m30();
				pxX = m.m03() - m.m00();
				pxY = m.m13() - m.m10();
				pxZ = m.m23() - m.m20();
				pxW = m.m33() - m.m30();
				nyX = m.m03() + m.m01();
				nyY = m.m13() + m.m11();
				nyZ = m.m23() + m.m21();
				nyW = m.m33() + m.m31();
				pyX = m.m03() - m.m01();
				pyY = m.m13() - m.m11();
				pyZ = m.m23() - m.m21();
				pyW = m.m33() - m.m31();
				nzX = m.m03() + m.m02();
				nzY = m.m13() + m.m12();
				nzZ = m.m23() + m.m22();
				nzW = m.m33() + m.m32();
				pzX = m.m03() - m.m02();
				pzY = m.m13() - m.m12();
				pzZ = m.m23() - m.m22();
				pzW = m.m33() - m.m32();
			}

			public void set(Vector3f start, Vector3f p0, Vector3f p1, Vector3f p2, Vector3f p3) {

//				nxX = m.m03() + m.m00();
//				nxY = m.m13() + m.m10();
//				nxZ = m.m23() + m.m20();
//				nxW = m.m33() + m.m30();
//
//				pxX = m.m03() - m.m00();
//				pxY = m.m13() - m.m10();
//				pxZ = m.m23() - m.m20();
//				pxW = m.m33() - m.m30();
//
//				nyX = m.m03() + m.m01();
//				nyY = m.m13() + m.m11();
//				nyZ = m.m23() + m.m21();
//				nyW = m.m33() + m.m31();
//
//				pyX = m.m03() - m.m01();
//				pyY = m.m13() - m.m11();
//				pyZ = m.m23() - m.m21();
//				pyW = m.m33() - m.m31();
//
//				nzX = m.m03() + m.m02();
//				nzY = m.m13() + m.m12();
//				nzZ = m.m23() + m.m22();
//				nzW = m.m33() + m.m32();
//
//				pzX = m.m03() - m.m02();
//				pzY = m.m13() - m.m12();
//				pzZ = m.m23() - m.m22();
//				pzW = m.m33() - m.m32();
			}

			public boolean checkChunk(Chunk chunk) {
				chunkPosMin.set(chunk.getChunkPos().x, chunk.getChunkPos().y, chunk.getChunkPos().z).sub(world.shift.x, world.shift.y, world.shift.z).mul(CHUNK_SIZE);
				chunkPosMax.set(chunkPosMin).add(CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE);

				return frustumIntersection.testAab(chunkPosMin, chunkPosMax);
			}

			public boolean testAab(Vector3fc min, Vector3fc max) {
				return testAab(min.x(), min.y(), min.z(), max.x(), max.y(), max.z());
			}

			public boolean testAab(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
				return nxX * (nxX < 0 ? minX : maxX) + nxY * (nxY < 0 ? minY : maxY) + nxZ * (nxZ < 0 ? minZ : maxZ) >= -nxW &&
				       pxX * (pxX < 0 ? minX : maxX) + pxY * (pxY < 0 ? minY : maxY) + pxZ * (pxZ < 0 ? minZ : maxZ) >= -pxW &&
				       nyX * (nyX < 0 ? minX : maxX) + nyY * (nyY < 0 ? minY : maxY) + nyZ * (nyZ < 0 ? minZ : maxZ) >= -nyW &&
				       pyX * (pyX < 0 ? minX : maxX) + pyY * (pyY < 0 ? minY : maxY) + pyZ * (pyZ < 0 ? minZ : maxZ) >= -pyW &&
				       nzX * (nzX < 0 ? minX : maxX) + nzY * (nzY < 0 ? minY : maxY) + nzZ * (nzZ < 0 ? minZ : maxZ) >= -nzW &&
				       pzX * (pzX < 0 ? minX : maxX) + pzY * (pzY < 0 ? minY : maxY) + pzZ * (pzZ < 0 ? minZ : maxZ) >= -pzW;
			}
		}
	}
}
