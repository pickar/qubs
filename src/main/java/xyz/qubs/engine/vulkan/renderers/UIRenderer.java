package xyz.qubs.engine.vulkan.renderers;

import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import org.joml.Matrix4f;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.Window;
import xyz.qubs.engine.event.EventManager;
import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.event.events.WindowResizedEvent;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.calculation.RenderTree;
import xyz.qubs.engine.vulkan.VulkanUtils;
import xyz.qubs.engine.vulkan.models.VulkanBuffer;
import xyz.qubs.engine.vulkan.models.VulkanImage;
import xyz.qubs.engine.vulkan.models.VulkanPipeline;
import xyz.qubs.engine.vulkan.models.VulkanUIRenderObject;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorPool;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorSet;
import xyz.qubs.engine.vulkan.models.descriptors.DescriptorSetLayout;
import xyz.qubs.engine.vulkan.vertices.GeneralVertex;
import xyz.qubs.game.block.TextureRegistry;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.vulkan.VK10.*;
import static xyz.qubs.engine.Vulkan.copyMemory;
import static xyz.qubs.engine.Vulkan.scImageCount;
import static xyz.qubs.engine.settings.SettingsManager.debugMode;
import static xyz.qubs.engine.vulkan.VulkanUtils.VkCheck;

public class UIRenderer implements Renderer {
	public static final UIRenderer INSTANCE = new UIRenderer();
	public static int textureCountPerSet;
	private static int updateId = 0;
	private final IntBuffer intBuf = memAllocInt(1);
	private final LongBuffer longBuf = memAllocLong(1);
	private final PointerBuffer pointerBuf = memAllocPointer(1);
	private final int matrixBufferSize = 4 * 16;
	private final int modelBufferSize = 4 * 4;
	private final Matrix4f projection = new Matrix4f();
	private final Queue<UpdateInfo> updateQueue = new LinkedList<>();
	private final ByteBuffer matrixBuffer = memAlloc(matrixBufferSize);
	private ByteBuffer modelBuffer = memAlloc(modelBufferSize);
	private ByteBuffer modelBuffer2 = memAlloc(modelBufferSize);
	private VulkanBuffer[] matrixUniformBuffers;
	private VulkanBuffer[] modelUniformBuffers;
	private VulkanBuffer[] modelUniformBuffers2;
	private DescriptorSetLayout textureDescriptorSetLayout;
	private DescriptorSetLayout uboDescriptorSetLayout;
	private DescriptorSet[][] textureDescriptorSets;
	private DescriptorSet[] uboDescriptorSets;
	private DescriptorPool textureDescriptorPool;
	private DescriptorPool uboDescriptorPool;
	private long textureSampler;
	private int dynamicAlignment;
	//	private int textureSetCount;
	private VulkanPipeline[] graphicPipelines;
	private boolean dirty;
	//	private int textureCount;
	private int objectCount;
	private ObjectList<ObjectList<VulkanUIRenderObject>> renderObjectList;
	private final Object2ObjectMap<UIControl, ObjectList<VulkanUIRenderObject>> customObjects = new Object2ObjectOpenHashMap<>();

	public static void addKey(UIControl control) {
		INSTANCE.customObjects.put(control, new ObjectArrayList<>());
	}

	public static void removeKey(UIControl control) {
		INSTANCE.customObjects.remove(control);
	}

	public static void addObject(UIControl key, VulkanUIRenderObject vro) {
		INSTANCE.customObjects.get(key).add(vro);
	}

	public static void addObjects(UIControl key, Collection<VulkanUIRenderObject> vros) {
		INSTANCE.customObjects.get(key).addAll(vros);
	}

	public static void removeObject(UIControl key, VulkanUIRenderObject vro) {
		INSTANCE.customObjects.get(key).remove(vro);
	}

	public static void removeObjects(UIControl key, Collection<VulkanUIRenderObject> vros) {
		INSTANCE.customObjects.get(key).removeAll(vros);
	}

	public static void update(RenderTree renderTree, boolean isTexturesUpdated, ObjectList<VulkanImage> disposalQueue) {
		if (debugMode) {
			int count = 0;
			for (ObjectList<VulkanUIRenderObject> vulkanUIRenderObjects : renderTree.colorTextureRenderTree) {
				count += vulkanUIRenderObjects.size();
			}

			if (renderTree.countOfVro != count) {
				throw new RuntimeException("UI Error: VROs count wrong!");
			}
		}

		INSTANCE.updateQueue.add(new UpdateInfo(updateId++, renderTree, isTexturesUpdated, disposalQueue));
	}

	@SubscribeEvent
	public void onWindowResized(WindowResizedEvent event) {
		projection.identity().setOrtho2D(0, Window.width, Window.height, 0);
	}

	public DescriptorSetLayout createUboDescriptorSetLayout() {
		return DescriptorSetLayout.builder()
				.addBinding(0, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT)
				.addBinding(1, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
				.addBinding(2, 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
				.build();
	}

	public DescriptorSetLayout createTextureDescriptorSetLayout() {
		return DescriptorSetLayout.builder()
				.addBinding(0, 1, VK_DESCRIPTOR_TYPE_SAMPLER, VK_SHADER_STAGE_FRAGMENT_BIT)
				.addBinding(1, textureCountPerSet, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, VK_SHADER_STAGE_FRAGMENT_BIT)
				.build();
	}

	public VulkanPipeline[] createPipelines() {
		VulkanPipeline[] pipelines = new VulkanPipeline[2];

		try (MemoryStack ignored = stackPush()) {
			long vertShaderModule = Vulkan.createShaderModule("xyz/qubs/shaders/ui_shader.vert", VK_SHADER_STAGE_VERTEX_BIT);
			long fragShaderModule = Vulkan.createShaderModuleCustom("xyz/qubs/shaders/ui_shader.frag", VK_SHADER_STAGE_FRAGMENT_BIT, (str) -> str.replace("texture_count", textureCountPerSet + ""));

			VkPipelineShaderStageCreateInfo.Buffer shaderStages = Vulkan.createShaderStages(new long[] { vertShaderModule }, new long[] { fragShaderModule });

			VkPipelineVertexInputStateCreateInfo vertexInputInfo = Vulkan.createVertexInputInfo(new GeneralVertex()); //TODO: Change to vertex without additional data
			VkPipelineInputAssemblyStateCreateInfo inputAssembly = Vulkan.createInputAssembly(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
			VkPipelineViewportStateCreateInfo viewportState = Vulkan.createViewportState();
			VkPipelineRasterizationStateCreateInfo rasterizer = Vulkan.createRasterizer(VK_POLYGON_MODE_FILL, 1.0f, VK_CULL_MODE_NONE);
			VkPipelineMultisampleStateCreateInfo multisampling = Vulkan.createMultisampling();

			VkPipelineColorBlendStateCreateInfo colorBlendingDepth = Vulkan.createColorBlending(VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT);
			VkPipelineColorBlendStateCreateInfo colorBlendingColor = Vulkan.createColorBlending(VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT);

			VkPipelineLayoutCreateInfo pipelineLayout = Vulkan.createPipelineLayout(new long[] { uboDescriptorSetLayout.getHandle(), textureDescriptorSetLayout.getHandle() }, null);

			VkPipelineDepthStencilStateCreateInfo depthStencilDepth = Vulkan.createDepthStencil(VK_COMPARE_OP_LESS, true);
			VkPipelineDepthStencilStateCreateInfo depthStencilColor = Vulkan.createDepthStencil(VK_COMPARE_OP_EQUAL, false);

			pipelines[0] = Vulkan.createGraphicsPipeline(shaderStages, vertexInputInfo, inputAssembly, viewportState, rasterizer, multisampling, colorBlendingDepth, pipelineLayout, depthStencilDepth); //depth write pipeline
			pipelines[1] = Vulkan.createGraphicsPipeline(shaderStages, vertexInputInfo, inputAssembly, viewportState, rasterizer, multisampling, colorBlendingColor, pipelineLayout, depthStencilColor); //color write pipeline

			vkDestroyShaderModule(Vulkan.device, vertShaderModule, null);
			vkDestroyShaderModule(Vulkan.device, fragShaderModule, null);
		}
		return pipelines;
	}

	private long createTextureSampler() {
		try (MemoryStack stack = stackPush()) {
			VkSamplerCreateInfo samplerInfo = VkSamplerCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO)
					.magFilter(VK_FILTER_NEAREST)
					.minFilter(VK_FILTER_NEAREST)
					.addressModeU(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.addressModeV(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.addressModeW(VK_SAMPLER_ADDRESS_MODE_REPEAT)
					.anisotropyEnable(false)
					.maxAnisotropy(1)
					.borderColor(VK_BORDER_COLOR_INT_OPAQUE_BLACK)
					.unnormalizedCoordinates(false)
					.compareEnable(false)
					.compareOp(VK_COMPARE_OP_ALWAYS)
					.mipLodBias(0)
					.minLod(0)
					.maxLod(0);

			VkCheck(vkCreateSampler(Vulkan.device, samplerInfo, null, longBuf), "Failed to create texture sampler");
			return longBuf.get(0);
		}
	}

	private DescriptorSet[] createUboDescriptorSets() {
		try (MemoryStack stack = stackPush()) {
			DescriptorSet[] descriptorSets = uboDescriptorPool.createDescriptorSets(uboDescriptorSetLayout, scImageCount).toArray(new DescriptorSet[0]);

			for (int i = 0; i < scImageCount; i++) {
				VkDescriptorBufferInfo.Buffer matrixBuffer = VkDescriptorBufferInfo.callocStack(1, stack)
						.buffer(matrixUniformBuffers[i].buffer)
						.offset(0)
						.range(4 * 16); //=64

				VkDescriptorBufferInfo.Buffer modelBuffer = VkDescriptorBufferInfo.callocStack(1, stack)
						.buffer(modelUniformBuffers[i].buffer)
						.offset(0)
						.range(VK_WHOLE_SIZE);

				VkDescriptorBufferInfo.Buffer modelBuffer2 = VkDescriptorBufferInfo.callocStack(1, stack)
						.buffer(modelUniformBuffers2[i].buffer)
						.offset(0)
						.range(VK_WHOLE_SIZE);

				descriptorSets[i].updateBuilder()
						.addWrite(0, 1, 0).pBufferInfo(matrixBuffer).add()
						.addWrite(0, 1, 1).pBufferInfo(modelBuffer).add()
						.addWrite(0, 1, 2).pBufferInfo(modelBuffer2).add()
						.update();
			}

			return descriptorSets;
		}
	}

	private void updateTextureDescriptorSets(DescriptorSet[] descriptorSets, int set, ObjectList<VulkanUIRenderObject> renderObjects) {
		try (MemoryStack stack = stackPush()) {
			VkDescriptorImageInfo.Buffer samplerInfo = VkDescriptorImageInfo.callocStack(1, stack);
			samplerInfo.sampler(textureSampler); //TODO: refactor this out

			VkDescriptorImageInfo.Buffer imageInfo = VkDescriptorImageInfo.calloc(textureCountPerSet);

			for (int i = 0; i < renderObjects.size(); i++) {
				if (renderObjects.get(i).isColor()) {
					imageInfo.get(i)
							.imageLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
							.imageView(TextureRegistry.errorTexture.vulkanImage.imageView);
					renderObjects.get(i).textureId = -1;
				} else {
					imageInfo.get(i)
							.imageLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
							.imageView(renderObjects.get(i).texture.imageView);
					renderObjects.get(i).textureId = set * textureCountPerSet + i;
				}
			}
			for (int i = renderObjects.size(); i < textureCountPerSet; i++) {
				imageInfo.get(i)
						.imageLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
						.imageView(TextureRegistry.errorTexture.vulkanImage.imageView);
			}

			for (int i = 0; i < scImageCount; i++) {
				descriptorSets[i].updateBuilder()
						.addWrite(0, 1, 0).pImageInfo(samplerInfo).add()
						.addWrite(0, textureCountPerSet, 1).pImageInfo(imageInfo).add()
						.update();
			}
			imageInfo.free();
		}
	}

	private DescriptorSet[] createTextureDescriptorSets(int set, ObjectList<VulkanUIRenderObject> renderObjects) {
		DescriptorSet[] descriptorSets = textureDescriptorPool.createDescriptorSets(textureDescriptorSetLayout, scImageCount).toArray(new DescriptorSet[0]);
		updateTextureDescriptorSets(descriptorSets, set, renderObjects);
		return descriptorSets;
	}

	private DescriptorPool createTextureDescriptorPool() {
		return DescriptorPool.builder()
				.setTypeSize(VK_DESCRIPTOR_TYPE_SAMPLER, scImageCount)
				.setTypeSize(VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, scImageCount * textureCountPerSet)
				.setMaxSets(scImageCount)
				.build();
	}

	private DescriptorPool createUboDescriptorPool() {
		return DescriptorPool.builder()
				.setTypeSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, scImageCount)
				.setTypeSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, scImageCount * 2)
				.setMaxSets(scImageCount)
				.build();
	}

	@Override
	public void init() {
		EventManager.registerListeners(this);
		renderObjectList = new ObjectArrayList<>();
		renderObjectList.add(new ObjectArrayList<>());
		renderObjectList.add(new ObjectArrayList<>());

		objectCount = 1;

		dynamicAlignment = VulkanUtils.calculateUniformBufferAlignment(4 * 4 * 16);
//		uboSetCount = (int) Math.ceil((dynamicAlignment * objectCount) / 65536.0);
//		textureSetCount = (int) Math.ceil((float) textureCount / VulkanUtils.getMaxDescrSampledImages());

		textureCountPerSet = Math.min(VulkanUtils.getMaxDescrSampledImages() - 10, 500);

		textureSampler = createTextureSampler();
		uboDescriptorSetLayout = createUboDescriptorSetLayout();
		textureDescriptorSetLayout = createTextureDescriptorSetLayout();
		createSwapChain();

		projection.setOrtho2D(0, Window.width, Window.height, 0);

		dirty = true;
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	@Override
	public void prepareCommandBuffer(VkCommandBuffer commandBuffer, int imageIndex) {
		try (MemoryStack stack = stackPush()) {
			LongBuffer zero = stack.callocLong(1);
			for (VulkanPipeline pipeline : graphicPipelines) {
				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.pipeline);
				int offset = 0;
				int index = 0;
				int textureIndex = -1;
				IntBuffer offsets = stack.callocInt(2);
				LongBuffer uboDesc = stack.longs(uboDescriptorSets[imageIndex].getHandle());
				LongBuffer[] textureDesc = new LongBuffer[textureDescriptorSets.length];
				for (int i = 0; i < textureDescriptorSets.length; i++) {
					textureDesc[i] = stack.longs(textureDescriptorSets[i][imageIndex].getHandle());
				}

				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, uboDesc, offsets);
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 1, textureDesc[0], null);

				vkCmdBindVertexBuffers(commandBuffer, 0, stack.longs(VulkanUIRenderObject.vertexBuffer.buffer), zero);
				vkCmdBindIndexBuffer(commandBuffer, VulkanUIRenderObject.indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);

				for (ObjectList<VulkanUIRenderObject> vros : renderObjectList) {
					if (textureIndex > 0) {
						vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 1, textureDesc[textureIndex], null);
					}
					for (VulkanUIRenderObject vro : vros) {
						if (vro.visibility) {
							vkCmdDrawIndexed(commandBuffer, VulkanUIRenderObject.indices.length, 1, 0, 0, index);
						}

						if (++index == 15) {
							index = 0;
							offset += dynamicAlignment;
							offsets.put(0, offset);
							offsets.put(1, offset);
							vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.layout, 0, uboDesc, offsets);
						}
					}
					textureIndex++;
				}
			}
		}
	}

	private void updateUniformBuffer(int imageIndex) {
		projection.get(0, matrixBuffer);
		copyMemory(memAddress(matrixBuffer), matrixBufferSize, matrixUniformBuffers[imageIndex].allocation);

		int index = 0;
		int dataXY;
		int dataZTexture;
		int dataWH;
		int dataClipXClipY;
		int dataClipWClipH;
		int color;
		int rounding;
		for (ObjectList<VulkanUIRenderObject> vulkanUIRenderObjects : renderObjectList) {
			for (VulkanUIRenderObject vro : vulkanUIRenderObjects) {
				dataXY = (0x0000FFFF & vro.location.x.get()) | ((0x0000FFFF & (vro.location.y.get() - vro.size.y.get())) << 16);
				dataZTexture = (0x0000FFFF & vro.zIndex) | ((0x0000FFFF & (vro.textureId + 1)) << 16);
				dataWH = (0x0000FFFF & vro.size.x.get()) | ((0x0000FFFF & vro.size.y.get()) << 16);
				dataClipXClipY = (0x0000FFFF & vro.minSize.x.get()) | ((0x0000FFFF & (vro.minSize.y.get() - vro.maxSize.y.get())) << 16);
				dataClipWClipH = (0x0000FFFF & vro.maxSize.x.get()) | ((0x0000FFFF & vro.maxSize.y.get()) << 16);
				color = vro.color.toInt();
				rounding = vro.cornerRadius;

				modelBuffer.putInt(index + 0, dataXY);
				modelBuffer.putInt(index + 4, dataZTexture);
				modelBuffer.putInt(index + 8, dataWH);
				modelBuffer.putInt(index + 12, dataClipXClipY);

				modelBuffer2.putInt(index + 0, dataClipWClipH);
				modelBuffer2.putInt(index + 4, color);
				modelBuffer2.putInt(index + 8, rounding);

				index += 16;
			}
		}

		copyMemory(memAddress(modelBuffer), modelBufferSize * objectCount, modelUniformBuffers[imageIndex].allocation);
		copyMemory(memAddress(modelBuffer2), modelBufferSize * objectCount, modelUniformBuffers2[imageIndex].allocation);
	}

	@Override
	public void beforeFrame(int imageIndex) {
		if (!updateQueue.isEmpty()) {
			UpdateInfo info = updateQueue.poll();
			renderObjectList = info.renderTree.colorTextureRenderTree;
			if (objectCount < info.renderTree.countOfVro) {
				memFree(modelBuffer);
				memFree(modelBuffer2);

				modelBuffer = memAlloc(modelBufferSize * info.renderTree.countOfVro);
				modelBuffer2 = memAlloc(modelBufferSize * info.renderTree.countOfVro);
			}
			objectCount = info.renderTree.countOfVro;

			if (info.isTexturesUpdated) {
				for (int i = 0; i < renderObjectList.size() - 1; i++) {
					updateTextureDescriptorSets(textureDescriptorSets[i], i, renderObjectList.get(i + 1));
				}
			}
			Vulkan.imageDisposalQueue.addAll(info.disposalQueue);

			dirty = true;
		}
		updateUniformBuffer(imageIndex);
	}

	@Override
	public void afterFrame(int imageIndex) {
		if (dirty) {
			dirty = false;
		}
	}

	private void createModelBuffers() {
		modelUniformBuffers = Vulkan.createUniformBuffers(65536);
		modelUniformBuffers2 = Vulkan.createUniformBuffers(65536);
	}

	private void freeModelBuffers() {
		for (VulkanBuffer uniformBuffer : modelUniformBuffers) {
			uniformBuffer.free();
		}
		for (VulkanBuffer uniformBuffer : modelUniformBuffers2) {
			uniformBuffer.free();
		}
	}

	@Override
	public void createSwapChain() {
		textureDescriptorPool = createTextureDescriptorPool();
		uboDescriptorPool = createUboDescriptorPool();

		graphicPipelines = createPipelines();

		matrixUniformBuffers = Vulkan.createUniformBuffers(matrixBufferSize);
		createModelBuffers();

		uboDescriptorSets = createUboDescriptorSets();
		textureDescriptorSets = new DescriptorSet[renderObjectList.size() - 1][];
		for (int i = 0; i < renderObjectList.size() - 1; i++) {
			textureDescriptorSets[i] = createTextureDescriptorSets(i, renderObjectList.get(i + 1));
		}
	}

	@Override
	public void cleanupSwapChain() {
		for (VulkanBuffer uniformBuffer : matrixUniformBuffers) {
			uniformBuffer.free();
		}
		freeModelBuffers();
		for (VulkanPipeline graphicPipeline : graphicPipelines) {
			graphicPipeline.free();
		}
		textureDescriptorPool.dispose();
		uboDescriptorPool.dispose();
	}

	@Override
	public void dispose() {
		EventManager.unregisterListeners(this);
		vkDestroySampler(Vulkan.device, textureSampler, null);
		uboDescriptorSetLayout.dispose();
		textureDescriptorSetLayout.dispose();

		updateQueue.forEach(updateInfo -> Vulkan.imageDisposalQueue.addAll(updateInfo.disposalQueue));

		memFree(matrixBuffer);
		memFree(modelBuffer);
		memFree(modelBuffer2);
		memFree(intBuf);
		memFree(longBuf);
		memFree(pointerBuf);
	}

	private static class UpdateInfo {
		private final int updateId;
		private final RenderTree renderTree;
		private final boolean isTexturesUpdated;
		private final ObjectList<VulkanImage> disposalQueue;

		public UpdateInfo(int updateId, RenderTree renderTree, boolean isTexturesUpdated, ObjectList<VulkanImage> disposalQueue) {
			this.updateId = updateId;
			this.renderTree = renderTree;
			this.isTexturesUpdated = isTexturesUpdated;
			this.disposalQueue = disposalQueue;
		}
	}
}
