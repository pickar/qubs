package xyz.qubs.engine.vulkan;

public class VulkanException extends AssertionError {
	public VulkanException(String message) {
		super(message);
	}
}
