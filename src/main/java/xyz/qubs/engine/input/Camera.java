package xyz.qubs.engine.input;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.utils.Maths;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.block.types.CubeBlockType;
import xyz.qubs.game.collision.AABB;
import xyz.qubs.game.world.chunk.Chunk;

import static org.joml.Math.cos;
import static org.joml.Math.sin;
import static xyz.qubs.Engine.world;
import static xyz.qubs.game.world.chunk.Chunk.CHUNK_SIZE;
import static xyz.qubs.game.world.chunk.Chunk.LOG2_CHUNK_SIZE;

public class Camera {
	private final Vector3f position;
	private final Vector3f realPosition = new Vector3f();
	public Vector3i selectedBlockPos;
	public Side selectedBlockSide;
	public int selectedBlockAABB;
	public float pitch, yaw;
	public boolean matrixChanged = true;
	private Matrix4f viewMatrix = new Matrix4f();

	public Camera() {
		position = new Vector3f();
	}

	private float intBound(float s, float ds) {
		if (Math.round(s) == s && ds < 0) {
			return 0;
		}
		if (ds < 0) {
			return intBound(-s, -ds);
		} else {
			s = (s % 1 + 1) % 1;
			return (1 - s) / ds;
		}
	}

	public Chunk getChunk() {
		Vector3f realPos = getPosition();
		int x = Maths.floor(realPos.x) >> LOG2_CHUNK_SIZE;
		int y = Maths.floor(realPos.y) >> LOG2_CHUNK_SIZE;
		int z = Maths.floor(realPos.z) >> LOG2_CHUNK_SIZE;

		return world.getChunk(x, y, z);
	}

	public Vector3f getLocalPosition() {
		return position;
	}

	public Vector3f getPosition() {
		return realPosition.set(
				world.shift.x * CHUNK_SIZE + position.x,
				world.shift.y * CHUNK_SIZE + position.y,
				world.shift.z * CHUNK_SIZE + position.z
		);
	}

	public Vector3f movePosition(float offsetX, float offsetY, float offsetZ) {
		Vector3f positionChange = new Vector3f(position);
		if (offsetZ != 0) {
			position.x -= sin(pitch) * offsetZ;
			position.z += cos(pitch) * offsetZ;
		}
		if (offsetX != 0) {
			position.x -= org.joml.Math.sin(pitch - Maths.PIOver2) * offsetX;
			position.z += org.joml.Math.cos(pitch - Maths.PIOver2) * offsetX;
		}
		position.y += offsetY;
		return positionChange.sub(position);
	}

	public void rayCast() {
		if (!matrixChanged) { return; }
		matrixChanged = false;
		final int radius = 7;
		Vector3f startPos = new Vector3f(position);
		int addX = world.shift.x << LOG2_CHUNK_SIZE;
		int addY = world.shift.y << LOG2_CHUNK_SIZE;
		int addZ = world.shift.z << LOG2_CHUNK_SIZE;

		Matrix4f view = new Matrix4f().rotateXYZ(yaw, pitch, 0).translate(-startPos.x, -startPos.y, -startPos.z);

		float dx = -view.m02();
		float dy = -view.m12();
		float dz = -view.m22();

		if (dx == 0 && dy == 0 && dz == 0) { throw new RuntimeException("Raycast in zero direction!"); }

		int x = Maths.floor(startPos.x);
		int y = Maths.floor(startPos.y);
		int z = Maths.floor(startPos.z);

		final Vector3f invDirection = new Vector3f(1 / dx, 1 / dy, 1 / dz);

		int stepX = (dx >= 0) ? 1 : -1;
		int stepY = (dy >= 0) ? 1 : -1;
		int stepZ = (dz >= 0) ? 1 : -1;

		int lastX = x + stepX * radius;
		int lastY = y + stepY * radius;
		int lastZ = z + stepZ * radius;

		float tMaxX = (dx != 0) ? intBound(startPos.x, dx) : Float.MAX_VALUE;
		float tMaxY = (dy != 0) ? intBound(startPos.y, dy) : Float.MAX_VALUE;
		float tMaxZ = (dz != 0) ? intBound(startPos.z, dz) : Float.MAX_VALUE;

		float tDeltaX = (dx != 0) ? stepX / dx : Float.MAX_VALUE;
		float tDeltaY = (dy != 0) ? stepY / dy : Float.MAX_VALUE;
		float tDeltaZ = (dz != 0) ? stepZ / dz : Float.MAX_VALUE;

		Side side = null;

		BlockType block;
		while (x != lastX && y != lastY && z != lastZ) { //TODO: fix this check
			block = world.getBlockType(x + addX, y + addY, z + addZ);
			if (block != null && block.getAABBs().length != 0) {
				if (block.getAABBs() == CubeBlockType.CUBE_AABB) {
					selectedBlockPos = new Vector3i(x + addX, y + addY, z + addZ);
					selectedBlockSide = side;
					selectedBlockAABB = 0;

					return;
				} else {
					final Vector3f origin = new Vector3f(startPos).sub(x, y, z);
					AABB.IntersectionResult minResult = block.getAABBs()[0].intersectsWith(origin, invDirection);
					int aabbIndex = 0;

					for (int i = 1; i < block.getAABBs().length; i++) {
						AABB.IntersectionResult result = block.getAABBs()[i].intersectsWith(origin, invDirection);

						if (result.intersected && minResult.lengthOfRay > result.lengthOfRay) {
							minResult = result;
							aabbIndex = i;
						}
					}
					if (minResult.intersected) {
						selectedBlockPos = new Vector3i(x + addX, y + addY, z + addZ);
						float dot = Math.round(new Vector3f(dx, dy, dz).mul(minResult.lengthOfRay).add(origin).sub(block.getAABBs()[aabbIndex].min).dot(minResult.side.x, minResult.side.y, minResult.side.z) * 100f) / 100f;

						if (dot == 0) {
							selectedBlockSide = minResult.side.opposite();
						} else {
							selectedBlockSide = minResult.side;
						}

						selectedBlockAABB = aabbIndex;
						return;
					}
				}
			}

			if (tMaxX < tMaxY) {
				if (tMaxX < tMaxZ) {
					x += stepX;
					tMaxX += tDeltaX;
					if (-stepX > 0) {
						side = Side.RIGHT;
					} else {
						side = Side.LEFT;
					}
				} else {
					z += stepZ;
					tMaxZ += tDeltaZ;
					if (-stepZ > 0) {
						side = Side.FRONT;
					} else {
						side = Side.BACK;
					}
				}
			} else {
				if (tMaxY < tMaxZ) {
					y += stepY;
					tMaxY += tDeltaY;
					if (-stepY > 0) {
						side = Side.TOP;
					} else {
						side = Side.BOTTOM;
					}
				} else {
					z += stepZ;
					tMaxZ += tDeltaZ;
					if (-stepZ > 0) {
						side = Side.FRONT;
					} else {
						side = Side.BACK;
					}
				}
			}
		}

		selectedBlockPos = null;
		selectedBlockSide = null;
		selectedBlockAABB = -1;
	}

	public void moveRotation(float offsetX, float offsetY) {
		yaw += offsetX * 0.01f;
		pitch += offsetY * 0.01f;

		if (yaw > Maths.PIOver2) {
			yaw = Maths.PIOver2;
		}
		if (yaw < -Maths.PIOver2) {
			yaw = -Maths.PIOver2;
		}
		if (pitch < -2 * Maths.PI) {
			pitch += 2 * Maths.PI;
		}
		if (pitch > 2 * Maths.PI) {
			pitch -= 2 * Maths.PI;
		}
	}

	public Matrix4f getViewMatrix() {
		Vector3f cameraPos = this.getLocalPosition();
		Matrix4f matrix4f = new Matrix4f().rotateXYZ(yaw, pitch, 0).translate(-cameraPos.x, -cameraPos.y, -cameraPos.z);
		if (!viewMatrix.equals(matrix4f)) {
			viewMatrix = matrix4f;
			matrixChanged = true;
		}
		return viewMatrix;
	}
}