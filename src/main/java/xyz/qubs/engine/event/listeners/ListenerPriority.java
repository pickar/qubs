package xyz.qubs.engine.event.listeners;

public enum ListenerPriority {
	HIGHEST, HIGH, NORMAL, LOW, LOWEST
}
