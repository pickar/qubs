package xyz.qubs.engine.event.annotations;

import xyz.qubs.engine.event.listeners.ListenerPriority;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SubscribeEvent {
	ListenerPriority priority() default ListenerPriority.NORMAL;
}
