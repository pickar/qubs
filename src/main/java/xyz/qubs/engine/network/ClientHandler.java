package xyz.qubs.engine.network;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import xyz.qubs.engine.network.packets.Packet;
import xyz.qubs.engine.network.packets.PacketHandler;
import xyz.qubs.engine.network.packets.Side;

public class ClientHandler extends SimpleChannelInboundHandler<DatagramPacket> {

	@Override
	public void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) {
//		System.err.println(msg);
		Packet packet = PacketHandler.getPacketFromMessage(msg);
		packet.read();
		packet.receive(ctx, msg, Side.CLIENT);

//		System.out.println(p);

//		String response = packet.content().toString(CharsetUtil.UTF_8);
//		if (response.startsWith("QOTM: ")) {
//			System.out.println("Quote of the Moment: " + response.substring(6));
//			ctx.close();
//		}

		ctx.close();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}
}
