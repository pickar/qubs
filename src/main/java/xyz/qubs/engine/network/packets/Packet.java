package xyz.qubs.engine.network.packets;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;

public abstract class Packet {
	public static char id_count = 0;
	public int length;

	public ByteBuf buffer;

	public Packet() {
		this.buffer = Unpooled.buffer();
	}

	public abstract char getId();

	public abstract void write();

	public abstract void read();

	public void send(ChannelHandlerContext ctx, DatagramPacket msg, Side side) {
		System.err.println("Sent packet: " + this + " from " + side);
	}

	public void receive(ChannelHandlerContext ctx, DatagramPacket msg, Side side) {
		System.err.println("Received packet: " + this + " on " + side);
	}
}
