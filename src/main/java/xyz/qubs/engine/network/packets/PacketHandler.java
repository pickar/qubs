package xyz.qubs.engine.network.packets;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;

import java.lang.reflect.InvocationTargetException;

public class PacketHandler {
	private static final Int2ObjectArrayMap<Class<? extends Packet>> packets = new Int2ObjectArrayMap<>();

	public static void init() {
		PacketHandler.registerPacket(TestPacket.id, TestPacket.class);
		PacketHandler.registerPacket(RandomQuotePacket.id, RandomQuotePacket.class);
	}

	public static Packet getPacket(int id) {
		try {
			return packets.get(id).getDeclaredConstructor().newInstance();
		} catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	public static void sendToServer(Packet packet, ChannelHandlerContext ctx) {

	}

	public static Packet getPacketFromMessage(DatagramPacket msg) {
		char id = msg.content().readChar();
		Packet packet = PacketHandler.getPacket(id);
		packet.buffer = msg.content();
		return packet;
	}

	public static void registerPacket(int id, Class<? extends Packet> packet) {
		packets.put(id, packet);
	}
}
