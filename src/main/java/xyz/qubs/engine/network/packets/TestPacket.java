package xyz.qubs.engine.network.packets;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;

import java.nio.charset.StandardCharsets;

public class TestPacket extends Packet {
	public static final char id = id_count++;

	public int someInt;
	public String someString;

	@Override
	public char getId() {
		return id;
	}

	@Override
	public void write() {
		buffer.writeInt(someInt);

		buffer.writeInt(someString.length());
		buffer.writeCharSequence(someString, StandardCharsets.UTF_8);
	}

	@Override
	public void read() {
		someInt = buffer.readInt();

		int strLength = buffer.readInt();
		someString = (String) buffer.readCharSequence(strLength, StandardCharsets.UTF_8);
	}

	@Override
	public void send(ChannelHandlerContext ctx, DatagramPacket msg, Side side) {
		super.send(ctx, msg, side);
	}

	@Override
	public void receive(ChannelHandlerContext ctx, DatagramPacket msg, Side side) {
		super.receive(ctx, msg, side);
		if (side == Side.SERVER) {
			RandomQuotePacket p = new RandomQuotePacket();
			p.buffer.writeChar(p.getId());
			p.write();
			p.send(ctx, msg, side);
			ctx.write(new DatagramPacket(p.buffer, msg.sender()));
		}
	}

	@Override
	public String toString() {
		return "TestPacket_" + (int) id + "{" +
		       "someInt=" + someInt +
		       ", someString='" + someString + '\'' +
		       '}';
	}
}
