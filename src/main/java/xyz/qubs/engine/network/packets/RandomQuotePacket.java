package xyz.qubs.engine.network.packets;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class RandomQuotePacket extends Packet {
	public static final char id = Packet.id_count++;
	private static final Random random = new Random();
	private static final String[] quotes = {
			"Where there is love there is life.",
			"First they ignore you, then they laugh at you, then they fight you, then you win.",
			"Be the change you want to see in the world.",
			"The weak can never forgive. Forgiveness is the attribute of the strong.",
	};
	public String quote;

	private static String nextQuote() {
		int quoteId;
		synchronized (random) {
			quoteId = random.nextInt(quotes.length);
		}
		return quotes[quoteId];
	}

	@Override
	public char getId() {
		return id;
	}

	@Override
	public void write() {
		quote = nextQuote();
		buffer.writeInt(quote.length());
		buffer.writeCharSequence(quote, StandardCharsets.UTF_8);
	}

	@Override
	public void read() {
		int len = buffer.readInt();
		quote = (String) buffer.readCharSequence(len, StandardCharsets.UTF_8);
	}

	@Override
	public void send(ChannelHandlerContext ctx, DatagramPacket msg, Side side) {
		super.send(ctx, msg, side);
	}

	@Override
	public void receive(ChannelHandlerContext ctx, DatagramPacket msg, Side side) {
		super.receive(ctx, msg, side);
		System.out.println("Random quote is: " + quote);
	}

	@Override
	public String toString() {
		return "RandomQuotePacket_" + (int) id + "{" +
		       "quote='" + quote + '\'' +
		       '}';
	}
}
