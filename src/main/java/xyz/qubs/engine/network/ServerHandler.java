package xyz.qubs.engine.network;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import xyz.qubs.engine.network.packets.Packet;
import xyz.qubs.engine.network.packets.PacketHandler;
import xyz.qubs.engine.network.packets.Side;

public class ServerHandler extends SimpleChannelInboundHandler<DatagramPacket> {

	@Override
	public void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
//		System.err.println(msg);
		Packet packet = PacketHandler.getPacketFromMessage(msg);
		packet.read();
		packet.receive(ctx, msg, Side.SERVER);

//		TestPacket p = new TestPacket();
//		p.someInt = 25;
//		p.someString = "Hello world";
//		p.buffer.writeChar(p.getId());
//		p.write();
//		p.send(Side.SERVER);
//		ctx.write(new DatagramPacket(p.buffer, packet.sender()));
//		if ("QOTM?".equals(packet.content().toString(CharsetUtil.UTF_8))) {
//			ctx.write(new DatagramPacket(
//					Unpooled.copiedBuffer("QOTM: " + nextQuote(), CharsetUtil.UTF_8), packet.sender()));
//		}
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
	}
}
