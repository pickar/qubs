package xyz.qubs.engine.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.internal.SocketUtils;
import xyz.qubs.engine.network.packets.PacketHandler;
import xyz.qubs.engine.network.packets.Side;
import xyz.qubs.engine.network.packets.TestPacket;

public final class Client {

	private static final int PORT = Integer.parseInt(System.getProperty("port", "7686"));

	public static void main(String[] args) throws Exception {
		PacketHandler.init();
		EventLoopGroup group = new NioEventLoopGroup();
		try {
			Bootstrap b = new Bootstrap();
			b.group(group)
					.channel(NioDatagramChannel.class)
					.option(ChannelOption.SO_BROADCAST, true)
					.handler(new ClientHandler());

			Channel ch = b.bind(0).sync().channel();

			TestPacket p = new TestPacket();
			p.buffer.writeChar(p.getId());
			p.someString = "asda";
			p.someInt = 42;
			p.write();
			p.send(null, null, Side.CLIENT);

			// Broadcast the QOTM request to port 8080.
			ch.writeAndFlush(new DatagramPacket(p.buffer, SocketUtils.socketAddress("255.255.255.255", PORT))).sync();
			ch.read();
			ch.writeAndFlush(new DatagramPacket(p.buffer, SocketUtils.socketAddress("255.255.255.255", PORT))).sync();
			ch.read();

			// QuoteOfTheMomentClientHandler will close the DatagramChannel when a
			// response is received.  If the channel is not closed within 5 seconds,
			// print an error message and quit.
			if (!ch.closeFuture().await(5000)) {
				System.err.println("QOTM request timed out.");
			}
		} finally {
			group.shutdownGracefully();
		}
	}
}
