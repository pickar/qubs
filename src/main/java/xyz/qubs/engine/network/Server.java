package xyz.qubs.engine.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import xyz.qubs.engine.network.packets.PacketHandler;

public final class Server {

	private static final int PORT = Integer.parseInt(System.getProperty("port", "7686"));

	public static void main(String[] args) throws Exception {
		PacketHandler.init();
		EventLoopGroup group = new NioEventLoopGroup();
		try {
			Bootstrap b = new Bootstrap();
			b.group(group)
					.channel(NioDatagramChannel.class)
					.option(ChannelOption.SO_BROADCAST, true)
					.handler(new ServerHandler());

			b.bind(PORT).sync().channel().closeFuture().await();
		} finally {
			group.shutdownGracefully();
		}
	}
}
