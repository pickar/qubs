package xyz.qubs.engine.settings;

import org.apache.logging.log4j.core.Filter;
import xyz.qubs.engine.logger.MarkerConfig;

public class SettingsManager {
	public static final String resourcePath = "xyz/qubs/";
	// Logger
	public static boolean logToFile = false;
	public static final MarkerConfig[] markers = new MarkerConfig[] {
			new MarkerConfig("UI", Filter.Result.DENY, Filter.Result.DENY),
			new MarkerConfig("Registries", Filter.Result.DENY, Filter.Result.DENY)
	};
	// Engine
	public static final boolean debugMode = true;
	public static float FOV = 90;
	public static int maxChunksGenerated = 32;
	public static int maxChunksLightened = 24;
	public static int loadDistanceHReal = 12;
	public static int loadDistanceVReal = 12;
	public static boolean rotateTextures = true;
	public static boolean fullScreen = false;
}
