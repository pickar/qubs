package xyz.qubs.engine;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Pair<K, V> {
	@Getter @Setter public K key;
	@Getter @Setter public V value;
}
