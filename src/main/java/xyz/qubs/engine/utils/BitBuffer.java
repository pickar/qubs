package xyz.qubs.engine.utils;

import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.bytes.ByteList;

public class BitBuffer {
	private static final int[] mask = { 0b00000001, 0b00000010, 0b00000100, 0b00001000, 0b00010000, 0b00100000, 0b01000000, 0b10000000 };
	private final ByteList bytes;
	private byte currentBitIndex;
	private byte currentByte;

	public BitBuffer() {
		currentBitIndex = 7;
		currentByte = 0;
		bytes = new ByteArrayList();
	}

	public void writeBit(int bit) {
		currentByte |= (bit << currentBitIndex);
		if (currentBitIndex-- == 0) {
			bytes.add(currentByte);
			currentBitIndex = 7;
			currentByte = 0;
		}
	}

	public void writeBits(int count, int bit) {
		for (int i = 1; i <= count; i++) {
			writeBit(bit >> (count - i) & 1);
		}
	}

	public byte[] toByteArray() {
		if (currentBitIndex != 7) {
			bytes.add(currentByte);
		}
		byte[] array = bytes.toByteArray();
		if (currentBitIndex != 7) {
			bytes.removeByte(bytes.size() - 1);
		}
		return array;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("BitBuffer{ ");
		if (currentBitIndex != 7) {
			bytes.add(currentByte);
		}
		for (int b = 0; b < bytes.size(); b++) {
			byte aByte = bytes.getByte(b);
			for (int i = 7; i >= 0; i--) {
				sb.append(((aByte & mask[i]) >> i) == 0 ? 0 : 1);
			}
			if (b != bytes.size() - 1) {
				sb.append(", ");
			}
		}
		if (currentBitIndex != 7) {
			bytes.removeByte(bytes.size() - 1);
		}
		sb.append(" }");

		return sb.toString();
	}
}
