package xyz.qubs.engine.utils;

public class Timer {
	private double lastLoopTime;

	public Timer() {
		lastLoopTime = getTimeMs();
	}

	public static double getTimeMs() {
		return System.nanoTime() / 1_000_000.0;
	}

	public float getElapsedTime() {
		double time = getTimeMs();
		float elapsedTime = (float) (time - lastLoopTime);
		lastLoopTime = time;
		return elapsedTime;
	}

	public double getLastLoopTime() {
		return lastLoopTime;
	}
}
