package xyz.qubs.engine.utils;

public class BitReader {
	private final byte[] bytes;
	private int currentByteIndex;
	private int currentByte;
	private int currentBitIndex;
	private int remaining;

	public BitReader(byte[] bytes) {
		this.bytes = bytes;
		remaining = bytes.length * 8;
		currentBitIndex = 7;
		currentByteIndex = 0;
		currentByte = bytes[0];
	}

	public static void main(String[] args) {
		BitBuffer buffer = new BitBuffer();
		buffer.writeBits(12, 0b010110101111); // 1455
		buffer.writeBit(0);                     // 0
		buffer.writeBits(4, 0b0011); // 3

		BitReader reader = new BitReader(buffer.toByteArray());
		System.out.println(reader.readBits(12));
		System.out.println(reader.readBit());
		System.out.println(reader.readBits(4));
	}

	public int readBit() {
		remaining--;
		final int bit = (currentByte >> currentBitIndex) & 1;

		if (currentBitIndex-- == 0) {
			currentBitIndex = 7;
			if (++currentByteIndex != bytes.length) {
				currentByte = bytes[currentByteIndex];
			}
		}

		return bit;
	}

	public int readBits(int count) {
		int value = 0;
		for (int i = count - 1; i >= 0; i--) {
			final int bit = readBit();
			value |= bit << i;
		}

		return value;
	}

	public int remaining() {
		return remaining;
	}
}
