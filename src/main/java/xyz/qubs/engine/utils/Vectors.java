package xyz.qubs.engine.utils;

import org.joml.Vector3f;

public class Vectors {
	public static boolean isBigger(Vector3f vec, Vector3f vec1) {
		return vec.x > vec1.x && vec.y > vec1.y && vec.z > vec1.z;
	}
}
