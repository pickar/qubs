package xyz.qubs.engine.utils;

public class PriorityLock {
	private boolean locked;
	private boolean priorityWaiting;

	public synchronized void acquire() throws InterruptedException {
		while (locked || priorityWaiting) {
			wait();
		}
		locked = true;
	}

	public synchronized void acquirePriority() throws InterruptedException {
		priorityWaiting = true;
		try {
			while (locked) {
				wait();
			}
			locked = true;
		} finally {
			priorityWaiting = false;
		}
	}

	public synchronized void release() {
		locked = false;
		notifyAll();
	}
}