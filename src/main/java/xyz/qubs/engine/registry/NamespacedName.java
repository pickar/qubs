package xyz.qubs.engine.registry;

public class NamespacedName {
	public static final String SEPARATOR = ":";

	public final String namespace;
	public final String name;

	public final String fullName;

	public NamespacedName(String namespace, String name) {
		this.namespace = namespace.toLowerCase();
		this.name = name.toLowerCase();

		fullName = this.namespace + SEPARATOR + this.name;
	}

	public NamespacedName(String fullName) {
		if (fullName.contains(SEPARATOR) && fullName.split(SEPARATOR).length == 2) {
			this.fullName = fullName.toLowerCase();
			this.namespace = this.fullName.split(SEPARATOR)[0];
			this.name = this.fullName.split(SEPARATOR)[1];
		} else {
			throw new IllegalArgumentException("Full namespaced name doesn't contain separator");
		}
	}

	@Override
	public String toString() {
		return fullName;
	}
}
