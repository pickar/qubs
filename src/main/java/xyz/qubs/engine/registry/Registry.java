package xyz.qubs.engine.registry;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import xyz.qubs.engine.logger.Logger;

import java.lang.reflect.Type;
import java.util.Collection;

public class Registry<E extends IRegistryEntry> implements IRegistry<E> {
	private final Type type;
	private final NamespacedName registryName;
	protected Object2ObjectMap<String, E> map = new Object2ObjectOpenHashMap<>();
	protected Object2IntMap<String> idMap = new Object2IntOpenHashMap<>();
	private int counter = 0;

	public Registry(NamespacedName registryName, Type type) {
		this.registryName = registryName;
		this.type = type;
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}

	@Override
	public void register(E element) {
		if (!map.containsKey(element.getRegistryName().fullName)) {
			map.put(element.getRegistryName().fullName, element);
			idMap.put(element.getRegistryName().fullName, counter++);
		} else {
			Logger.instance.warn("Tried to register " + element.getClass() + " with registry name " + element.getRegistryName() + " that already registered!");
		}
	}

	@Override
	public E getElement(NamespacedName registryName) {
		return map.get(registryName.fullName);
	}

	@Override
	public E getElement(String fullName) {
		return map.get(fullName);
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public Collection<E> getAllEntries() {
		return map.values();
	}

	@Override
	public int getEntryCount() {
		return counter;
	}

	public int getInternalId(NamespacedName registryName) {
		return idMap.getInt(registryName.fullName);
	}

	public int getInternalId(String fullName) {
		return idMap.getInt(fullName);
	}
}
