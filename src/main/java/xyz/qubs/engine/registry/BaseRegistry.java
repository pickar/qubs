package xyz.qubs.engine.registry;

import org.apache.logging.log4j.MarkerManager;
import xyz.qubs.engine.event.EventManager;
import xyz.qubs.engine.logger.Logger;
import xyz.qubs.engine.settings.SettingsManager;

import java.lang.reflect.Type;

@SuppressWarnings("rawtypes")
public class BaseRegistry extends Registry<IRegistry> {
	private static final BaseRegistry INSTANCE = new BaseRegistry();

	private BaseRegistry() {
		super(new NamespacedName("core:base"), IRegistry.class);
		register(this);
	}

	public static void init() {
		EventManager.registerListeners(INSTANCE);
		EventManager.callEvent(new RegistryCreationEvent());
		StringBuilder sb = new StringBuilder();
		for (IRegistry registry : INSTANCE.map.values()) {
			if (registry != INSTANCE) {
				registry.registerEntries();
			}
			if (SettingsManager.debugMode) {
				sb.append("Registered ").append(registry.getAllEntries().size()).append(" entries in ").append(registry.getRegistryName()).append(":\n");
				for (Object entry : registry.getAllEntries()) {
					sb.append("\t").append(registry.getRegistryName()).append(":").append(((IRegistryEntry) entry).getRegistryName().name).append("\n");
				}
			}
		}
		if (SettingsManager.debugMode) {
			sb.deleteCharAt(sb.length() - 1);
			Logger.instance.debug(MarkerManager.getMarker("Registries"), sb.toString());
		}
	}

	public static IRegistry getRegistryByName(NamespacedName registryName) {
		return INSTANCE.getElement(registryName);
	}

	public static IRegistry getRegistryByName(String fullName) {
		return INSTANCE.getElement(fullName);
	}

	public static IRegistry getRegistryByType(Type type) {
		return INSTANCE.map.values().stream().filter(registry -> registry.getType().equals(type)).findFirst().orElse(null);
	}
}
