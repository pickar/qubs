package xyz.qubs.engine.registry;

import xyz.qubs.engine.event.EventManager;

import java.lang.reflect.Type;
import java.util.Collection;

public interface IRegistry<E extends IRegistryEntry> extends IRegistryEntry {
	void register(E element);

	E getElement(NamespacedName registryName);

	E getElement(String fullName);

	Type getType();

	default void registerEntries() {
		EventManager.callEvent(new RegisterEntriesEvent<>(this, getType()));
	}

	Collection<E> getAllEntries();

	int getEntryCount();
}
