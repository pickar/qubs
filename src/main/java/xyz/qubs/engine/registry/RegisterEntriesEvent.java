package xyz.qubs.engine.registry;

import xyz.qubs.engine.event.events.GenericEvent;

import java.lang.reflect.Type;

public class RegisterEntriesEvent<E extends IRegistryEntry> extends GenericEvent {
	public IRegistry<E> registry;

	protected RegisterEntriesEvent(IRegistry<E> registry, Type type) {
		super(type);
		this.registry = registry;
	}
}
