package xyz.qubs.engine.registry;

public interface IRegistryEntry {
	NamespacedName getRegistryName();
}
