package xyz.qubs.engine.registry;

import xyz.qubs.engine.event.events.Event;

public class RegistryCreationEvent extends Event {
	public BaseRegistry registry = (BaseRegistry) BaseRegistry.getRegistryByName("core:base");
}
