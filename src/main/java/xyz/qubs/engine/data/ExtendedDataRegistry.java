package xyz.qubs.engine.data;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.logger.Logger;
import xyz.qubs.engine.registry.IRegistry;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.registry.RegisterEntriesEvent;
import xyz.qubs.engine.registry.RegistryCreationEvent;

import java.lang.reflect.Type;
import java.util.Collection;

@SuppressWarnings("rawtypes")
public class ExtendedDataRegistry implements IRegistry<ExtendedDataHandler> {
	public static final BlockCountDataHandler BLOCK_COUNT_DATA_HANDLER = new BlockCountDataHandler();
	public static final ExtendedDataRegistry INSTANCE = new ExtendedDataRegistry(new NamespacedName("core", "extended_data"), ExtendedDataHandler.class);
	private final Type type;
	private final NamespacedName registryName;
	protected Object2ObjectMap<String, ExtendedDataHandler> map = new Object2ObjectOpenHashMap<>();
	protected Object2ObjectMap<Type, Object2IntMap<String>> idMap = new Object2ObjectOpenHashMap<>();
	private int counter = 0;

	public ExtendedDataRegistry(NamespacedName registryName, Type type) {
		this.registryName = registryName;
		this.type = type;
	}

	public static ExtendedDataHandler getDataHandlerByName(NamespacedName registryName) {
		return INSTANCE.getElement(registryName);
	}

	public static ExtendedDataHandler getDataHandlerByName(String fullName) {
		return INSTANCE.getElement(fullName);
	}

	@SubscribeEvent
	public static void onRegistryCreation(RegistryCreationEvent event) {
		event.registry.register(INSTANCE);
	}

	@SubscribeEvent
	public static void onRegisterEntriesExtendedDataHandler(RegisterEntriesEvent<ExtendedDataHandler> event) {
		event.registry.register(BLOCK_COUNT_DATA_HANDLER);
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}

	@Override
	public void register(ExtendedDataHandler element) {
		if (!map.containsKey(element.getRegistryName().fullName)) {
			map.put(element.getRegistryName().fullName, element);
			if (!idMap.containsKey(element.getContainerType())) {
				idMap.put(element.getContainerType(), new Object2IntOpenHashMap<>());
			}
			final int count = idMap.get(element.getContainerType()).values().size();
			idMap.get(element.getContainerType()).put(element.getRegistryName().fullName, count);
			element.setDataInternalId(count);
			counter++;
		} else {
			Logger.instance.warn("Tried to register <" + element.getClass() + " in " + element.getContainerType() + "> with registry name " + element.getRegistryName() + " that already registered!");
		}
	}

	@Override
	public ExtendedDataHandler getElement(NamespacedName registryName) {
		return map.get(registryName.fullName);
	}

	@Override
	public ExtendedDataHandler getElement(String fullName) {
		return map.get(fullName);
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public Collection<ExtendedDataHandler> getAllEntries() {
		return map.values();
	}

	@Override
	public int getEntryCount() {
		return counter;
	}

	public int getEntryCountByType(Type type) {
		return idMap.get(type).values().size();
	}
}
