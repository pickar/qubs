package xyz.qubs.engine.data;

import xyz.qubs.engine.registry.IRegistryEntry;

import java.lang.reflect.Type;

public abstract class ExtendedDataHandler<Container extends IExtendable> implements IRegistryEntry {
	private final Type containerType;
	private int internalDataId;

	ExtendedDataHandler(Type containerType) {
		this.containerType = containerType;
	}

	public int getDataInternalId() {
		return internalDataId;
	}

	public void setDataInternalId(int id) {
		internalDataId = id;
	}

	public abstract IExtendedData getData(Container from);

	public Type getContainerType() {
		return containerType;
	}
}
