package xyz.qubs.engine.data;

import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.world.chunk.IChunk;

public class BlockCountDataHandler extends ExtendedDataHandler<IChunk> {
	private final NamespacedName registryName;

	public BlockCountDataHandler() {
		super(IChunk.class);
		registryName = new NamespacedName("core", "block_count_data");
	}

	@Override
	public BlockCountData getData(IChunk from) {
		BlockCountData data = (BlockCountData) from.getData(getDataInternalId());
		if (data == null) {
			data = new BlockCountData();
			for (BlockType blockType : from.getBlockTypeArray()) {
				if (blockType != null && !blockType.isAir()) {
					data.blockCount++;
				}
			}
			from.setData(data);
		}
		return data;
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}
}
