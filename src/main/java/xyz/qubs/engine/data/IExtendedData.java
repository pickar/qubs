package xyz.qubs.engine.data;

public interface IExtendedData {
	int getHandlerInternalId();

	byte[] toBytes();

	IExtendedData fromBytes(byte[] bytes);
}
