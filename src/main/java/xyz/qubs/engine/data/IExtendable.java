package xyz.qubs.engine.data;

public interface IExtendable {
	IExtendedData getData(int id);

	void setData(IExtendedData data);
}
