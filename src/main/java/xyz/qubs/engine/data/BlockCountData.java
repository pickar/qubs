package xyz.qubs.engine.data;

public class BlockCountData implements IExtendedData {
	public int blockCount;

	@Override
	public int getHandlerInternalId() {
		return ExtendedDataRegistry.BLOCK_COUNT_DATA_HANDLER.getDataInternalId();
	}

	@Override
	public byte[] toBytes() {
		return new byte[0];
	}

	@Override
	public IExtendedData fromBytes(byte[] bytes) {
		return null;
	}
}
