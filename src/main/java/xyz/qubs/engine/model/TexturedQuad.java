package xyz.qubs.engine.model;

import org.joml.Vector2f;
import org.joml.Vector3f;
import xyz.qubs.demo.TerrainNoise;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.utils.Maths;
import xyz.qubs.engine.vulkan.models.VulkanBuffer;
import xyz.qubs.engine.vulkan.vertices.GeneralVertex;
import xyz.qubs.game.block.BlockTexture;
import xyz.qubs.game.block.TextureRegistry;

public class TexturedQuad {
	private final BlockTexture texture;
	private final Vector3f size;
	private final Side side;
	private final byte[] ao;
	private final byte aoValue;
	public float x;
	public float y;
	public float z;
	public int rotation;

	public TexturedQuad(float x, float y, float z, Side side, BlockTexture texture, byte[] ao) {
		this.texture = texture;
		this.side = side;
		size = new Vector3f(1, 1, 1);
		this.ao = ao;
		aoValue = (byte) (((ao[0] & 0b11) << 6) | ((ao[1] & 0b11) << 4) | ((ao[2] & 0b11) << 2) | (ao[3] & 0b11));
		rotation = 0;//rotation(x, y, z);

		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static int rotation(float x, float y, float z) {
		return Maths.floor((TerrainNoise.noise.evaluate(x, y, z) + 0.9) * 512) & 15;
	}

	public static void dispose() {
		if (!SidedQuad.isFree) {
			for (int i = 0; i < SidedQuad.indBuffer.length; i++) {
				SidedQuad.indBuffer[i].free();
				SidedQuad.vertBuffer[i].free();
			}
			SidedQuad.isFree = true;
		}
	}

	public int getTextureId() {
		return texture != null ? texture.id : TextureRegistry.errorTexture.id;
	}

	public Side getSide() {
		return side;
	}

	public byte[] getAo() {
		return ao;
	}

	public void setAo(byte[] ao) {
		System.arraycopy(ao, 0, this.ao, 0, ao.length);
	}

	public byte getAoValue() {
		return aoValue;
	}

	public Vector3f getSize() {
		return size;
	}

	public static class SidedQuad { //FRONT, BACK, TOP, BOTTOM, RIGHT, LEFT
		public static boolean isFree = false;
		public static GeneralVertex[][] vertices = new GeneralVertex[][] {
				new GeneralVertex[] {
						new GeneralVertex(new Vector3f(0.0f, 1.0f, 1.0f), new Vector2f(0.0f, 0.0f)), //0
						new GeneralVertex(new Vector3f(0.0f, 0.0f, 1.0f), new Vector2f(0.0f, 1.0f)), //1
						new GeneralVertex(new Vector3f(1.0f, 0.0f, 1.0f), new Vector2f(1.0f, 1.0f)), //2
						new GeneralVertex(new Vector3f(1.0f, 1.0f, 1.0f), new Vector2f(1.0f, 0.0f)), //3
				},
				new GeneralVertex[] {
						new GeneralVertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector2f(1.0f, 0.0f)), //4
						new GeneralVertex(new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(1.0f, 1.0f)), //5
						new GeneralVertex(new Vector3f(1.0f, 0.0f, 0.0f), new Vector2f(0.0f, 1.0f)), //6
						new GeneralVertex(new Vector3f(1.0f, 1.0f, 0.0f), new Vector2f(0.0f, 0.0f)), //7
				},
				new GeneralVertex[] {
						new GeneralVertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector2f(0.0f, 0.0f)), //8
						new GeneralVertex(new Vector3f(1.0f, 1.0f, 0.0f), new Vector2f(1.0f, 0.0f)), //9
						new GeneralVertex(new Vector3f(0.0f, 1.0f, 1.0f), new Vector2f(0.0f, 1.0f)), //10
						new GeneralVertex(new Vector3f(1.0f, 1.0f, 1.0f), new Vector2f(1.0f, 1.0f)), //11
				},
				new GeneralVertex[] {
						new GeneralVertex(new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 1.0f)), //12
						new GeneralVertex(new Vector3f(1.0f, 0.0f, 0.0f), new Vector2f(1.0f, 1.0f)), //13
						new GeneralVertex(new Vector3f(0.0f, 0.0f, 1.0f), new Vector2f(0.0f, 0.0f)), //14
						new GeneralVertex(new Vector3f(1.0f, 0.0f, 1.0f), new Vector2f(1.0f, 0.0f)), //15
				},
				new GeneralVertex[] {
						new GeneralVertex(new Vector3f(1.0f, 0.0f, 1.0f), new Vector2f(0.0f, 1.0f)), //16
						new GeneralVertex(new Vector3f(1.0f, 0.0f, 0.0f), new Vector2f(1.0f, 1.0f)), //17
						new GeneralVertex(new Vector3f(1.0f, 1.0f, 0.0f), new Vector2f(1.0f, 0.0f)), //18
						new GeneralVertex(new Vector3f(1.0f, 1.0f, 1.0f), new Vector2f(0.0f, 0.0f)), //19
				},
				new GeneralVertex[] {
						new GeneralVertex(new Vector3f(0.0f, 0.0f, 1.0f), new Vector2f(1.0f, 1.0f)), //20
						new GeneralVertex(new Vector3f(0.0f, 0.0f, 0.0f), new Vector2f(0.0f, 1.0f)), //21
						new GeneralVertex(new Vector3f(0.0f, 1.0f, 0.0f), new Vector2f(0.0f, 0.0f)), //22
						new GeneralVertex(new Vector3f(0.0f, 1.0f, 1.0f), new Vector2f(1.0f, 0.0f)), //23
				},
		};

		public static int[][] indices = {
				new int[] { 0, 1, 3, 3, 1, 2 },
				new int[] { 1, 0, 3, 3, 2, 1 },
				new int[] { 0, 2, 1, 1, 2, 3 },
				new int[] { 2, 0, 1, 1, 3, 2 },
				new int[] { 0, 1, 3, 3, 1, 2 },
				new int[] { 1, 0, 3, 3, 2, 1 },
		};

		public static int[][] indicesFlipped = {
				new int[] { 1, 2, 0, 0, 2, 3 },
				new int[] { 0, 3, 2, 2, 1, 0 },
				new int[] { 2, 3, 0, 0, 3, 1 },
				new int[] { 0, 1, 3, 3, 2, 0 },
				new int[] { 1, 2, 0, 0, 2, 3 },
				new int[] { 0, 3, 2, 2, 1, 0 },
		};

		public static VulkanBuffer[] vertBuffer = new VulkanBuffer[6];
		public static VulkanBuffer[] indBuffer = new VulkanBuffer[6];

		static {
			for (int i = 0; i < 6; i++) {
				vertBuffer[i] = Vulkan.createVertexBuffer(vertices[i]);
				indBuffer[i] = Vulkan.createIndexBuffer(indices[i]);
			}
		}
	}
}
