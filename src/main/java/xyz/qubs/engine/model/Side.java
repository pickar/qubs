package xyz.qubs.engine.model;

public enum Side {
	FRONT(0, 1, 0, 0, 1),
	BACK(0, 1, 0, 0, -1),
	TOP(0, 2, 0, 1, 0),
	BOTTOM(0, 2, 0, -1, 0),
	RIGHT(2, 1, 1, 0, 0),
	LEFT(2, 1, -1, 0, 0);

	public static final int count = values().length;
	public static final Side[] values = { FRONT, BACK, TOP, BOTTOM, RIGHT, LEFT };
	private static final Side[] opposites = { BACK, FRONT, BOTTOM, TOP, LEFT, RIGHT };
	public final int textureX, textureY;
	public final int x, y, z;

	Side(int textureX, int textureY, int x, int y, int z) {
		this.textureX = textureX;
		this.textureY = textureY;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Side opposite() {
		return opposites[this.ordinal()];
	}
}
