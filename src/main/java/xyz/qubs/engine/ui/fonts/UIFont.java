package xyz.qubs.engine.ui.fonts;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import lombok.Getter;
import lombok.NonNull;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.registry.IRegistryEntry;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.settings.SettingsManager;
import xyz.qubs.engine.vulkan.models.VulkanImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

public final class UIFont implements IRegistryEntry {
	public static final FontRenderContext frc = new FontRenderContext(new AffineTransform(), true, false);
	private final NamespacedName registryName;
	private @Getter final String charset;
	private @Getter final Font font;
	private final Object2ObjectOpenHashMap<Character, VulkanImage> characterCache = new Object2ObjectOpenHashMap<>(Character.MAX_VALUE);

	public UIFont(@NonNull final NamespacedName registryName, @NonNull String charset) {
		this.charset = charset;
		this.registryName = registryName;
		this.font = new Font(registryName.name, Font.PLAIN, 16);
		createCharactersCache(font);
	}

	public UIFont(@NonNull final NamespacedName registryName, @NonNull String charset, final int fontType, boolean isResource) {
		this.charset = charset;
		this.registryName = registryName;
		Font font = null;
		try {
			InputStream inputStream;
			if (isResource) {
				inputStream = Thread.currentThread().getContextClassLoader().getResource(SettingsManager.resourcePath + "fonts/" + registryName.name).openStream();
			} else {
				// TODO: create input stream from file
				inputStream = null;
			}

			font = Font.createFont(fontType, inputStream);
			createCharactersCache(font);
		}
		catch (IOException | FontFormatException exception) {
			throw new RuntimeException("InputStream read error: " + exception);
		}
		this.font = font;
	}

	private void createCharactersCache(final Font font) {
		final CharsetEncoder characterEncoder = Charset.forName(charset).newEncoder();
		for (char character = 0; character < Character.MAX_VALUE; character++) {
			if (!characterEncoder.canEncode(character)) { continue; }
			final VulkanImage texture = createCharacter(font, String.valueOf(character));
			if (texture != null && !characterCache.containsKey(character)) {
				characterCache.put(character, texture);
			}
		}
	}

	private VulkanImage createCharacter(final Font font, final String strCharacter) {
		final Rectangle2D bounds = font.getStringBounds(strCharacter, UIFont.frc);
		final int width = (int) Math.ceil(bounds.getWidth());
		final int height = (int) Math.ceil(bounds.getHeight());
		if (width == 0 || height == 0) {
			return null;
		}
		final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D graphics = image.createGraphics();

		graphics.setColor(Color.WHITE);
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.setFont(font);

		graphics.drawString(strCharacter, 0, graphics.getFontMetrics().getAscent());

		graphics.dispose();
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			ImageIO.write(image, "png", out);
			out.flush();
			final byte[] data = out.toByteArray();
			final ByteBuffer buf = ByteBuffer.allocateDirect(data.length);
			buf.put(data, 0, data.length);
			buf.flip();
			return Vulkan.createTextureFromBytes(buf);
		} catch (IOException exception) {
			throw new RuntimeException("Bytebuffer write error: " + exception);
		}
	}

	public VulkanImage getCharacter(Character character) {
		if (characterCache.containsKey(character)) {
			return characterCache.get(character);
		}

		final VulkanImage texture = createCharacter(this.font, String.valueOf(character));
		characterCache.put(character, texture);
		return texture;
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}

	public void dispose() {
		for (VulkanImage value : characterCache.values()) {
			value.markForDisposal();
		}
	}
}
