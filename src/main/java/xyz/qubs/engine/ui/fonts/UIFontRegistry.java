package xyz.qubs.engine.ui.fonts;

import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.registry.RegisterEntriesEvent;
import xyz.qubs.engine.registry.Registry;
import xyz.qubs.engine.registry.RegistryCreationEvent;

import java.lang.reflect.Type;

public class UIFontRegistry extends Registry<UIFont> {
	public static final UIFontRegistry INSTANCE = new UIFontRegistry(new NamespacedName("core", "fonts"), UIFont.class);

	public static final UIFont Consolas = new UIFont(new NamespacedName("core", "Consolas"), "Windows-1251");

	public UIFontRegistry(NamespacedName registryName, Type type) {
		super(registryName, type);
	}

	public static UIFont getFontByName(NamespacedName registryName) {
		return INSTANCE.getElement(registryName);
	}

	public static UIFont getFontByName(String fullName) {
		return INSTANCE.getElement(fullName);
	}

	@SubscribeEvent
	public static void onRegistryCreation(RegistryCreationEvent event) {
		event.registry.register(INSTANCE);
	}

	@SubscribeEvent
	public static void onRegisterEntriesIPhysics(RegisterEntriesEvent<UIFont> event) {
		event.registry.register(Consolas);
	}
}
