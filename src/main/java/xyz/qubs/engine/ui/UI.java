package xyz.qubs.engine.ui;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.Engine;
import xyz.qubs.engine.event.EventManager;
import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.event.events.WindowResizedEvent;
import xyz.qubs.engine.event.listeners.ListenerPriority;
import xyz.qubs.engine.ui.elements.base.Rectangle;
import xyz.qubs.engine.ui.elements.base.StaticLabel;
import xyz.qubs.engine.ui.events.*;
import xyz.qubs.engine.ui.layouts.StackLayout;
import xyz.qubs.engine.ui.layouts.VulkanLayout;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.properties.UIColor;
import xyz.qubs.engine.ui.models.properties.UIPoint;
import xyz.qubs.engine.ui.models.properties.UIVector2u;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public final class UI {
	private static final ObjectList<UIControl> rootControls = new ObjectArrayList<>();
	private static final Timer timer = new Timer();
	private static VulkanLayout vulkanLayout;
	private static HashMap<String, UIControl> identifiedElements;
	private static UIControl nowControl;

	public static void init() {
		if (vulkanLayout != null) {
			return;
		}

		EventManager.registerListeners(UI.class);
		vulkanLayout = new VulkanLayout();
		identifiedElements = new HashMap<>();

		StackLayout layoutRoot = new StackLayout(new UIPoint(10, 10), new UIVector2u(300, 100));
		layoutRoot.setPadding(20, 20, 0, 30);
		layoutRoot.setBackground(new UIColor(UIColor.DARK_GRAY, 0.5f));
		layoutRoot.setAlignment(StackLayout.UIAlignment.Vertical);

		Rectangle rectangle = new Rectangle();
//		rectangle.setPadding(30, 4, 0, 4);
		rectangle.setSize(new UIVector2u(100, 10));
		rectangle.setBackground(UIColor.DARK_GRAY);
		layoutRoot.addControl(rectangle);

		StaticLabel coordinates = new StaticLabel();
		coordinates.setPadding(0, 10, 0, 10);
//		coordinates.setBackground(UIColor.BLUE);
		identifiedElements.put("coord", coordinates);
		layoutRoot.addControl(coordinates);

		StaticLabel fpsCounter = new StaticLabel();
//		fpsCounter.setBackground(new UIColor(UIColor.GREEN, 0.9f));
		fpsCounter.setPadding(0, 0, 0, 10);
		identifiedElements.put("fps", fpsCounter);
		layoutRoot.addControl(fpsCounter);

//		Label staticText = new Label();
//		staticText.setBackground(UIColor.RED);
//		staticText.setText("Static");
//		layoutRoot.addControl(staticText);

		rectangle = new Rectangle();
		rectangle.setSize(new UIVector2u(250, 2));
		rectangle.setPadding(0, 4, 0, 4);
		rectangle.setBackground(UIColor.DARK_GRAY);
		rectangle.setCornerRadius(10);
		layoutRoot.addControl(rectangle);

		identifiedElements.put("main", layoutRoot);
		UI.addRootControl(layoutRoot);
		UI.recreateScene();

		//TODO: Load templates to memory
		//TODO: Parser to another class and get UI to control of it.
		/*String testInput = "<GridLayout cols=\"50, *\" rows=\"*\"><Button id=\"button\" col=\"0\" row=\"0\" text=\"Text\" onClick=\"\" /></GridLayout>";
		Parser parser = new Parser(identifiedElements, identifiedLayouts);
		parser.parseLayout(testInput);*/

		final NumberFormat numberFormat = NumberFormat.getNumberInstance();
		numberFormat.setMinimumFractionDigits(3);
		numberFormat.setMaximumFractionDigits(3);

		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				String str = Engine.camera.getPosition().toString(numberFormat);

				((StaticLabel) identifiedElements.get("coord")).setText(str.substring(1, str.length() - 1)).update();
//				timer.cancel();
			}
		}, 0, 100);

		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				((StaticLabel) identifiedElements.get("fps")).setText("FPS: " + Engine.fpsToShow.getAndSet(0)).update();
			}
		}, 0, 1000);
	}

	public static ObjectList<UIControl> getRootControls() {
		return rootControls;
	}

	public static void addRootControl(UIControl control) {
		rootControls.add(control);
	}

	public static void removeRootControl(UIControl control) {
		rootControls.remove(control);
	}

	public static UIControl getControl(String idOfControl) {
		return identifiedElements.get(idOfControl);
	}

	public synchronized static void updateControlSynchronized(UIControl control) {
		vulkanLayout.updateScene(control);
	}

	public synchronized static void updateControlsSynchronized(ObjectList<UIControl> controls) {
		vulkanLayout.updateScene(controls);
	}

	public synchronized static void recreateSceneSceneSynchronized() {
		vulkanLayout.recreateScene(rootControls);
	}

	public static void updateControl(UIControl control) {
		vulkanLayout.updateScene(control);
	}

	public static void updateControls(ObjectList<UIControl> controls) {
		vulkanLayout.updateScene(controls);
	}

	public static void recreateScene() {
		vulkanLayout.recreateScene(rootControls);
	}

	public static void dispose() {
		timer.cancel();
		EventManager.unregisterListeners(UI.class);
		vulkanLayout.dispose();
	}

	@SubscribeEvent(priority = ListenerPriority.HIGH)
	private static void onWindowResize(WindowResizedEvent event) {
		UI.recreateScene();
	}

	@SubscribeEvent
	private static void onKeyDown(KeyDownEvent event) {
	}

	@SubscribeEvent
	private static void onKeyUp(KeyUpEvent event) {
	}

	@SubscribeEvent
	private static void onMouseClick(MouseClickEvent event) {
		//root.onMouseClick(event);
	}

	@SubscribeEvent
	private static void onMouseMove(MouseMoveEvent event) {

	}

	@SubscribeEvent
	private static void onMouseDrag(MouseClickEvent event) {
		//root.onMouseEvent(event);
	}

	@SubscribeEvent
	private static void onMouseScroll(MouseScrollEvent event) {
		//root.onMouseEvent(event);
	}
}
