package xyz.qubs.engine.ui.layouts;

import lombok.Getter;
import lombok.NonNull;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.controls.UILayout;
import xyz.qubs.engine.ui.models.properties.UIPoint;
import xyz.qubs.engine.ui.models.properties.UIVector2u;
import xyz.qubs.engine.ui.models.properties.UseAxis;

public class StackLayout extends UILayout {
	// Properties
	private @Getter UIAlignment alignment = UIAlignment.Horizontal;

	public StackLayout() {}

	public StackLayout(UIPoint location) {
		this.location = location;
	}

	public StackLayout(UIPoint location, UIVector2u size) {
		this.location = location;
		this.setSize(size);
	}

	public StackLayout setAlignment(UIAlignment alignment) {
		if (this.alignment != alignment) {
			this.alignment = alignment;
			updateControlsAlignments();
		}

		return this;
	}

	@Override
	public StackLayout addControl(@NonNull UIControl control) {
		control.setAxis(UIAlignment.useOnly(alignment));

		for (UIControl childControl : controls) {
			control.getLocation().addToShiftLocation(childControl);
		}

		controls.add(control);
		super.addControl(control);

		return this;
	}

	@Override
	public StackLayout removeControl(@NonNull UIControl control) {
		control.setAxis(UseAxis.Both);

		int index = controls.indexOf(control);
		if (index == 1) { return this; }

		for (int i = index + 1; i < controls.size(); i++) {
			controls.get(i).getLocation().removeFromShiftLocation(control);
		}

		controls.remove(index);
		super.removeControl(control);

		return this;
	}

	@Override
	protected UIVector2u getAutoSize() {
		return null;
	}

	protected void updateControlsAlignments() {
		for (UIControl uiControl : controls) {
			uiControl.setAxis(UIAlignment.useOnly(alignment));
		}
	}

	public enum UIAlignment {
		Horizontal,
		Vertical;

		public static UseAxis useOnly(UIAlignment alignment) {
			return
			switch (alignment) {
				case Vertical -> UseAxis.Y;
				case Horizontal -> UseAxis.X;
			};
		}
	}
}
