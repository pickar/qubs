package xyz.qubs.engine.ui.layouts;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import lombok.NonNull;
import lombok.val;
import org.apache.logging.log4j.MarkerManager;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.Window;
import xyz.qubs.engine.logger.Logger;
import xyz.qubs.engine.settings.SettingsManager;
import xyz.qubs.engine.ui.models.UIControlRepresentation;
import xyz.qubs.engine.ui.fonts.UIFont;
import xyz.qubs.engine.ui.fonts.UIFontRegistry;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.properties.UIIndentation;
import xyz.qubs.engine.ui.models.calculation.CalculationUnit;
import xyz.qubs.engine.ui.models.properties.UIVector2u;
import xyz.qubs.engine.ui.models.calculation.RenderTree;
import xyz.qubs.engine.vulkan.models.VulkanImage;
import xyz.qubs.engine.vulkan.models.VulkanUIRenderObject;
import xyz.qubs.engine.vulkan.renderers.UIRenderer;

// Schrödinger's layout. As layout, but not.
// Transfer UIControl to VRO with level cache system
public final class VulkanLayout {
	public static ObjectList<VulkanImage> disposalQueue = new ObjectArrayList<>();
	// Renderer level
	private RenderTree renderTree;
	// Cache generations
	// First cache generation using for permanent objects whose VROs are never deleted from memory
	// Second cache generation using for save VROs before control doesn't remove from scene
	private final Object2ObjectOpenHashMap<UIControl, UIControlRepresentation> firstCacheControlsGeneration = new Object2ObjectOpenHashMap<>();
	private final Object2ObjectOpenHashMap<UIControl, UIControlRepresentation> secondCacheControlsGeneration = new Object2ObjectOpenHashMap<>();
	// Cache sizes using for save calculations of objects sizes per scene
	private final Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedWithoutPaddingSizes = new Object2ObjectOpenHashMap<>();

	public void updateScene(@NonNull final UIControl control) {
		frameUpdate(updateControlOnScene(control));
	}

	public void updateScene(@NonNull final ObjectList<UIControl> controls) {
		boolean updateTextures = false;
		for (final UIControl control : controls) {
			if (control == null) { continue; }
			updateTextures = updateControlOnScene(control) || updateTextures;
		}

		frameUpdate(updateTextures);
	}

	/*
	 * Recreate control and its children on scene
	 * Recreate render tree for all controls on scene
	 * @param  rootControls  list of parent controls that need to be update
	 */
	public void recreateScene(@NonNull final ObjectList<UIControl> rootControls) {
		renderTree = new RenderTree();

		for (final UIControl uiControl : rootControls) {
			if (uiControl == null) { continue; }
			if (uiControl.isDisplayOnScene()) {
				recreateControlOnScene(uiControl);
			} else {
				secondCacheControlsGeneration.remove(uiControl);
			}
		}

		frameUpdate(true);
	}

	/*
	 * Update control and its children
	 * Doesn't recreate render tree
	 * @param  control  current element for update
	 * @return  flag that show texture updated or not
	 */
	private boolean updateControlOnScene(final UIControl control) {
		boolean updateTextures = false;
		final UIControlRepresentation representation = getRepresentationByControl(control);
		final UIControlRepresentation parentRepresentation = control.getParent() != null ? getRepresentationByControl(control.getParent()) : null;

		if (representation == null) { return false; }
		final UIVector2u parentSize, parentLocation;
		if (parentRepresentation == null) {
			parentSize = CalculationUnit.screenSize;
			parentLocation = CalculationUnit.screenLocation;
		} else {
			VulkanUIRenderObject parentVRO;
			if ((parentVRO = parentRepresentation.getColorVRO()) != null) {
				parentSize = parentVRO.size;
				final UIIndentation indentation = control.getParent().getPadding();
				parentLocation = new UIVector2u(parentVRO.location.x, Window.height - parentVRO.location.y.get())
						.add(indentation.left, indentation.top);
			} else {
				parentVRO = parentRepresentation.getTextureVRO();
				parentSize = parentVRO.size;
				parentLocation = new UIVector2u(parentVRO.location.x, Window.height - parentVRO.location.y.get());
			}
		}

		for (VulkanUIRenderObject vulkanUIRenderObject : representation) {
			updateTextures = updateVRO(control, parentSize, parentLocation, vulkanUIRenderObject) || updateTextures;
		}

		return updateTextures;
	}

	/*
	 * We go through the tree of elements that begins from control parameter
	 * Create CalculationUnit for every control. Use for size, position calculations
	 * This functions uses for recreate render tree
	 * @param  control  begin node (control) in elements tree
	 */
	private void recreateControlOnScene(final UIControl control) {
		final ObjectArrayFIFOQueue<CalculationUnit> uiControls = new ObjectArrayFIFOQueue<>();
		uiControls.enqueue(CalculationUnit.builder()
				.parentLocation(CalculationUnit.screenLocation)
				.parentSize(CalculationUnit.screenSize)
				.control(control)
				.build());

		while (!uiControls.isEmpty()) {
			final CalculationUnit calculationUnit = uiControls.dequeue();
			final UIControl uiControl = calculationUnit.getControl();
			createVRO(calculationUnit);

			final UIVector2u uiControlLocation = uiControl.getLocation()
					.calculateLocationWithCacheLevel(calculationUnit.getParentSize(), cachedWithoutPaddingSizes)
					.add(calculationUnit.getParentLocation())
					.add(uiControl.getPadding().left, uiControl.getPadding().top);
			final UIVector2u uiControlSize = uiControl.getSizeWithoutPadding(calculationUnit.getParentSize(), cachedWithoutPaddingSizes);

			final ObjectList<UIControl> controls = calculationUnit.getControl().getControls();
			if (controls == null) { continue; }
			for (final UIControl childControl : controls) {
				if (!childControl.isDisplayOnScene()) { continue; }
				uiControls.enqueue(CalculationUnit.builder()
						.parentLocation(uiControlLocation)
						.parentSize(uiControlSize)
						.control(childControl)
						.build());
			}
		}
	}

	/*
	 * Create VulkanUIRenderObject and add it to renderTree
	 * It will be created if has content or background color
	 * @param  calculationUnit  parent parameters for update. Use for calculations
	 */
	private void createVRO(final CalculationUnit calculationUnit) {
		final UIControl control = calculationUnit.getControl();
		// val: final Object2ObjectOpenHashMap<UIControl, ObjectList<VulkanUIRenderObject>>
		val currentGeneration = getCacheControlsGeneration(control.isFirstGenerationControl());

		if (!currentGeneration.containsKey(control)) {
			// Create VRO for control content. Example: text in Label
			// Texture is caching in control
			final UIControlRepresentation representation = new UIControlRepresentation();
			if (control.getTexture(calculationUnit.getParentSize(), cachedWithoutPaddingSizes) != null) {
				final VulkanUIRenderObject textureVRO = representation.setTextureVRO(new VulkanUIRenderObject(false));
				updateVRO(calculationUnit, textureVRO);

				this.renderTree.addTextureVROToTreeRenderControls(textureVRO);
			}

			// Create VRO for background color
			final VulkanUIRenderObject colorVRO = representation.setColorVRO(new VulkanUIRenderObject(true));
			updateVRO(calculationUnit, colorVRO);

			this.renderTree.addColorVROToRenderControls(colorVRO);

			if (!representation.isEmpty()) {
				currentGeneration.put(control, representation);
			}
		} else {
			// Update VRO in cache
			final UIControlRepresentation representation = currentGeneration.get(control);
			for (VulkanUIRenderObject vulkanUIRenderObject : representation) {
				updateVRO(calculationUnit, vulkanUIRenderObject);
				this.renderTree.addColorVROToRenderControls(vulkanUIRenderObject);
			}
		}
	}

	/*
	 * Update all params
	 * @param  control  data source for update
	 * @param  calculationUnit  parent parameters for update. Use for calculations
	 */
	private void updateVRO(final CalculationUnit calculationUnit, final VulkanUIRenderObject vro) {
		vro.minSize(calculationUnit.getParentLocation())
				.maxSize(calculationUnit.getParentSize());

		updateVRO(calculationUnit.getControl(), calculationUnit.getParentSize(), calculationUnit.getParentLocation(), vro);
	}

	/*
	* Update all params without parent bounds(minSize and maxSize in vro)
	* @param  control  data source for update
	* @param  parentSize  size of parent control. Use for calculations
	* @param  parentLocation  location of parent control. Use for calculations
	* @param  vro  consumer of data
	* @return  flag that show texture updated or not
	 */
	private boolean updateVRO(final UIControl control, final UIVector2u parentSize, final UIVector2u parentLocation, final VulkanUIRenderObject vro) {
		boolean updateTextures = false;

		if (vro.isColor()) {
			vro.color(control.getBackground())
					.size(control.getSizeWithPadding(parentSize, cachedWithoutPaddingSizes))
					.location(control.getLocation()
							.calculateLocationWithCacheLevel(parentSize, cachedWithoutPaddingSizes)
							.add(parentLocation))
					.zIndex(0)
					.cornerRadius(control.getCornerRadius())
					.visibility(control.isVisibility());
		}
		else {
			final VulkanImage texture = control.getTexture(parentSize, cachedWithoutPaddingSizes);
			if (texture != vro.texture) {
				updateTextures = true;
			}

			vro.texture(texture)
					.size(control.getSizeWithoutPadding(parentSize, cachedWithoutPaddingSizes))
					.location(control.getLocation()
							.calculateLocationWithCacheLevel(parentSize, cachedWithoutPaddingSizes)
							.add(control.getPadding().left, control.getPadding().top)
							.add(parentLocation))
					.zIndex(0)
					.cornerRadius(control.getCornerRadius())
					.visibility(control.isVisibility())
					.mask(control.getMaskColor());
		}

		// Check children controls
		if (control.getControls() != null) {
			for (final UIControl uiControl : control.getControls()) {
				if (uiControl == null) { continue; }
				updateControlOnScene(uiControl);
			}
		}

		return updateTextures;
	}

	/*
	* Transfer control tree to UIRenderer
	* @param  updateTexture  flag that show texture updated or not
	*/
	private void frameUpdate(final boolean updateTexture) {
		cachedWithoutPaddingSizes.clear();
		if (SettingsManager.debugMode) {
			Logger.instance.debug(MarkerManager.getMarker("UI"), renderTree.toString());
		}

		UIRenderer.update(renderTree, updateTexture, disposalQueue);
		disposalQueue = new ObjectArrayList<>();
	}

	/*
	 * Get cache generation for control by flag
	 * @param  isFirstCacheGeneration  flag that show item from first or second cache generation
	 * @return  cache generation
	 */
	private Object2ObjectOpenHashMap<UIControl, UIControlRepresentation> getCacheControlsGeneration(boolean isFirstCacheGeneration) {
		return isFirstCacheGeneration ? firstCacheControlsGeneration : secondCacheControlsGeneration;
	}

	/*
	 * Get VulkanUIRenderObjects for control by flag
	 * @param  control  control xD
	 * @return  VulkanUIRenderObject list
	 */
	private UIControlRepresentation getRepresentationByControl(final UIControl control) {
		return control.isFirstGenerationControl() ? firstCacheControlsGeneration.getOrDefault(control, null) :
				secondCacheControlsGeneration.getOrDefault(control, null);
	}

	/*
	 * Dispose VulkanLayout and its dependencies
	 */
	public void dispose() {
		// Dispose cached VRO
		firstCacheControlsGeneration.values().forEach(UIControlRepresentation::dispose);
		secondCacheControlsGeneration.values().forEach(UIControlRepresentation::dispose);

		// Dispose generated fonts in registry
		UIFontRegistry.INSTANCE.getAllEntries().forEach(UIFont::dispose);
		Vulkan.imageDisposalQueue.addAll(disposalQueue);
	}
}
