package xyz.qubs.engine.ui.layouts;

import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.controls.UILayout;
import xyz.qubs.engine.ui.models.properties.UIPoint;
import xyz.qubs.engine.ui.models.units.UIUnitPercent;
import xyz.qubs.engine.ui.models.properties.UIVector2u;

public class AbsoluteLayout extends UILayout {
	public AbsoluteLayout() {
		this.setSize(new UIVector2u(new UIUnitPercent(100), new UIUnitPercent(100)));
	}

	public AbsoluteLayout(UIPoint location) {
		this.location = location;
		this.setSize(new UIVector2u(new UIUnitPercent(100), new UIUnitPercent(100)));
	}

	public AbsoluteLayout(UIPoint location, UIVector2u size) {
		this.location = location;
		this.setSize(size);
	}

	@Override
	public AbsoluteLayout addControl(UIControl control) {
		controls.add(control);
		return this;
	}

	@Override
	public AbsoluteLayout removeControl(UIControl control) {
		controls.remove(control);
		return this;
	}

	@Override
	public UIVector2u getAutoSize() {
		return null;
	}
}
