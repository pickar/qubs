package xyz.qubs.engine.ui.layouts;

import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.controls.UILayout;
import xyz.qubs.engine.ui.models.properties.UIPoint;
import xyz.qubs.engine.ui.models.properties.UIVector2u;
import xyz.qubs.engine.ui.models.units.UIUnitPercent;

public class GridLayout extends UILayout {
	private int columnsCount, rowsCount;
	// TODO: Constraints system for rows and columns

	public GridLayout(int columns, int rows) {
		super(columns * rows);
		this.setSize(new UIVector2u(new UIUnitPercent(100), new UIUnitPercent(100)));
		this.columnsCount = columns;
		this.rowsCount = rows;
		fillControls();
	}

	public GridLayout(int columns, int rows, UIPoint location) {
		super(columns * rows);
		this.location = location;
		this.setSize(new UIVector2u(new UIUnitPercent(100), new UIUnitPercent(100)));
		this.columnsCount = columns;
		this.rowsCount = rows;
		fillControls();
	}

	public GridLayout(int columns, int rows, UIPoint location, UIVector2u size) {
		super(columns * rows);
		this.location = location;
		this.setSize(size);
		this.columnsCount = columns;
		this.rowsCount = rows;
		fillControls();
	}

	@Override
	public GridLayout addControl(UIControl control) {
		int item = 0;
		for (; item < controls.size(); item++) {
			if (controls.get(item) == null) {
//				if (!control.getMaxSize().isSetByUser) {
//					// TODO: set maxSize
//					// control.getMaxSize().setSize(, false);
//				}
				return setElement(control, item);
			}
		}

		addColumn();
		setElement(control, item);

		return this;
	}

	@Override
	public GridLayout removeControl(UIControl control) {
		// TODO: remove column if it need
		controls.set(controls.indexOf(control), null);
		super.removeControl(control);

		return this;
	}

	@Override
	public UIVector2u getAutoSize() {
		return null;
	}

	public GridLayout addRow() {
		for (int column = 0; column < columnsCount; column++) {
			controls.add((column * rowsCount) + rowsCount - 1, null);
		}

		rowsCount++;

		return this;
	}

	public GridLayout addColumn() {
		for (int item = 0; item < rowsCount; item++) {
			controls.add(null);
		}
		columnsCount++;

		return this;
	}

	public GridLayout setElement(UIControl control, int column, int row) {
		return setElement(control, (column * rowsCount) + row);
	}

	private GridLayout setElement(UIControl control, int item) {
		control.getLocation().setShiftLocation(this.location);

		for (int i = 0; i < rowsCount - (item % rowsCount); i++) {
			UIControl uiControl = controls.get(item + i);
			control.getLocation().addToShiftLocation(uiControl);
		}
		controls.set(item, control);
		super.addControl(control);

		return this;
	}

	public UIControl getElement(int column, int row) {
		return controls.get((column * rowsCount) + row);
	}

	public GridLayout removeRow(int row) {
		for (int column = 0; column < columnsCount; column++) {
			controls.set((column * rowsCount) + row, null);
		}
		rowsCount--;

		return this;
	}

	public GridLayout removeColumn(int column) {
		int elementId = (column * rowsCount);
		for (int item = elementId; item < elementId + rowsCount; item++) {
			controls.set(item, null);
		}
		columnsCount--;

		return this;
	}

	private void fillControls() {
		for (int item = 0; item < columnsCount * rowsCount; item++) {
			controls.add(null);
		}
	}
}
