package xyz.qubs.engine.ui.events;

import org.joml.Vector2i;
import xyz.qubs.engine.event.events.Event;

public class MouseMoveEvent extends Event {
	public Vector2i location;

	public MouseMoveEvent(Vector2i location) {
		this.location = location;
	}
}
