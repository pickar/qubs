package xyz.qubs.engine.ui.events;

import xyz.qubs.engine.event.events.Event;

public class KeyUpEvent extends Event {
	public int key;

	public KeyUpEvent(int key) {
		this.key = key;
	}
}
