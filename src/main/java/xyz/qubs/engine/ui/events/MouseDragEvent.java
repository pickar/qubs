package xyz.qubs.engine.ui.events;

import org.joml.Vector2i;
import xyz.qubs.engine.event.events.Event;

public class MouseDragEvent extends Event {
	public Vector2i location;
	public Vector2i oldLocation;
	public MouseButton mouseButton;

	public MouseDragEvent(Vector2i oldLocation, Vector2i location, MouseButton mouseButton) {
		this.oldLocation = oldLocation;
		this.location = location;
		this.mouseButton = mouseButton;
	}
}
