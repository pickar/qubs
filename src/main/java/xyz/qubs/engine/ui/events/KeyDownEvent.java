package xyz.qubs.engine.ui.events;

import xyz.qubs.engine.event.events.Event;

public class KeyDownEvent extends Event {
	public int key;
	public int action;

	public KeyDownEvent(int key, int action) {
		this.key = key;
		this.action = action;
	}
}
