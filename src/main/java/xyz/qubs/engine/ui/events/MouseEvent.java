package xyz.qubs.engine.ui.events;

import org.joml.Vector2i;
import xyz.qubs.engine.event.events.Event;

public class MouseEvent extends Event {
	public Vector2i location;

	public MouseEvent(Vector2i location) {
		this.location = location;
	}
}
