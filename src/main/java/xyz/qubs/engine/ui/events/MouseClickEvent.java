package xyz.qubs.engine.ui.events;

import org.joml.Vector2i;
import xyz.qubs.engine.event.events.Event;

public class MouseClickEvent extends Event {
	public Vector2i location;
	public MouseButton mouseButton;

	public MouseClickEvent(Vector2i location, MouseButton mouseButton) {
		this.location = location;
		this.mouseButton = mouseButton;
	}
}
