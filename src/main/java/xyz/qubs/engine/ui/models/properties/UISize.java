package xyz.qubs.engine.ui.models.properties;

import lombok.Getter;
import xyz.qubs.engine.ui.models.units.IUnit;

public class UISize extends UIVector2u {
	private @Getter boolean isUser;

	public UISize(UIVector2u size, boolean isUser) {
		super(size);
		this.isUser = isUser;
	}

	public UISize(int x, int y, boolean isUser) {
		super(x, y);
		this.isUser = isUser;
	}

	public UISize(IUnit x, int y, boolean isUser) {
		super(x, y);
		this.isUser = isUser;
	}

	public UISize(int x, IUnit y, boolean isUser) {
		super(x, y);
		this.isUser = isUser;
	}

	public UISize(IUnit x, IUnit y, boolean isUser) {
		super(x, y);
		this.isUser = isUser;
	}

	@Override
	public UIVector2u setVector(int x, int y) {
		this.isUser = false;
		super.setVector(x, y);
		return this;
	}

	@Override
	public UISize setVector(UIVector2u size) {
		this.isUser = false;
		super.setVector(size);
		return this;
	}

	@Override
	public UISize setVector(IUnit x, IUnit y) {
		this.isUser = false;
		super.setVector(x, y);
		return this;
	}

	public UIVector2u setVectorByUser(int x, int y) {
		this.isUser = true;
		super.setVector(x, y);
		return this;
	}

	public UISize setVectorByUser(UIVector2u size) {
		this.isUser = true;
		super.setVector(size);
		return this;
	}

	public UISize setVectorByUser(IUnit x, IUnit y) {
		this.isUser = true;
		super.setVector(x, y);
		return this;
	}
}
