package xyz.qubs.engine.ui.models.properties;

public enum UseAxis {
	X,
	Y,
	Both
}
