package xyz.qubs.engine.ui.models.controls;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectList;
import lombok.*;
import lombok.experimental.Accessors;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.event.events.Event;
import xyz.qubs.engine.ui.UI;
import xyz.qubs.engine.ui.models.properties.*;
import xyz.qubs.engine.ui.events.MouseClickEvent;
import xyz.qubs.engine.ui.events.MouseDragEvent;
import xyz.qubs.engine.ui.events.MouseMoveEvent;
import xyz.qubs.engine.ui.events.MouseScrollEvent;
import xyz.qubs.engine.vulkan.models.VulkanImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Stack;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

@Accessors(chain = true)
public abstract class UIControl {
	public static final UISize standardMinSize = new UISize(1, 1, false);
	public static final UIIndentation standardPadding = new UIIndentation(0, 0, 0, 0);

	private @Getter final boolean isFirstGenerationControl;
	// Events
	public BiConsumer<UIControl, MouseClickEvent> onMouseClick;
	public BiConsumer<UIControl, Event> onMouseEnter;
	public BiConsumer<UIControl, Event> onMouseLeave;
	public BiConsumer<UIControl, MouseScrollEvent> onMouseScroll;
	public BiConsumer<UIControl, MouseMoveEvent> onMouseMove;
	public BiConsumer<UIControl, MouseDragEvent> onMouseDrag;
	public BiConsumer<UIControl, Event> onRefreshed;
	// Properties
	// Control always will be add on scene layer
	protected @Getter boolean visibility = true;
	// That affects on recalculate all scene
	// If showOnScene will have been false control willn't add on scene
	protected @Getter boolean displayOnScene = true;
	protected @Getter @Setter UseAxis axis = UseAxis.Both;
	protected @Getter @Setter
	UIColor background = UIColor.TRANSPARENT;
	protected @Getter @Setter UIColor maskColor = UIColor.WHITE;
	protected @Getter @Setter int cornerRadius = 0;
	protected @Getter UIIndentation padding = standardPadding;
	protected @Getter
	UIPoint location = new UIPoint(0, 0);
	protected @Getter UISize maxSize;
	protected @Getter UISize size;
	protected @Getter UISize minSize = standardMinSize;
	protected @Getter UILayout parent = null;
	protected @Getter ObjectList<UIControl> controls;
	// Temporary properties for texture cache layer
	private @Getter(AccessLevel.PROTECTED) VulkanImage texture;
	protected boolean isTextureDirty = true;

	public UIControl() {
		this.isFirstGenerationControl = false;
	}

	public UIControl(boolean isFirstGenerationLayout) {
		this.isFirstGenerationControl = isFirstGenerationLayout;
	}

	public VulkanImage getTexture(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedSizes) {
		if (isTextureDirty) {
			refreshTexture(parentLayoutSize, cachedSizes);
		}
		return texture;
	}

	public UIVector2u getSizeWithoutPadding(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedSizes) {
		if (cachedSizes.containsKey(this)) {
			return cachedSizes.get(this);
		}

		UIVector2u calculatedSize;
		if (size == null) {
			calculatedSize = getAutoSize();
		} else {
			calculatedSize = size.calculate(parentLayoutSize);
		}

		if (calculatedSize == null) {
			calculatedSize = new UIVector2u(1, 1);
		}
		else {
			// MaxSize constraints
			if (maxSize != null) {
				UIVector2u calculatedMaxSize = this.maxSize.calculate(parentLayoutSize);
				if (calculatedSize.x.get() > calculatedMaxSize.x.get()) {
					calculatedSize.x = calculatedMaxSize.x;
				}
				if (calculatedSize.y.get() > calculatedMaxSize.y.get()) {
					calculatedSize.y = calculatedMaxSize.y;
				}
			}

			// MinSize constraints
			if (minSize != standardMinSize) {
				UIVector2u calculatedMinSize = this.minSize.calculate(parentLayoutSize);
				if (calculatedSize.x.get() < calculatedMinSize.x.get()) {
					calculatedSize.x = calculatedMinSize.x;
				}
				if (calculatedSize.y.get() < calculatedMinSize.y.get()) {
					calculatedSize.y = calculatedMinSize.y;
				}
			}
		}

		cachedSizes.put(this, calculatedSize);
		return calculatedSize;
	}

	public UIVector2u getSizeWithPadding(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cacheSizes) {
		return new UIVector2u(getSizeWithoutPadding(parentLayoutSize, cacheSizes)).add(padding);
	}

	public UIControl setSize(UIVector2u size) {
		if (size == null) {
			this.size = null;
		}
		else if (this.size == null) {
			this.size = new UISize(size, true);
		}
		else {
			this.size.setVectorByUser(size);
		}

		return this;
	}

	public UIControl setMaxSize(UIVector2u size) {
		if (size == null) {
			this.maxSize = null;
		}
		else if (this.maxSize == null) {
			this.maxSize = new UISize(size, true);
		}
		else {
			this.maxSize.setVectorByUser(size);
		}

		return this;
	}

	public UIControl setMinSize(@NonNull UIVector2u size) {
		if (minSize == standardMinSize) {
			this.minSize = new UISize(size, true);
		}
		else {
			this.minSize.setVectorByUser(size);
		}

		return this;
	}

	public UIControl setPadding(int left, int top, int right, int bottom) {
		this.padding = new UIIndentation(left, right, top, bottom);
		return this;
//		if (this.padding == standardPadding) {
//			this.padding = new UIIndentation(left, right, top, bottom);
//		}
//		else {
//			 this.padding.set(left, top, right, bottom);
//		}
//
//		return this;
	}

	public UIControl showControl() {
		return goAndSetControls(control -> control.visibility = true);
	}

	public UIControl hideControl() {
		return goAndSetControls(control -> control.visibility = false);
	}

	public UIControl showOnScene() {
		return goAndSetControls(control -> control.displayOnScene = true);
	}

	public UIControl hideOnScene() {
		return goAndSetControls(control -> control.displayOnScene = false);
	}

	public UIControl addControl(UIControl control) {
		return this;
	}

	public UIControl removeControl(UIControl control) {
		return this;
	}

	protected abstract UIVector2u getAutoSize();

	public void update() {
		UI.updateControl(this);
	}

	public UIControl goAndSetControls(Consumer<UIControl> consumer) {
		final Stack<UIControl> uiControls = new Stack<>();
		uiControls.push(this);

		while (!uiControls.isEmpty()) {
			final UIControl control = uiControls.pop();
			consumer.accept(control);

			final ObjectList<UIControl> controls = control.controls;
			if (controls != null) {
				uiControls.addAll(controls);
			}
		}
		return this;
	}

	public void refreshTexture(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedSizes) {
		refreshTexture(drawElementTexture(parentLayoutSize, cachedSizes));
	}

	protected void refreshTexture(VulkanImage texture) {
		if (this.texture != null) {
			this.texture.markForDisposal();
		}
		this.texture = texture;
		isTextureDirty = false;
	}

	protected void refreshTextureWithoutMark(VulkanImage texture) {
		this.texture = texture;
		isTextureDirty = false;
	}

	protected void refreshTexture(BufferedImage texture) {
		ByteBuffer buf;
		if (this.texture != null) {
			this.texture.markForDisposal();
		}
		try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			if (texture != null) {
				ImageIO.write(texture, "png", out);
				out.flush();
				byte[] data = out.toByteArray();
				buf = ByteBuffer.allocateDirect(data.length);
				buf.put(data, 0, data.length);
				buf.flip();
				this.texture = Vulkan.createTextureFromBytes(buf);
			} else {
				this.texture = null;
			}
		} catch (IOException ignored) { }
		isTextureDirty = false;
	}

	// Returns null if control without texture
	protected abstract BufferedImage drawElementTexture(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedSizes);

	protected BufferedImage createImageForDrawing(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cacheSizes) {
		UIVector2u size = getSizeWithoutPadding(parentLayoutSize, cacheSizes);
		return new BufferedImage(size.x.get(), size.y.get(), BufferedImage.TYPE_INT_ARGB);
	}
}
