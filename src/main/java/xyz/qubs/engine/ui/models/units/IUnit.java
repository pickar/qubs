package xyz.qubs.engine.ui.models.units;

public interface IUnit {
	IUnit cloneUnit();
	IUnit subtract(int value);
	IUnit add(int value);
	int calculate(int parentSize);
	IUnit set(int value);
	int get();
}
