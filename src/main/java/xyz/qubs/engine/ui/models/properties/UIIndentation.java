package xyz.qubs.engine.ui.models.properties;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UIIndentation {
	public int left;
	public int right;
	public int top;
	public int bottom;

	public UIIndentation set(int left, int top, int right, int bottom) {
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;

		return this;
	}

	public int getWidthIndentation() {
		return left + right;
	}

	public int getHeightIndentation() {
		return top + bottom;
	}

	@Override
	public String toString() {
		return "Left: " + left + " Right: " + right + " Top: " + top + " Bottom: " + bottom;
	}
}
