package xyz.qubs.engine.ui.models;

import xyz.qubs.engine.vulkan.models.VulkanUIRenderObject;

import java.util.Iterator;

public final class UIControlRepresentation implements Iterable<VulkanUIRenderObject> {
	private final VulkanUIRenderObject[] vros = new VulkanUIRenderObject[2];

	public VulkanUIRenderObject getColorVRO() {
		return vros[0];
	}

	public VulkanUIRenderObject setColorVRO(VulkanUIRenderObject colorVro) {
		vros[0] = colorVro;
		return colorVro;
	}

	public VulkanUIRenderObject getTextureVRO() {
		return vros[1];
	}

	public VulkanUIRenderObject setTextureVRO(VulkanUIRenderObject textureVro) {
		vros[1] = textureVro;
		return textureVro;
	}

	public boolean isEmpty() {
		return vros[0] == null && vros[1] == null;
	}

	public void dispose() {
		for (VulkanUIRenderObject vro : vros) {
			if (vro != null) {
				vro.dispose();
			}
		}
	}

	@Override
	public Iterator<VulkanUIRenderObject> iterator() {
		Iterator<VulkanUIRenderObject> it = new Iterator<>() {

			private int currentIndex = 0;

			@Override
			public boolean hasNext() {
				while (currentIndex < vros.length) {
					if (vros[currentIndex] != null) {
						return true;
					}
					currentIndex++;
				}
				return false;
			}

			@Override
			public VulkanUIRenderObject next() {
				return vros[currentIndex++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		return it;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder("UIControlRepresentation\n");

		for (int i = 0; i < vros.length; i++) {
			stringBuilder.append(i);
			stringBuilder.append(" ");
			stringBuilder.append(vros[i].toString());
		}

		return stringBuilder.toString();
	}
}
