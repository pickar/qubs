package xyz.qubs.engine.ui.models.units;

public class UIUnitPixel extends UnitBase implements IUnit {
	public UIUnitPixel(int value) {
		super(value);
	}

	@Override
	public IUnit cloneUnit() {
		return new UIUnitPixel(value);
	}

	public int calculate(int parentSize) {
		return value;
	}

	@Override
	public String toString() {
		return value + "px";
	}
}
