package xyz.qubs.engine.ui.models.properties;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import lombok.AllArgsConstructor;
import xyz.qubs.engine.ui.models.units.IUnit;
import xyz.qubs.engine.ui.models.units.UIUnitPixel;

@AllArgsConstructor
public class UIVector2u {
	public IUnit x;
	public IUnit y;

	public UIVector2u(int x, int y) {
		this.x = new UIUnitPixel(x);
		this.y = new UIUnitPixel(y);
	}

	public UIVector2u(IUnit x, int y) {
		this.x = x.cloneUnit();
		this.y = new UIUnitPixel(y);
	}

	public UIVector2u(int x, IUnit y) {
		this.x = new UIUnitPixel(x);
		this.y = y.cloneUnit();
	}

	public UIVector2u(UIVector2u vector) {
		this.x = vector.x.cloneUnit();
		this.y = vector.y.cloneUnit();
	}

	public UIVector2u add(UIVector2u vector) {
		return add(vector.x.get(), vector.y.get());
	}

	public UIVector2u add(UIIndentation indentation) {
		return add(indentation.getWidthIndentation(), indentation.getHeightIndentation());
	}

	public UIVector2u add(int x, int y) {
		this.x.add(x);
		this.y.add(y);

		return this;
	}

	public UIVector2u subtract(UIVector2u vector) {
		return subtract(vector.x.get(), vector.y.get());
	}

	public UIVector2u subtract(UIIndentation indentation) {
		return subtract(indentation.getWidthIndentation(), indentation.getHeightIndentation());
	}

	public UIVector2u subtract(int x, int y) {
		this.x.subtract(x);
		this.y.subtract(y);

		return this;
	}

	public UIVector2u setVector(int x, int y) {
		this.x = new UIUnitPixel(x);
		this.y = new UIUnitPixel(y);

		return this;
	}

	public UIVector2u setVector(UIVector2u size) {
		this.x.set(size.x.get());
		this.y.set(size.y.get());

		return this;
	}

	public UIVector2u setVector(IUnit x, IUnit y) {
		this.x = x;
		this.y = y;

		return this;
	}

	public UIVector2u calculate(UIVector2u parentLayoutSize) {
		return new UIVector2u(x.calculate(parentLayoutSize.x.get()), y.calculate(parentLayoutSize.y.get()));
	}

	public UIVector2u calculateWithCache(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIVector2u, UIVector2u> cachedSizes) {
		if (cachedSizes.containsKey(this)) {
			return cachedSizes.get(this);
		}

		final UIVector2u result = calculate(parentLayoutSize);
		cachedSizes.put(this, result);
		return result;
	}

	@Override
	public String toString() {
		return "X: " + x + "; Y: " + y;
	}
}
