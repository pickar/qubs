package xyz.qubs.engine.ui.models.calculation;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.engine.vulkan.models.VulkanUIRenderObject;
import xyz.qubs.engine.vulkan.renderers.UIRenderer;

public final class RenderTree {
	// colorTextureRenderTree - list that sends to vulkan
	public final ObjectList<ObjectList<VulkanUIRenderObject>> colorTextureRenderTree;
	public int countOfTexturesInLastBuffer;
	public int countOfVro;

	public RenderTree() {
		colorTextureRenderTree = new ObjectArrayList<>();
		// Buffer for VROs with background
		colorTextureRenderTree.add(new ObjectArrayList<>());
		// First buffer for controls with content
		colorTextureRenderTree.add(new ObjectArrayList<>());
		countOfTexturesInLastBuffer = 0;
		countOfVro = 0;
	}

	// UIRenderer.textureCountPerSet - Count VRO with textures per buffer
	// This function watching that buffer willn't overflow
	public void addTextureVROToTreeRenderControls(VulkanUIRenderObject vro) {
		if (countOfTexturesInLastBuffer++ < UIRenderer.textureCountPerSet) {
			ObjectList<VulkanUIRenderObject> vroTextureBuffer = colorTextureRenderTree.get(colorTextureRenderTree.size() - 1);
			vroTextureBuffer.add(vro);
		} else {
			ObjectList<VulkanUIRenderObject> newVroTextureBuffer = new ObjectArrayList<>();
			newVroTextureBuffer.add(vro);
			countOfTexturesInLastBuffer = 1;
			colorTextureRenderTree.add(newVroTextureBuffer);
		}
		countOfVro++;
	}

	public void addColorVROToRenderControls(VulkanUIRenderObject vro) {
		colorTextureRenderTree.get(0).add(vro);
		countOfVro++;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (ObjectList<VulkanUIRenderObject> vulkanUIRenderObjects : colorTextureRenderTree) {
			stringBuilder.append("----------------LAYER----------------\n");
			for (VulkanUIRenderObject vulkanUIRenderObject : vulkanUIRenderObjects) {
				stringBuilder.append("----------------ELEMENT----------------\n");
				stringBuilder.append(" Location: ").append(vulkanUIRenderObject.location).append('\n');
				stringBuilder.append(" Size: ").append(vulkanUIRenderObject.size).append('\n');
				stringBuilder.append(" MinSize: ").append(vulkanUIRenderObject.minSize).append('\n');
				stringBuilder.append(" MaxSize: ").append(vulkanUIRenderObject.maxSize).append('\n');
				stringBuilder.append(" Color: ").append(vulkanUIRenderObject.color).append('\n');
				stringBuilder.append("----------------END ELEMENT----------------\n");
			}
			stringBuilder.append("----------------END LAYER----------------\n");
		}

		return stringBuilder.toString();
	}
}
