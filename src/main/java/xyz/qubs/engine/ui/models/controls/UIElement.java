package xyz.qubs.engine.ui.models.controls;

import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.engine.settings.SettingsManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public abstract class UIElement extends UIControl {
	protected ObjectList<UIControl> controls;

	public UIElement() {}

	public UIElement(String layoutName) {
		try (InputStreamReader in = new InputStreamReader(Thread.currentThread().getContextClassLoader().getResource(SettingsManager.resourcePath + "layouts/" + layoutName).openStream())) {
			new BufferedReader(in).lines().parallel().collect(Collectors.joining("\n"));
		} catch (Exception ex) {
			throw new RuntimeException("Can't find .mxaml layout in resources: " + layoutName);
		}
	}
}
