package xyz.qubs.engine.ui.models.units;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class UnitBase implements IUnit {
	protected int value;

	public IUnit add(int value) {
		this.value += value;
		return this;
	}

	public IUnit subtract(int value) {
		this.value -= value;
		return this;
	}

	public IUnit set(int value) {
		this.value = value;
		return this;
	}

	public int get() {
		return value;
	}

	@Override
	public String toString() {
		return value + "[NONE]";
	}
}
