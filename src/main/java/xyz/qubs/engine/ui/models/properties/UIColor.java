package xyz.qubs.engine.ui.models.properties;

import lombok.AllArgsConstructor;

import java.awt.*;

@AllArgsConstructor
public class UIColor {
	public static final UIColor TRANSPARENT = new UIColor(1f, 1f, 1f, 0);
	public static final UIColor RED = new UIColor(1, 0, 0, 1f);
	public static final UIColor GREEN = new UIColor(0, 1, 0, 1f);
	public static final UIColor BLUE = new UIColor(0, 0, 1, 1f);
	public static final UIColor GRAY = new UIColor(0.5f, 0.5f, 0.5f, 1f);
	public static final UIColor LIGHT_GRAY = new UIColor(0.75f, 0.75f, 0.75f, 1f);
	public static final UIColor DARK_GRAY = new UIColor(0.25f, 0.25f, 0.25f, 1f);
	public static final UIColor WHITE = new UIColor(1f, 1f, 1f, 1f);
	public float r, g, b, a;

	public UIColor(UIColor color) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.a = color.a;
	}

	public UIColor(UIColor color, float opacity) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.a = opacity;
	}

	public static int rgbToInt(int r, int g, int b) {
		return (r & 0xff) << 24 | (g & 0xff) << 16 | (b & 0xff) << 8;
	}

	public static UIColor getRandomColor() {
		return new UIColor((float) Math.random(), (float) Math.random(), (float) Math.random(), 1f);
	}

	public static UIColor getRandomColorWithAlpha() {
		return new UIColor((float) Math.random(), (float) Math.random(), (float) Math.random(), (float) Math.random());
	}

	public void setColor(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public Color toColor() {
		return new Color(r, g, b, a);
	}

	public int toInt() {
		return this.toColor().getRGB();
	}

	@Override
	public String toString() {
		return "R: " + r + " G: " + g + " B: " + b + " A: " + a;
	}
}
