package xyz.qubs.engine.ui.models.controls;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.engine.ui.UI;
import xyz.qubs.engine.ui.models.properties.UIVector2u;
import xyz.qubs.engine.ui.models.properties.UseAxis;

import java.awt.image.BufferedImage;
import java.util.Stack;

public abstract class UILayout extends UIControl {
	public UILayout() {
		controls = new ObjectArrayList<>();
	}

	public UILayout(int controlsSize) {
		controls = new ObjectArrayList<>(controlsSize);
	}

	public UILayout(boolean isFirstGenerationObject) {
		super(isFirstGenerationObject);
		controls = new ObjectArrayList<>();
	}

	public UILayout(int controlsSize, boolean isFirstGenerationObject) {
		super(isFirstGenerationObject);
		controls = new ObjectArrayList<>(controlsSize);
	}

	@Override
	public UILayout addControl(UIControl control) {
		control.parent = this;
		return this;
	}

	@Override
	public UILayout removeControl(UIControl control) {
		control.parent = null;
		control.getLocation().clearAnotherSizes();

		return this;
	}

	public UILayout removeAllControls() {
		for (UIControl control : this.controls) {
			control.parent = null;
			control.setAxis(UseAxis.Both);
			control.getLocation().clearAnotherSizes();
		}

		this.controls.clear();
		return this;
	}

	@Override
	protected BufferedImage drawElementTexture(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedSizes) {
		return null;
	}

	@Override
	public void update() {
		ObjectList<UIControl> updateControls = new ObjectArrayList<>();
		updateControls.add(this);
		Stack<UIControl> uiControls = new Stack<>();
		uiControls.addAll(controls);

		while (!uiControls.isEmpty()) {
			UIControl uiControl = uiControls.pop();
			if (uiControl.controls != null) {
				uiControls.addAll(uiControl.controls);
			}
			updateControls.add(uiControl);
		}

		UI.updateControls(updateControls);
	}
}
