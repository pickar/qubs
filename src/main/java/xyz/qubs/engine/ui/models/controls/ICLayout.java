package xyz.qubs.engine.ui.models.controls;

// Inner control layout
// Using for combine controls (click event will be raise up on all controls)
public abstract class ICLayout extends UILayout {
	@Override
	public UILayout addControl(UIControl control) {
		return this;
	}

	@Override
	public UILayout removeControl(UIControl control) {
		return this;
	}
}
