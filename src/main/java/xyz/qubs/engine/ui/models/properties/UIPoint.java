package xyz.qubs.engine.ui.models.properties;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import xyz.qubs.engine.ui.models.controls.UIControl;

import java.util.HashSet;
import java.util.Objects;

public class UIPoint {
	public UIVector2u shiftLocation;
	public final HashSet<UIControl> sizeOfAnotherElements = new HashSet<>();

	public UIPoint(int x, int y) {
		shiftLocation = new UIVector2u(x, y);
	}

	public UIPoint(UIVector2u shiftLocation) {
		this.shiftLocation = shiftLocation;
	}

	public void setShiftLocation(UIPoint shiftPoint) {
		this.shiftLocation = shiftPoint.shiftLocation;
		this.sizeOfAnotherElements.addAll(shiftPoint.sizeOfAnotherElements);
	}

	public void setShiftLocation(UIVector2u shiftLocation) {
		this.shiftLocation = shiftLocation;
	}

	public void addToShiftLocation(UIControl control) {
		sizeOfAnotherElements.add(control);
	}

	public void addToShiftLocation(UIControl control, UIPoint point) {
		sizeOfAnotherElements.add(control);
		sizeOfAnotherElements.addAll(point.sizeOfAnotherElements);
	}

	public void removeFromShiftLocation(UIControl control) {
		sizeOfAnotherElements.remove(control);
	}

	public void clearAnotherSizes() {
		sizeOfAnotherElements.clear();
	}

	public UIVector2u calculateLocationWithCacheLevel(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cacheSizes) {
		final UIVector2u location = this.shiftLocation.calculate(parentLayoutSize);
		for (final UIControl element : this.sizeOfAnotherElements) {
			final UIVector2u calculatedSize = element.getSizeWithoutPadding(parentLayoutSize, cacheSizes);

			switch (element.getAxis()) {
				case X -> location.add(calculatedSize.x.get(), 0).add(Objects.requireNonNull(element.getPadding()).getWidthIndentation(), 0);
				case Y -> location.add(0, calculatedSize.y.get()).add(0, Objects.requireNonNull(element.getPadding()).getHeightIndentation());
				case Both -> location.add(calculatedSize).add(Objects.requireNonNull(element.getPadding()));
			}
		}
		return location;
	}

	@Override
	public String toString() {
		StringBuilder sizesOfAnotherElementsStringBuilder = new StringBuilder();
		for (UIControl element : sizeOfAnotherElements) {
			sizesOfAnotherElementsStringBuilder.append("\nIndentation: ").append(element.getPadding()).append(" Size: ").append(element.getSize());
		}
		return shiftLocation.toString() + "\n" + "Another sizes:" + sizesOfAnotherElementsStringBuilder.toString() + "\n";
	}
}
