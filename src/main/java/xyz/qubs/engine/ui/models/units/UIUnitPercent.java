package xyz.qubs.engine.ui.models.units;

public class UIUnitPercent extends UnitBase implements IUnit {
	public UIUnitPercent(int value) {
		super(value);
	}

	public IUnit cloneUnit() {
		return new UIUnitPercent(value);
	}

	public int calculate(int parentSize) {
		return parentSize * value / 100;
	}

	@Override
	public String toString() {
		return value + "%";
	}
}
