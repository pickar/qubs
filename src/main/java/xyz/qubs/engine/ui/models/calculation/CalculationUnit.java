package xyz.qubs.engine.ui.models.calculation;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import xyz.qubs.engine.Window;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.properties.UIVector2u;

@Builder
@ToString
public final class CalculationUnit {
	public static final UIVector2u screenSize = new UIVector2u(Window.width, Window.height);
	public static final UIVector2u screenLocation = new UIVector2u(0, 0);
	private @Getter final UIVector2u parentLocation;
	private @Getter final UIVector2u parentSize;
	private @Getter final UIControl control;
}
