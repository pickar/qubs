package xyz.qubs.engine.ui.elements.base;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.properties.UIVector2u;
import xyz.qubs.engine.vulkan.models.VulkanImage;

import java.awt.image.BufferedImage;

public class Image extends UIControl {
	@Override
	public BufferedImage drawElementTexture(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedSizes) {
		return null;
	}

	@Override
	public UIVector2u getAutoSize() {
		VulkanImage texture = this.getTexture();
		return new UIVector2u(texture.width, texture.height);
	}

	public Image setImageWithoutMark(VulkanImage image) {
		refreshTextureWithoutMark(image);
		return this;
	}

	public Image setImage(String imagePath) {
		refreshTexture(Vulkan.createTextureFromPath(imagePath));
		return this;
	}

	public Image setImage(BufferedImage image) {
		refreshTexture(image);
		return this;
	}

	public Image setImage(VulkanImage image) {
		refreshTexture(image);
		return this;
	}
}
