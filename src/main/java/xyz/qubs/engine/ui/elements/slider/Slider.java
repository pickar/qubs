package xyz.qubs.engine.ui.elements.slider;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.controls.UIElement;
import xyz.qubs.engine.ui.models.properties.UIVector2u;

import java.awt.image.BufferedImage;

public class Slider extends UIElement {
	public Slider() {
		super("slider.mxaml");
		onMouseClick = (control, mouseClickEvent) -> {
			sliderMouseClick();
		};
		onMouseDrag = (control, mouseDragEvent) -> {
			sliderMouseDrag();
		};
	}

	private void sliderMouseClick() {

	}

	public void sliderMouseDrag() {

	}

	@Override
	protected BufferedImage drawElementTexture(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedSizes) {
		return null;
	}

	@Override
	public UIVector2u getAutoSize() {
		return null;
	}
}
