package xyz.qubs.engine.ui.elements.base;

import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import lombok.Getter;
import xyz.qubs.engine.ui.fonts.UIFont;
import xyz.qubs.engine.ui.fonts.UIFontRegistry;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.properties.UIVector2u;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class StaticLabel extends UIControl {
	@Getter protected String text = "[NONE]";
	protected UIFont font = UIFontRegistry.Consolas;
	protected @Getter int charPadding = 2;

	public StaticLabel() {
		setText(text);
	}

	public StaticLabel setFont(UIFont font) {
		this.font = font;
		isTextureDirty = true;

		return this;
	}

	public StaticLabel setText(String text) {
		this.text = text;
		isTextureDirty = true;

		return this;
	}

	public StaticLabel setCharPadding(int charPadding) {
		this.charPadding = charPadding;
		isTextureDirty = true;

		return this;
	}

	@Override
	protected BufferedImage drawElementTexture(UIVector2u parentLayoutSize, Object2ObjectOpenHashMap<UIControl, UIVector2u> cachedSizes) {
		BufferedImage texture = createImageForDrawing(parentLayoutSize, cachedSizes);
		Graphics2D graphics = texture.createGraphics();
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		graphics.setFont(font.getFont());
		FontMetrics fontMetrics = graphics.getFontMetrics();
		graphics.setColor(Color.black);

		int startX = 1;
		int startY = fontMetrics.getAscent() + 1;
		for (char c : text.toCharArray()) {
			graphics.drawString(Character.toString(c), startX, startY);
			startX += fontMetrics.charWidth(c) + charPadding;
		}

		startX = 0;
		startY -= 1;
		graphics.setColor(Color.white);
		for (char c : text.toCharArray()) {
			graphics.drawString(Character.toString(c), startX, startY);
			startX += fontMetrics.charWidth(c) + charPadding;
		}

		graphics.dispose();
		return texture;

	}

	@Override
	protected UIVector2u getAutoSize() {
		final Rectangle2D bounds = font.getFont().getStringBounds(text, UIFont.frc);
		return new UIVector2u((int) Math.ceil(bounds.getWidth()) + (text.length() * charPadding), (int) Math.ceil(bounds.getHeight()));
	}
}
