package xyz.qubs.engine.ui.elements.base;

import lombok.Getter;
import xyz.qubs.engine.ui.fonts.UIFont;
import xyz.qubs.engine.ui.fonts.UIFontRegistry;
import xyz.qubs.engine.ui.layouts.StackLayout;
import xyz.qubs.engine.ui.models.properties.UIColor;
import xyz.qubs.engine.ui.models.properties.UIVector2u;

import java.awt.geom.Rectangle2D;

public class Label extends StackLayout {
	protected @Getter String text = "[NONE]";
	protected UIFont font = UIFontRegistry.Consolas;
	protected @Getter int charPadding = 0;

	public Label() {
		setText(text);
	}

	public Label setFont(UIFont font) {
		this.font = font;
		return setText(text);
	}

	public Label setText(String text) {
		removeAllControls();
		for (char character : text.toCharArray()) {
			super.addControl(new Image().setImageWithoutMark(this.font.getCharacter(character)).setBackground(UIColor.getRandomColor()));
		}
		this.text = text;
		return this;
	}

	public Label setCharPadding(int charPadding) {
		this.charPadding = charPadding;
		// TODO: recalculate controls position
		return this;
	}

	@Override
	public UIVector2u getAutoSize() {
		final Rectangle2D bounds = font.getFont().getStringBounds(text, UIFont.frc);
		return new UIVector2u((int) Math.ceil(bounds.getWidth()) + (text.length() * charPadding), (int) Math.ceil(bounds.getHeight()));
	}
}
