package xyz.qubs.engine.ui.utils;

import lombok.experimental.UtilityClass;
import xyz.qubs.engine.ui.elements.base.Image;
import xyz.qubs.engine.ui.elements.base.Label;
import xyz.qubs.engine.ui.elements.base.Rectangle;
import xyz.qubs.engine.ui.elements.slider.Slider;
import xyz.qubs.engine.ui.layouts.AbsoluteLayout;
import xyz.qubs.engine.ui.layouts.GridLayout;
import xyz.qubs.engine.ui.layouts.StackLayout;

import java.util.HashMap;

@UtilityClass
public class Parser {
	public static HashMap<String, Class> uiElementsHashMap = new HashMap<>();
	public static HashMap<String, Class> uiLayoutsHashMap = new HashMap<>();

	static {
		uiLayoutsHashMap.put("GridLayout", GridLayout.class);
		uiLayoutsHashMap.put("StackLayout", StackLayout.class);
		uiLayoutsHashMap.put("AbsoluteLayout", AbsoluteLayout.class);

		uiElementsHashMap.put("Label", Label.class);
		uiElementsHashMap.put("Image", Image.class);
		uiElementsHashMap.put("Rectangle", Rectangle.class);
		uiElementsHashMap.put("Slider", Slider.class);
	}

	/*private final HashMap<String, UIControl> identifiedElements;

	public Parser(HashMap<String, UIControl> identifiedElements, HashMap<String, UILayout> layouts) {
		this.identifiedElements = identifiedElements;
		this.identifiedLayouts = layouts;
	}

	public UIControl parseLayout(String input) {
		Pattern pattern = Pattern.compile("<.*?>");
		Matcher matcher = pattern.matcher(input);

		while (matcher.find()) {
			String tag = matcher.group();
			try {
				parseTag(tag);
			} catch (Exception ex) {
				System.out.println("Error in tag: " + tag + ". Exception(" + matcher.regionStart() + "): " + ex.getMessage());
			}
		}

		Iterator<UILayout> iterator = identifiedLayouts.values().iterator();
		return iterator.hasNext() ? iterator.next() : null;
	}

	private void parseTag(String tag) {
		int openedQuotationTag = 0;

		for (char symbol : tag.toCharArray()) {

		}

		// Field[] fields = typeOfLayout.getFields();


	}*/
}
