package xyz.qubs.engine.ui.animation.translate.linear;

import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.ui.animation.IAnimation;
import xyz.qubs.engine.ui.animation.IAnimationFabric;

public class LinearTranslateFabric implements IAnimationFabric {
	private final NamespacedName registryName;

	public LinearTranslateFabric(NamespacedName registryName) {
		this.registryName = registryName;
	}

	@Override
	public IAnimation generate() {
		return new LinearTranslate();
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}
}
