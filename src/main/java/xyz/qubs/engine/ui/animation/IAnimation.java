package xyz.qubs.engine.ui.animation;

import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.ui.events.FrameEvent;

public interface IAnimation {
	@SubscribeEvent
	void animationFrame(FrameEvent event);

	void animate();
}
