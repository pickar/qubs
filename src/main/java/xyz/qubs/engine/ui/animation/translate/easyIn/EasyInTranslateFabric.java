package xyz.qubs.engine.ui.animation.translate.easyIn;

import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.ui.animation.IAnimation;
import xyz.qubs.engine.ui.animation.IAnimationFabric;
import xyz.qubs.engine.ui.animation.translate.linear.LinearTranslate;

public class EasyInTranslateFabric implements IAnimationFabric {
	private final NamespacedName registryName;

	public EasyInTranslateFabric(NamespacedName registryName) {
		this.registryName = registryName;
	}

	@Override
	public IAnimation generate() {
		return new LinearTranslate();
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}
}
