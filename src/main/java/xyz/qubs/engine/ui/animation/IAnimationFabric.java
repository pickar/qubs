package xyz.qubs.engine.ui.animation;

import xyz.qubs.engine.registry.IRegistryEntry;

public interface IAnimationFabric extends IRegistryEntry {
	IAnimation generate();
}
