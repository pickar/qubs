package xyz.qubs.engine.ui.animation.translate.easyIn;

import xyz.qubs.engine.event.EventManager;
import xyz.qubs.engine.ui.animation.IAnimation;
import xyz.qubs.engine.ui.events.FrameEvent;

public class EaseInTranslate implements IAnimation {
	private final boolean isParametersSet = false;

	@Override
	public void animationFrame(FrameEvent event) {

	}

	@Override
	public void animate() {
		if (isParametersSet) {
			EventManager.registerListeners(this);
		}
	}
}
