package xyz.qubs.engine.ui.animation.translate.linear;

import xyz.qubs.engine.event.EventManager;
import xyz.qubs.engine.ui.animation.IAnimation;
import xyz.qubs.engine.ui.events.FrameEvent;
import xyz.qubs.engine.ui.models.controls.UIControl;
import xyz.qubs.engine.ui.models.properties.UIVector2u;

public class LinearTranslate implements IAnimation {
	private UIControl control;
	private UIVector2u endLocation;
	private int time;
	private boolean isParametersSet = false;

	@Override
	public void animationFrame(FrameEvent event) {

	}

	@Override
	public void animate() {
		if (isParametersSet) {
			EventManager.registerListeners(this);
		}
	}

	public LinearTranslate set(UIControl control, UIVector2u endLocation, int time) {
		this.control = control;
		this.endLocation = endLocation;
		this.time = time;
		isParametersSet = true;

		return this;
	}
}
