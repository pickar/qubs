package xyz.qubs.engine.ui.animation;

import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.registry.RegisterEntriesEvent;
import xyz.qubs.engine.registry.Registry;
import xyz.qubs.engine.registry.RegistryCreationEvent;
import xyz.qubs.engine.ui.animation.translate.easyIn.EasyInTranslateFabric;
import xyz.qubs.engine.ui.animation.translate.linear.LinearTranslateFabric;

import java.lang.reflect.Type;

public class AnimationRegistry extends Registry<IAnimationFabric> {
	public static final IAnimationFabric EASE_IN_FABRIC = new EasyInTranslateFabric(new NamespacedName("core", "translate/ease-in"));
	public static final IAnimationFabric LINEAR_FABRIC = new LinearTranslateFabric(new NamespacedName("core", "translate/linear"));
	private static final AnimationRegistry INSTANCE = new AnimationRegistry(new NamespacedName("core", "animation"), IAnimationFabric.class);

	public AnimationRegistry(NamespacedName registryName, Type type) {
		super(registryName, type);
	}

	public static IAnimationFabric getIAnimationByName(NamespacedName registryName) {
		return INSTANCE.getElement(registryName);
	}

	public static IAnimationFabric getIAnimationByName(String fullName) {
		return INSTANCE.getElement(fullName);
	}

	@SubscribeEvent
	public static void onRegistryCreation(RegistryCreationEvent event) {
		event.registry.register(INSTANCE);
	}

	@SubscribeEvent
	public static void onRegisterEntriesIAnimation(RegisterEntriesEvent<IAnimationFabric> event) {
		event.registry.register(EASE_IN_FABRIC);
		event.registry.register(LINEAR_FABRIC);
	}
}
