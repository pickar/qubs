package xyz.qubs.engine;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import it.unimi.dsi.fastutil.objects.ObjectLists;
import org.joml.Math;
import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.util.vma.VmaAllocationCreateInfo;
import org.lwjgl.util.vma.VmaAllocatorCreateInfo;
import org.lwjgl.util.vma.VmaVulkanFunctions;
import org.lwjgl.vulkan.*;
import xyz.qubs.Engine;
import xyz.qubs.engine.data.ExtendedDataRegistry;
import xyz.qubs.engine.event.EventManager;
import xyz.qubs.engine.event.events.WindowResizedEvent;
import xyz.qubs.engine.logger.Logger;
import xyz.qubs.engine.model.TexturedQuad;
import xyz.qubs.engine.registry.BaseRegistry;
import xyz.qubs.engine.settings.SettingsManager;
import xyz.qubs.engine.ui.fonts.UIFontRegistry;
import xyz.qubs.engine.utils.PriorityLock;
import xyz.qubs.engine.vulkan.VulkanException;
import xyz.qubs.engine.vulkan.VulkanUtils;
import xyz.qubs.engine.vulkan.models.VulkanBuffer;
import xyz.qubs.engine.vulkan.models.VulkanImage;
import xyz.qubs.engine.vulkan.models.VulkanPipeline;
import xyz.qubs.engine.vulkan.models.VulkanUIRenderObject;
import xyz.qubs.engine.vulkan.renderers.AdditionalRenderer;
import xyz.qubs.engine.vulkan.renderers.Renderer;
import xyz.qubs.engine.vulkan.renderers.UIRenderer;
import xyz.qubs.engine.vulkan.renderers.WorldRenderer;
import xyz.qubs.engine.vulkan.vertices.IVertex;
import xyz.qubs.engine.vulkan.vertices.VertexInfo;
import xyz.qubs.game.block.BlockTypeRegistry;
import xyz.qubs.game.block.TextureRegistry;
import xyz.qubs.game.collision.physics.PhysicsTypeRegistry;
import xyz.qubs.game.world.World;
import xyz.qubs.game.world.biome.BiomeRegistry;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

import static org.lwjgl.glfw.GLFW.glfwGetFramebufferSize;
import static org.lwjgl.glfw.GLFW.glfwWaitEvents;
import static org.lwjgl.glfw.GLFWVulkan.*;
import static org.lwjgl.stb.STBImage.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.util.vma.Vma.*;
import static org.lwjgl.vulkan.EXTDebugUtils.*;
import static org.lwjgl.vulkan.EXTValidationFeatures.*;
import static org.lwjgl.vulkan.KHRSurface.*;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.VK10.*;
import static xyz.qubs.Engine.world;
import static xyz.qubs.engine.settings.SettingsManager.FOV;
import static xyz.qubs.engine.settings.SettingsManager.debugMode;
import static xyz.qubs.engine.vulkan.VulkanUtils.*;

public class Vulkan {
	private static final long UINT64_MAX = 0xFFFFFFFFFFFFFFFFL;
	private static final int MAX_FRAMES_IN_FLIGHT = 1;
	private static final ObjectList<VulkanBuffer> disposalQueue = ObjectLists.synchronize(new ObjectArrayList<>());
	private static final LongBuffer waitSemaphore = BufferUtils.createLongBuffer(1); //stack.longs(imageAvailableSemaphores[currentFrame]);
	private static final IntBuffer waitStages = BufferUtils.createIntBuffer(1); //stack.ints(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
	private static final PointerBuffer commandBuf = BufferUtils.createPointerBuffer(1); //stack.pointers(scData[imageIndex].commandBuffer.address());
	private static final LongBuffer signalSemaphore = BufferUtils.createLongBuffer(1); //stack.longs(renderFinishedSemaphores[currentFrame]);
	private static final VkSubmitInfo submitInfo = VkSubmitInfo.calloc()
			.sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
			.waitSemaphoreCount(1);
	private static final ObjectList<Renderer> renderers = new ObjectArrayList<>();
	private static final ByteBuffer[] validationLayers = {
			memUTF8("VK_LAYER_KHRONOS_validation")
	};
	private static final PointerBuffer memoryPointer = memAllocPointer(1);
	public static long vmaAllocator;
	public static VkPhysicalDevice physicalDevice;
	public static VkExtent2D extent;
	public static int extentWidth;
	public static int extentHeight;
	public static VkDevice device;
	public static long transferCommandPool;
	public static List<Pair<Integer, Long>> transferCommandPools;
	public static volatile boolean disposed = false;
	public static int scImageCount = -1;
	public static UniformBufferObject ubo = new UniformBufferObject(); //TODO: remove this
	public static ObjectList<VulkanImage> imageDisposalQueue = ObjectLists.synchronize(new ObjectArrayList<>());
	private static SwapChainData[] scData;
	private static int imageIndex;
	private static int currentFrame = 0;
	private static VkInstance instance;
	private static VkSurfaceFormatKHR surfaceFormat;
	private static VkPhysicalDeviceMemoryProperties memoryProperties;
	private static VulkanImage depth;
	private static long[] imageAvailableSemaphores;
	private static long[] renderFinishedSemaphores;
	private static long[] inFlightFences;
	private static long[] transferFences;
	private static long[] imagesInFlight;
	private static long renderPass;
	private static long swapChain;
	private static long drawCommandPool;
	private static long debugMessengerHandle;
	private static long surfaceHandle;
	private static boolean framebufferResized = false;
	private static VkDebugUtilsMessengerCallbackEXT debugMessengerCallback = null;
	private static VkRect2D renderArea;
	private static VkQueues queues;

	static {
		System.setProperty("vulkan.validation", Boolean.toString(debugMode));
	}

	private Vulkan() {
	}

	public static Pair<Integer, Long> getFreeCommandPool() {
		return transferCommandPools.remove(0);
	}

	public static void returnCommandPool(Pair<Integer, Long> pair) {
		transferCommandPools.add(pair);
	}

	public static void markForDisposal(VulkanBuffer buffer) {
		if (!disposalQueue.contains(buffer)) {
			disposalQueue.add(buffer);
		}
	}

	public static void disposeBuffers() {
		for (VulkanBuffer buffer : disposalQueue) {
			buffer.free();
			disposalQueue.remove(buffer);
		}
	}

	private static void disposeImages() {
		for (VulkanImage image : imageDisposalQueue) {
			image.free();
			imageDisposalQueue.remove(image);
		}
	}

	public static void framebufferResized() {
		framebufferResized = true;
	}

	public static void init() {
		if (!glfwVulkanSupported()) {
			throw new VulkanException("GLFW failed to find the Vulkan loader");
		}

		VertexInfo.init();

		createInstance();
		setupDebugging();
		createSurface();
		createPhysicalDevice(1);
		VulkanUtils.init();
		createLogicalDeviceAndQueueFamily();
//		printComputerInfo();
		createAllocator();
		createDeviceQueue();

		transferCommandPool = createCommandPool(VK_COMMAND_POOL_CREATE_TRANSIENT_BIT, queues.graphics.index);
		drawCommandPool = createCommandPool(VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, queues.graphics.index);
		transferCommandPools = new CopyOnWriteArrayList<>();
		for (int i = 0; i < 8; i++) {
			transferCommandPools.add(new Pair<>(i, createCommandPool(VK_COMMAND_POOL_CREATE_TRANSIENT_BIT, queues.transfer.index)));
		}
		renderers.add(WorldRenderer.INSTANCE);
		renderers.add(AdditionalRenderer.INSTANCE);
		renderers.add(UIRenderer.INSTANCE);

		transferFences = new long[transferCommandPools.size() + 1];

		VkFenceCreateInfo fenceInfo = VkFenceCreateInfo.calloc()
				.sType(VK_STRUCTURE_TYPE_FENCE_CREATE_INFO)
				.flags(VK_FENCE_CREATE_SIGNALED_BIT);

		LongBuffer longBuffer = memAllocLong(1);
		for (int i = 0; i < transferFences.length; i++) {
			VkCheck(vkCreateFence(device, fenceInfo, null, longBuffer), "Failed to create semaphore");
			transferFences[i] = longBuffer.get(0);
		}
		fenceInfo.free();
		memFree(longBuffer);
		waitStages.put(0, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);

		EventManager.registerListeners(PhysicsTypeRegistry.class);
		EventManager.registerListeners(BiomeRegistry.class);
		EventManager.registerListeners(BlockTypeRegistry.class);
		EventManager.registerListeners(ExtendedDataRegistry.class);
		EventManager.registerListeners(UIFontRegistry.class);
		BaseRegistry.init();
		TextureRegistry.init();

		world = new World();

		recreateSwapChain(true);
		createSyncObjects();
	}

	private static void printComputerInfo() {
//		try (MemoryStack stack = stackPush()) {
//			VkPhysicalDeviceProperties deviceProperties = VkPhysicalDeviceProperties.callocStack(stack);
//			VkPhysicalDeviceFeatures deviceFeatures = VkPhysicalDeviceFeatures.callocStack(stack);

//			vkGetPhysicalDeviceProperties(physicalDevice, deviceProperties);
//			vkGetPhysicalDeviceFeatures(physicalDevice, deviceFeatures);

//			SystemInfo systemInfo = new SystemInfo();
//			System.out.println(systemInfo.getOperatingSystem());
//			System.out.println(deviceProperties.deviceNameString());
//			System.out.println(deviceProperties.limits().maxPerStageDescriptorSampledImages());
//			System.out.println(deviceProperties.limits().maxDescriptorSetSampledImages());
//			System.out.println(systemInfo.getHardware().getProcessor());
//			System.out.println(systemInfo.getHardware().getMemory());
//			System.out.println(Arrays.toString(systemInfo.getHardware().getDisplays()));
//		}
	}

	public static VulkanImage createTextureFromPath(String texturePath) {
		texturePath = SettingsManager.resourcePath + "textures/" + texturePath;
		ByteBuffer imageBuffer;
		try {
			imageBuffer = ioResourceToByteBuffer(texturePath, 8 * 1024);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		VulkanImage texture = createTextureImage(loadImageFromMemory(imageBuffer));
		texture.imageView = createImageView(texture.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
		return texture;
	}

	public static VulkanImage createTextureFromBytes(ByteBuffer data) {
		VulkanImage texture = createTextureImage(loadImageFromMemory(data));
		texture.imageView = createImageView(texture.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
		return texture;
	}

	private static void createDepthResources() {
		int depthFormat = findDepthFormat();

		depth = createImage(extentWidth, extentHeight, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VMA_MEMORY_USAGE_GPU_ONLY);
		depth.imageView = createImageView(depth.image, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);

		transitionImageLayout(depth.image, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
	}

	private static boolean hasStencilComponent(int format) {
		return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
	}

	private static int findDepthFormat() {
		return findSupportedFormat(
				new int[] { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
				VK_IMAGE_TILING_OPTIMAL,
				VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
		);
	}

	private static int findSupportedFormat(int[] candidates, int tiling, int features) {
		try (MemoryStack stack = stackPush()) {
			for (int format : candidates) {
				VkFormatProperties props = VkFormatProperties.callocStack(stack);
				vkGetPhysicalDeviceFormatProperties(physicalDevice, format, props);
				if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures() & features) == features) {
					return format;
				} else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures() & features) == features) {
					return format;
				}
			}
		}
		throw new VulkanException("Failed to find supported format");
	}

	private static long createImageView(long image, int format, int aspectFlags) {
		try (MemoryStack stack = stackPush()) {
			LongBuffer longBuffer = stack.mallocLong(1);
			VkImageViewCreateInfo imageViewCreateInfo = VkImageViewCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO)
					.image(image)
					.viewType(VK_IMAGE_VIEW_TYPE_2D)
					.format(format)
					.subresourceRange(VkImageSubresourceRange.callocStack(stack).set(aspectFlags, 0, 1, 0, 1));

			VkCheck(vkCreateImageView(device, imageViewCreateInfo, null, longBuffer), "Failed to create texture image view");
			return longBuffer.get(0);
		}
	}

	private static void copyBufferToImage(long buffer, long image, int width, int height) {
		try (MemoryStack stack = stackPush()) {
			VkBufferImageCopy.Buffer region = VkBufferImageCopy.callocStack(1, stack)
					.bufferOffset(0)
					.bufferRowLength(0)
					.bufferImageHeight(0)
					.imageSubresource(VkImageSubresourceLayers.callocStack(stack).set(VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1))
					.imageOffset(VkOffset3D.callocStack(stack).set(0, 0, 0))
					.imageExtent(VkExtent3D.callocStack(stack).set(width, height, 1));

			VkCommandBuffer commandBuffer = beginSingleTimeCommands();
			vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, region);
			endSingleTimeCommands(commandBuffer);
		}
	}

	private static void transitionImageLayout(long image, int format, int oldLayout, int newLayout) {
		try (MemoryStack stack = stackPush()) {
			VkImageMemoryBarrier.Buffer barrier = VkImageMemoryBarrier.callocStack(1, stack)
					.sType(VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
					.oldLayout(oldLayout)
					.newLayout(newLayout)
					.srcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
					.dstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
					.image(image)
					.subresourceRange(sr -> {
						sr.aspectMask(VK_IMAGE_ASPECT_COLOR_BIT);
						sr.baseMipLevel(0);
						sr.levelCount(1);
						sr.baseArrayLayer(0);
						sr.layerCount(1);
					})
					.srcAccessMask(0)
					.dstAccessMask(0);

			int sourceStage;
			int destinationStage;

			if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
				barrier.subresourceRange().aspectMask(VK_IMAGE_ASPECT_DEPTH_BIT);

				if (hasStencilComponent(format)) {
					barrier.subresourceRange().aspectMask(barrier.subresourceRange().aspectMask() | VK_IMAGE_ASPECT_STENCIL_BIT);
				}
			} else {
				barrier.subresourceRange().aspectMask(VK_IMAGE_ASPECT_COLOR_BIT);
			}

			if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
				barrier.srcAccessMask(0);
				barrier.dstAccessMask(VK_ACCESS_TRANSFER_WRITE_BIT);

				sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
				destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			} else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
				barrier.srcAccessMask(VK_ACCESS_TRANSFER_WRITE_BIT);
				barrier.dstAccessMask(VK_ACCESS_SHADER_READ_BIT);

				sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
				destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			} else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
				barrier.srcAccessMask(0);
				barrier.dstAccessMask(VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);

				sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
				destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
			} else {
				throw new IllegalArgumentException("unsupported layout transition!");
			}

			VkCommandBuffer commandBuffer = beginSingleTimeCommands();
			vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, VK_DEPENDENCY_BY_REGION_BIT, null, null, barrier);
			endSingleTimeCommands(commandBuffer);
		}
	}

	public static VulkanBuffer createBuffer(int size, int usage, int memoryUsage) {
		VulkanBuffer buffer = new VulkanBuffer();
		try (MemoryStack stack = stackPush()) {
			VkBufferCreateInfo bufferInfo = VkBufferCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO)
					.size(size)
					.usage(usage)
					.sharingMode(VK_SHARING_MODE_EXCLUSIVE);

			LongBuffer longBuffer = stack.mallocLong(1);
			PointerBuffer pointerBuffer = stack.mallocPointer(1);

			VkCheck(vmaCreateBuffer(vmaAllocator, bufferInfo, VmaAllocationCreateInfo.callocStack(stack).usage(memoryUsage), longBuffer, pointerBuffer, null), "Failed to allocate buffer");

			buffer.buffer = longBuffer.get(0);
			buffer.allocation = pointerBuffer.get(0);
		}

		return buffer;
	}

	public static VkCommandBuffer beginSingleTimeCommands(long commandPoolHandle) {
		try (MemoryStack stack = stackPush()) {
			PointerBuffer pointerBuf = stack.mallocPointer(1);
			VkCommandBufferAllocateInfo allocateInfo = VkCommandBufferAllocateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
					.level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
					.commandPool(commandPoolHandle)
					.commandBufferCount(1);

			vkAllocateCommandBuffers(device, allocateInfo, pointerBuf);
			VkCommandBuffer commandBuffer = new VkCommandBuffer(pointerBuf.get(0), device);

			VkCommandBufferBeginInfo beginInfo = VkCommandBufferBeginInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
					.flags(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
			vkBeginCommandBuffer(commandBuffer, beginInfo);

			return commandBuffer;
		}
	}

	public static void endSingleTimeCommands(VkCommandBuffer commandBuffer, Pair<Integer, Long> pair) {
		try (MemoryStack stack = stackPush()) {
			vkEndCommandBuffer(commandBuffer);

			VkSubmitInfo submitInfo = VkSubmitInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
					.pCommandBuffers(stack.pointers(commandBuffer.address()));

			vkResetFences(device, transferFences[pair.getKey()]);
			try {
				queues.transfer.lock.acquire();
				vkQueueSubmit(queues.transfer.queue, submitInfo, transferFences[pair.getKey()]);
			} catch (InterruptedException ignored) {
			} finally {
				queues.transfer.lock.release();
			}
			vkWaitForFences(device, transferFences[pair.getKey()], true, UINT64_MAX);
			vkFreeCommandBuffers(device, pair.getValue(), commandBuffer);
		}
	}

	public static VkCommandBuffer beginSingleTimeCommands() {
		try (MemoryStack stack = stackPush()) {
			PointerBuffer pointerBuf = stack.mallocPointer(1);
			VkCommandBufferAllocateInfo allocateInfo = VkCommandBufferAllocateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
					.level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
					.commandPool(transferCommandPool)
					.commandBufferCount(1);

			vkAllocateCommandBuffers(device, allocateInfo, pointerBuf);
			VkCommandBuffer commandBuffer = new VkCommandBuffer(pointerBuf.get(0), device);

			VkCommandBufferBeginInfo beginInfo = VkCommandBufferBeginInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
					.flags(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
			vkBeginCommandBuffer(commandBuffer, beginInfo);

			return commandBuffer;
		}
	}

	public synchronized static void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
		try (MemoryStack stack = stackPush()) {
			vkEndCommandBuffer(commandBuffer);

			VkSubmitInfo submitInfo = VkSubmitInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
					.pCommandBuffers(stack.pointers(commandBuffer.address()));

			vkResetFences(device, transferFences[transferFences.length - 1]);
			try {
				queues.graphics.lock.acquire();
				vkQueueSubmit(queues.graphics.queue, submitInfo, transferFences[transferFences.length - 1]);
			} catch (InterruptedException ignored) {
			} finally {
				queues.graphics.lock.release();
			}
			vkWaitForFences(device, transferFences[transferFences.length - 1], true, UINT64_MAX);
			vkFreeCommandBuffers(device, transferCommandPool, commandBuffer);
		}
	}

	public static void copyBuffer(VulkanBuffer srcBuffer, VulkanBuffer dstBuffer, int size) {
		try (MemoryStack stack = stackPush()) {
			Pair<Integer, Long> pair = getFreeCommandPool();
			VkCommandBuffer commandBuffer = beginSingleTimeCommands(pair.getValue());

			VkBufferCopy.Buffer copyRegion = VkBufferCopy.callocStack(1, stack).size(size);
			vkCmdCopyBuffer(commandBuffer, srcBuffer.buffer, dstBuffer.buffer, copyRegion);

			endSingleTimeCommands(commandBuffer, pair);
			returnCommandPool(pair);
		}
	}

	private static BufferedImage loadImageFromMemory(ByteBuffer data) {
		BufferedImage image = new BufferedImage();
		try (MemoryStack stack = stackPush()) {
			IntBuffer texWidth = stack.mallocInt(1);
			IntBuffer texHeight = stack.mallocInt(1);
			IntBuffer texChannels = stack.mallocInt(1);

			image.buffer = stbi_load_from_memory(data, texWidth, texHeight, texChannels, STBI_rgb_alpha);
			image.width = texWidth.get(0);
			image.height = texHeight.get(0);
		}
		return image;
	}

	public static BufferedImage loadImageFromPath(String texturePath) {
		String path = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource(texturePath)).getPath().substring(6);
		BufferedImage image = new BufferedImage();
		try (MemoryStack stack = stackPush()) {
			IntBuffer texWidth = stack.mallocInt(1);
			IntBuffer texHeight = stack.mallocInt(1);
			IntBuffer texChannels = stack.mallocInt(1);

			image.buffer = stbi_load(path, texWidth, texHeight, texChannels, STBI_rgb_alpha);
			image.width = texWidth.get(0);
			image.height = texHeight.get(0);
		}
		return image;
	}

	public static VulkanImage createTextureImage(BufferedImage image) {
		VulkanImage texture;

		if (image.buffer == null) {
			throw new RuntimeException("Failed to load texture image: " + stbi_failure_reason());
		}

		VulkanBuffer stagingBuffer = createBuffer(image.buffer.remaining(), VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

		copyMemory(memAddress(image.buffer), image.buffer.remaining(), stagingBuffer.allocation);

		stbi_image_free(image.buffer);
		texture = createImage(image.width, image.height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VMA_MEMORY_USAGE_GPU_ONLY);

		transitionImageLayout(texture.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		copyBufferToImage(stagingBuffer.buffer, texture.image, image.width, image.height);
		transitionImageLayout(texture.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		stagingBuffer.free();

		texture.width = image.width;
		texture.height = image.height;
		return texture;
	}

	private static VulkanImage createImage(int width, int height, int format, int tiling, int usage, int memoryUsage) {
		VulkanImage image = new VulkanImage();
		try (MemoryStack stack = stackPush()) {
			VkImageCreateInfo imageInfo = VkImageCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO)
					.imageType(VK_IMAGE_TYPE_2D)
					.extent(e -> {
						e.width(width);
						e.height(height);
						e.depth(1);
					})
					.mipLevels(1)
					.arrayLayers(1)
					.format(format)
					.tiling(tiling)
					.initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
					.usage(usage)
					.sharingMode(VK_SHARING_MODE_EXCLUSIVE)
					.samples(VK_SAMPLE_COUNT_1_BIT);

			LongBuffer longBuffer = stack.mallocLong(1);
			PointerBuffer pointerBuffer = stack.mallocPointer(1);

			VkCheck(vmaCreateImage(vmaAllocator, imageInfo, VmaAllocationCreateInfo.callocStack(stack).usage(memoryUsage), longBuffer, pointerBuffer, null), "Failed to create image");
			image.image = longBuffer.get(0);
			image.allocation = pointerBuffer.get(0);
		}
		return image;
	}

	public static VulkanBuffer[] createUniformBuffers(int bufferSize) {
		VulkanBuffer[] uniformBuffers = new VulkanBuffer[scImageCount];

		for (int i = 0; i < uniformBuffers.length; i++) {
			uniformBuffers[i] = createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
		}
		return uniformBuffers;
	}

	public static long createDescriptorSetLayout(VkDescriptorSetLayoutCreateInfo layoutInfo) {
		try (MemoryStack stack = stackPush()) {
			LongBuffer longBuffer = stack.mallocLong(1);
			VkCheck(vkCreateDescriptorSetLayout(device, layoutInfo, null, longBuffer), "Failed to create descriptor set layout");
			return longBuffer.get(0);
		}
	}

	public synchronized static void copyMemory(long from, int size, long allocation) {
//		try (MemoryStack stack = stackPush()) {
		VkCheck(vmaMapMemory(vmaAllocator, allocation, memoryPointer), "Unable to map memory");
		memCopy(from, memoryPointer.get(0), size);
		vmaUnmapMemory(vmaAllocator, allocation);
//		}
	}

	public static VulkanBuffer createIndexBuffer(int[] indices) {
		int bufferSize = 4 * indices.length;

		VulkanBuffer stagingBuffer = createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

		IntBuffer indicesBuffer = memAllocInt(indices.length);
		indicesBuffer.put(indices);
		indicesBuffer.flip();
		copyMemory(memAddress(indicesBuffer), bufferSize, stagingBuffer.allocation);

		VulkanBuffer indexBuffer = createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY);
		copyBuffer(stagingBuffer, indexBuffer, bufferSize);

		stagingBuffer.free();
		memFree(indicesBuffer);

		return indexBuffer;
	}

	private static void createAllocator() {
		try (MemoryStack stack = stackPush()) {
			VmaVulkanFunctions functions = VmaVulkanFunctions.callocStack(stack).set(instance, device);

			VmaAllocatorCreateInfo allocatorInfo = VmaAllocatorCreateInfo.callocStack(stack)
					.device(device)
					.physicalDevice(physicalDevice)
					.pVulkanFunctions(functions);

			if (device.getCapabilities().VK_KHR_dedicated_allocation) {
				allocatorInfo.flags(VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT);
			}
			PointerBuffer pointerBuffer = stack.mallocPointer(1);

			VkCheck(vmaCreateAllocator(allocatorInfo, pointerBuffer), "Unable to create VMA allocator");
			vmaAllocator = pointerBuffer.get(0);
		}
	}

	public static VulkanBuffer createVertexBuffer(IVertex[] vertices) {
		int bufferSize = vertices[0].getSize() * vertices.length;

		VulkanBuffer stagingBuffer = createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);

		ByteBuffer verticesBuffer = memAlloc(vertices[0].getSize() * vertices.length);
		for (IVertex vertex : vertices) {
			verticesBuffer.put(vertex.getBytes());
		}
		verticesBuffer.flip();

		copyMemory(memAddress(verticesBuffer), bufferSize, stagingBuffer.allocation);

		VulkanBuffer vertexBuffer = createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY);
		copyBuffer(stagingBuffer, vertexBuffer, bufferSize);

		stagingBuffer.free();

		memFree(verticesBuffer);
		return vertexBuffer;
	}

	public static void drawFrame(double interpolation) {
		try (MemoryStack stack = stackPush()) {
			vkWaitForFences(device, inFlightFences[currentFrame], true, UINT64_MAX);
			IntBuffer intBuffer = stack.mallocInt(1);

			int result = vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, intBuffer);
			imageIndex = intBuffer.get(0);

			if (result == VK_ERROR_OUT_OF_DATE_KHR) {
				recreateSwapChain(false);
				return;
			} else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
				throw new VulkanException("Failed to acquire swap chain image");
			}

			if (imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
				vkWaitForFences(device, imagesInFlight[imageIndex], true, UINT64_MAX);
			}
			imagesInFlight[imageIndex] = inFlightFences[currentFrame];

			disposeImages();
			for (int i = 0; i < renderers.size(); i++) {
				Renderer renderer = renderers.get(i);
				renderer.beforeFrame(imageIndex);

				if (renderer.isDirty()) {
					for (SwapChainData scDatum : scData) {
						vkResetCommandBuffer(scDatum.secondaryCommandBuffers[i], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
					}
					recordSecondaryCommandBuffers(i);
				}
			}
			for (SwapChainData scDatum : scData) {
				vkResetCommandBuffer(scDatum.commandBuffer, VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
			}
			recordPrimaryCommandBuffers();

//			waitSemaphore.put(0, imageAvailableSemaphores[currentFrame]);
//			commandBuf.put(0, scData[imageIndex].commandBuffer.address());
//			signalSemaphore.put(0, renderFinishedSemaphores[currentFrame]);

			submitInfo.pWaitSemaphores(stack.longs(imageAvailableSemaphores[currentFrame]))
					.waitSemaphoreCount(1)
					.pWaitDstStageMask(waitStages)
					.pCommandBuffers(stack.pointers(scData[imageIndex].commandBuffer.address()))
					.pSignalSemaphores(stack.longs(renderFinishedSemaphores[currentFrame]));

			vkResetFences(device, inFlightFences[currentFrame]);
			try {
				queues.graphics.lock.acquirePriority();
				VkCheck(vkQueueSubmit(queues.graphics.queue, submitInfo, inFlightFences[currentFrame]), "Failed to submit draw command buffer");
			} catch (InterruptedException ignored) {
			} finally {
				queues.graphics.lock.release();
			}

			VkPresentInfoKHR presentInfo = VkPresentInfoKHR.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_PRESENT_INFO_KHR)
					.pWaitSemaphores(stack.longs(renderFinishedSemaphores[currentFrame]))
					.swapchainCount(1)
					.pSwapchains(stack.longs(swapChain))
					.pImageIndices(stack.ints(imageIndex));

			result = vkQueuePresentKHR(queues.present.queue, presentInfo);

			for (Renderer renderer : renderers) {
				renderer.afterFrame(imageIndex);
			}

			if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized) {
				framebufferResized = false;
				recreateSwapChain(false);
			} else if (result != VK_SUCCESS) {
				throw new VulkanException("Failed to present swap chain image");
			}

			currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
		}
	}

	public static void fillUBO() {
		ubo.view = Engine.camera.getViewMatrix();
	}

	public static void waitIdle() {
		vkDeviceWaitIdle(device);
	}

	private static void createSyncObjects() {
		try (MemoryStack stack = stackPush()) {
			LongBuffer longBuf = stack.mallocLong(1);
			imageAvailableSemaphores = new long[MAX_FRAMES_IN_FLIGHT];
			renderFinishedSemaphores = new long[MAX_FRAMES_IN_FLIGHT];

			inFlightFences = new long[MAX_FRAMES_IN_FLIGHT];
			imagesInFlight = new long[scImageCount];

			VkSemaphoreCreateInfo semaphoreInfo = VkSemaphoreCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO);

			VkFenceCreateInfo fenceInfo = VkFenceCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_FENCE_CREATE_INFO)
					.flags(VK_FENCE_CREATE_SIGNALED_BIT);

			for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
				VkCheck(vkCreateSemaphore(device, semaphoreInfo, null, longBuf), "Failed to create semaphore");
				imageAvailableSemaphores[i] = longBuf.get(0);
				VkCheck(vkCreateSemaphore(device, semaphoreInfo, null, longBuf), "Failed to create semaphore");
				renderFinishedSemaphores[i] = longBuf.get(0);

				VkCheck(vkCreateFence(device, fenceInfo, null, longBuf), "Failed to create semaphore");
				inFlightFences[i] = longBuf.get(0);
			}
		}
	}

	public static VkCommandBuffer[] createCommandBuffers(int level) {
		try (MemoryStack stack = stackPush()) {
			VkCommandBuffer[] commandBuffers = new VkCommandBuffer[scImageCount];

			PointerBuffer pCommandBuffer = stack.mallocPointer(commandBuffers.length);

			VkCommandBufferAllocateInfo allocInfo = VkCommandBufferAllocateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
					.commandPool(drawCommandPool)
					.level(level)
					.commandBufferCount(commandBuffers.length);

			VkCheck(vkAllocateCommandBuffers(device, allocInfo, pCommandBuffer), "Failed to allocate command buffers");

			for (int i = 0; i < commandBuffers.length; i++) {
				commandBuffers[i] = new VkCommandBuffer(pCommandBuffer.get(i), device);
			}
			return commandBuffers;
		}
	}

	public static void recordPrimaryCommandBuffers() {
		try (MemoryStack stack = stackPush()) {
			VkCommandBufferBeginInfo beginInfo = VkCommandBufferBeginInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);
			VkClearValue.Buffer clearValues = VkClearValue.callocStack(2, stack);
			clearValues.get(0).color()
					.float32(0, 100 / 255.0f)
					.float32(1, 149 / 255.0f)
					.float32(2, 237 / 255.0f)
					.float32(3, 1.0f);
			clearValues.get(1).depthStencil()
					.set(1, 0);

			for (int i = 0; i < scImageCount; i++) {
				VkCommandBuffer commandBuffer = scData[i].commandBuffer;

				VkRenderPassBeginInfo renderPassInfo = VkRenderPassBeginInfo.callocStack(stack)
						.sType(VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO)
						.renderPass(renderPass)
						.framebuffer(scData[i].framebuffer)
						.renderArea(renderArea)
						.pClearValues(clearValues);

				VkCheck(vkBeginCommandBuffer(commandBuffer, beginInfo), "Failed to begin recording command buffer");
				vkCmdBeginRenderPass(commandBuffer, renderPassInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

				PointerBuffer pointerBuffer = stack.callocPointer(renderers.size());
				for (int k = 0; k < renderers.size(); k++) {
					pointerBuffer.put(k, scData[i].secondaryCommandBuffers[k].address());
				}

				vkCmdExecuteCommands(commandBuffer, pointerBuffer);

				vkCmdEndRenderPass(commandBuffer);

				VkCheck(vkEndCommandBuffer(commandBuffer), "Failed to record command buffer");
			}
		}
	}

	public static void recordSecondaryCommandBuffers(int index) {
		try (MemoryStack stack = stackPush()) {
			for (int i = 0; i < scImageCount; i++) {
				VkCommandBuffer commandBuffer = scData[i].secondaryCommandBuffers[index];

				VkCommandBufferInheritanceInfo inheritanceInfo = VkCommandBufferInheritanceInfo.callocStack(stack)
						.renderPass(renderPass)
						.framebuffer(scData[i].framebuffer)
						.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO);

				VkCommandBufferBeginInfo beginInfo = VkCommandBufferBeginInfo.callocStack(stack)
						.pInheritanceInfo(inheritanceInfo)
						.flags(VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT)
						.sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);

				VkCheck(vkBeginCommandBuffer(commandBuffer, beginInfo), "Failed to begin recording command buffer");

				renderers.get(index).prepareCommandBuffer(commandBuffer, imageIndex);

				VkCheck(vkEndCommandBuffer(commandBuffer), "Failed to record command buffer");
			}
		}
	}

	private static long createCommandPool(int flags, int familyIndex) {
		try (MemoryStack stack = stackPush()) {
			LongBuffer longBuf = stack.mallocLong(1);
			VkCommandPoolCreateInfo poolInfo = VkCommandPoolCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO)
					.flags(flags)
					.queueFamilyIndex(familyIndex);

			VkCheck(vkCreateCommandPool(device, poolInfo, null, longBuf), "Failed to create command pool");
			return longBuf.get(0);
		}
	}

	private static void createFramebuffers() {
		try (MemoryStack stack = stackPush()) {
			LongBuffer longBuf = stack.mallocLong(1);
			for (SwapChainData data : scData) {
				LongBuffer attachments = stack.longs(data.imageView, depth.imageView);

				VkFramebufferCreateInfo framebufferInfo = VkFramebufferCreateInfo.callocStack(stack)
						.sType(VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO)
						.renderPass(renderPass)
						.pAttachments(attachments)
						.width(extentWidth)
						.height(extentHeight)
						.layers(1);

				VkCheck(vkCreateFramebuffer(device, framebufferInfo, null, longBuf), "Failed to create framebuffer");
				data.framebuffer = longBuf.get(0);
			}
		}
	}

	private static void createRenderPass() {
		try (MemoryStack stack = stackPush()) {
			VkAttachmentDescription.Buffer attachments = VkAttachmentDescription.callocStack(2, stack);
			attachments.get(0)
					.format(surfaceFormat.format())
					.samples(VK_SAMPLE_COUNT_1_BIT)
					.loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR)
					.storeOp(VK_ATTACHMENT_STORE_OP_STORE)
					.stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
					.stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
					.initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
					.finalLayout(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
			attachments.get(1)
					.format(findDepthFormat())
					.samples(VK_SAMPLE_COUNT_1_BIT)
					.loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR)
					.storeOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
					.stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
					.stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
					.initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
					.finalLayout(VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

			VkAttachmentReference.Buffer colorAttachmentRef = VkAttachmentReference.callocStack(1, stack)
					.attachment(0)
					.layout(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

			VkAttachmentReference depthAttachmentRef = VkAttachmentReference.callocStack(stack)
					.attachment(1)
					.layout(VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

			VkSubpassDescription.Buffer subpass = VkSubpassDescription.callocStack(1, stack);
			subpass.get(0)
					.pipelineBindPoint(VK_PIPELINE_BIND_POINT_GRAPHICS)
					.colorAttachmentCount(1)
					.pColorAttachments(colorAttachmentRef)
					.pDepthStencilAttachment(depthAttachmentRef);

			VkSubpassDependency.Buffer dependency = VkSubpassDependency.callocStack(1, stack)
					.srcSubpass(VK_SUBPASS_EXTERNAL)
					.dstSubpass(0)
					.srcStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
					.srcAccessMask(0)
					.dstStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
					.dstAccessMask(VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

			VkRenderPassCreateInfo renderPassInfo = VkRenderPassCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO)
					.pAttachments(attachments)
					.pSubpasses(subpass)
					.pDependencies(dependency);

			LongBuffer longBuf = stack.mallocLong(1);
			VkCheck(vkCreateRenderPass(device, renderPassInfo, null, longBuf), "Failed to create render pass");
			renderPass = longBuf.get(0);
		}
	}

	public static long createShaderModuleCustom(String path, int shaderType, Function<String, String> function) {
		try (MemoryStack stack = stackPush()) {
			ByteBuffer code = glslToSPIRV(path, shaderType, function);
			LongBuffer longBuffer = stack.mallocLong(1);

			VkShaderModuleCreateInfo createInfo = VkShaderModuleCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO)
					.pCode(code);
			VkCheck(vkCreateShaderModule(device, createInfo, null, longBuffer), "Failed to create shader module " + path);
			return longBuffer.get(0);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static long createShaderModule(String path, int shaderType) {
		try (MemoryStack stack = stackPush()) {
			ByteBuffer code = glslToSPIRV(path, shaderType);
			LongBuffer longBuffer = stack.mallocLong(1);

			VkShaderModuleCreateInfo createInfo = VkShaderModuleCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO)
					.pCode(code);
			VkCheck(vkCreateShaderModule(device, createInfo, null, longBuffer), "Failed to create shader module " + path);
			return longBuffer.get(0);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static VkPipelineShaderStageCreateInfo.Buffer createShaderStages(long[] vertexShaderModules, long[] fragmentShaderModules) {
		ByteBuffer name = stackUTF8("main");

		VkPipelineShaderStageCreateInfo.Buffer shaderStages = VkPipelineShaderStageCreateInfo.callocStack(vertexShaderModules.length + fragmentShaderModules.length);
		for (int i = 0; i < vertexShaderModules.length; i++) {
			shaderStages.get(i)
					.sType(VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
					.stage(VK_SHADER_STAGE_VERTEX_BIT)
					.module(vertexShaderModules[i])
					.pName(name);
		}
		for (int i = 0; i < fragmentShaderModules.length; i++) {
			shaderStages.get(i + vertexShaderModules.length)
					.sType(VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
					.stage(VK_SHADER_STAGE_FRAGMENT_BIT)
					.module(fragmentShaderModules[i])
					.pName(name);
		}
		return shaderStages;
	}

	public static VkPipelineVertexInputStateCreateInfo createVertexInputInfo(IVertex vertex) {
		return VkPipelineVertexInputStateCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO)
				.pVertexAttributeDescriptions(vertex.getAttributeDescription())
				.pVertexBindingDescriptions(vertex.getBindingDescription());
	}

	public static VkPipelineInputAssemblyStateCreateInfo createInputAssembly(int topology) {
		return VkPipelineInputAssemblyStateCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO)
				.topology(topology)
				.primitiveRestartEnable(false);
	}

	public static VkPipelineViewportStateCreateInfo createViewportState() {
		VkViewport.Buffer viewport = VkViewport.callocStack(1)
				.x(0)
				.y(0)
				.width(extentWidth)
				.height(extentHeight)
				.minDepth(0)
				.maxDepth(1);

		VkOffset2D offset = VkOffset2D.callocStack().set(0, 0);
		VkRect2D.Buffer scissor = VkRect2D.callocStack(1)
				.offset(offset)
				.extent(extent);

		return VkPipelineViewportStateCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO)
				.viewportCount(1)
				.pViewports(viewport)
				.scissorCount(1)
				.pScissors(scissor);
	}

	public static VkPipelineRasterizationStateCreateInfo createRasterizer(int polygonMode, float lineWidth, int cullMode) {
		return VkPipelineRasterizationStateCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO)
				.depthClampEnable(false)
				.rasterizerDiscardEnable(false)
				.polygonMode(polygonMode)
				.lineWidth(lineWidth)
				.cullMode(cullMode)
				.frontFace(VK_FRONT_FACE_COUNTER_CLOCKWISE)
				.depthBiasEnable(false);
	}

	public static VkPipelineMultisampleStateCreateInfo createMultisampling() {
		return VkPipelineMultisampleStateCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO)
				.sampleShadingEnable(false)
				.rasterizationSamples(VK_SAMPLE_COUNT_1_BIT);
	}

	public static VkPipelineColorBlendStateCreateInfo createColorBlending(int colorWriteMask) {
		VkPipelineColorBlendAttachmentState.Buffer colorBlendAttachment = VkPipelineColorBlendAttachmentState.callocStack(1)
				.blendEnable(true)
				.srcColorBlendFactor(VK_BLEND_FACTOR_SRC_ALPHA)
				.dstColorBlendFactor(VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA)
				.colorBlendOp(VK_BLEND_OP_ADD)
				.srcAlphaBlendFactor(VK_BLEND_FACTOR_ONE)
				.dstAlphaBlendFactor(VK_BLEND_FACTOR_ZERO)
				.alphaBlendOp(VK_BLEND_OP_ADD)
				.colorWriteMask(colorWriteMask);

		return VkPipelineColorBlendStateCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO)
				.logicOpEnable(false)
				.pAttachments(colorBlendAttachment)
				.logicOp(VK_LOGIC_OP_COPY)
				.blendConstants(stackCallocFloat(4));
	}

	public static VkPipelineColorBlendStateCreateInfo createColorBlending(int colorWriteMask, int srcAlpha, int dstAlpha) {
		VkPipelineColorBlendAttachmentState.Buffer colorBlendAttachment = VkPipelineColorBlendAttachmentState.callocStack(1)
				.blendEnable(true)
				.srcColorBlendFactor(VK_BLEND_FACTOR_SRC_ALPHA)
				.dstColorBlendFactor(VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA)
				.colorBlendOp(VK_BLEND_OP_ADD)
				.srcAlphaBlendFactor(srcAlpha)
				.dstAlphaBlendFactor(dstAlpha)
				.alphaBlendOp(VK_BLEND_OP_ADD)
				.colorWriteMask(colorWriteMask);

		return VkPipelineColorBlendStateCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO)
				.logicOpEnable(false)
				.pAttachments(colorBlendAttachment)
				.logicOp(VK_LOGIC_OP_COPY)
				.blendConstants(stackCallocFloat(4));
	}

	public static VkPipelineLayoutCreateInfo createPipelineLayout(long layout, VkPushConstantRange.Buffer pushConstantRanges) {
		return VkPipelineLayoutCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO)
				.pSetLayouts(stackLongs(layout))
				.pPushConstantRanges(pushConstantRanges);
	}

	public static VkPipelineLayoutCreateInfo createPipelineLayout(long[] layouts, VkPushConstantRange.Buffer pushConstantRanges) {
		//TODO: save buffer somewhere and dispose it by myself
		LongBuffer buffer = MemoryStack.stackMallocLong(layouts.length);
		for (int i = 0; i < layouts.length; i++) {
			buffer.put(i, layouts[i]);
		}
		return VkPipelineLayoutCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO)
				.pSetLayouts(buffer)
				.pPushConstantRanges(pushConstantRanges);
	}

	public static VkPipelineDepthStencilStateCreateInfo createDepthStencil(int depthCompareOp, boolean enableDepthWrite) {
		return VkPipelineDepthStencilStateCreateInfo.callocStack()
				.sType(VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO)
				.depthCompareOp(depthCompareOp)
				.depthWriteEnable(enableDepthWrite)
				.depthTestEnable(true)
				.depthBoundsTestEnable(false)
				.stencilTestEnable(false);
	}

	public static VulkanPipeline createGraphicsPipeline(VkPipelineShaderStageCreateInfo.Buffer shaderStages,
	                                                    VkPipelineVertexInputStateCreateInfo vertexInputInfo,
	                                                    VkPipelineInputAssemblyStateCreateInfo inputAssembly,
	                                                    VkPipelineViewportStateCreateInfo viewportState,
	                                                    VkPipelineRasterizationStateCreateInfo rasterizer,
	                                                    VkPipelineMultisampleStateCreateInfo multisampling,
	                                                    VkPipelineColorBlendStateCreateInfo colorBlending,
	                                                    VkPipelineLayoutCreateInfo pipelineLayoutInfo,
	                                                    VkPipelineDepthStencilStateCreateInfo depthStencil) {
		VulkanPipeline pipeline = new VulkanPipeline();
		try (MemoryStack stack = stackPush()) {
			LongBuffer longBuffer = stack.mallocLong(1);
			VkCheck(vkCreatePipelineLayout(device, pipelineLayoutInfo, null, longBuffer), "Failed to create pipeline layout");
			pipeline.layout = longBuffer.get(0);

			VkGraphicsPipelineCreateInfo.Buffer pipelineInfos = VkGraphicsPipelineCreateInfo.callocStack(1, stack)
					.sType(VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO)
					.pStages(shaderStages)
					.pVertexInputState(vertexInputInfo)
					.pInputAssemblyState(inputAssembly)
					.pViewportState(viewportState)
					.pRasterizationState(rasterizer)
					.pMultisampleState(multisampling)
					.pColorBlendState(colorBlending)
					.pDepthStencilState(depthStencil)
					.layout(pipeline.layout)
					.renderPass(renderPass)
					.subpass(0)
					.basePipelineHandle(VK_NULL_HANDLE);

			VkCheck(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, pipelineInfos, null, longBuffer), "Failed to create graphics pipeline");
			pipeline.pipeline = longBuffer.get(0);
		}
		return pipeline;
	}

	private static void createImageViews() {
		for (SwapChainData data : scData) {
			data.imageView = createImageView(data.image, surfaceFormat.format(), VK_IMAGE_ASPECT_COLOR_BIT);
		}
	}

	private static void createSwapChain() {
		try (MemoryStack stack = stackPush()) {
			SwapChainSupportDetails details = querySwapChainSupport();

			surfaceFormat = chooseSwapSurfaceFormat(details.formats);
			int presentMode = chooseSwapPresentMode(details.presentModes);

			extent = chooseSwapExtent(details.capabilities);
			extentWidth = extent.width();
			extentHeight = extent.height();
			if (renderArea != null) {
				renderArea.free();
			}

			renderArea = VkRect2D.calloc();
			renderArea.offset().set(0, 0);
			renderArea.extent(extent);

			int imgCount = details.capabilities.minImageCount() + 1;
			if (details.capabilities.maxImageCount() > 0 && imgCount > details.capabilities.maxImageCount()) {
				imgCount = details.capabilities.maxImageCount();
			}

			VkSwapchainCreateInfoKHR createInfo = VkSwapchainCreateInfoKHR.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR)
					.surface(surfaceHandle)
					.minImageCount(imgCount)
					.imageFormat(surfaceFormat.format())
					.imageColorSpace(surfaceFormat.colorSpace())
					.imageExtent(extent)
					.imageArrayLayers(1)
					.imageUsage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)
					.preTransform(details.capabilities.currentTransform())
					.compositeAlpha(VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
					.presentMode(presentMode)
					.clipped(true);

			if (queues.graphics.index != queues.present.index) {
				IntBuffer indices = stack.ints(queues.graphics.index, queues.present.index);
				createInfo.imageSharingMode(VK_SHARING_MODE_CONCURRENT);
				createInfo.pQueueFamilyIndices(indices);
			} else {
				createInfo.imageSharingMode(VK_SHARING_MODE_EXCLUSIVE);
			}
			LongBuffer longBuf = stack.mallocLong(1);
			IntBuffer intBuf = stack.mallocInt(1);

			VkCheck(vkCreateSwapchainKHR(device, createInfo, null, longBuf), "Failed to create swap chain");
			swapChain = longBuf.get(0);

			vkGetSwapchainImagesKHR(device, swapChain, intBuf, null);
			if (intBuf.get(0) != 0) {
				LongBuffer imagesBuf = stack.mallocLong(intBuf.get(0));
				vkGetSwapchainImagesKHR(device, swapChain, intBuf, imagesBuf);
				scImageCount = imagesBuf.capacity();
				scData = new SwapChainData[imagesBuf.capacity()];
				for (int i = 0; i < scImageCount; i++) {
					scData[i] = new SwapChainData();
					scData[i].image = imagesBuf.get(i);
					scData[i].secondaryCommandBuffers = new VkCommandBuffer[renderers.size()];
				}
			}

			details.free();
		}
	}

	private static VkExtent2D chooseSwapExtent(VkSurfaceCapabilitiesKHR capabilities) {
		try (MemoryStack stack = stackPush()) {
			int UINT32_MAX = 0xffffffff;
			if (capabilities.currentExtent().width() != UINT32_MAX) {
				return capabilities.currentExtent();
			} else {
				IntBuffer widthBuf = stack.mallocInt(1);
				IntBuffer heightBuf = stack.mallocInt(1);

				glfwGetFramebufferSize(Window.handle, widthBuf, heightBuf);
				Window.width = widthBuf.get(0);
				Window.height = heightBuf.get(0);

				VkExtent2D actualExtent = VkExtent2D.callocStack(stack)
						.height(Window.height)
						.width(Window.width);

				actualExtent.width(Math.max(capabilities.minImageExtent().width(), Math.min(capabilities.maxImageExtent().width(), actualExtent.width())));
				actualExtent.height(Math.max(capabilities.minImageExtent().height(), Math.min(capabilities.maxImageExtent().height(), actualExtent.height())));
				return actualExtent;
			}
		}
	}

	private static VkSurfaceFormatKHR chooseSwapSurfaceFormat(VkSurfaceFormatKHR.Buffer formats) { //TODO: check different formats
		for (VkSurfaceFormatKHR format : formats) {
			//VK_FORMAT_R8G8B8A8_UNORM, VK_FORMAT_B8G8R8A8_UNORM
			if (format.format() == VK_FORMAT_R8G8B8A8_UNORM && format.colorSpace() == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				return format;
			}
		}
		return formats.get(0);
	}

	private static int chooseSwapPresentMode(IntBuffer presentModes) {
		int swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;
		for (int i = 0; i < presentModes.capacity(); i++) {
			int availablePresentMode = presentModes.get(i);
			if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return availablePresentMode;
			}
			if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
				swapchainPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
			}
		}
		return swapchainPresentMode;
	}

	private static SwapChainSupportDetails querySwapChainSupport() {
		try (MemoryStack stack = stackPush()) {
			IntBuffer intBuf = stack.mallocInt(1);
			SwapChainSupportDetails details = new SwapChainSupportDetails();

			details.capabilities = VkSurfaceCapabilitiesKHR.calloc();
			vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surfaceHandle, details.capabilities);

			vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surfaceHandle, intBuf, null);
			if (intBuf.get(0) != 0) {
				details.formats = VkSurfaceFormatKHR.calloc(intBuf.get(0));
				vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surfaceHandle, intBuf, details.formats);
			}

			vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surfaceHandle, intBuf, null);
			if (intBuf.get(0) != 0) {
				details.presentModes = memAllocInt(intBuf.get(0));
				vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surfaceHandle, intBuf, details.presentModes);
			}
			return details;
		}
	}

	private static void createDeviceQueue() {
		try (MemoryStack stack = stackPush()) {
			PointerBuffer pointerBuf = stack.mallocPointer(1);
			vkGetDeviceQueue(device, queues.graphics.index, 0, pointerBuf);
			queues.graphics.queue = new VkQueue(pointerBuf.get(0), device);

			vkGetDeviceQueue(device, queues.present.index, 0, pointerBuf);
			queues.present.queue = new VkQueue(pointerBuf.get(0), device);

			vkGetDeviceQueue(device, queues.transfer.index, 0, pointerBuf);
			queues.transfer.queue = new VkQueue(pointerBuf.get(0), device);
		}
	}

	private static void createLogicalDeviceAndQueueFamily() {
		try (MemoryStack stack = stackPush()) {
			IntBuffer intBuf = stack.mallocInt(1);
			VkPhysicalDeviceMemoryProperties memoryProperties = VkPhysicalDeviceMemoryProperties.calloc();

			ByteBuffer[] extensionsNames = {
					stack.UTF8(VK_KHR_SWAPCHAIN_EXTENSION_NAME)
			};

			PointerBuffer extensions = stack.mallocPointer(extensionsNames.length);
			for (ByteBuffer buffer : extensionsNames) {
				extensions.put(buffer);
			}
			extensions.flip();

			vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, intBuf, null);
			int queueCount = intBuf.get(0);
			VkQueueFamilyProperties.Buffer queueProps = VkQueueFamilyProperties.callocStack(queueCount, stack);
			vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, intBuf, queueProps);

			queues = new VkQueues();

			for (int i = 0; i < queueCount; i++) {
				int flags = queueProps.get(i).queueFlags();
				if (i != queues.transfer.index) {
					if ((flags & VK_QUEUE_GRAPHICS_BIT) != 0 && queues.graphics.index == -1) {
						queues.graphics.index = i;
					}
				}

				vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surfaceHandle, intBuf);
				if (i != queues.transfer.index) {
					if (intBuf.get(0) == VK_TRUE && queues.present.index == -1) {
						queues.present.index = i;
					}
				}

				if (i != queues.graphics.index && i != queues.present.index) {
					if ((flags & VK_QUEUE_COMPUTE_BIT) == 0 && (flags & VK_QUEUE_GRAPHICS_BIT) == 0 && (flags & VK_QUEUE_TRANSFER_BIT) != 0 && queues.transfer.index == -1) {
						queues.transfer.index = i;
					}
				}

				if (queues.graphics.index != -1 && queues.present.index != -1 && queues.transfer.index != -1) {
					break;
				}
			}

			if (queues.transfer.index == -1) {
				queues.transfer.index = queues.graphics.index;
			}

			Set<Integer> uniqueIndices = new TreeSet<>();
			uniqueIndices.add(queues.graphics.index);
			uniqueIndices.add(queues.present.index);
			uniqueIndices.add(queues.transfer.index);

			VkDeviceQueueCreateInfo.Buffer queueCreateInfo = VkDeviceQueueCreateInfo.callocStack(uniqueIndices.size(), stack);
			Iterator<Integer> iterator = uniqueIndices.iterator();
			for (int i = 0; iterator.hasNext(); i++) {
				int index = iterator.next();
				PriorityLock lock = new PriorityLock();
				if (queues.transfer.index == index) {
					queues.transfer.lock = lock;
				}
				if (queues.graphics.index == index) {
					queues.graphics.lock = lock;
				}
				if (queues.present.index == index) {
					queues.present.lock = lock;
				}
				queueCreateInfo.get(i)
						.sType(VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO)
						.queueFamilyIndex(index)
						.pQueuePriorities(stack.floats(0));
			}

			PointerBuffer ppEnabledLayerNames = null;
			if (debugMode) {
				ppEnabledLayerNames = stack.mallocPointer(validationLayers.length);
				for (ByteBuffer layer : validationLayers) {
					ppEnabledLayerNames.put(layer);
				}
				ppEnabledLayerNames.flip();
			}

			VkPhysicalDeviceFeatures deviceFeatures = VkPhysicalDeviceFeatures.callocStack(stack)
					.samplerAnisotropy(true);
//					.fillModeNonSolid(true)
//					.wideLines(true);

			VkDeviceCreateInfo deviceCreateInfo = VkDeviceCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO)
					.pQueueCreateInfos(queueCreateInfo)
					.ppEnabledExtensionNames(extensions)
					.ppEnabledLayerNames(ppEnabledLayerNames)
					.pEnabledFeatures(deviceFeatures);

			PointerBuffer pointerBuf = stack.mallocPointer(1);

			VkCheck(vkCreateDevice(physicalDevice, deviceCreateInfo, null, pointerBuf), "Failed to create device");
			long deviceHandle = pointerBuf.get(0);
			device = new VkDevice(deviceHandle, physicalDevice, deviceCreateInfo);

			vkGetPhysicalDeviceMemoryProperties(physicalDevice, memoryProperties);
			Vulkan.memoryProperties = memoryProperties;
		}
	}

	private static void createPhysicalDevice(int id) {
		try (MemoryStack stack = stackPush()) {
			IntBuffer intBuf = stack.mallocInt(1);
			VkCheck(vkEnumeratePhysicalDevices(instance, intBuf, null), "Failed to get number of physical devices");
			int count = intBuf.get(0);
			if (count == 0) {
				throw new RuntimeException("Can't find videocard!");
			}
			PointerBuffer pPhysicalDevices = stack.mallocPointer(count);

			VkCheck(vkEnumeratePhysicalDevices(instance, intBuf, pPhysicalDevices), "Failed to get physical devices");

			physicalDevice = new VkPhysicalDevice(pPhysicalDevices.get(Math.min(count - 1, id)), instance);

			VkPhysicalDeviceProperties deviceProperties = VkPhysicalDeviceProperties.callocStack(stack);
			vkGetPhysicalDeviceProperties(physicalDevice, deviceProperties);
			Window.title += " @ " + deviceProperties.deviceNameString();
			GLFW.glfwSetWindowTitle(Window.handle, Window.title);
		}
	}

	private static void createSurface() {
		try (MemoryStack stack = stackPush()) {
			LongBuffer longBuf = stack.mallocLong(1);
			VkCheck(glfwCreateWindowSurface(instance, Window.handle, null, longBuf), "Failed to create surface");
			surfaceHandle = longBuf.get(0);
		}
	}

	private static void setupDebugging() {
		if (!debugMode) {
			return;
		}
		try (MemoryStack stack = stackPush()) {
			LongBuffer longBuf = stack.mallocLong(1);
			int messageTypes = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
			int messageSeverities = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;

			debugMessengerCallback = new VkDebugUtilsMessengerCallbackEXT() {
				@Override
				public int invoke(int messageSeverity, int messageTypes, long pCallbackData, long pUserData) {
					VkDebugUtilsMessengerCallbackDataEXT data = VkDebugUtilsMessengerCallbackDataEXT.create(pCallbackData);
					String severity;
					if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT) {
						severity = "VERBOSE";
					} else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
						severity = "ERROR";
					} else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
						severity = "WARNING";
					} else {
						severity = "UNKNOWN";
					}
					Logger.instance.warn("[VULKAN " + severity + "] " + data.pMessageString());
					return 0;
				}
			};

			VkDebugUtilsMessengerCreateInfoEXT dbgMessengerInfo = VkDebugUtilsMessengerCreateInfoEXT.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT)
					.messageSeverity(messageSeverities)
					.messageType(messageTypes)
					.pfnUserCallback(debugMessengerCallback);

			VkCheck(vkCreateDebugUtilsMessengerEXT(instance, dbgMessengerInfo, null, longBuf), "Failed to create DebugUtilsMessenger");
			debugMessengerHandle = longBuf.get(0);
		}
	}

	private static Set<ByteBuffer> checkExtensions(ByteBuffer layer, Set<String> extensionsNames) {
		Set<ByteBuffer> set = new HashSet<>();
		try (MemoryStack stack = stackPush()) {
			IntBuffer intBuf = stack.mallocInt(1);
			vkEnumerateInstanceExtensionProperties(layer, intBuf, null);
			VkExtensionProperties.Buffer availableExtensions = VkExtensionProperties.callocStack(intBuf.get(0), stack);
			vkEnumerateInstanceExtensionProperties(layer, intBuf, availableExtensions);
			for (String extension : extensionsNames) {
				for (VkExtensionProperties ext : availableExtensions) {
					if (ext.extensionNameString().equals(extension)) {
						set.add(ext.extensionName());
						break;
					}
				}
			}
		}
		return set;
	}

	private static void createInstance() {
		try (MemoryStack stack = stackPush()) {
			PointerBuffer requiredExtensions = glfwGetRequiredInstanceExtensions();
			if (requiredExtensions == null) {
				throw new VulkanException("Failed to find list of required Vulkan extensions");
			}

			ByteBuffer appName = stack.UTF8("qubsGame");
			ByteBuffer engineName = stack.UTF8("qubsEngine");

			Set<String> extensionsNames = new HashSet<>();
			extensionsNames.add("VK_NV_external_memory_capabilities");

			VkValidationFeaturesEXT validationFeatures;
			IntBuffer enabledFeatures;
			if (debugMode) {
				extensionsNames.add(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
				extensionsNames.add(VK_EXT_VALIDATION_FEATURES_EXTENSION_NAME);

				int[] enabledFeaturesFlags = {
						VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
						VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT
				};
				enabledFeatures = stack.mallocInt(enabledFeaturesFlags.length);
				enabledFeatures.put(enabledFeaturesFlags);
				enabledFeatures.flip();

				validationFeatures = VkValidationFeaturesEXT.callocStack(stack)
						.sType(VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT)
						.pEnabledValidationFeatures(enabledFeatures);
			}

			Set<ByteBuffer> extensions = new HashSet<>();

			for (ByteBuffer layer : validationLayers) {
				extensions.addAll(checkExtensions(layer, extensionsNames));
			}
			extensions.addAll(checkExtensions(null, extensionsNames));

			PointerBuffer ppEnabledExtensionNames = stack.mallocPointer(requiredExtensions.remaining() + extensions.size());

			ppEnabledExtensionNames.put(requiredExtensions);
			for (ByteBuffer buffer : extensions) {
				ppEnabledExtensionNames.put(buffer);
			}
			ppEnabledExtensionNames.flip();

			PointerBuffer ppEnabledLayerNames = null;
			if (debugMode) {
				ppEnabledLayerNames = stack.mallocPointer(validationLayers.length);
				for (ByteBuffer layer : validationLayers) {
					ppEnabledLayerNames.put(layer);
				}
				ppEnabledLayerNames.flip();
			}

			VkApplicationInfo appInfo = VkApplicationInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_APPLICATION_INFO)
					.apiVersion(VK_MAKE_VERSION(1, 2, 0))
					.pApplicationName(appName)
					.applicationVersion(VK_MAKE_VERSION(0, 1, 0))
					.pEngineName(engineName)
					.engineVersion(VK_MAKE_VERSION(1, 0, 0));

			if (debugMode) {
				appInfo.pNext(validationFeatures.address());
			}

			VkInstanceCreateInfo pCreateInfo = VkInstanceCreateInfo.callocStack(stack)
					.sType(VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO)
					.pApplicationInfo(appInfo)
					.ppEnabledExtensionNames(ppEnabledExtensionNames)
					.ppEnabledLayerNames(ppEnabledLayerNames);

			PointerBuffer pointerBuf = stack.mallocPointer(1);

			VkCheck(vkCreateInstance(pCreateInfo, null, pointerBuf), "Failed to create VkInstance");
			long instanceHandle = pointerBuf.get(0);

			instance = new VkInstance(instanceHandle, pCreateInfo);
		}
	}

	public static void dispose() {
		for (ByteBuffer layer : validationLayers) {
			memFree(layer);
		}
		memoryProperties.free();

		if (renderArea != null) {
			renderArea.free();
		}

		VertexInfo.dispose();
		TexturedQuad.dispose();
		disposalQueue.add(VulkanUIRenderObject.indexBuffer);
		disposalQueue.add(VulkanUIRenderObject.vertexBuffer);
		world.dispose();
		submitInfo.free();

		cleanupSwapChain();
		renderers.forEach(Renderer::dispose);

		TextureRegistry.dispose();

		if (debugMode) {
			vkDestroyDebugUtilsMessengerEXT(instance, debugMessengerHandle, null);
			debugMessengerCallback.free();
		}
		vkWaitForFences(device, transferFences, true, UINT64_MAX);
		vkDestroyCommandPool(device, drawCommandPool, null);
		vkDestroyCommandPool(device, transferCommandPool, null);
		transferCommandPools.forEach(pool -> {
			vkDestroyCommandPool(device, pool.getValue(), null);
			vkDestroyFence(device, transferFences[pool.getKey()], null);
		});
		vkDestroyFence(device, transferFences[transferFences.length - 1], null);

		vkWaitForFences(device, inFlightFences, true, UINT64_MAX);
		for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
			vkDestroySemaphore(device, imageAvailableSemaphores[i], null);
			vkDestroySemaphore(device, renderFinishedSemaphores[i], null);
			vkDestroyFence(device, inFlightFences[i], null);
		}

		disposalQueue.forEach(VulkanBuffer::free);
		imageDisposalQueue.forEach(VulkanImage::free);
		vmaDestroyAllocator(vmaAllocator);

		vkDestroyDevice(device, null);
		vkDestroySurfaceKHR(instance, surfaceHandle, null);
		vkDestroyInstance(instance, null);
		memFree(memoryPointer);
	}

	private static void cleanupSwapChain() {
		depth.free();

		for (SwapChainData data : scData) {
			vkDestroyFramebuffer(device, data.framebuffer, null);
			vkFreeCommandBuffers(device, drawCommandPool, data.commandBuffer);
			vkDestroyImageView(device, data.imageView, null);
		}

		vkDestroyRenderPass(device, renderPass, null);

		vkDestroySwapchainKHR(device, swapChain, null);

		renderers.forEach(Renderer::cleanupSwapChain);
	}

	private static void recreateSwapChain(boolean firstTime) {
		if (!firstTime) {
			try (MemoryStack stack = stackPush()) {
				IntBuffer widthBuf = stack.mallocInt(1);
				IntBuffer heightBuf = stack.mallocInt(1);
				int width = 0, height = 0;
				while (width == 0 || height == 0) {
					glfwGetFramebufferSize(Window.handle, widthBuf, heightBuf);
					glfwWaitEvents();
					width = widthBuf.get(0);
					height = heightBuf.get(0);
				}

				ubo.proj.identity().perspective(Math.toRadians(FOV), (float) width / height, 0.01f, 10000.0f).scale(1, -1, 1);
				EventManager.callEvent(new WindowResizedEvent());
			}

			vkDeviceWaitIdle(device);
			cleanupSwapChain();
		}

		createSwapChain();
		createImageViews();
		createRenderPass();
		createDepthResources();
		createFramebuffers();
		VkCommandBuffer[] primaryCommandBuffers = createCommandBuffers(VK_COMMAND_BUFFER_LEVEL_PRIMARY);
		VkCommandBuffer[][] secondaryCommandBuffers = new VkCommandBuffer[renderers.size()][];
		for (int i = 0; i < renderers.size(); i++) {
			secondaryCommandBuffers[i] = createCommandBuffers(VK_COMMAND_BUFFER_LEVEL_SECONDARY);
		}

		if (!firstTime) {
			renderers.forEach(Renderer::createSwapChain);
		} else {
			renderers.forEach(Renderer::init);
		}

		for (int i = 0; i < scImageCount; i++) {
			scData[i].commandBuffer = primaryCommandBuffers[i];
			for (int k = 0; k < renderers.size(); k++) {
				scData[i].secondaryCommandBuffers[k] = secondaryCommandBuffers[k][i];
			}
		}

		for (int i = 0; i < renderers.size(); i++) {
			recordSecondaryCommandBuffers(i);
		}
		recordPrimaryCommandBuffers();
	}

	private static class VulkanQueue {
		VkQueue queue;
		int index = -1;
		PriorityLock lock;
	}

	private static class VkQueues {
		VulkanQueue graphics = new VulkanQueue();
		VulkanQueue present = new VulkanQueue();
		VulkanQueue transfer = new VulkanQueue();
	}

	public static class SwapChainData {
		public VkCommandBuffer commandBuffer;
		public VkCommandBuffer[] secondaryCommandBuffers;
		public long image;
		public long imageView;
		public long framebuffer;
	}

	private static class SwapChainSupportDetails {
		VkSurfaceCapabilitiesKHR capabilities;
		VkSurfaceFormatKHR.Buffer formats;
		IntBuffer presentModes;

		public void free() {
			this.capabilities.free();
			this.formats.free();
			memFree(this.presentModes);
		}
	}

	public static class UniformBufferObject {
		public Matrix4f model = new Matrix4f();
		public Matrix4f view = new Matrix4f();
		public Matrix4f proj = new Matrix4f();
	}

	private static class BufferedImage {
		ByteBuffer buffer;
		int width;
		int height;
	}
}
