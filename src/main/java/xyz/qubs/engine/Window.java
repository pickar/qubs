package xyz.qubs.engine;

import org.lwjgl.glfw.GLFWFramebufferSizeCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import xyz.qubs.engine.event.EventManager;
import xyz.qubs.engine.input.MouseInput;
import xyz.qubs.engine.settings.SettingsManager;
import xyz.qubs.engine.ui.events.KeyDownEvent;
import xyz.qubs.engine.ui.events.KeyUpEvent;

import static org.lwjgl.glfw.GLFW.*;

public class Window {
	public static long handle;
	public static String title = "qubs";
	public static boolean shouldClose = false;
	public static int width = 1280;
	public static int height = 720;
	public static boolean cursorGrabbed = false;
	private static GLFWKeyCallback keyCallback;
	private static GLFWWindowSizeCallback windowSizeCallback;
	private static GLFWFramebufferSizeCallback framebufferSizeCallback;

	public static void update() {
		shouldClose = glfwWindowShouldClose(handle);
	}

	public static void init() {
		if (!glfwInit()) {
			throw new RuntimeException("Failed to initialize GLFW");
		}

		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

		long monitor = glfwGetPrimaryMonitor();
		GLFWVidMode mode = glfwGetVideoMode(monitor);
		assert mode != null;
		if (SettingsManager.fullScreen) {
			handle = glfwCreateWindow(mode.width(), mode.height(), title, monitor, 0);
		} else {
			handle = glfwCreateWindow(width, height, title, 0, 0);
		}
//		glfwSetWindowMonitor(handle, monitor, 0, 0, mode.width(), mode.height(), 0);
		glfwSetWindowPos(handle, (mode.width() - width) / 2, (mode.height() - height) / 2);

		glfwSetKeyCallback(handle, keyCallback = new GLFWKeyCallback() {
			public void invoke(long window, int key, int scancode, int action, int mods) { //TODO: user keyboard input here
				if (action == GLFW_PRESS || action == GLFW_REPEAT) {
					EventManager.callEvent(new KeyDownEvent(key, action));
				} else if (action == GLFW_RELEASE) {
					EventManager.callEvent(new KeyUpEvent(key));
				}

				if (key == GLFW_KEY_LEFT_ALT) {
					if (action == GLFW_PRESS) {
						glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
						cursorGrabbed = false;
					} else {
						MouseInput.first = true;
						glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
						cursorGrabbed = true;
					}
				}
				if (action != GLFW_RELEASE) {
					return;
				}
				if (key == GLFW_KEY_ESCAPE) {
					glfwSetWindowShouldClose(window, true);
				}
			}
		});

		glfwSetWindowSizeCallback(handle, windowSizeCallback = new GLFWWindowSizeCallback() {
			public void invoke(long window, int width, int height) {
				if (width <= 0 || height <= 0) {
					return;
				}
				Window.width = width;
				Window.height = height;
			}
		});

		glfwSetFramebufferSizeCallback(handle, framebufferSizeCallback = new GLFWFramebufferSizeCallback() {
			public void invoke(long window, int width, int height) {
				Vulkan.framebufferResized();
			}
		});

		glfwShowWindow(handle);

		if (glfwGetWindowAttrib(handle, GLFW_HOVERED) == GLFW_TRUE) {
			cursorGrabbed = true;
			glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		} else {
			cursorGrabbed = false;
			glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}

	public static boolean isKeyPressed(int key) {
		return glfwGetKey(handle, key) == GLFW_PRESS;
	}

	public static void dispose() {
		keyCallback.free();
		windowSizeCallback.free();
		framebufferSizeCallback.free();

		glfwDestroyWindow(handle);
		glfwTerminate();
	}
}
