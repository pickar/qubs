package xyz.qubs.game.transport.energy;

import xyz.qubs.game.block.IBlock;
import xyz.qubs.game.transport.AbstractTransportNet;

public class EnergyNet<T extends IEnergyContainer & IBlock> extends AbstractTransportNet<T> {
	@Override
	public void tick() {
		producers.forEach(producer -> {
			int energy = producer.consumeEnergy(producer.getOutcomeRate(null));

			if (energy > 0) {
				for (T receiver : distances.get(producer)) {
					energy -= receiver.receiveEnergy(Math.min(receiver.getIncomeRate(null), energy));
					if (energy == 0) {
						break;
					}
				}
				producer.receiveEnergy(energy);
			}
		});

		containers.forEach(T::tick);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("RECEIVERS: \n");
		consumers.forEach(r -> sb.append("\t").append(r).append("\n"));
		sb.append("GIVERS: \n");
		producers.forEach(r -> sb.append("\t").append(r).append("\n"));
		return sb.toString();
	}
}
