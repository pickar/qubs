package xyz.qubs.game.transport.energy;

import xyz.qubs.engine.model.Side;

public interface IEnergyContainer {
	int consumeEnergy(int amount);

	int receiveEnergy(int amount);

	int getEnergy();

	int getMaxEnergy();

	int getIncomeRate(Side side);

	int getOutcomeRate(Side side);

	boolean tick();
}
