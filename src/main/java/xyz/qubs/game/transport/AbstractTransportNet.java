package xyz.qubs.game.transport;

import it.unimi.dsi.fastutil.objects.*;
import xyz.qubs.game.block.IBlock;

import java.util.Comparator;

public abstract class AbstractTransportNet<T extends IBlock> implements ITransportNet<T> {
	public ObjectSet<T> producers = new ObjectArraySet<>();
	public ObjectSet<T> consumers = new ObjectArraySet<>();

	public ObjectSet<T> containers = new ObjectArraySet<>();

	public Object2ObjectMap<T, ObjectSet<T>> distances = new Object2ObjectArrayMap<>();

	public ObjectSet<T> getDistanceSet(T producer) {
		return new ObjectRBTreeSet<>(Comparator.comparingInt(c -> -producer.getPosition().getDistanceTo(c.getPosition())));
	}

	@Override
	public void addProducer(T producer) {
		producers.add(producer);
		containers.add(producer);
		ObjectSet<T> set = getDistanceSet(producer);
		set.addAll(consumers);
		distances.put(producer, set);
	}

	@Override
	public void removeProducer(T producer) {
		producers.remove(producer);
		distances.remove(producer);
		if (!consumers.contains(producer)) {
			containers.remove(producer);
		}
	}

	@Override
	public void addConsumer(T consumer) {
		consumers.add(consumer);
		containers.add(consumer);
		distances.forEach((producer, distanceSet) -> distanceSet.add(consumer));
	}

	@Override
	public void removeConsumer(T consumer) {
		consumers.remove(consumer);
		distances.forEach((producer, distanceSet) -> distanceSet.remove(consumer));
		if (!producers.contains(consumer)) {
			containers.remove(consumer);
		}
	}
}
