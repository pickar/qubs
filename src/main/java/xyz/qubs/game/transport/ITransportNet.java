package xyz.qubs.game.transport;

public interface ITransportNet<T> {
	void tick();

	void addProducer(T producer);

	void removeProducer(T producer);

	void addConsumer(T consumer);

	void removeConsumer(T consumer);
}
