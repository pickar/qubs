package xyz.qubs.game.entity;

import it.unimi.dsi.fastutil.objects.ObjectList;
import org.joml.Vector3f;
import xyz.qubs.game.collision.AABB;
import xyz.qubs.game.collision.BB;
import xyz.qubs.game.collision.physics.IPhysics;
import xyz.qubs.game.world.chunk.IChunk;

public interface IEntity {
	ObjectList<IChunk> getChunks(); // REWORKING

	Vector3f getLocalPosition();

	Vector3f getWorldPosition();

	Vector3f getRotation(); // pitch, yaw, roll

	void moveLocalDirectional(float x, float y, float z);

	void moveLocal(float x, float y, float z);

	float getWeight();

	Vector3f getDirectionVector();

	Vector3f getAcceleration();

	Vector3f getMaxSpeed();

	Vector3f getVelocity();

	IPhysics getIPhysics(); // Use real or minecraft physics engine for entity

	AABB[] getAxisAlignedBoundingBoxes(); // Using for simply physics calculations

	BB[] getBoundingBoxes(); // Using for real physics calculations
}
