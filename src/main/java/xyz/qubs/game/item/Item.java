package xyz.qubs.game.item;

public class Item {
	public final ItemType type;
	public int count;
	//NBT data here ?
	//enum Type? tool, armor, etc.

	public Item(ItemType itemType, int count) {
		this.type = itemType;
		this.count = count;
	}
}
