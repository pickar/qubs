package xyz.qubs.game.item;

import xyz.qubs.game.item.filter.IItemFilter;

public class FilteredSlot extends Slot {
	private final IItemFilter itemFilter;

	public FilteredSlot(IItemFilter itemFilter) {
		this.itemFilter = itemFilter;
	}

	@Override
	public void setItem(Item item) {
		this.item = itemFilter.filter(item);
	}
}
