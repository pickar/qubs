package xyz.qubs.game.item;

public class Slot {
	protected Item item;

	int getMaxItemCount() {
		if (this.getItem() != null) {
			return this.getItem().type.getMaxStackSize();
		}
		return 0;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Item takeItems(int count) {
		if (item != null && count >= 0) {
			final int countTaken = Math.min(item.count, count);
			item.count -= countTaken;
			final Item returnItems = new Item(item.type, countTaken);
			if (item.count == 0) {
				item = null;
			}
			return returnItems;
		}
		return null;
	}

	public Item takeAll() {
		if (item != null) {
			return takeItems(item.count);
		}
		return null;
	}

	public int addItems(int count) {
		if (count <= 0) {
			return 0;
		}
		final int countAdded = Math.min(getMaxItemCount() - item.count, count);
		item.count += countAdded;

		return countAdded;
	}

	public int transferItems(Item from, int count) {
		if (from != null && item.type == from.type && count > 0) {
			final int countTransferred = Math.min(count, Math.min(getMaxItemCount() - item.count, from.count));
			from.count -= countTransferred;
			item.count += countTransferred;
			return countTransferred;
		}
		return 0;
	}

	public int transferItems(Item from) {
		return transferItems(from, from.count);
	}
}
