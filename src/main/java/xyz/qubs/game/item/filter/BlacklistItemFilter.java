package xyz.qubs.game.item.filter;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.game.item.Item;

import java.util.stream.Collectors;

public class BlacklistItemFilter extends AbstractItemFilter {
	@Override
	public ObjectList<Item> filter(ObjectList<Item> items) {
		return items.stream().filter(item -> !itemsToFilter.contains(item.type)).collect(Collectors.toCollection(ObjectArrayList::new));
	}

	@Override
	public Item filter(Item item) {
		if (itemsToFilter.contains(item.type)) {
			return null;
		}
		return item;
	}
}
