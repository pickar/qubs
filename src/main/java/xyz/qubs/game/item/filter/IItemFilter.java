package xyz.qubs.game.item.filter;

import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.game.item.Item;
import xyz.qubs.game.item.ItemType;

public interface IItemFilter {
	ObjectList<Item> filter(ObjectList<Item> items);

	Item filter(Item item);

	void addToFilter(ObjectList<ItemType> items);

	void addToFilter(ItemType item);

	void removeFromFilter(ObjectList<ItemType> items);

	void removeFromFilter(ItemType item);
}
