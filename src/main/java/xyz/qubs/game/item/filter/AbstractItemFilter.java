package xyz.qubs.game.item.filter;

import it.unimi.dsi.fastutil.objects.ObjectArraySet;
import it.unimi.dsi.fastutil.objects.ObjectList;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import xyz.qubs.game.item.ItemType;

public abstract class AbstractItemFilter implements IItemFilter {
	transient ObjectSet<ItemType> itemsToFilter = new ObjectArraySet<>();

	@Override
	public void addToFilter(ObjectList<ItemType> items) {
		itemsToFilter.addAll(items);
	}

	@Override
	public void addToFilter(ItemType item) {
		itemsToFilter.add(item);
	}

	@Override
	public void removeFromFilter(ObjectList<ItemType> items) {
		itemsToFilter.removeAll(items);
	}

	@Override
	public void removeFromFilter(ItemType item) {
		itemsToFilter.remove(item);
	}
}
