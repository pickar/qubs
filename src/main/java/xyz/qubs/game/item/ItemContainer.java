package xyz.qubs.game.item;

public abstract class ItemContainer {
	private final Slot[] slots;

	protected ItemContainer() {
		slots = setupSlots();
	}

	abstract Slot[] setupSlots();

	public Item getItemFromSlot(int slotId) {
		return slots[slotId].getItem();
	}

	public void swapSlotsContents(int slotIdFirst, int slotIdSecond) {
		final Item first = slots[slotIdFirst].takeAll();
		final Item second = slots[slotIdSecond].takeAll();

		slots[slotIdFirst].setItem(second);
		slots[slotIdSecond].setItem(first);
	}

	public int addItemsToSlot(int slotId, int count) {
		return slots[slotId].addItems(count);
	}

	public Item takeItemsFromSlot(int slotId, int count) {
		return slots[slotId].takeItems(count);
	}

	public Item takeOneStackFromSlot(int slotId) {
		return slots[slotId].takeItems(slots[slotId].getItem().type.getMaxStackSize());
	}

	public int putItemIntoSlot(int slotId, Item item) {
		if (item == null || slots[slotId].getItem() == null) {
			slots[slotId].setItem(item);
			return getItemCountInSlot(slotId);
		} else if (getItemTypeInSlot(slotId) == item.type) {
			return slots[slotId].transferItems(item);
		}
		return 0;
	}

	public void setItemInSlot(int slotId, Item item) {
		slots[slotId].setItem(item);
	}

	public int getItemCountInSlot(int slotId) {
		final Item item = slots[slotId].getItem();
		return item == null ? 0 : item.count;
	}

	public ItemType getItemTypeInSlot(int slotId) {
		final Item item = slots[slotId].getItem();
		return item == null ? null : item.type;
	}
}
