package xyz.qubs.game.item;

import xyz.qubs.engine.registry.IRegistryEntry;
import xyz.qubs.engine.registry.NamespacedName;

public abstract class ItemType implements IRegistryEntry {
	private final NamespacedName registryName;

	public ItemType(NamespacedName registryName) {
		this.registryName = registryName;
	}

	@Override
	public String toString() {
		return getRegistryName().toString();
	}

	public int getMaxStackSize() {
		return 64;
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}
}
