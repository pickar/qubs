package xyz.qubs.game.item;

import xyz.qubs.game.item.filter.IItemFilter;
import xyz.qubs.game.item.filter.WhitelistItemFilter;

public class PlayerInventory extends ItemContainer {
	@Override
	Slot[] setupSlots() {
		final Slot[] slots = new Slot[4 + 1 + 9 * 4];
		final IItemFilter armorFilter = new WhitelistItemFilter();
//		armorFilter.addToFilter(armor1);
//		armorFilter.addToFilter(armor2);
		for (int i = 0; i < 4; i++) {
			slots[i] = new FilteredSlot(armorFilter);
		}
		for (int i = 4; i < slots.length; i++) {
			slots[i] = new Slot();
		}
		return slots;
	}
}
