package xyz.qubs.game.block.types;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import org.joml.Vector2f;
import org.joml.Vector3f;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.model.TexturedQuad;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.ui.models.properties.UIColor;
import xyz.qubs.engine.utils.Maths;
import xyz.qubs.engine.vulkan.vertices.CompressedVertex;
import xyz.qubs.game.block.BlockModel;
import xyz.qubs.game.block.BlockTexture;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.block.TextureRegistry;
import xyz.qubs.game.world.chunk.Chunk;

public class OakLeavesBlockType extends CubeBlockType {
	private static BlockTexture texture;

	public OakLeavesBlockType(String localizedName, NamespacedName registryName) {
		super(localizedName, registryName);

		texture = TextureRegistry.createNewTexture("oak_leaves", "blocks/oak_leaves_gray.png");
	}

	@Override
	public BlockTexture getSidedTexture(Side side) {
		return texture;
	}

	@Override
	public boolean isTransparent(Side side) {
		return true;
	}

	@Override
	public BlockModel getModel(int x, int y, int z, Chunk chunk, BlockType[] blocksAround) {
		final ObjectList<CompressedVertex> vertices = new ObjectArrayList<>();
		final IntList indices = new IntArrayList();

		int r = (x + chunk.chunkX) & 511;
		if (r > 255) { r = 511 - r; }
		int b = (z + chunk.chunkZ) & 511;
		if (b > 255) { b = 511 - b; }

		final int color = UIColor.rgbToInt(255 - r, 255 - (y + chunk.chunkY) * 2, 255 - b);

		for (Side side : Side.values) {
			BlockType blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				final byte[] ao = quadAO(blocksAround, side);
				final int[] quadIndices;
				if (side == Side.TOP || side == Side.BOTTOM) {
					if (ao[0] + ao[3] < ao[1] + ao[2]) {
						quadIndices = TexturedQuad.SidedQuad.indicesFlipped[side.ordinal()];
					} else {
						quadIndices = TexturedQuad.SidedQuad.indices[side.ordinal()];
					}
				} else {
					if (ao[0] + ao[2] < ao[1] + ao[3]) {
						quadIndices = TexturedQuad.SidedQuad.indicesFlipped[side.ordinal()];
					} else {
						quadIndices = TexturedQuad.SidedQuad.indices[side.ordinal()];
					}
				}
				for (int quadIndex : quadIndices) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos());
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord());

					vertices.add(new CompressedVertex(Maths.floor(position.x) + x, Maths.floor(position.y) + y, Maths.floor(position.z) + z, ao[k], Maths.floor(texture.x), Maths.floor(texture.y), getSidedTexture(side).id, color));
				}
			}
		}

		if (indices.size() > 0) {
			return new BlockModel(vertices.toArray(new CompressedVertex[0]), indices.toIntArray());
		} else {
			return null;
		}
	}
}
