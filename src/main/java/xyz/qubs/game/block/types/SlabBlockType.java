package xyz.qubs.game.block.types;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import org.joml.Vector2f;
import org.joml.Vector3f;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.model.TexturedQuad;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.vulkan.vertices.GeneralVertex;
import xyz.qubs.game.block.*;
import xyz.qubs.game.collision.AABB;
import xyz.qubs.game.world.chunk.Chunk;

import static xyz.qubs.game.block.types.CubeBlockType.quadAO;

public class SlabBlockType extends BlockType {
	private final BlockTexture texture = TextureRegistry.createNewTexture("oak_plank_slab", "blocks/oak_plank.png");

	public SlabBlockType(String localizedName, NamespacedName registryName) {
		super(localizedName, registryName);
	}

	@Override
	public AABB[] getAABBs() {
		return new AABB[] {
				new AABB(new Vector3f(0, 0f, 0), new Vector3f(1, 0.5f, 1)),
				new AABB(new Vector3f(0.5f, 0.5f, 0), new Vector3f(1, 1f, 1)),
		};
	}

	@Override
	public BlockModel getModel(int x, int y, int z, Chunk chunk, BlockType[] blocksAround) {
		Data data = (Data) chunk.getBlock(x, y, z).data;
		ObjectList<GeneralVertex> vertices = new ObjectArrayList<>();
		IntList indices = new IntArrayList();

		Side side = Side.TOP;
		BlockType blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
		byte[] ao = quadAO(blocksAround, side);
		int[] inds = TexturedQuad.SidedQuad.indices[side.ordinal()];
		BlockTexture blockTexture = getSidedTexture(side);
		if (data.rotation == 0) {

			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 1, 1).add(0.5f, 0, 0);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 1).add(0.5f, 0);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
			}
			for (int quadIndex : inds) {
				indices.add(vertices.size() + quadIndex);
			}
			for (int k = 0; k < 4; k++) {
				Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 1, 1).sub(0, 0.5f, 0);
				Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 1);

				vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
			}

			side = Side.BOTTOM;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);

			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				addDataToBuffers(TexturedQuad.SidedQuad.indices[side.ordinal()], TexturedQuad.SidedQuad.vertices[side.ordinal()], getSidedTexture(side).id, ao, indices, vertices);
			}

			side = Side.FRONT;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);
			inds = TexturedQuad.SidedQuad.indices[side.ordinal()];
			blockTexture = getSidedTexture(side);

			if (blockAround != null && (blockAround.isTransparent(side.opposite()))) {
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 1, 1).add(0.5f, 0, 0);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 1).add(0.5f, 0);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 0.5f, 1);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 0.5f).add(0, 0.5f);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
			}

			side = Side.BACK;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);
			inds = TexturedQuad.SidedQuad.indices[side.ordinal()];
			blockTexture = getSidedTexture(side);

			if (blockAround != null && (blockAround.isTransparent(side.opposite()))) {
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 1, 1).add(0.5f, 0, 0);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 1);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 0.5f, 1);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 0.5f).add(0.5f, 0.5f);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
			}

			side = Side.LEFT;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);
			inds = TexturedQuad.SidedQuad.indices[side.ordinal()];
			blockTexture = getSidedTexture(side);

			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(1, 0.5f, 1);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(1, 0.5f).add(0, 0.5f);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
			}
			for (int quadIndex : inds) {
				indices.add(vertices.size() + quadIndex);
			}
			for (int k = 0; k < 4; k++) {
				Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(1, 0.5f, 1).add(0.5f, 0.5f, 0);
				Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(1, 0.5f);

				vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
			}

			side = Side.RIGHT;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);

			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				addDataToBuffers(TexturedQuad.SidedQuad.indices[side.ordinal()], TexturedQuad.SidedQuad.vertices[side.ordinal()], getSidedTexture(side).id, ao, indices, vertices);
			}
		} else {

			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 1, 1);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 1);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
			}
			for (int quadIndex : inds) {
				indices.add(vertices.size() + quadIndex);
			}
			for (int k = 0; k < 4; k++) {
				Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 1, 1).add(0.5f, -0.5f, 0);
				Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 1).add(0.5f, 0);

				vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
			}

			side = Side.BOTTOM;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);

			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				addDataToBuffers(TexturedQuad.SidedQuad.indices[side.ordinal()], TexturedQuad.SidedQuad.vertices[side.ordinal()], getSidedTexture(side).id, ao, indices, vertices);
			}

			side = Side.FRONT;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);
			inds = TexturedQuad.SidedQuad.indices[side.ordinal()];
			blockTexture = getSidedTexture(side);

			if (blockAround != null && (blockAround.isTransparent(side.opposite()))) {
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 0.5f, 1).add(0.5f, 0, 0);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 0.5f).add(0.5f, 0.5f);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 1, 1);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 1);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
			}

			side = Side.BACK;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);
			inds = TexturedQuad.SidedQuad.indices[side.ordinal()];
			blockTexture = getSidedTexture(side);

			if (blockAround != null && (blockAround.isTransparent(side.opposite()))) {
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 0.5f, 1).add(0.5f, 0, 0);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 0.5f).add(0, 0.5f);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(0.5f, 1, 1);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(0.5f, 1).add(0.5f, 0);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
			}

			side = Side.LEFT;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);

			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				addDataToBuffers(TexturedQuad.SidedQuad.indices[side.ordinal()], TexturedQuad.SidedQuad.vertices[side.ordinal()], getSidedTexture(side).id, ao, indices, vertices);
			}

			side = Side.RIGHT;
			blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			ao = quadAO(blocksAround, side);
			inds = TexturedQuad.SidedQuad.indices[side.ordinal()];
			blockTexture = getSidedTexture(side);

			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				for (int quadIndex : inds) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(1, 0.5f, 1).add(-0.5f, 0.5f, 0);
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(1, 0.5f);

					vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
				}
			}
			for (int quadIndex : inds) {
				indices.add(vertices.size() + quadIndex);
			}
			for (int k = 0; k < 4; k++) {
				Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos()).mul(1, 0.5f, 1);
				Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord()).mul(1, 0.5f).add(0, 0.5f);

				vertices.add(new GeneralVertex(position, texture, (blockTexture.id & 0x3fffffff) | (ao[k] << 30)));
			}
		}

		if (indices.size() > 0) {
			return new BlockModel(vertices.toArray(new GeneralVertex[0]), indices.toIntArray());
		} else {
			return null;
		}
	}

	@Override
	public BlockData<SlabBlockType> createData() {
		return new Data(this);
	}

	@Override
	public boolean hasData() {
		return true;
	}

	@Override
	public boolean isTransparent(Side side) {
		return side != Side.BOTTOM && side != Side.RIGHT;
	}

	@Override
	public BlockTexture getSidedTexture(Side side) {
		return texture;
	}

	public static class Data extends BlockData<SlabBlockType> {
		public int rotation = 0;

		public Data(SlabBlockType type) {
			super(type);
		}
	}
}
