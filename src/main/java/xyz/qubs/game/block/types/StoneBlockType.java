package xyz.qubs.game.block.types;

import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.block.BlockTexture;
import xyz.qubs.game.block.TextureRegistry;

public class StoneBlockType extends CubeBlockType {
	private static BlockTexture topTexture;
	private static BlockTexture sideTexture;

	public StoneBlockType(String localizedName, NamespacedName registryName) {
		super(localizedName, registryName);

		topTexture = TextureRegistry.createNewTexture("stone", "blocks/stone.png");
		sideTexture = TextureRegistry.createNewTexture("stoneSide", "blocks/stone_side.png");
	}

	@Override
	public BlockTexture getSidedTexture(Side side) {
		if (side == Side.TOP || side == Side.BOTTOM) {
			return topTexture;
		} else {
			return sideTexture;
		}
	}
}
