package xyz.qubs.game.block.types;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import org.joml.Vector2f;
import org.joml.Vector3f;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.model.TexturedQuad;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.utils.Maths;
import xyz.qubs.engine.vulkan.vertices.CompressedVertex;
import xyz.qubs.game.block.BlockModel;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.collision.AABB;
import xyz.qubs.game.world.chunk.Chunk;

public abstract class CubeBlockType extends BlockType {
	public static final AABB[] CUBE_AABB = { new AABB(new Vector3f(), new Vector3f(1)) };

	public CubeBlockType(String localizedName, NamespacedName registryName) {
		super(localizedName, registryName);
	}

	public static byte vertexAO(boolean side1, boolean side2, boolean corner) {
		if (side1 && side2) {
			return 3;
		}
		return (byte) ((side1 ? 1 : 0) + (side2 ? 1 : 0) + (corner ? 1 : 0));
	}

	private static boolean isSuitable(BlockType blockType, Side side) {
		return blockType != null && !blockType.isTransparent(side);
	}

	public static byte[] quadAO(BlockType[] blocksAround, Side side) {
		final byte[] ao = new byte[4];
		if (side == Side.FRONT || side == Side.BACK) {
			final int z = 1 + side.z;
			side = side.opposite();

			final boolean b1 = isSuitable(blocksAround[blockAroundIndex(1, 2, z)], side);
			final boolean b2 = isSuitable(blocksAround[blockAroundIndex(1, 0, z)], side);
			final boolean b3 = isSuitable(blocksAround[blockAroundIndex(0, 1, z)], side);
			final boolean b4 = isSuitable(blocksAround[blockAroundIndex(2, 1, z)], side);

			ao[0] = vertexAO(b1, b3, isSuitable(blocksAround[blockAroundIndex(0, 2, z)], side));
			ao[1] = vertexAO(b2, b3, isSuitable(blocksAround[blockAroundIndex(0, 0, z)], side));
			ao[2] = vertexAO(b2, b4, isSuitable(blocksAround[blockAroundIndex(2, 0, z)], side));
			ao[3] = vertexAO(b1, b4, isSuitable(blocksAround[blockAroundIndex(2, 2, z)], side));
		} else if (side == Side.TOP || side == Side.BOTTOM) {
			final int y = 1 + side.y;
			side = side.opposite();

			final boolean b1 = isSuitable(blocksAround[blockAroundIndex(1, y, 2)], side);
			final boolean b2 = isSuitable(blocksAround[blockAroundIndex(1, y, 0)], side);
			final boolean b3 = isSuitable(blocksAround[blockAroundIndex(0, y, 1)], side);
			final boolean b4 = isSuitable(blocksAround[blockAroundIndex(2, y, 1)], side);

			ao[2] = vertexAO(b1, b3, isSuitable(blocksAround[blockAroundIndex(0, y, 2)], side));
			ao[0] = vertexAO(b2, b3, isSuitable(blocksAround[blockAroundIndex(0, y, 0)], side));
			ao[1] = vertexAO(b2, b4, isSuitable(blocksAround[blockAroundIndex(2, y, 0)], side));
			ao[3] = vertexAO(b1, b4, isSuitable(blocksAround[blockAroundIndex(2, y, 2)], side));
		} else {
			final int x = 1 + side.x;
			side = side.opposite();

			final boolean b1 = isSuitable(blocksAround[blockAroundIndex(x, 2, 1)], side);
			final boolean b2 = isSuitable(blocksAround[blockAroundIndex(x, 0, 1)], side);
			final boolean b3 = isSuitable(blocksAround[blockAroundIndex(x, 1, 0)], side);
			final boolean b4 = isSuitable(blocksAround[blockAroundIndex(x, 1, 2)], side);

			ao[2] = vertexAO(b1, b3, isSuitable(blocksAround[blockAroundIndex(x, 2, 0)], side));
			ao[1] = vertexAO(b2, b3, isSuitable(blocksAround[blockAroundIndex(x, 0, 0)], side));
			ao[0] = vertexAO(b2, b4, isSuitable(blocksAround[blockAroundIndex(x, 0, 2)], side));
			ao[3] = vertexAO(b1, b4, isSuitable(blocksAround[blockAroundIndex(x, 2, 2)], side));
		}
		return ao;
	}

	@Override
	public AABB[] getAABBs() {
		return CUBE_AABB;
	}

	@Override
	public BlockModel getModel(int x, int y, int z, Chunk chunk, BlockType[] blocksAround) {
		final ObjectList<CompressedVertex> vertices = new ObjectArrayList<>();
		final IntList indices = new IntArrayList();

		for (Side side : Side.values) {
			BlockType blockAround = blocksAround[blockAroundIndex(1 + side.x, 1 + side.y, 1 + side.z)];
			if (blockAround != null && blockAround.isTransparent(side.opposite())) {
				final byte[] ao = quadAO(blocksAround, side);
				final int[] quadIndices;
				if (side == Side.TOP || side == Side.BOTTOM) {
					if (ao[0] + ao[3] < ao[1] + ao[2]) {
						quadIndices = TexturedQuad.SidedQuad.indicesFlipped[side.ordinal()];
					} else {
						quadIndices = TexturedQuad.SidedQuad.indices[side.ordinal()];
					}
				} else {
					if (ao[0] + ao[2] < ao[1] + ao[3]) {
						quadIndices = TexturedQuad.SidedQuad.indicesFlipped[side.ordinal()];
					} else {
						quadIndices = TexturedQuad.SidedQuad.indices[side.ordinal()];
					}
				}
				for (int quadIndex : quadIndices) {
					indices.add(vertices.size() + quadIndex);
				}
				for (int k = 0; k < 4; k++) {
					Vector3f position = new Vector3f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getPos());
					Vector2f texture = new Vector2f(TexturedQuad.SidedQuad.vertices[side.ordinal()][k].getTexCoord());

					vertices.add(new CompressedVertex(Maths.floor(position.x) + x, Maths.floor(position.y) + y, Maths.floor(position.z) + z, ao[k], Maths.floor(texture.x), Maths.floor(texture.y), getSidedTexture(side).id));
				}
			}
		}

		if (indices.size() > 0) {
			return new BlockModel(vertices.toArray(new CompressedVertex[0]), indices.toIntArray());
		} else {
			return null;
		}
	}
}
