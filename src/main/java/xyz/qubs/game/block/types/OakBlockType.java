package xyz.qubs.game.block.types;

import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.block.BlockTexture;
import xyz.qubs.game.block.TextureRegistry;

public class OakBlockType extends CubeBlockType {
	private static BlockTexture topTexture;
	private static BlockTexture sideTexture;

	public OakBlockType(String localizedName, NamespacedName registryName) {
		super(localizedName, registryName);

		topTexture = TextureRegistry.createNewTexture("oak", "blocks/oak.png");
		sideTexture = TextureRegistry.createNewTexture("oakSide", "blocks/oak_side.png");
	}

	@Override
	public BlockTexture getSidedTexture(Side side) {
		if (side == Side.TOP || side == Side.BOTTOM) {
			return topTexture;
		} else {
			return sideTexture;
		}
	}
}
