package xyz.qubs.game.block.types;

import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.block.BlockTexture;

public class SingleTextureCube extends CubeBlockType {
	private final BlockTexture texture;
	private boolean rotateTexture;

	public SingleTextureCube(String localizedName, NamespacedName registryName, BlockTexture texture) {
		super(localizedName, registryName);
		this.texture = texture;
	}

	public void setRotateTexture(boolean rotateTexture) {
		this.rotateTexture = rotateTexture;
	}

	@Override
	public boolean rotateTexture(Side side) {
		return rotateTexture;
	}

	@Override
	public BlockTexture getSidedTexture(Side side) {
		return texture;
	}
}
