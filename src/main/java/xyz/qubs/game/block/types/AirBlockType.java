package xyz.qubs.game.block.types;

import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.block.BlockType;

public class AirBlockType extends BlockType {
	public AirBlockType(String localizedName, NamespacedName registryName) {
		super(localizedName, registryName);
	}

	@Override
	public boolean isAir() {
		return true;
	}

	@Override
	public boolean isTransparent(Side side) {
		return true;
	}
}
