package xyz.qubs.game.block;

public class Position {
	private int x, y, z;

	public Position(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public void set(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getDistanceTo(int x, int y, int z) {
		return Math.abs(this.x - x) + Math.abs(this.y - y) + Math.abs(this.z - z);
	}

	public int getDistanceTo(Position position) {
		return getDistanceTo(position.getX(), position.getY(), position.getZ());
	}
}
