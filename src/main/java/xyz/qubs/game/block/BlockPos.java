package xyz.qubs.game.block;

import org.joml.Vector3i;
import xyz.qubs.game.world.chunk.Chunk;
import xyz.qubs.game.world.chunk.IChunk;

import static xyz.qubs.game.world.chunk.IChunk.CHUNK_SIZE;

public class BlockPos {
	private static final BlockPos[] array = new BlockPos[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];

	static {
		int index = 0;
		for (int x = 0; x < CHUNK_SIZE; x++) {
			for (int y = 0; y < CHUNK_SIZE; y++) {
				for (int z = 0; z < CHUNK_SIZE; z++) {
					array[index++] = new BlockPos(x, y, z);
				}
			}
		}
	}

	public final int x; // from 0 to (CHUNK_SIZE-1)
	public final int y;
	public final int z;

	private BlockPos(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static BlockPos get(int x, int y, int z) {
		return array[Chunk.index(x, y, z)];
	}

	public static BlockPos get(int index) {
		return array[index];
	}

	public static BlockPos fromGlobal(Vector3i vector) {
		return new BlockPos(vector.x >> IChunk.LOG2_CHUNK_SIZE, vector.y >> IChunk.LOG2_CHUNK_SIZE, vector.z >> IChunk.LOG2_CHUNK_SIZE);
	}

	@Override
	public boolean equals(Object o) {
		return this == o;
	}

	@Override
	public int hashCode() {
		return Chunk.index(x, y, z);
	}
}
