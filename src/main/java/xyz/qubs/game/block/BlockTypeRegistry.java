package xyz.qubs.game.block;

import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.registry.RegisterEntriesEvent;
import xyz.qubs.engine.registry.Registry;
import xyz.qubs.engine.registry.RegistryCreationEvent;
import xyz.qubs.game.block.types.*;

import java.lang.reflect.Type;

public class BlockTypeRegistry extends Registry<BlockType> {
	public static final BlockType AIR = new AirBlockType("Air", new NamespacedName("core", "air"));
	public static final BlockType DIRT = new SingleTextureCube("Dirt", new NamespacedName("core", "dirt"), TextureRegistry.createNewTexture("dirt", "blocks/dirt.png"));
	public static final BlockType GRASS = new GrassBlockType("Grass", new NamespacedName("core", "grass"));
	public static final BlockType OAK = new OakBlockType("Oak log", new NamespacedName("core", "oak"));
	public static final BlockType STONE = new StoneBlockType("Stone", new NamespacedName("core", "stone"));
	public static final BlockType OAK_LEAVES = new OakLeavesBlockType("Oak leaves", new NamespacedName("core", "oak_leaves"));
	public static final BlockType SAND = new SingleTextureCube("Sand", new NamespacedName("core", "sand"), TextureRegistry.createNewTexture("sand", "blocks/sand.png"));
	public static final BlockType OAK_PLANK = new SingleTextureCube("Oak plank", new NamespacedName("core", "oak_plank"), TextureRegistry.createNewTexture("oak_plank", "blocks/oak_plank.png"));
	public static final BlockType OAK_SLAB = new SlabBlockType("Oak plank slab", new NamespacedName("core", "oak_plank_slab"));

	private static final BlockTypeRegistry INSTANCE = new BlockTypeRegistry(new NamespacedName("core", "block_type"), BlockType.class);

	private BlockTypeRegistry(NamespacedName registryName, Type type) {
		super(registryName, type);
	}

	public static BlockType getBlockTypeByName(NamespacedName registryName) {
		return INSTANCE.getElement(registryName);
	}

	public static BlockType getBlockTypeByName(String fullName) {
		return INSTANCE.getElement(fullName);
	}

	@SubscribeEvent
	public static void onRegistryCreation(RegistryCreationEvent event) {
		event.registry.register(INSTANCE);
	}

	@SubscribeEvent
	public static void onRegisterEntriesBlockType(RegisterEntriesEvent<BlockType> event) {
		event.registry.register(AIR);
		event.registry.register(DIRT);
		event.registry.register(GRASS);
		event.registry.register(OAK);
		event.registry.register(STONE);
		event.registry.register(OAK_LEAVES);
		event.registry.register(SAND);
		event.registry.register(OAK_PLANK);
		event.registry.register(OAK_SLAB);
	}
}
