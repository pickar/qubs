package xyz.qubs.game.block;

import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.vulkan.models.VulkanImage;

public class BlockTexture {
	public final String name;

	public final VulkanImage vulkanImage;
	public final int id;

	public BlockTexture(String name, String path, int id) {
		this.name = name;
		this.id = id;
		vulkanImage = Vulkan.createTextureFromPath(path);
	}

	public int getWidth() {
		return vulkanImage.width;
	}

	public int getHeight() {
		return vulkanImage.height;
	}
}
