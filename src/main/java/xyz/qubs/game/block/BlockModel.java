package xyz.qubs.game.block;

import xyz.qubs.engine.vulkan.vertices.IVertex;
import xyz.qubs.engine.vulkan.vertices.IVertexTransparent;

public class BlockModel {
	public final IVertex[] vertices;
	public final int[] indices;
	public final IVertexTransparent[] verticesTransparent;
	public final int[] indicesTransparent;

	public BlockModel(IVertex[] vertices, int[] indices) {
		this.vertices = vertices;
		this.indices = indices;
		verticesTransparent = new IVertexTransparent[0];
		indicesTransparent = new int[0];
	}

	public BlockModel(IVertex[] vertices, int[] indices, IVertexTransparent[] verticesTransparent, int[] indicesTransparent) {
		this.vertices = vertices;
		this.indices = indices;
		this.verticesTransparent = verticesTransparent;
		this.indicesTransparent = indicesTransparent;
	}
}
