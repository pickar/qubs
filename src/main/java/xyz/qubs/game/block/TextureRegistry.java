package xyz.qubs.game.block;

import xyz.qubs.engine.vulkan.models.VulkanImage;

import java.util.HashMap;
import java.util.Map;

public class TextureRegistry {
	//TODO: refactor this plz
	private static final Map<String, BlockTexture> textureMap = new HashMap<>();
	private static final Map<Integer, BlockTexture> textureMapId = new HashMap<>();
	public static BlockTexture errorTexture;
	public static BlockTexture selectionTexture;
	public static BlockTexture crossTexture;
	private static int id = 0;

	private TextureRegistry() {
	}

	public static void init() {
		errorTexture = createNewTexture("error", "error_texture.png");
		selectionTexture = createNewTexture("selection", "blocks/selection.png");
		crossTexture = createNewTexture("cross", "cross.png");
	}

	public static BlockTexture createNewTexture(String name, String path) {
		if (textureMap.containsKey(name)) {
			throw new RuntimeException("Texture with that name already exists!");
		}
		BlockTexture texture = new BlockTexture(name, path, id++);
		textureMap.put(name, texture);
		textureMapId.put(id, texture);
		return texture;
	}

	public static BlockTexture getTexture(String name) {
		if (textureMap.containsKey(name)) {
			return textureMap.get(name);
		}
		return errorTexture;
	}

	public static int getTextureId(String name) {
		if (textureMap.containsKey(name)) {
			return textureMap.get(name).id;
		}
		return 0;
	}

	public static BlockTexture getTexture(int id) {
		if (textureMapId.containsKey(id)) {
			return textureMapId.get(id);
		}
		return errorTexture;
	}

	public static VulkanImage[] getVulkanImageArray() {
		VulkanImage[] images = new VulkanImage[textureMapId.values().size()];
		int index = 0;
		for (BlockTexture value : textureMapId.values()) {
			images[index++] = value.vulkanImage;
		}
		return images;
	}

	public static void dispose() {
		textureMap.forEach((name, texture) -> texture.vulkanImage.free());
	}
}
