package xyz.qubs.game.block;

import xyz.qubs.engine.model.Side;
import xyz.qubs.game.transport.energy.EnergyNet;
import xyz.qubs.game.transport.energy.IEnergyContainer;

public class OperationalBlock implements IEnergyContainer, IBlock {
	private final Position position = new Position(0, 0, 0);
	private final int lastActive = 0;
	public int TICKS_PER_OPERATION = 20;
	public int ENERGY_PER_OPERATION = 500;
	public int ENERGY_PER_TICK = ENERGY_PER_OPERATION / TICKS_PER_OPERATION;
	public int currentEnergy = 0;
	public int currentOperationTick = 0;
	public int incomeRate = 200;
	public int outcomeRate = 0;
	public int maxEnergy = 100000;
	public EnergyNet<OperationalBlock> energyNet;
	public int operationsMade = 0;
	public int operationsNeeded = 200;

	public boolean hasWork() {
		return operationsMade < operationsNeeded;
	}

	private void doOperation() {
		operationsMade++;
	}

	public boolean tick() {
		boolean hasWork = hasWork();
		if (hasWork) {
			if (currentEnergy >= ENERGY_PER_TICK) {
				currentEnergy -= ENERGY_PER_TICK;
				currentOperationTick++;
				if (currentOperationTick == TICKS_PER_OPERATION) {
					currentOperationTick = 0;
					doOperation();
				}
			}
		}
		return hasWork;
	}

	/*
	public void catchUp(int ticksPassed) {
		while (ticksPassed > 0 && currentOperationTick != 0 && tick()) {
			ticksPassed--;
		}
		if (hasWork() && ticksPassed >= TICKS_PER_OPERATION && currentEnergy >= ENERGY_PER_OPERATION) {
			int operations = Math.min(ticksPassed/TICKS_PER_OPERATION, currentEnergy/ENERGY_PER_OPERATION);

			currentEnergy -= ENERGY_PER_OPERATION*operations;
			ticksPassed -= TICKS_PER_OPERATION*operations;

			for (; operations > 0; operations--) {
				if (!hasWork()) break;
				doOperation();
			}

			currentEnergy += ENERGY_PER_OPERATION*operations;
			ticksPassed += TICKS_PER_OPERATION*operations;
		}
		while (ticksPassed > 0 && tick()) {
			ticksPassed--;
		}
	}

	public void load(int currentTicks) {
		catchUp(currentTicks - lastActive);
	}

	public void unload(int currentTicks) {
		lastActive = currentTicks;
	}
	*/

	@Override
	public int consumeEnergy(int amount) {
		int consumed = amount;
		if (currentEnergy - amount < 0) {
			consumed = currentEnergy;
		}
		currentEnergy -= consumed;
		return consumed;
	}

	@Override
	public int receiveEnergy(int amount) {
		int received = amount;
		if (currentEnergy + amount > getMaxEnergy()) {
			received = getMaxEnergy() - currentEnergy;
		}
		currentEnergy += received;
		return received;
	}

	@Override
	public int getEnergy() {
		return currentEnergy;
	}

	@Override
	public Position getPosition() {
		return position;
	}

	@Override
	public int getIncomeRate(Side side) {
		return incomeRate;
	}

	@Override
	public int getOutcomeRate(Side side) {
		return outcomeRate;
	}

	@Override
	public int getMaxEnergy() {
		return maxEnergy;
	}

	@Override
	public String toString() {
		return "Energy: " + getEnergy() + "/" + getMaxEnergy() + "; operationsMade: " + operationsMade + "/" + operationsNeeded;
	}
}
