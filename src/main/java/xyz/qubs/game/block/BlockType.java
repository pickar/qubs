package xyz.qubs.game.block;

import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import org.joml.Vector2f;
import org.joml.Vector3f;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.registry.IRegistryEntry;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.vulkan.vertices.GeneralVertex;
import xyz.qubs.game.collision.AABB;
import xyz.qubs.game.world.chunk.Chunk;

public abstract class BlockType implements IRegistryEntry {
	private static final Vector3f speedModifier = new Vector3f();
	private final String localizedName; //localization must be done from .lang files, not here!
	private final NamespacedName registryName;

	public BlockType(String localizedName, NamespacedName registryName) {
		this.localizedName = localizedName;
		this.registryName = registryName;
	}

	public static int blockAroundIndex(int x, int y, int z) {
		return x * 9 + y * 3 + z;
	}

	public boolean isFullBlock() {
		return true;
	}

	public BlockModel getModel(int x, int y, int z, Chunk chunk, BlockType[] blocksAround) {
		return null;
	}

	public BlockData<? extends BlockType> createData() {
		return null;
	}

	protected void addDataToBuffers(int[] quadIndices, GeneralVertex[] quadVertices, int textureId, byte[] ao, IntList indices, ObjectList<GeneralVertex> vertices) {
		for (int quadIndex : quadIndices) {
			indices.add(vertices.size() + quadIndex);
		}
		for (int k = 0; k < 4; k++) {
			Vector3f position = new Vector3f(quadVertices[k].getPos());
			Vector2f texture = new Vector2f(quadVertices[k].getTexCoord());

			vertices.add(new GeneralVertex(position, texture, (textureId & 0x3fffffff) | (ao[k] << 30))); //TODO: remove vectors
		}
	}

	public BlockTexture getSidedTexture(Side side) {
		return null;
	}

	public String getLocalizedName() {
		return localizedName;
	}

	public boolean hasData() {
		return false;
	}

	public NamespacedName getRegistryName() {
		return registryName;
	}

	public Vector3f getSpeedModifier() {
		return speedModifier;
	}

	public boolean isAir() {
		return false;
	}

	public boolean isTransparent(Side side) {
		return false;
	}

	public boolean isPassable(Side side) {
		return false;
	}

	public AABB[] getAABBs() {
		return new AABB[0];
	}

	public boolean rotateTexture(Side side) {
		return false;
	}

	@Override
	public String toString() {
		return getRegistryName().toString();
	}
}
