package xyz.qubs.game.block;

public interface IBlock {
	Position getPosition();
}
