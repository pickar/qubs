package xyz.qubs.game.block;

public class Block {
	public final BlockPos blockPos;
	public final BlockType type;
	public BlockData<? extends BlockType> data;

	public Block(BlockPos blockPos, BlockType type) {
		this.type = type;
		this.blockPos = blockPos;
	}

	public Block(BlockPos blockPos, BlockType type, BlockData<? extends BlockType> data) {
		this.type = type;
		this.blockPos = blockPos;
		this.data = data;
	}
}
