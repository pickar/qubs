package xyz.qubs.game.world;

import it.unimi.dsi.fastutil.objects.*;
import org.joml.Vector3i;
import xyz.qubs.Engine;
import xyz.qubs.engine.Pair;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.vulkan.renderers.WorldRenderer;
import xyz.qubs.game.block.Block;
import xyz.qubs.game.block.BlockPos;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.block.BlockTypeRegistry;
import xyz.qubs.game.world.biome.BiomeRegistry;
import xyz.qubs.game.world.biome.IBiome;
import xyz.qubs.game.world.chunk.Chunk;
import xyz.qubs.game.world.chunk.ChunkPos;
import xyz.qubs.game.world.chunk.IChunk;
import xyz.qubs.game.world.dimension.DefaultDimension;
import xyz.qubs.game.world.dimension.IDimension;
import xyz.qubs.game.world.generation.GenerationState;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;

import static xyz.qubs.Engine.threadPoolGeneration;
import static xyz.qubs.Engine.threadPoolLighting;
import static xyz.qubs.engine.settings.SettingsManager.*;

public class World implements IWorld {
	public static final AtomicInteger chunksGenerated = new AtomicInteger();
	public static final AtomicInteger chunksLightened = new AtomicInteger();
	public static final int loadDistanceH = loadDistanceHReal * 2 + 3;
	public static final int loadDistanceV = loadDistanceVReal * 2 + 3;
	public static int worldCount = 0;
	public final ObjectList<Chunk> generationQueue1;
	public final ObjectList<Chunk> generationQueue2;
	public final ObjectList<Chunk> generationQueue21;
	public final ObjectList<Chunk> generationQueue3;
	public final ObjectList<Chunk> generationQueue4;
	public final ObjectList<Chunk> updateQueue;
	public final int id;
	public final Object2ObjectMap<ChunkPos, Chunk> chunksMap;
	private final ObjectList<ChunkPos> chunksToRemove = new ObjectArrayList<>();
	public Vector3i localCenter;
	public long seed;
	public Vector3i shift;
	IDimension dimension = new DefaultDimension();

	public World() {
		dimension.getGenerator().init();

		id = worldCount++;
		shift = new Vector3i();
		chunksMap = Object2ObjectMaps.synchronize(new Object2ObjectOpenHashMap<>());
		localCenter = new Vector3i(loadDistanceH / 2, loadDistanceV / 2, loadDistanceH / 2);

		generationQueue1 = new ObjectArrayList<>();
		generationQueue2 = ObjectLists.synchronize(new ObjectArrayList<>());
		generationQueue21 = ObjectLists.synchronize(new ObjectArrayList<>());
		generationQueue3 = ObjectLists.synchronize(new ObjectArrayList<>());
		generationQueue4 = ObjectLists.synchronize(new ObjectArrayList<>());
		updateQueue = new ObjectArrayList<>();

		Engine.camera.movePosition(160, loadDistanceV << (IChunk.LOG2_CHUNK_SIZE - 2), 0);
		shiftChunks(loadDistanceH / 2 - 10, loadDistanceV / 4, loadDistanceH / 2);
	}

	public static int index(int x, int y, int z) {
		return x * loadDistanceH * loadDistanceV + y * loadDistanceH + z;
	}

	public static Pair<ChunkPos, BlockPos> convertToLocal(final int x, final int y, final int z) {
		return new Pair<>(new ChunkPos(x >> IChunk.LOG2_CHUNK_SIZE, y >> IChunk.LOG2_CHUNK_SIZE, z >> IChunk.LOG2_CHUNK_SIZE), BlockPos.get(x & (IChunk.CHUNK_SIZE - 1), y & (IChunk.CHUNK_SIZE - 1), z & (IChunk.CHUNK_SIZE - 1)));
	}

	@Override
	public IDimension getDimension() {
		return dimension;
	}

	public Chunk getChunk(int x, int y, int z) {
		return chunksMap.get(new ChunkPos(x, y, z));
	}

	public Chunk getChunk(ChunkPos pos) {
		return chunksMap.get(pos);
	}

	@Override
	public Block getBlock(int x, int y, int z) {
		final Pair<ChunkPos, BlockPos> positions = convertToLocal(x, y, z);
		final Chunk chunk = getChunk(positions.getKey());
		if (chunk != null) {
			return chunk.getBlock(positions.getValue());
		}
		return new Block(BlockPos.get(x, y, z), BlockTypeRegistry.AIR);
	}

	public void shiftChunks(int x, int y, int z) {
		shift.sub(x, y, z);
		WorldRenderer.worldMoved = true;
		WorldRenderer.moved.add(x, y, z);
		WorldRenderer.updateFrustum(true);
		generate();
	}

	@Override
	public BlockType getBlockType(int x, int y, int z) {
		final Pair<ChunkPos, BlockPos> positions = convertToLocal(x, y, z);
		final Chunk chunk = getChunk(positions.getKey());
		if (chunk != null) {
			return chunk.getBlockType(positions.getValue());
		}
		return BlockTypeRegistry.AIR;
	}

	@Override
	public IBiome getBiome(int x, int y, int z) {
		Pair<ChunkPos, BlockPos> positions = convertToLocal(x, y, z);
		final Chunk chunk = getChunk(positions.getKey());
		if (chunk != null) {
			return chunk.getBiome(positions.getValue());
		}
		return BiomeRegistry.PLAINS;
	}

	@Override
	public long getSeed() {
		return 0;
	}

	@Override
	public void update() {
		chunksToRemove.clear();
		synchronized (chunksMap) {
			for (Chunk chunk : chunksMap.values()) {
				if (chunk != null && !chunk.disposed) {
					if (chunk.getChunkPos().x - shift.x >= 0 && chunk.getChunkPos().x - shift.x < loadDistanceH &&
					    chunk.getChunkPos().y - shift.y >= 0 && chunk.getChunkPos().y - shift.y < loadDistanceV &&
					    chunk.getChunkPos().z - shift.z >= 0 && chunk.getChunkPos().z - shift.z < loadDistanceH) {
						chunk.ticksToRemoval = Engine.UPDATES_PER_SECOND;
					} else {
						if (--chunk.ticksToRemoval == 0) {
							chunk.dispose();
							chunksToRemove.add(chunk.getChunkPos());
						}
					}
				}
			}

			for (ChunkPos chunkPos : chunksToRemove) {
				chunksMap.remove(chunkPos);
			}
		}
		synchronized (generationQueue1) {
			generationQueue1.removeIf(Chunk::isDisposed);
			generationQueue1.unstableSort(Comparator.comparingDouble(Chunk::getDistanceSquaredToCamera));
		}
		while (!generationQueue1.isEmpty() && chunksGenerated.get() < maxChunksGenerated) {
			final Chunk chunk;
			synchronized (generationQueue1) {
				chunk = generationQueue1.remove(0);
			}
			if (!chunk.disposed) {
				chunksGenerated.getAndIncrement();
				if (threadPoolGeneration.isShutdown()) { return; }
				threadPoolGeneration.execute(() -> {
					if (Vulkan.disposed) {
						chunksGenerated.getAndDecrement();
						return;
					}
					getDimension().getGenerator().generateBiomes(chunk);
					getDimension().getGenerator().generateFiller(chunk);
					chunk.floodFill();
					chunk.generationState.set(GenerationState.GENERATED_FILLER);
					chunksGenerated.getAndDecrement();
//					if (chunk.markedForUpdate.compareAndSet(false, true)) {
//						updateQueue.add(chunk);
//					}

					final ChunkPos[] chunkPosArray = new ChunkPos[125];
					int index = 0;
					for (int i = 0; i < 5; i++) {
						for (int j = 0; j < 5; j++) {
							for (int k = 0; k < 5; k++) {
								chunkPosArray[index++] = new ChunkPos(chunk.getChunkPos().x - 2 + i, chunk.getChunkPos().y - 2 + j, chunk.getChunkPos().z - 2 + k);
							}
						}
					}

					for (int x = -1; x <= 1; x++) {
						for (int y = -1; y <= 1; y++) {
							for (int z = -1; z <= 1; z++) {
								Chunk updChunk = getChunk(chunkPosArray[(x + 2) * 25 + (y + 2) * 5 + z + 2]);
								if (updChunk != null) {
									if (updChunk.generationState.get() == GenerationState.GENERATED_FILLER) {
										boolean allAroundReady = true;
										out:
										for (int x1 = -1; x1 <= 1; x1++) {
											for (int y1 = -1; y1 <= 1; y1++) {
												for (int z1 = -1; z1 <= 1; z1++) {
													Chunk aroundChunk = getChunk(chunkPosArray[(x + x1 + 2) * 25 + (y + y1 + 2) * 5 + z + z1 + 2]);
													if (aroundChunk == null || aroundChunk.generationState.get().ordinal() < GenerationState.GENERATED_FILLER.ordinal()) {
														allAroundReady = false;
														break out;
													}
												}
											}
										}
										if (allAroundReady && updChunk.generationState.compareAndSet(GenerationState.GENERATED_FILLER, GenerationState.GENERATING_BLOCKS)) {
											synchronized (generationQueue21) {
												generationQueue21.add(updChunk);
											}
										}
									}
//									if (updChunk.markedForUpdate.compareAndSet(false, true)) {
//										updateQueue.add(updChunk);
//									}
								}
							}
						}
					}
				});
			}
		}
		synchronized (generationQueue21) {
			generationQueue2.addAll(generationQueue21);
			generationQueue21.clear();
		}

		generationQueue2.removeIf(Chunk::isDisposed);
		generationQueue2.unstableSort(Comparator.comparingDouble(Chunk::getDistanceSquaredToCamera));
		while (!generationQueue2.isEmpty() && chunksGenerated.get() < maxChunksGenerated) {
			Chunk chunk = generationQueue2.remove(0);
			if (!chunk.disposed && chunk.generationState.get() == GenerationState.GENERATING_BLOCKS) {
				chunksGenerated.getAndIncrement();
				if (threadPoolGeneration.isShutdown()) { return; }
				threadPoolGeneration.execute(() -> {
					if (Vulkan.disposed) {
						chunksGenerated.getAndDecrement();
						return;
					}
					getDimension().getGenerator().generateBlocks(chunk);
					getDimension().getGenerator().generateFeatures(chunk);
					chunk.generationState.set(GenerationState.GENERATED_BLOCKS);
					chunksGenerated.getAndDecrement();
					if (chunk.markedForUpdate.compareAndSet(false, true)) {
						synchronized (updateQueue) {
							updateQueue.add(chunk);
						}
					}

					for (int x = -1; x < 2; x++) {
						for (int y = -1; y < 2; y++) {
							for (int z = -1; z < 2; z++) {
								Chunk updChunk = getChunk(x + chunk.getChunkPos().x, y + chunk.getChunkPos().y, z + chunk.getChunkPos().z);
								if (updChunk != null) {
									if (updChunk.markedForUpdate.compareAndSet(false, true)) {
										synchronized (updateQueue) {
											updateQueue.add(updChunk);
										}
									}
								}
							}
						}
					}
				});
			}
		}

		synchronized (updateQueue) {
			updateQueue.removeIf(Chunk::isDisposed);
			updateQueue.unstableSort(Comparator.comparingDouble(Chunk::getDistanceSquaredToCamera));
		}
		while (!updateQueue.isEmpty() && chunksLightened.get() < maxChunksLightened) {
			final Chunk chunk;
			synchronized (updateQueue) {
				chunk = updateQueue.remove(0);
			}
			if (!chunk.disposed && chunk.markedForUpdate.compareAndSet(true, false) && chunk.generationState.get().last()) {
				if (chunk.isAir()) {
					chunk.updatedBuffers = true;
				} else {
					chunksLightened.getAndIncrement();
					if (threadPoolLighting.isShutdown()) { return; }
					threadPoolLighting.execute(() -> {
						if (Vulkan.disposed) {
							return;
						}
						chunk.makeFullChunkMesh();
						chunksLightened.getAndDecrement();
						if (chunk.indexCount > 0 && WorldRenderer.ChunkCuller.frustumIntersection.checkChunk(chunk)) {
							WorldRenderer.updateFrustum(false);
						}
					});
				}
			}
		}
	}

	@Override
	public void setBlockType(int x, int y, int z, BlockType blockType) {
		Pair<ChunkPos, BlockPos> positions = convertToLocal(x, y, z);
		final Chunk chunk = chunksMap.get(positions.getKey());
		if (chunk != null && chunk.getBlockType(positions.getValue()) != blockType) {
			chunk.setBlockType(positions.getValue(), blockType);
//			chunk.saveToDisk();
			chunk.updateModel(positions.getValue().x, positions.getValue().y, positions.getValue().z);
//			chunk.makeFullChunkMesh();
			Chunk updChunk;
			ChunkPos updPos;
			if (positions.getValue().x == 0) {
				updPos = new ChunkPos(positions.getKey());
				updPos.x -= 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().x == IChunk.CHUNK_SIZE - 1) {
				updPos = new ChunkPos(positions.getKey());
				updPos.x += 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().y == 0) {
				updPos = new ChunkPos(positions.getKey());
				updPos.y -= 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().y == IChunk.CHUNK_SIZE - 1) {
				updPos = new ChunkPos(positions.getKey());
				updPos.y += 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().z == 0) {
				updPos = new ChunkPos(positions.getKey());
				updPos.z -= 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().z == IChunk.CHUNK_SIZE - 1) {
				updPos = new ChunkPos(positions.getKey());
				updPos.z += 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
		}
	}

	@Override
	public void setBlockTypeWithoutUpdate(int x, int y, int z, BlockType block) {
		Pair<ChunkPos, BlockPos> positions = convertToLocal(x, y, z);
		final Chunk chunk = chunksMap.get(positions.getKey());
		if (chunk != null && chunk.getBlockType(positions.getValue()) != block) {
			chunk.setBlockType(positions.getValue(), block);
		}
	}

	public void setBlock(int x, int y, int z, Block block) {
		Pair<ChunkPos, BlockPos> positions = convertToLocal(x, y, z);

		final Chunk chunk = chunksMap.get(positions.getKey());
		if (chunk != null) {
			chunk.setBlock(block);
//			chunk.saveToDisk();
			chunk.makeFullChunkMesh();
			Chunk updChunk;
			ChunkPos updPos;
			if (positions.getValue().x == 0) {
				updPos = new ChunkPos(positions.getKey());
				updPos.x -= 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().x == IChunk.CHUNK_SIZE - 1) {
				updPos = new ChunkPos(positions.getKey());
				updPos.x += 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().y == 0) {
				updPos = new ChunkPos(positions.getKey());
				updPos.y -= 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().y == IChunk.CHUNK_SIZE - 1) {
				updPos = new ChunkPos(positions.getKey());
				updPos.y += 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().z == 0) {
				updPos = new ChunkPos(positions.getKey());
				updPos.z -= 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
			if (positions.getValue().z == IChunk.CHUNK_SIZE - 1) {
				updPos = new ChunkPos(positions.getKey());
				updPos.z += 1;
				updChunk = chunksMap.get(updPos);
				if (updChunk != null) {
					updChunk.makeFullChunkMesh();
				}
			}
		}
	}

	public void generate() {
		final ObjectList<Chunk> chunksToUpdate = new ObjectArrayList<>();
		ChunkPos chunkPos;
		Chunk chunk;
		for (int i = 0; i < loadDistanceH; i++) {
			for (int j = 0; j < loadDistanceV; j++) {
				for (int k = 0; k < loadDistanceH; k++) {
					chunkPos = new ChunkPos(shift.x + i, shift.y + j, shift.z + k);
					if (!chunksMap.containsKey(chunkPos)) {
						chunk = new Chunk(chunkPos, this);
						chunksToUpdate.add(chunk);
					}
				}
			}
		}
		synchronized (chunksMap) {
			for (Chunk chunk1 : chunksToUpdate) {
				chunksMap.put(chunk1.getChunkPos(), chunk1);
			}
		}
		synchronized (generationQueue1) {
			generationQueue1.addAll(chunksToUpdate);
		}
	}

	public void dispose() {
		for (Chunk chunk : chunksMap.values()) {
			chunk.dispose();
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		World world = (World) o;

		return id == world.id;
	}

	@Override
	public int hashCode() {
		return id;
	}
}
