package xyz.qubs.game.world;

import xyz.qubs.game.block.Block;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.world.biome.IBiome;
import xyz.qubs.game.world.chunk.Chunk;
import xyz.qubs.game.world.chunk.ChunkPos;
import xyz.qubs.game.world.dimension.IDimension;

public interface IWorld {
	IDimension getDimension();

	Chunk getChunk(int x, int y, int z);

	Chunk getChunk(ChunkPos pos);

	Block getBlock(int x, int y, int z);

	BlockType getBlockType(int x, int y, int z); //TODO: add isAir check that gives false when chunk is not loaded, fixes NPE when chunk unloads during feature check

	IBiome getBiome(int x, int y, int z);

	long getSeed();

	void update();

	void setBlockType(int x, int y, int z, BlockType blockType);

	void setBlockTypeWithoutUpdate(int x, int y, int z, BlockType blockType);

	default void replaceBlockType(int x, int y, int z, BlockType expectedBlockType, BlockType blockType) {
		if (getBlockType(x, y, z) == expectedBlockType) {
			setBlockType(x, y, z, blockType);
		}
	}

	default void replaceBlockTypeWithoutUpdate(int x, int y, int z, BlockType expectedBlockType, BlockType blockType) {
		if (getBlockType(x, y, z) == expectedBlockType) {
			setBlockTypeWithoutUpdate(x, y, z, blockType);
		}
	}

	//TODO: chunk storages to render different sets of chunks in 1 world
	//TODO: getTileEntity? OperationalBlock?
	//TODO: mobs, players, items, ...
	//TODO: long getTicksPassed();
	//TODO: weather
}
