package xyz.qubs.game.world.chunk;

import org.joml.Vector3i;

import static xyz.qubs.game.world.chunk.Chunk.CHUNK_SIZE;

public class ChunkPos {
	public int x;
	public int y;
	public int z;

	public ChunkPos(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public ChunkPos(Vector3i vector) {
		this.x = vector.x;
		this.y = vector.y;
		this.z = vector.z;
	}

	public ChunkPos(ChunkPos pos) {
		this.x = pos.x;
		this.y = pos.y;
		this.z = pos.z;
	}

	public static ChunkPos fromGlobal(Vector3i vector) {
		return new ChunkPos(vector.x & (CHUNK_SIZE - 1), vector.y & (CHUNK_SIZE - 1), vector.z & (CHUNK_SIZE - 1));
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ", " + z + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		ChunkPos chunkPos = (ChunkPos) o;

		if (x != chunkPos.x) { return false; }
		if (y != chunkPos.y) { return false; }
		return z == chunkPos.z;
	}

	@Override
	public int hashCode() {
		return (x << 30) + (y << 20) + (z << 10);
	}
}
