package xyz.qubs.game.world.chunk;

import xyz.qubs.engine.data.IExtendable;
import xyz.qubs.engine.utils.Maths;
import xyz.qubs.game.block.Block;
import xyz.qubs.game.block.BlockPos;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.world.World;
import xyz.qubs.game.world.biome.IBiome;

public interface IChunk extends IExtendable {
	int CHUNK_SIZE = 16;
	int LOG2_CHUNK_SIZE = Maths.log2(CHUNK_SIZE);
	int MAX_LOD_LEVEL = LOG2_CHUNK_SIZE;

	ChunkPos getChunkPos();

	boolean isAir();

	IBiome getBiome(int x, int y, int z);

	default IBiome getBiome(BlockPos pos) {
		return getBiome(pos.x, pos.y, pos.z);
	}

	void setBiome(int x, int y, int z, IBiome biome);

	default void setBiome(BlockPos pos, IBiome biome) {
		setBiome(pos.x, pos.y, pos.z, biome);
	}

	Block getBlock(int x, int y, int z);

	default Block getBlock(BlockPos pos) {
		return getBlock(pos.x, pos.y, pos.z);
	}

	BlockType getBlockType(int x, int y, int z);

	default BlockType getBlockType(BlockPos pos) {
		return getBlockType(pos.x, pos.y, pos.z);
	}

	void setBlockType(int x, int y, int z, BlockType blockType);

	default void setBlockType(BlockPos pos, BlockType blockType) {
		setBlockType(pos.x, pos.y, pos.z, blockType);
	}

	long getSeed();

	void setBlocks(BlockType[] blocks);

	World getWorld();

	BlockType[] getBlockTypeArray();

	void update();
}
