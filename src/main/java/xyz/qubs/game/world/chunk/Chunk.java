package xyz.qubs.game.world.chunk;

import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.*;
import org.lwjgl.glfw.GLFW;
import xyz.qubs.Engine;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.data.ExtendedDataRegistry;
import xyz.qubs.engine.data.IExtendedData;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.vulkan.models.VulkanBuffer;
import xyz.qubs.engine.vulkan.renderers.WorldRenderer;
import xyz.qubs.engine.vulkan.vertices.IVertex;
import xyz.qubs.game.block.*;
import xyz.qubs.game.world.World;
import xyz.qubs.game.world.biome.BiomeRegistry;
import xyz.qubs.game.world.biome.IBiome;
import xyz.qubs.game.world.generation.GenerationState;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Chunk implements IChunk {
	private static final ObjectList<FloodFillInfo> startingPoints = new ObjectArrayList<>();

	static {
		int colorId = 0;
		int index = 0;
		for (int x = 0; x < CHUNK_SIZE; x++) {
			for (int y = 0; y < CHUNK_SIZE; y++) {
				for (int z = 0; z < CHUNK_SIZE; z++) {
					if (x == 0 || x == CHUNK_SIZE - 1 || y == 0 || y == CHUNK_SIZE - 1 || z == 0 || z == CHUNK_SIZE - 1) {
						startingPoints.add(0, new FloodFillInfo(x, y, z, index, ++colorId));
					}
					index++;
				}
			}
		}
		Engine.addHook(GLFW.GLFW_KEY_T, () -> () -> System.out.println(Engine.camera.getChunk().getChunkPos()));
	}

	public final int chunkX;
	public final int chunkY;
	public final int chunkZ;
	public final AtomicBoolean markedForUpdate = new AtomicBoolean(false);
	public final AtomicReference<GenerationState> generationState = new AtomicReference<>(GenerationState.PENDING);
	private final long seed;
	private final World world;
	//	private final boolean[] blocksAround; //can be cached
	private final ChunkPos chunkPos;
	private final IExtendedData[] extendedData;
	//	private int[] floodFilledColor; //TODO: make this take less space (max color for 16*16*16 chunk is 1352, max effective color is even less(2-3))
	private final boolean isAir = true;
	private final Object2ObjectMap<BlockPos, Block> blocksWithData;
	private final Object2ObjectMap<BlockPos, BlockModel> modelMap;
	public RenderData renderData;
	public boolean updatedBuffers;
	public boolean[][] sideConnections; //TODO: compress
	public Int2ObjectMap<EnumSet<Side>> connectionsMap;
	public boolean visited = false;
	public int ticksToRemoval = 20;
	public int indexCount = 0;
	public boolean disposed;
	private IBiome[] biomes;
	private BlockType[] blocks;
	private long chunkSeed;
	private boolean isOneBiome = true;
	private IBiome onlyBiome;
	private boolean isOneBlockType = true;
	private BlockType onlyBlockType;

	public Chunk(ChunkPos chunkPos, World world) {
		extendedData = new IExtendedData[ExtendedDataRegistry.INSTANCE.getEntryCountByType(IChunk.class)];
		blocksWithData = new Object2ObjectOpenHashMap<>(0);
		modelMap = Object2ObjectMaps.synchronize(new Object2ObjectOpenHashMap<>());

		this.chunkPos = chunkPos;
//		this.blocks = new BlockType[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
//		this.biomes = new IBiome[blocks.length];
		this.world = world;
		updatedBuffers = false;
		disposed = false;
		renderData = new RenderData();
		chunkX = chunkPos.x << LOG2_CHUNK_SIZE;
		chunkY = chunkPos.y << LOG2_CHUNK_SIZE;
		chunkZ = chunkPos.z << LOG2_CHUNK_SIZE;

		sideConnections = new boolean[6][6];
//		floodFilledColor = new int[blocks.length];
		connectionsMap = new Int2ObjectArrayMap<>();

		seed = world.getDimension().getGenerator().generateChunkSeed(this);
	}

	public static int index(int x, int y, int z) {
		return x << LOG2_CHUNK_SIZE * 2 | y << LOG2_CHUNK_SIZE | z;
	}

	public static int blockAroundIndex(int x, int y, int z) {
		return x * (CHUNK_SIZE + 2) * (CHUNK_SIZE + 2) + y * (CHUNK_SIZE + 2) + z;
	}

	public boolean isAir() {
		return isOneBlockType && onlyBlockType == BlockTypeRegistry.AIR;
	}

	public boolean isDisposed() {
		return disposed;
	}

	public void floodFill() {
		sideConnections = new boolean[6][6];
		boolean[] floodFilled = new boolean[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
//		floodFilledColor = new int[blocks.length];
		connectionsMap = new Int2ObjectArrayMap<>();

		Stack<FloodFillInfo> stack = new Stack<>();
		stack.addAll(startingPoints.stream().filter(floodFillInfo -> getBlockType(floodFillInfo.index).isAir()).collect(Collectors.toList()));
		if (stack.isEmpty()) { return; }

		FloodFillInfo info;
		int x, y, z, index, lastColor = stack.peek().color;
		EnumSet<Side> sides = EnumSet.noneOf(Side.class);
		int countFilled = 0;
		BlockType blockType;
		while (!stack.isEmpty()) {
			info = stack.pop();

			if (info.color != lastColor) {
				if (countFilled > 0) {
					for (Side side1 : sides) {
						for (Side side2 : sides) {
							if (side1 != side2) {
								sideConnections[side1.ordinal()][side2.ordinal()] = true;
							}
						}
					}
					connectionsMap.put(lastColor, sides);
					sides = EnumSet.noneOf(Side.class);
				}

				lastColor = info.color;
				countFilled = 0;
			}

			if (info.x == 0) { sides.add(Side.LEFT); }
			if (info.x == CHUNK_SIZE - 1) { sides.add(Side.RIGHT); }
			if (info.y == 0) { sides.add(Side.BOTTOM); }
			if (info.y == CHUNK_SIZE - 1) { sides.add(Side.TOP); }
			if (info.z == 0) { sides.add(Side.BACK); }
			if (info.z == CHUNK_SIZE - 1) { sides.add(Side.FRONT); }

			if (!floodFilled[info.index]) {
//				floodFilledColor[info.index] = -1;
				floodFilled[info.index] = true;
				countFilled++;
			}

			for (Side side : Side.values) {
				x = info.x + side.x;
				y = info.y + side.y;
				z = info.z + side.z;

				if (x >= 0 && x < CHUNK_SIZE && y >= 0 && y < CHUNK_SIZE && z >= 0 && z < CHUNK_SIZE) {
					index = index(x, y, z);
					if (!floodFilled[index]) {
						blockType = getBlockType(index);
						if (blockType == null || blockType.isAir()) {
							stack.push(new FloodFillInfo(x, y, z, index, lastColor));
//							floodFilledColor[index] = -1;
						} else {
//							floodFilledColor[index] = lastColor;
						}
						floodFilled[index] = true;
						countFilled++;
					}
				}
			}
			info = null;
		}
		floodFilled = null;
		//TODO: transparent blocks support
		//TODO: update this when needed
		//TODO: use this to split index/vertex buffer into parts and render only part that can be actually seen
		//TODO: e.g. if color 1 touches sides TOP and BACK render only if path came from that sides and not render otherwise
		//TODO: render color 0 only if player is in that chunk
		//TODO: not render color -1 because its air (can help with some checks maybe)
//		Map<Integer, Integer> map = Arrays.stream(floodFilledColor).boxed().collect(Collectors.toMap(Function.identity(), integer -> 1, Integer::sum));
//		synchronized (Engine.class) {
//			System.out.println(position.toString(NumberFormat.getIntegerInstance()));
//			System.out.println(map.toString());
//			System.out.println(connectionsMap.toString());
//		}
	}

	public boolean isConnected(Side from, Side to) {
		return sideConnections[from.ordinal()][to.ordinal()];
	}

	public double getDistanceSquaredToCamera() {
		return Engine.camera.getPosition().distanceSquared(chunkX + CHUNK_SIZE / 2f, chunkY + CHUNK_SIZE / 2f, chunkZ + CHUNK_SIZE / 2f);
	}

//	public void readFromDisk() {
//		byte[] bytes = new byte[0];
//		try {
//			bytes = Files.readAllBytes(Paths.get("saves/chunk_" + chunkPos.x + "_" + chunkPos.y + "_" + chunkPos.z + ".dat"));
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//		BlockType[] newBlocks = new BlockType[blocks.length];
//
//		BitReader reader = new BitReader(bytes);
//		final int bitsInIdCount = reader.readBits(6);
//		final int writeCount = reader.readBits(15);
//		reader.readBits(3); //skip n bits because it was written as two different buffers and after main info there is n zero bits
//
//		int currentWrite = 0;
//		int count;
//
//		BlockType currentType = BlockRegistry.getById(reader.readBits(bitsInIdCount));
//		if (reader.readBit() == 1) {
//			count = reader.readBits(reader.readBits(4));
//		} else {
//			count = 1;
//		}
//		for (int y = 0; y < CHUNK_SIZE; y++) {
//			for (int x = 0; x < CHUNK_SIZE; x++) {
//				for (int z = 0; z < CHUNK_SIZE; z++) {
//					newBlocks[index(x, y, z)] = currentType;
//					if (--count == 0 && ++currentWrite < writeCount) {
//						currentType = BlockRegistry.getById(reader.readBits(bitsInIdCount));
//						if (reader.readBit() == 1) {
//							count = reader.readBits(reader.readBits(4));
//						} else {
//							count = 1;
//						}
//					}
//				}
//			}
//		}
//		blocks = newBlocks;
//	}

//	public void saveToDisk() {
//		final BitBuffer bitBufferInfo = new BitBuffer();
//		final BitBuffer bitBufferBlocks = new BitBuffer();
//		final int bitsInIdCount = getMinBitCount(BlockRegistry.getCount());
//
//		bitBufferInfo.writeBits(6, bitsInIdCount);
//		BlockType lastType = blocks[0];
//		int count = 0;
//		int writeCount = 0;
//		for (int y = 0; y < CHUNK_SIZE; y++) {
//			for (int x = 0; x < CHUNK_SIZE; x++) {
//				for (int z = 0; z < CHUNK_SIZE; z++) {
//					final BlockType currentType = blocks[index(x, y, z)];
//					if (currentType == lastType) {
//						count++;
//					} else {
//						bitBufferBlocks.writeBits(bitsInIdCount, lastType.getId());
//						if (count == 1) {
//							bitBufferBlocks.writeBit(0);
//						} else {
//							bitBufferBlocks.writeBit(1);
//							final int bitCount = getMinBitCount(count);
//							bitBufferBlocks.writeBits(4, bitCount);
//							bitBufferBlocks.writeBits(bitCount, count);
//						}
//						writeCount++;
//						lastType = currentType;
//						count = 1;
//					}
//				}
//			}
//		}
//		bitBufferBlocks.writeBits(bitsInIdCount, lastType.getId());
//		if (count == 1) {
//			bitBufferBlocks.writeBit(0);
//		} else {
//			bitBufferBlocks.writeBit(1);
//			final int bitCount = getMinBitCount(count);
//			bitBufferBlocks.writeBits(4, bitCount);
//			bitBufferBlocks.writeBits(bitCount, count);
//		}
//		writeCount++;
//		bitBufferInfo.writeBits(15, writeCount);
//
//		try (FileOutputStream fos = new FileOutputStream("saves/chunk_" + chunkPos.x + "_" + chunkPos.y + "_" + chunkPos.z + ".dat")) {
//			fos.write(bitBufferInfo.toByteArray());
//			fos.write(bitBufferBlocks.toByteArray());
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//	}

	@Override
	public void setBlockType(int x, int y, int z, BlockType blockType) {
		if (isOneBlockType) {
			if (onlyBlockType == null) {
				onlyBlockType = blockType;
			} else {
				if (onlyBlockType != blockType) {
					blocks = new BlockType[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
					Arrays.fill(blocks, onlyBlockType);
					blocks[index(x, y, z)] = blockType;
					isOneBlockType = false;
				}
			}
		} else {
			final int index = index(x, y, z);
			final BlockType oldBlockType = blocks[index];
			final BlockPos pos = BlockPos.get(index);
			if (oldBlockType.hasData()) {
				blocksWithData.remove(pos);
			}
			blocks[index] = blockType;
			if (blockType.hasData()) {
				blocksWithData.put(pos, new Block(pos, blockType, blockType.createData()));
			}
		}
	}

	public void setBlock(Block block) {
		setBlock(block.blockPos.x, block.blockPos.y, block.blockPos.z, block);
	}

	public void setBlock(int x, int y, int z, Block block) {
		if (isOneBlockType && !block.type.hasData()) {
			if (onlyBlockType == null) {
				onlyBlockType = block.type;
			} else {
				if (onlyBlockType != block.type) {
					blocks = new BlockType[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
					Arrays.fill(blocks, onlyBlockType);
					blocks[index(x, y, z)] = block.type;
					isOneBlockType = false;
				}
			}
		} else {
			final int index = index(x, y, z);
			final BlockType oldBlockType = blocks[index];
			final BlockPos pos = BlockPos.get(index);
			if (oldBlockType.hasData()) {
				blocksWithData.remove(pos);
			}
			blocks[index] = block.type;

			if (block.type.hasData()) {
				if (block.data == null) {
					block.data = block.type.createData();
				}
				blocksWithData.put(pos, block);
			}
		}
	}

	@Override
	public ChunkPos getChunkPos() {
		return chunkPos;
	}

	@Override
	public IBiome getBiome(int x, int y, int z) {
		if (isOneBiome) {
			return onlyBiome != null ? onlyBiome : BiomeRegistry.PLAINS;
		}
		return biomes[index(x, y, z)];
	}

	@Override
	public void setBiome(int x, int y, int z, IBiome biome) {
		if (isOneBiome) {
			if (onlyBiome == null) {
				onlyBiome = biome;
			} else {
				if (onlyBiome != biome) {
					biomes = new IBiome[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
					Arrays.fill(biomes, onlyBiome);
					biomes[index(x, y, z)] = biome;
					isOneBiome = false;
				}
			}
		} else {
			biomes[index(x, y, z)] = biome;
		}
	}

	@Override
	public Block getBlock(int x, int y, int z) {
		final int index = index(x, y, z);
		final BlockPos pos = BlockPos.get(index);
		return blocksWithData.get(pos);
	}

	public BlockType getBlockType(int x, int y, int z) {
		if (isOneBlockType) {
			return onlyBlockType != null ? onlyBlockType : BlockTypeRegistry.AIR;
		}
		return blocks[index(x, y, z)];
	}

	public BlockType getBlockType(int index) {
		if (isOneBlockType) {
			return onlyBlockType != null ? onlyBlockType : BlockTypeRegistry.AIR;
		}
		return blocks[index];
	}

	@Override
	public long getSeed() {
		return seed;
	}

	@Override
	public void setBlocks(BlockType[] blocks) {
		isOneBlockType = true;
		onlyBlockType = blocks[0];
	}

	@Override
	public World getWorld() {
		return world;
	}

	@Override
	public void update() {

	}

	@Override
	public BlockType[] getBlockTypeArray() {
		if (isOneBlockType) {
			return new BlockType[] { onlyBlockType };
		}
		return blocks;
	}

	public void updateModel(int x, int y, int z) {
		if (disposed) {
			return;
		}
		BlockType[] localBlocksAround = new BlockType[27];
		int localIndex;
		BlockPos blockPos;
		BlockModel model;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				for (int k = -1; k <= 1; k++) {
					final int x1 = x + i;
					final int y1 = y + j;
					final int z1 = z + k;

					if (x1 >= 0 && x1 < CHUNK_SIZE && y1 >= 0 && y1 < CHUNK_SIZE && z1 >= 0 && z1 < CHUNK_SIZE) {
						blockPos = BlockPos.get(x1, y1, z1);
						modelMap.remove(blockPos);

						localIndex = 0;
						for (int x_local = -1; x_local <= 1; x_local++) {
							for (int y_local = -1; y_local <= 1; y_local++) {
								for (int z_local = -1; z_local <= 1; z_local++) {
									final int x2 = x1 + x_local;
									final int y2 = y1 + y_local;
									final int z2 = z1 + z_local;
									if (x2 < 0 || x2 >= CHUNK_SIZE || y2 < 0 || y2 >= CHUNK_SIZE || z2 < 0 || z2 >= CHUNK_SIZE) {
										localBlocksAround[localIndex++] = world.getBlockType(x2 + chunkX, y2 + chunkY, z2 + chunkZ);
									} else {
										localBlocksAround[localIndex++] = getBlockType(x2, y2, z2);
									}
								}
							}
						}

						model = localBlocksAround[13].getModel(x1, y1, z1, this, localBlocksAround);
						if (model != null && model.indices.length > 0) {
							modelMap.put(blockPos, model);
						}
					}
				}
			}
		}

		RenderData newData;
		if (isAir()) {
			newData = new RenderData();
		} else {
			newData = makeBuffers(modelMap);
		}
		int indexCount = 0;
		for (int i : newData.indexCount) {
			indexCount += i;
		}

		RenderData oldData = renderData;
		renderData = newData;
		this.indexCount = indexCount;
		if (oldData != null) {
			oldData.disposeBuffers();
		}

		if (disposed) {
			if (renderData != null) {
				renderData.disposeBuffers();
			}
		} else {
			floodFill();
			updatedBuffers = true;
			WorldRenderer.INSTANCE.setDirty(true);
		}
	}

	private synchronized RenderData generateFullMesh() {
		modelMap.clear();

		BlockType[] blocksAround = new BlockType[(CHUNK_SIZE + 2) * (CHUNK_SIZE + 2) * (CHUNK_SIZE + 2)];
		int index = 0;
		int index2 = 0;
		for (int x = -1; x < CHUNK_SIZE + 1; x++) {
			for (int y = -1; y < CHUNK_SIZE + 1; y++) {
				for (int z = -1; z < CHUNK_SIZE + 1; z++) {
					if (x < 0 || x >= CHUNK_SIZE || y < 0 || y >= CHUNK_SIZE || z < 0 || z >= CHUNK_SIZE) {
						blocksAround[index++] = world.getBlockType(x + chunkX, y + chunkY, z + chunkZ);
					} else {
						blocksAround[index++] = getBlockType(index2++);
					}
				}
			}
		}

		BlockType[] localBlocksAround = new BlockType[27];
		BlockModel model;
		int localIndex;
		index = -1;
		BlockType blockType;
		for (int x = 0; x < CHUNK_SIZE; x++) {
			for (int y = 0; y < CHUNK_SIZE; y++) {
				for (int z = 0; z < CHUNK_SIZE; z++) {
					blockType = getBlockType(++index);
					if (blockType.isAir()) {
						continue;
					}
					localIndex = 0;
					for (int i = -1; i <= 1; i++) {
						for (int j = -1; j <= 1; j++) {
							for (int k = -1; k <= 1; k++) {
								localBlocksAround[localIndex++] = blocksAround[blockAroundIndex(x + i + 1, y + j + 1, z + k + 1)];
							}
						}
					}
					model = blockType.getModel(x, y, z, this, localBlocksAround);
					if (model != null && (model.indices.length > 0 || model.indicesTransparent.length > 0)) {
						modelMap.put(BlockPos.get(index), model);
					}
				}
			}
		}

		return makeBuffers(modelMap);
	}

	private RenderData makeBuffers(Object2ObjectMap<BlockPos, BlockModel> map) {
		RenderData newData = new RenderData();
		if (map.size() == 0) {
			return newData;
		}
		int vertSize = 0;
		int indSize = 0;

		int vertSizeTransparent = 0;
		int indSizeTransparent = 0;
		ObjectCollection<BlockModel> models = map.values();
		for (BlockModel model : models) {
			vertSize += model.vertices.length;
			indSize += model.indices.length;

			vertSizeTransparent += model.verticesTransparent.length;
			indSizeTransparent += model.indicesTransparent.length;
		}
		IVertex[] vertices = new IVertex[vertSize];
		int[] indices = new int[indSize];

		IVertex[] verticesTransparent = new IVertex[vertSizeTransparent];
		int[] indicesTransparent = new int[indSizeTransparent];

		int indexI = 0;
		int indexV = 0;

		int indexITransparent = 0;
		int indexVTransparent = 0;

		for (BlockModel model : models) {
			for (int index : model.indices) {
				indices[indexI++] = indexV + index;
			}
			for (IVertex vertex : model.vertices) {
				vertices[indexV++] = vertex;
			}
			for (int index : model.indicesTransparent) {
				indicesTransparent[indexITransparent++] = indexVTransparent + index;
			}
			for (IVertex vertex : model.verticesTransparent) {
				verticesTransparent[indexVTransparent++] = vertex;
			}
		}

		if (indexI > 0 && !disposed) {
			newData.indexCount[0] = indexI;
			newData.vertexBuffer = Vulkan.createVertexBuffer(vertices);
			newData.indexBuffer = Vulkan.createIndexBuffer(indices);
		}
		if (indexITransparent > 0 && !disposed) {
			newData.indexCountTransparent[0] = indexITransparent;
			newData.vertexBufferTransparent = Vulkan.createVertexBuffer(verticesTransparent);
			newData.indexBufferTransparent = Vulkan.createIndexBuffer(indicesTransparent);
		}
		return newData;
	}

	public void makeFullChunkMesh() {
		if (disposed) {
			return;
		}

		RenderData newData;//generateBuffers(quadsList);
		if (isAir()) {
			newData = new RenderData();
		} else {
			newData = generateFullMesh();
		}
		int indexCount = 0;
		for (int i : newData.indexCount) {
			indexCount += i;
		}
		for (int i : newData.indexCountTransparent) {
			indexCount += i;
		}

		RenderData oldData = renderData;
		renderData = newData;
		this.indexCount = indexCount;
		if (oldData != null) {
			oldData.disposeBuffers();
		}

		if (disposed) {
			if (renderData != null) {
				renderData.disposeBuffers();
			}
		} else {
			floodFill();
			updatedBuffers = true;
			WorldRenderer.INSTANCE.setDirty(true);
		}
	}

	public boolean ready() {
		return generationState.get().last() && updatedBuffers && !disposed;
	}

	public void dispose() {
		disposed = true;
		if (renderData != null) {
			renderData.disposeBuffers();
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Chunk chunk = (Chunk) o;

		if (!Objects.equals(chunkPos, chunk.chunkPos)) {
			return false;
		}
		return Objects.equals(world, chunk.world);
	}

	@Override
	public int hashCode() {
		int result = chunkPos != null ? chunkPos.hashCode() : 0;
		result = 31 * result + (world != null ? world.hashCode() : 0);
		return result;
	}

	@Override
	public IExtendedData getData(int id) {
		return extendedData[id];
	}

	@Override
	public void setData(IExtendedData data) {
		extendedData[data.getHandlerInternalId()] = data;
	}

	private static class FloodFillInfo {
		int x;
		int y;
		int z;
		int index;
		int color;

		private FloodFillInfo(int x, int y, int z, int index, int color) {
			this.x = x;
			this.y = y;
			this.z = z;
			this.index = index;
			this.color = color;
		}
	}

	public static class RenderData {
		public VulkanBuffer vertexBuffer;
		public VulkanBuffer indexBuffer;
		public int[] firstIndex;
		public int[] indexCount;
		public int[] vertexOffset;

		public VulkanBuffer vertexBufferTransparent;
		public VulkanBuffer indexBufferTransparent;
		public int[] firstIndexTransparent;
		public int[] indexCountTransparent;
		public int[] vertexOffsetTransparent;

		public RenderData() {
			firstIndex = new int[MAX_LOD_LEVEL + 1];
			indexCount = new int[MAX_LOD_LEVEL + 1];
			vertexOffset = new int[MAX_LOD_LEVEL + 1];

			firstIndexTransparent = new int[MAX_LOD_LEVEL + 1];
			indexCountTransparent = new int[MAX_LOD_LEVEL + 1];
			vertexOffsetTransparent = new int[MAX_LOD_LEVEL + 1];
		}

		public synchronized void disposeBuffers() {
			if (vertexBuffer != null) {
				Vulkan.markForDisposal(vertexBuffer);
			}
			if (indexBuffer != null) {
				Vulkan.markForDisposal(indexBuffer);
			}
			if (vertexBufferTransparent != null) {
				Vulkan.markForDisposal(vertexBufferTransparent);
			}
			if (indexBufferTransparent != null) {
				Vulkan.markForDisposal(indexBufferTransparent);
			}
		}
	}
}
