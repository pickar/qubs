package xyz.qubs.game.world.biome;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.block.BlockTypeRegistry;
import xyz.qubs.game.world.generation.features.DesertFeature;
import xyz.qubs.game.world.generation.features.IFeature;

public class DesertBiome implements IBiome {
	private static final NamespacedName registryName = new NamespacedName("core", "desert");
	private static final ObjectList<IFeature> features = new ObjectArrayList<>();

	static {
		features.add(new DesertFeature());
	}

	@Override
	public int getBaseHeight() {
		return 0;
	}

	@Override
	public int getMaximalHeight() {
		return 0;
	}

	@Override
	public int getMinimalHeight() {
		return 0;
	}

	@Override
	public ObjectList<IFeature> getFeatures() {
		return features;
	}

	@Override
	public BlockType getTopFiller() {
		return BlockTypeRegistry.SAND;
	}

	@Override
	public BlockType getMiddleFiller() {
		return BlockTypeRegistry.SAND;
	}

	@Override
	public BlockType getBaseFiller() {
		return BlockTypeRegistry.STONE;
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}
}
