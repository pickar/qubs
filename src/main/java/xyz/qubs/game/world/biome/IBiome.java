package xyz.qubs.game.world.biome;

import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.engine.registry.IRegistryEntry;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.world.generation.features.IFeature;

public interface IBiome extends IRegistryEntry {
	int getBaseHeight();

	int getMaximalHeight();

	int getMinimalHeight();

	ObjectList<IFeature> getFeatures();

	BlockType getTopFiller();

	BlockType getMiddleFiller();

	BlockType getBaseFiller();

	//grass color
	//sky color?
	//fog color?
	//colored overlay?
}
