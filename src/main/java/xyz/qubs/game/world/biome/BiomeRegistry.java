package xyz.qubs.game.world.biome;

import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.registry.RegisterEntriesEvent;
import xyz.qubs.engine.registry.Registry;
import xyz.qubs.engine.registry.RegistryCreationEvent;

import java.lang.reflect.Type;

public class BiomeRegistry extends Registry<IBiome> {
	public static final IBiome PLAINS = new PlainsBiome();
	public static final IBiome DESERT = new DesertBiome();

	private static final BiomeRegistry INSTANCE = new BiomeRegistry(new NamespacedName("core", "biome"), IBiome.class);

	private BiomeRegistry(NamespacedName registryName, Type type) {
		super(registryName, type);
	}

	public static IBiome getBiomeByName(NamespacedName registryName) {
		return INSTANCE.getElement(registryName);
	}

	public static IBiome getBiomeByName(String fullName) {
		return INSTANCE.getElement(fullName);
	}

	@SubscribeEvent
	public static void onRegistryCreation(RegistryCreationEvent event) {
		event.registry.register(INSTANCE);
	}

	@SubscribeEvent
	public static void onRegisterEntriesIBiome(RegisterEntriesEvent<IBiome> event) {
		event.registry.register(PLAINS);
		event.registry.register(DESERT);
	}
}
