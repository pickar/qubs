package xyz.qubs.game.world.biome;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.block.BlockTypeRegistry;
import xyz.qubs.game.world.generation.features.IFeature;
import xyz.qubs.game.world.generation.features.TreeFeature;

public class PlainsBiome implements IBiome {
	private static final NamespacedName registryName = new NamespacedName("core", "plains");
	private static final ObjectList<IFeature> features = new ObjectArrayList<>();

	static {
		features.add(new TreeFeature());
	}

	@Override
	public int getBaseHeight() {
		return 0;
	}

	@Override
	public int getMaximalHeight() {
		return 0;
	}

	@Override
	public int getMinimalHeight() {
		return 0;
	}

	@Override
	public ObjectList<IFeature> getFeatures() {
		return features;
	}

	@Override
	public BlockType getTopFiller() {
		return BlockTypeRegistry.GRASS;
	}

	@Override
	public BlockType getMiddleFiller() {
		return BlockTypeRegistry.DIRT;
	}

	@Override
	public BlockType getBaseFiller() {
		return BlockTypeRegistry.STONE;
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}
}
