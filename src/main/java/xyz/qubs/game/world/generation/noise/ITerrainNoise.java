package xyz.qubs.game.world.generation.noise;

public interface ITerrainNoise {
	double evaluate(double x, double y);

	double evaluate(double x, double y, double z);

	default double fbm(double x, double y, double z, int octaves, double lacunarity, double gain) {
		double amplitude = 0.8;
		double frequency = 1.0;
		double sum = 0.0;
		for (int i = 0; i < octaves; ++i) {
			sum += amplitude * evaluate(x * frequency, y * frequency, z * frequency);
			amplitude *= gain;
			frequency *= lacunarity;
		}
		return sum;
	}

	default long generateSeed(double x, double y, double z) {
		long seed = (long) (evaluate(x / 8287.0d, y / 6857.0d, z / 733.0d) * 61257688L) + 12458926950408837L;
		seed *= (long) (evaluate(x / 2729.0d, y / 997.0d, z / 3779.0d) * 612576924188L) + 12458926837L;
		seed = seed * 612576814879188L + 12458950408837L;

		return seed;
	}
}
