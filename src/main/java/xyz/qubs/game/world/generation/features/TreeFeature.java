package xyz.qubs.game.world.generation.features;

import xyz.qubs.game.block.BlockTypeRegistry;
import xyz.qubs.game.world.IWorld;

public class TreeFeature implements IFeature {
	private static final int height = 7;
	private static final int radius = 3;

	@Override
	public float chance(IWorld world, int x, int y, int z) {
		return .002f;
	}

	@Override
	public boolean check(IWorld world, int x, int y, int z) {
		if (world.getBlockType(x, y - 1, z) != BlockTypeRegistry.GRASS) {
			return false;
		}
		if (!world.getBlockType(x, y, z).isAir()) {
			return false;
		}
		for (int y1 = 1; y1 < height; y1++) {
			if (!world.getBlockType(x, y + y1, z).isAir()) {
				return false;
			}
		}
		for (int x1 = -radius + 1; x1 <= radius - 1; x1++) {
			for (int z1 = -radius + 1; z1 <= radius - 1; z1++) {
				if (!world.getBlockType(x + x1, y + height - 1, z + z1).isAir()) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void generate(IWorld world, int x, int y, int z) {
		for (int y1 = 0; y1 < height; y1++) {
			world.setBlockTypeWithoutUpdate(x, y + y1, z, BlockTypeRegistry.OAK);
		}
		int size;
		for (int y1 = 3; y1 <= height; y1++) {
			if (y1 == 3 || y1 == height) {
				size = radius - 1;
			} else {
				size = radius;
			}
			for (int x1 = -size; x1 <= size; x1++) {
				for (int z1 = -size; z1 <= size; z1++) {
					if ((Math.abs(x1) != size || Math.abs(z1) != size) && (size != radius || (Math.abs(x1) != size || Math.abs(z1) != size - 1) && (Math.abs(z1) != size || Math.abs(x1) != size - 1))) {
						world.replaceBlockTypeWithoutUpdate(x + x1, y + y1, z + z1, BlockTypeRegistry.AIR, BlockTypeRegistry.OAK_LEAVES);
					}
				}
			}
		}
	}
}
