package xyz.qubs.game.world.generation.features;

import xyz.qubs.game.block.BlockTypeRegistry;
import xyz.qubs.game.world.IWorld;

import java.util.Random;

public class DesertFeature implements IFeature {
	private static final int height = 4;

	@Override
	public float chance(IWorld world, int x, int y, int z) {
		return .002f;
	}

	@Override
	public boolean check(IWorld world, int x, int y, int z) {
		if (world.getBlockType(x, y - 1, z) != BlockTypeRegistry.SAND) {
			return false;
		}
		if (!world.getBlockType(x, y, z).isAir()) {
			return false;
		}
		for (int y1 = 1; y1 < height; y1++) {
			if (!world.getBlockType(x, y + y1, z).isAir()) {
				return false;
			}
		}
		for (int x1 = -1; x1 <= 1; x1++) {
			for (int z1 = -1; z1 <= 1; z1++) {
				if (!world.getBlockType(x + x1, y, z + z1).isAir()) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void generate(IWorld world, int x, int y, int z) {
		for (int y1 = 0; y1 < height; y1++) {
			world.setBlockTypeWithoutUpdate(x, y + y1, z, BlockTypeRegistry.OAK_PLANK);
		}
		world.setBlockTypeWithoutUpdate(x, y + height, z, BlockTypeRegistry.SAND);
		if (new Random().nextBoolean()) {
			world.setBlockTypeWithoutUpdate(x + 1, y, z, BlockTypeRegistry.OAK_PLANK);
			world.setBlockTypeWithoutUpdate(x - 1, y, z, BlockTypeRegistry.OAK_PLANK);
		} else {
			world.setBlockTypeWithoutUpdate(x, y, z + 1, BlockTypeRegistry.OAK_PLANK);
			world.setBlockTypeWithoutUpdate(x, y, z - 1, BlockTypeRegistry.OAK_PLANK);
		}
	}
}
