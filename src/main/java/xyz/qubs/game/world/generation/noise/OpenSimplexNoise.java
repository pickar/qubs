package xyz.qubs.game.world.generation.noise;

import xyz.qubs.engine.utils.Maths;

@SuppressWarnings("all")
public class OpenSimplexNoise implements ITerrainNoise {
	private static final int[] perm = new int[256];
	private static final int[] perm2D = new int[256];
	private static final int[] perm3D = new int[256];
	private static final int[] gradients2D = {
			5, 2, 2, 5,
			-5, 2, -2, 5,
			5, -2, 2, -5,
			-5, -2, -2, -5
	};
	private static final int[] gradients3D = {
			-11, 4, 4, -4, 11, 4, -4, 4, 11,
			11, 4, 4, 4, 11, 4, 4, 4, 11,
			-11, -4, 4, -4, -11, 4, -4, -4, 11,
			11, -4, 4, 4, -11, 4, 4, -4, 11,
			-11, 4, -4, -4, 11, -4, -4, 4, -11,
			11, 4, -4, 4, 11, -4, 4, 4, -11,
			-11, -4, -4, -4, -11, -4, -4, -4, -11,
			11, -4, -4, 4, -11, -4, 4, -4, -11
	};
	private static final Contribution2[] lookup2D = new Contribution2[64];
	private static final Contribution3[] lookup3D = new Contribution3[2048];
	private final double STRETCH_2D = -0.211324865405187;    //(1/Math.sqrt(2+1)-1)/2;
	private final double STRETCH_3D = -1.0 / 6.0;            //(1/Math.sqrt(3+1)-1)/3;
	private final double SQUISH_2D = 0.366025403784439;      //(Math.sqrt(2+1)-1)/2;
	private final double SQUISH_3D = 1.0 / 3.0;              //(Math.sqrt(3+1)-1)/3;
	private final double NORM_2D = 1.0 / 47.0;
	private final double NORM_3D = 1.0 / 103.0;

	public OpenSimplexNoise(long seed) {
		int[] source = new int[256];
		for (int i = 0; i < 256; i++) {
			source[i] = i;
		}
		seed = seed * 6364136223846793005L + 1442695040888963407L;
		seed = seed * 6364136223846793005L + 1442695040888963407L;
		seed = seed * 6364136223846793005L + 1442695040888963407L;
		for (int i = 255; i >= 0; i--) {
			seed = seed * 6364136223846793005L + 1442695040888963407L;
			int r = (int) ((seed + 31) % (i + 1));
			if (r < 0) {
				r += (i + 1);
			}
			perm[i] = source[r];
			perm2D[i] = perm[i] & 0x0E;
			perm3D[i] = (perm[i] % 24) * 3;
			source[r] = source[i];
		}

		int[][] base2D = new int[][] {
				new int[] { 1, 1, 0, 1, 0, 1, 0, 0, 0 },
				new int[] { 1, 1, 0, 1, 0, 1, 2, 1, 1 }
		};
		int[] p2D = new int[] { 0, 0, 1, -1, 0, 0, -1, 1, 0, 2, 1, 1, 1, 2, 2, 0, 1, 2, 0, 2, 1, 0, 0, 0 };
		int[] lookupPairs2D = new int[] { 0, 1, 1, 0, 4, 1, 17, 0, 20, 2, 21, 2, 22, 5, 23, 5, 26, 4, 39, 3, 42, 4, 43, 3 };

		Contribution2[] contributions2D = new Contribution2[p2D.length / 4];
		for (int i = 0; i < p2D.length; i += 4) {
			int[] baseSet = base2D[p2D[i]];
			Contribution2 previous = null, current = null;
			for (int k = 0; k < baseSet.length; k += 3) {
				current = new Contribution2(baseSet[k], baseSet[k + 1], baseSet[k + 2]);
				if (previous == null) {
					contributions2D[i / 4] = current;
				} else {
					previous.Next = current;
				}
				previous = current;
			}
			current.Next = new Contribution2(p2D[i + 1], p2D[i + 2], p2D[i + 3]);
		}
		for (int i = 0; i < lookupPairs2D.length; i += 2) {
			lookup2D[lookupPairs2D[i]] = contributions2D[lookupPairs2D[i + 1]];
		}

		int[][] base3D = new int[][] {
				new int[] { 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1 },
				new int[] { 2, 1, 1, 0, 2, 1, 0, 1, 2, 0, 1, 1, 3, 1, 1, 1 },
				new int[] { 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 2, 1, 1, 0, 2, 1, 0, 1, 2, 0, 1, 1 }
		};
		int[] p3D = new int[] { 0, 0, 1, -1, 0, 0, 1, 0, -1, 0, 0, -1, 1, 0, 0, 0, 1, -1, 0, 0, -1, 0, 1, 0, 0, -1, 1, 0, 2, 1, 1, 0, 1, 1, 1, -1, 0, 2, 1, 0, 1, 1, 1, -1, 1, 0, 2, 0, 1, 1, 1, -1, 1, 1, 1, 3, 2, 1, 0, 3, 1, 2, 0, 1, 3, 2, 0, 1, 3, 1, 0, 2, 1, 3, 0, 2, 1, 3, 0, 1, 2, 1, 1, 1, 0, 0, 2, 2, 0, 0, 1, 1, 0, 1, 0, 2, 0, 2, 0, 1, 1, 0, 0, 1, 2, 0, 0, 2, 2, 0, 0, 0, 0, 1, 1, -1, 1, 2, 0, 0, 0, 0, 1, -1, 1, 1, 2, 0, 0, 0, 0, 1, 1, 1, -1, 2, 3, 1, 1, 1, 2, 0, 0, 2, 2, 3, 1, 1, 1, 2, 2, 0, 0, 2, 3, 1, 1, 1, 2, 0, 2, 0, 2, 1, 1, -1, 1, 2, 0, 0, 2, 2, 1, 1, -1, 1, 2, 2, 0, 0, 2, 1, -1, 1, 1, 2, 0, 0, 2, 2, 1, -1, 1, 1, 2, 0, 2, 0, 2, 1, 1, 1, -1, 2, 2, 0, 0, 2, 1, 1, 1, -1, 2, 0, 2, 0 };
		int[] lookupPairs3D = new int[] { 0, 2, 1, 1, 2, 2, 5, 1, 6, 0, 7, 0, 32, 2, 34, 2, 129, 1, 133, 1, 160, 5, 161, 5, 518, 0, 519, 0, 546, 4, 550, 4, 645, 3, 647, 3, 672, 5, 673, 5, 674, 4, 677, 3, 678, 4, 679, 3, 680, 13, 681, 13, 682, 12, 685, 14, 686, 12, 687, 14, 712, 20, 714, 18, 809, 21, 813, 23, 840, 20, 841, 21, 1198, 19, 1199, 22, 1226, 18, 1230, 19, 1325, 23, 1327, 22, 1352, 15, 1353, 17, 1354, 15, 1357, 17, 1358, 16, 1359, 16, 1360, 11, 1361, 10, 1362, 11, 1365, 10, 1366, 9, 1367, 9, 1392, 11, 1394, 11, 1489, 10, 1493, 10, 1520, 8, 1521, 8, 1878, 9, 1879, 9, 1906, 7, 1910, 7, 2005, 6, 2007, 6, 2032, 8, 2033, 8, 2034, 7, 2037, 6, 2038, 7, 2039, 6 };

		Contribution3[] contributions3D = new Contribution3[p3D.length / 9];
		for (int i = 0; i < p3D.length; i += 9) {
			int[] baseSet = base3D[p3D[i]];
			Contribution3 previous = null, current = null;
			for (int k = 0; k < baseSet.length; k += 4) {
				current = new Contribution3(baseSet[k], baseSet[k + 1], baseSet[k + 2], baseSet[k + 3]);
				if (previous == null) {
					contributions3D[i / 9] = current;
				} else {
					previous.Next = current;
				}
				previous = current;
			}
			current.Next = new Contribution3(p3D[i + 1], p3D[i + 2], p3D[i + 3], p3D[i + 4]);
			current.Next.Next = new Contribution3(p3D[i + 5], p3D[i + 6], p3D[i + 7], p3D[i + 8]);
		}

		for (int i = 0; i < lookupPairs3D.length; i += 2) {
			lookup3D[lookupPairs3D[i]] = contributions3D[lookupPairs3D[i + 1]];
		}
	}

	public double evaluate(double x, double y) {
		double stretchOffset = (x + y) * STRETCH_2D;
		double xs = x + stretchOffset;
		double ys = y + stretchOffset;

		int xsb = Maths.floor(xs);
		int ysb = Maths.floor(ys);

		double squishOffset = (xsb + ysb) * SQUISH_2D;
		double dx0 = x - (xsb + squishOffset);
		double dy0 = y - (ysb + squishOffset);

		double xins = xs - xsb;
		double yins = ys - ysb;

		double inSum = xins + yins;

		int hash = (int) (xins - yins + 1) | ((int) (inSum) << 1) | ((int) (inSum + yins) << 2) | ((int) (inSum + xins) << 4);

		Contribution2 c = lookup2D[hash];

		double value = 0.0;
		while (c != null) {
			double dx = dx0 + c.dx;
			double dy = dy0 + c.dy;
			double attn = 2 - dx * dx - dy * dy;
			if (attn > 0.1) {
				int px = xsb + c.xsb;
				int py = ysb + c.ysb;

				int i = perm2D[(perm[px & 0xFF] + py) & 0xFF] & 0xFF;
				double valuePart = gradients2D[i] * dx + gradients2D[i + 1] * dy;

				value += attn * attn * attn * attn * valuePart;
			}
			c = c.Next;
		}
		return value * NORM_2D;
	}

	public double evaluate(double x, double y, double z) {
		double stretchOffset = (x + y + z) * STRETCH_3D;
		double xs = x + stretchOffset;
		double ys = y + stretchOffset;
		double zs = z + stretchOffset;

		int xsb = Maths.floor(xs);
		int ysb = Maths.floor(ys);
		int zsb = Maths.floor(zs);

		double squishOffset = (xsb + ysb + zsb) * SQUISH_3D;
		double dx0 = x - (xsb + squishOffset);
		double dy0 = y - (ysb + squishOffset);
		double dz0 = z - (zsb + squishOffset);

		double xins = xs - xsb;
		double yins = ys - ysb;
		double zins = zs - zsb;

		double inSum = xins + yins + zins;

		int hash =
				(int) (yins - zins + 1) |
				((int) (xins - yins + 1) << 1) |
				((int) (xins - zins + 1) << 2) |
				((int) inSum << 3) |
				((int) (inSum + zins) << 5) |
				((int) (inSum + yins) << 7) |
				((int) (inSum + xins) << 9);

		double value = 0.0;

		Contribution3 c = lookup3D[hash];

		while (c != null) {
			double dx = dx0 + c.dx;
			double dy = dy0 + c.dy;
			double dz = dz0 + c.dz;
			double attn = 2 - dx * dx - dy * dy - dz * dz;

			if (attn > 0.1) {
				int px = xsb + c.xsb;
				int py = ysb + c.ysb;
				int pz = zsb + c.zsb;

				int i = perm3D[(perm[(perm[px & 0xFF] + py) & 0xFF] + pz) & 0xFF];
				double valuePart = gradients3D[i] * dx + gradients3D[i + 1] * dy + gradients3D[i + 2] * dz;

				value += attn * attn * attn * attn * valuePart;
			}
			c = c.Next;
		}

		return value * NORM_3D;
	}

	private final class Contribution2 {
		final double dx, dy;
		final int xsb, ysb;
		Contribution2 Next;

		Contribution2(double multiplier, int xsb, int ysb) {
			dx = -xsb - multiplier * SQUISH_2D;
			dy = -ysb - multiplier * SQUISH_2D;
			this.xsb = xsb;
			this.ysb = ysb;
		}
	}

	private final class Contribution3 {
		final double dx, dy, dz;
		final int xsb, ysb, zsb;
		Contribution3 Next;

		Contribution3(double multiplier, int xsb, int ysb, int zsb) {
			dx = -xsb - multiplier * SQUISH_3D;
			dy = -ysb - multiplier * SQUISH_3D;
			dz = -zsb - multiplier * SQUISH_3D;

			this.xsb = xsb;
			this.ysb = ysb;
			this.zsb = zsb;
		}
	}
}
