package xyz.qubs.game.world.generation;

import it.unimi.dsi.fastutil.objects.ObjectList;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.utils.Maths;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.block.BlockTypeRegistry;
import xyz.qubs.game.world.biome.BiomeRegistry;
import xyz.qubs.game.world.biome.IBiome;
import xyz.qubs.game.world.chunk.Chunk;
import xyz.qubs.game.world.chunk.ChunkPos;
import xyz.qubs.game.world.chunk.IChunk;
import xyz.qubs.game.world.generation.features.IFeature;
import xyz.qubs.game.world.generation.noise.ITerrainNoise;
import xyz.qubs.game.world.generation.noise.OpenSimplexNoise;

import java.util.Arrays;
import java.util.Random;

import static xyz.qubs.game.world.World.loadDistanceH;
import static xyz.qubs.game.world.chunk.IChunk.CHUNK_SIZE;
import static xyz.qubs.game.world.chunk.IChunk.LOG2_CHUNK_SIZE;

public class DefaultWorldGenerator implements IWorldGenerator {
	private static final BlockType filler = BlockTypeRegistry.STONE;
	private static final BlockType air = BlockTypeRegistry.AIR;
	//	private static final BlockType[] allFiller = new BlockType[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	private static final BlockType[] allAir = new BlockType[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];

	static {
//		Arrays.fill(allFiller, filler);
		Arrays.fill(allAir, air);
	}

//	private IFeature treeFeature = new TreeFeature();

	ITerrainNoise noise;

//	private boolean[] allFillerBoolean = new boolean[CHUNK_SIZE*CHUNK_SIZE*CHUNK_SIZE];
//	private boolean[] allAirBoolean = new boolean[CHUNK_SIZE*CHUNK_SIZE*CHUNK_SIZE];

	private int cacheIndex(int x, int z) {
		return x * loadDistanceH + z;
	}

	private int blockIndex(int x, int z) {
		return x * CHUNK_SIZE + z;
	}

	@Override
	public ITerrainNoise getNoise() {
		return noise;
	}

	@Override
	public void createNoise(long worldSeed) {
		noise = new OpenSimplexNoise(worldSeed);
	}

	@Override
	public void init() {
		createNoise(1234);
	}

	@Override
	public void generateBiomes(IChunk chunk) {
		final ChunkPos pos = chunk.getChunkPos();
//		final int chunkX = pos.x << LOG2_CHUNK_SIZE;
//		final int chunkY = pos.y << LOG2_CHUNK_SIZE;
//		final int chunkZ = pos.z << LOG2_CHUNK_SIZE;
		if (pos.x < 9 && pos.z < 9) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				for (int y = 0; y < CHUNK_SIZE; y++) {
					for (int z = 0; z < CHUNK_SIZE; z++) {
						chunk.setBiome(x, y, z, BiomeRegistry.DESERT);
					}
				}
			}
		} else {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				for (int y = 0; y < CHUNK_SIZE; y++) {
					for (int z = 0; z < CHUNK_SIZE; z++) {
						chunk.setBiome(x, y, z, BiomeRegistry.PLAINS);
					}
				}
			}
		}
	}

	@Override
	public void generateFiller(IChunk chunk) {
//		System.out.println("GENERATING CHUNK " + chunk.getChunkPos());
//		Random random = new Random(chunk.getSeed());
		final ChunkPos pos = chunk.getChunkPos();
		final int chunkX = pos.x << LOG2_CHUNK_SIZE;
		final int chunkY = pos.y << LOG2_CHUNK_SIZE;
		final int chunkZ = pos.z << LOG2_CHUNK_SIZE;

		if (pos.y >= 33) {
			// air
			chunk.setBlocks(allAir);
		} else if (pos.y >= 0) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				for (int y = 0; y < CHUNK_SIZE; y++) {
					for (int z = 0; z < CHUNK_SIZE; z++) {
						double value = Maths.floor((noise.fbm((x + chunkX) / 32.0d, (y + chunkY) / 32.0d, (z + chunkZ) / 32.0d, 4, 0.8, 1.3) + 5) * 12) - 20;
						if (value - (y + chunkY) > 0) {
							chunk.setBlockType(x, y, z, filler);
						} else {
							chunk.setBlockType(x, y, z, air);
						}
					}
				}
			}
		} else {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				for (int y = 0; y < CHUNK_SIZE; y++) {
					for (int z = 0; z < CHUNK_SIZE; z++) {
						double noiseValue = noise.fbm((x + chunkX) / 16d, (y + chunkY) / 16d, (z + chunkZ) / 16d, 1, 0.6, 1.25);
						if (noiseValue > 0.8) {
							chunk.setBlockType(x, y, z, air);
						} else {
							chunk.setBlockType(x, y, z, filler);
						}
					}
				}
			}
		}
	}

	@Override
	public void generateBlocks(IChunk chunk) {
		final ChunkPos pos = chunk.getChunkPos();
		final int chunkX = pos.x << LOG2_CHUNK_SIZE;
		final int chunkY = pos.y << LOG2_CHUNK_SIZE;
		final int chunkZ = pos.z << LOG2_CHUNK_SIZE;

		BlockType block;
		BlockType blockBelow;
		IBiome biome;

		if (pos.y >= 0) {
			final Chunk above = chunk.getWorld().getChunk(pos.x, pos.y + 1, pos.z);
			final Chunk below = chunk.getWorld().getChunk(pos.x, pos.y - 1, pos.z);
			for (int x = 0; x < CHUNK_SIZE; x++) {
				for (int y = CHUNK_SIZE - 1; y >= 0; y--) {
					for (int z = 0; z < CHUNK_SIZE; z++) {
						if (chunk.getBlockType(x, y, z) == filler) {
							if (y == CHUNK_SIZE - 1) {
								block = above.getBlockType(x, 0, z);
							} else {
								block = chunk.getBlockType(x, y + 1, z);
							}

							if (block.isTransparent(Side.BOTTOM)) {
								if (y == 0) {
									blockBelow = below.getBlockType(x, CHUNK_SIZE - 1, z);
								} else {
									blockBelow = chunk.getBlockType(x, y - 1, z);
								}
								if (blockBelow == filler) {
									biome = chunk.getBiome(x, y, z);
									chunk.setBlockType(x, y, z, biome.getTopFiller());
									for (int i = 1; i < 4; i++) {
										final int y1 = y - i;
										if (y1 - 1 < 0) {
											blockBelow = below.getBlockType(x, (y1 - 1) & (CHUNK_SIZE - 1), z);
										} else {
											blockBelow = chunk.getBlockType(x, y1 - 1, z);
										}
										if (blockBelow == filler) {
											if (y1 < 0) {
												if (below.getBlockType(x, (y1) & (CHUNK_SIZE - 1), z) == filler) {
													below.setBlockType(x, (y1) & (CHUNK_SIZE - 1), z, biome.getMiddleFiller());
												}
											} else {
												if (chunk.getBlockType(x, y1, z) == filler) {
													chunk.setBlockType(x, y1, z, biome.getMiddleFiller());
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} else {
		}
	}

	@Override
	public void generateFeatures(IChunk chunk) {
		final ChunkPos pos = chunk.getChunkPos();
		final int chunkX = pos.x << LOG2_CHUNK_SIZE;
		final int chunkY = pos.y << LOG2_CHUNK_SIZE;
		final int chunkZ = pos.z << LOG2_CHUNK_SIZE;

//		BlockType block;
		float randValue;
		Random random = new Random(chunk.getSeed());
		if (pos.y >= 0) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				for (int y = 0; y < CHUNK_SIZE; y++) {
					for (int z = 0; z < CHUNK_SIZE; z++) {
						ObjectList<IFeature> features = chunk.getBiome(x, y, z).getFeatures();
						for (int i = 0, featuresSize = features.size(); i < featuresSize; i++) {
							IFeature feature = features.get(i);
							randValue = random.nextFloat();
							if (randValue < feature.chance(chunk.getWorld(), x + chunkX, y + chunkY, z + chunkZ) && feature.check(chunk.getWorld(), x + chunkX, y + chunkY, z + chunkZ)) {
								feature.generate(chunk.getWorld(), x + chunkX, y + chunkY, z + chunkZ);
							}
						}
					}
				}
			}
		} else {

		}
	}

	private static final class CacheObject {
		private final int[] map = new int[CHUNK_SIZE * CHUNK_SIZE + 1];
	}
}
