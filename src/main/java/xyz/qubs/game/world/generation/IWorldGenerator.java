package xyz.qubs.game.world.generation;

import xyz.qubs.game.world.chunk.IChunk;
import xyz.qubs.game.world.generation.noise.ITerrainNoise;

public interface IWorldGenerator {
	ITerrainNoise getNoise();

	void createNoise(long seed);

	void init();

	default long generateChunkSeed(IChunk chunk) {
		return getNoise().generateSeed(chunk.getChunkPos().x, chunk.getChunkPos().y, chunk.getChunkPos().z);
	}

	void generateBiomes(IChunk chunk); //generates biomes

	void generateFiller(IChunk chunk); //generates terrain outline from stone, using biome and chunk heights

	void generateBlocks(IChunk chunk); //fills terrain with biome-specific blocks (e.g. grass, dirt ...)

	void generateFeatures(IChunk chunk); //generates all kinds of decoration features (e.g. ores, trees, structures ...)
}
