package xyz.qubs.game.world.generation.features;

import xyz.qubs.game.world.IWorld;

public interface IFeature {
	float chance(IWorld world, int x, int y, int z);

	boolean check(IWorld world, int x, int y, int z);

	void generate(IWorld world, int x, int y, int z);
}
