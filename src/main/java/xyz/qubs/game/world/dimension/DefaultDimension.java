package xyz.qubs.game.world.dimension;

import org.joml.Vector3f;
import xyz.qubs.engine.ui.models.properties.UIColor;
import xyz.qubs.game.world.generation.DefaultWorldGenerator;
import xyz.qubs.game.world.generation.IWorldGenerator;

import static xyz.qubs.Engine.UPDATES_PER_SECOND;

public class DefaultDimension implements IDimension {
	private final IWorldGenerator generator = new DefaultWorldGenerator();
	private final Vector3f rotationVector = new Vector3f(0, -1, 0);

	@Override
	public IWorldGenerator getGenerator() {
		return generator;
	}

	@Override
	public String getRegistryName() {
		return "test";
	}

	@Override
	public String getFolderName() {
		return "test";
	}

	@Override
	public UIColor getSkyColor() {
		return UIColor.BLUE;
	}

	@Override
	public float getGravityConstant() {
		return 9.81f / UPDATES_PER_SECOND;
	}

	@Override
	public Vector3f getGravityRotation() {
		return rotationVector;
	}
}
