package xyz.qubs.game.world.dimension;

import org.joml.Vector3f;
import xyz.qubs.engine.ui.models.properties.UIColor;
import xyz.qubs.game.world.generation.IWorldGenerator;

public interface IDimension {
	IWorldGenerator getGenerator();

	String getRegistryName();

	String getFolderName();

	UIColor getSkyColor();

	float getGravityConstant();

	Vector3f getGravityRotation();

	//TODO: sun movement, sun color/texture and ticks per day
	//TODO: water color ...
}
