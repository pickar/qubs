package xyz.qubs.game.collision.physics;

import xyz.qubs.engine.registry.IRegistryEntry;
import xyz.qubs.game.entity.IEntity;

public interface IPhysics extends IRegistryEntry {
	void calculate(IEntity entity);
}
