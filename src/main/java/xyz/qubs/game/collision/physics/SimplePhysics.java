package xyz.qubs.game.collision.physics;

import org.joml.Vector3f;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.entity.IEntity;
import xyz.qubs.game.world.dimension.IDimension;

import static xyz.qubs.game.collision.physics.PhysicsManager.rayCast;

public class SimplePhysics implements IPhysics {
	private final NamespacedName registryName;

	public SimplePhysics(NamespacedName registryName) {
		this.registryName = registryName;
	}

	public void calculate(IEntity entity) {
		// TODO: Math.Ceil optimize check
		IDimension dimension = entity.getChunks().get(0).getWorld().getDimension();
		float g = dimension.getGravityConstant();
		Vector3f gravityRotation = dimension.getGravityRotation();

		int lengthOfGravityVector = (int) Math.ceil((entity.getWeight() * g) + entity.getVelocity().y);
		PhysicsManager.RayCastResult rayCastResult = rayCast(entity, gravityRotation, lengthOfGravityVector);
		if (!checkAABBCollisions(entity, rayCastResult) || rayCastResult.stepCount != 0) {
			return;
		} // If entity not on the ground
		if (entity.getDirectionVector().equals(0, 0, 0) && entity.getVelocity().equals(0, 0, 0)) { return; }
		// TODO: Check object collisions for direction vector and velocity
		// TODO: get vector
		// int lengthOfDirectionRay = (int)Math.ceil(entity.getDirection().length());

		// rayCastResult = rayCast(entity, entity.getDirectionVector(), lengthOfDirectionRay);
		// CheckAABBCollisions(rayCastResult, entity);
	}

	private boolean checkAABBCollisions(IEntity entity, PhysicsManager.RayCastResult rayCastResult) {
		// TODO: Change velocity of entity if object is collided with another
		return false;
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}
}
