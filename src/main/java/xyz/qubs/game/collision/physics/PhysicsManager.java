package xyz.qubs.game.collision.physics;

import it.unimi.dsi.fastutil.objects.ObjectList;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector3i;
import xyz.qubs.engine.utils.Maths;
import xyz.qubs.game.block.BlockType;
import xyz.qubs.game.block.types.CubeBlockType;
import xyz.qubs.game.collision.AABB;
import xyz.qubs.game.entity.IEntity;
import xyz.qubs.game.world.chunk.Chunk;
import xyz.qubs.game.world.chunk.IChunk;

import static xyz.qubs.Engine.world;
import static xyz.qubs.game.world.chunk.IChunk.LOG2_CHUNK_SIZE;

public class PhysicsManager {
	private static ObjectList<IEntity> entities;

	// Accept all entities on scene
	public static void calculatePhysics(ObjectList<IEntity> entities) {
		PhysicsManager.entities = entities;

		for (IEntity entity : entities) {
			entity.getIPhysics().calculate(entity);
		}
	}

	// Returns null if entity doesn't intersect another entities
	public static IEntity intersectWithEntities(IEntity currentEntity) {
		Vector3f currentPos = currentEntity.getWorldPosition();
		// Caching AABBs calculations for current entity
		AABB[] currentAABBs = currentEntity.getAxisAlignedBoundingBoxes();
		@SuppressWarnings("MismatchedReadAndWriteOfArray")
		AABB[] calculatedCurrentAABBs = new AABB[currentAABBs.length];

		for (int i = 0; i < currentAABBs.length; i++) {
			currentAABBs[i].max.add(currentPos, calculatedCurrentAABBs[i].max);
			currentAABBs[i].min.add(currentPos, calculatedCurrentAABBs[i].min);
		}

		for (IEntity entity : entities) {
			if (currentEntity == entity) {
				continue;
			}

			Vector3f entityPos = entity.getWorldPosition();

			for (AABB calculatedCurrentAABB : calculatedCurrentAABBs) {
				for (AABB aabb : entity.getAxisAlignedBoundingBoxes()) {
					if (AABB.intersectsWith(calculatedCurrentAABB, aabb, entityPos)) {
						return entity;
					}
				}
			}
		}

		return null;
	}

	// Returns null if ray doesn't intersect another blocks
	public static RayCastResult rayCast(IEntity entity, Vector3f rotation, int distance) {
		int addX = world.shift.x << LOG2_CHUNK_SIZE;
		int addY = world.shift.y << LOG2_CHUNK_SIZE;
		int addZ = world.shift.z << LOG2_CHUNK_SIZE;

		Vector3f startPos = entity.getLocalPosition();
		Matrix4f view = new Matrix4f().rotateXYZ(rotation.x, rotation.y, rotation.z).translate(-startPos.x, -startPos.y, -startPos.z);

		float dx = -view.m02();
		float dy = -view.m12();
		float dz = -view.m22();

		if (dx == 0 && dy == 0 && dz == 0) { throw new RuntimeException("Raycast in zero direction!"); }

		int x = Maths.floor(startPos.x);
		int y = Maths.floor(startPos.y);
		int z = Maths.floor(startPos.z);

		final Vector3f invDirection = new Vector3f(1 / dx, 1 / dy, 1 / dz);

		int stepX = (dx >= 0) ? 1 : -1;
		int stepY = (dy >= 0) ? 1 : -1;
		int stepZ = (dz >= 0) ? 1 : -1;

		int lastX = x + stepX * distance;
		int lastY = y + stepY * distance;
		int lastZ = z + stepZ * distance;

		float tMaxX = (dx != 0) ? intBound(startPos.x, dx) : Float.MAX_VALUE;
		float tMaxY = (dy != 0) ? intBound(startPos.y, dy) : Float.MAX_VALUE;
		float tMaxZ = (dz != 0) ? intBound(startPos.z, dz) : Float.MAX_VALUE;

		float tDeltaX = (dx != 0) ? stepX / dx : Float.MAX_VALUE;
		float tDeltaY = (dy != 0) ? stepY / dy : Float.MAX_VALUE;
		float tDeltaZ = (dz != 0) ? stepZ / dz : Float.MAX_VALUE;

		int stepCount = 0;

		BlockType block;
		while (x != lastX && y != lastY && z != lastZ) {
			block = getBlockFromChunk(entity, x + addX, y + addY, z + addZ);
			if (block != null && block.getAABBs().length != 0) {
				if (block.getAABBs() == CubeBlockType.CUBE_AABB) {
					return new RayCastResult(new Vector3i(x + addX, y + addY, z + addZ), 0, stepCount);
				} else {
					final Vector3f origin = new Vector3f(startPos).sub(x, y, z);
					AABB.IntersectionResult minResult = block.getAABBs()[0].intersectsWith(origin, invDirection);
					int aabb = 0;

					for (int i = 1; i < block.getAABBs().length; i++) {
						AABB.IntersectionResult result = block.getAABBs()[i].intersectsWith(origin, invDirection);
						if (result.intersected && minResult.lengthOfRay > result.lengthOfRay) {
							minResult = result;
							aabb = i;
						}
					}

					if (minResult != null) {
						return new RayCastResult(new Vector3i(x + addX, y + addY, z + addZ), aabb, stepCount);
					}
				}
			}

			if (tMaxX < tMaxY) {
				if (tMaxX < tMaxZ) {
					x += stepX;
					tMaxX += tDeltaX;
				} else {
					z += stepZ;
					tMaxZ += tDeltaZ;
				}
			} else {
				if (tMaxY < tMaxZ) {
					y += stepY;
					tMaxY += tDeltaY;
				} else {
					z += stepZ;
					tMaxZ += tDeltaZ;
				}
			}
			stepCount++;
		}

		return null;
	}

	private static float intBound(float s, float ds) {
		if (Math.round(s) == s && ds < 0) {
			return 0;
		}
		if (ds < 0) {
			return intBound(-s, -ds);
		} else {
			s = (s % 1 + 1) % 1;
			return (1 - s) / ds;
		}
	}

	public static BlockType getBlockFromChunk(IEntity entity, int x, int y, int z) {
		IChunk chunk = entity.getChunks().get(0);
		if (x >= Chunk.CHUNK_SIZE || y >= Chunk.CHUNK_SIZE || z >= Chunk.CHUNK_SIZE) {
			// TODO: chunks transaction
			chunk = world.getChunk(x, y, z);
			if (chunk == null) { return null; }
			entity.getChunks().set(0, chunk);
			entity.getChunks().get(1).getChunkPos();
			return null;
		} else {
			return chunk.getBlockType(x, y, z);
		}
	}

	public static final class RayCastResult {
		Vector3i blockPosition;
		int aabb;
		int stepCount;

		public RayCastResult(Vector3i blockPosition, int aabb, int stepCount) {
			this.aabb = aabb;
			this.blockPosition = blockPosition;
			this.stepCount = stepCount;
		}
	}
}
