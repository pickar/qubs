package xyz.qubs.game.collision.physics;

import xyz.qubs.engine.event.annotations.SubscribeEvent;
import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.engine.registry.RegisterEntriesEvent;
import xyz.qubs.engine.registry.Registry;
import xyz.qubs.engine.registry.RegistryCreationEvent;

import java.lang.reflect.Type;

public class PhysicsTypeRegistry extends Registry<IPhysics> {
	private static final PhysicsTypeRegistry INSTANCE = new PhysicsTypeRegistry(new NamespacedName("core", "physics"), IPhysics.class);

	public static final IPhysics SIMPLE_PHYSICS = new SimplePhysics(new NamespacedName("core", "simple"));
	public static final IPhysics REAL_PHYSICS = new RealPhysics(new NamespacedName("core", "real"));

	public PhysicsTypeRegistry(NamespacedName registryName, Type type) {
		super(registryName, type);
	}

	public static IPhysics getIPhysicsByName(NamespacedName registryName) {
		return INSTANCE.getElement(registryName);
	}

	public static IPhysics getIPhysicsByName(String fullName) {
		return INSTANCE.getElement(fullName);
	}

	@SubscribeEvent
	public static void onRegistryCreation(RegistryCreationEvent event) {
		event.registry.register(INSTANCE);
	}

	@SubscribeEvent
	public static void onRegisterEntriesIPhysics(RegisterEntriesEvent<IPhysics> event) {
		event.registry.register(SIMPLE_PHYSICS);
		event.registry.register(REAL_PHYSICS);
	}
}
