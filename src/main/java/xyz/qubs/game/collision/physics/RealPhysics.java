package xyz.qubs.game.collision.physics;

import xyz.qubs.engine.registry.NamespacedName;
import xyz.qubs.game.entity.IEntity;

public class RealPhysics implements IPhysics {
	private final NamespacedName registryName;

	public RealPhysics(NamespacedName registryName) {
		this.registryName = registryName;
	}

	public void calculate(IEntity entity) {
	}

	private boolean checkBBCollisions(IEntity entity, PhysicsManager.RayCastResult rayCastResult) {
		return false;
	}

	@Override
	public NamespacedName getRegistryName() {
		return registryName;
	}
}
