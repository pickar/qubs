package xyz.qubs.game.collision;

import org.joml.Vector3f;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.utils.Vectors;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class AABB { //AxisAlignedBoundingBox
	public Vector3f min;
	public Vector3f max;

	public AABB(Vector3f min, Vector3f max) {
		this.min = min;
		this.max = max;
	}

	public static boolean intersectsWith(AABB calculatedAABB, AABB compareAABB, Vector3f comparePos) {
		return Vectors.isBigger(compareAABB.max.add(comparePos), calculatedAABB.min) && Vectors.isBigger(compareAABB.min.add(comparePos), calculatedAABB.max);
	}

	public static boolean intersectsWith(AABB calculatedAABB, AABB calculatedCompareAABB) {
		return Vectors.isBigger(calculatedCompareAABB.max, calculatedAABB.min) && Vectors.isBigger(calculatedCompareAABB.min, calculatedAABB.max);
	}

	public boolean intersectsWith(Vector3f pos, AABB compareAABB, Vector3f comparePos) {
		return Vectors.isBigger(compareAABB.max.add(comparePos), min.add(pos)) && Vectors.isBigger(compareAABB.min.add(comparePos), max.add(pos));
	}

	public IntersectionResult intersectsWith(Vector3f origin, Vector3f invDirection) {
		float t1 = (min.x - origin.x) * invDirection.x;
		float t2 = (max.x - origin.x) * invDirection.x;

		float tMin = min(t1, t2);
		float tMax = max(t1, t2);
		Side side = Side.RIGHT;

		t1 = (min.y - origin.y) * invDirection.y;
		t2 = (max.y - origin.y) * invDirection.y;

		float t1Min = min(t1, t2);
		if (t1Min > tMin) {
			tMin = t1Min;
			side = Side.TOP;
		}
		tMax = min(tMax, max(t1, t2));
		t1 = (min.z - origin.z) * invDirection.z;
		t2 = (max.z - origin.z) * invDirection.z;

		t1Min = min(t1, t2);
		if (t1Min > tMin) {
			tMin = t1Min;
			side = Side.FRONT;
		}
		tMax = min(tMax, max(t1, t2));

		return new IntersectionResult(tMin >= 0 ? tMin : tMax, tMax - tMin >= 0, side);
	}

	public static class IntersectionResult {
		public final float lengthOfRay;
		public final boolean intersected;
		public final Side side;

		public IntersectionResult(float lengthOfRay, boolean intersected, Side side) {
			this.lengthOfRay = lengthOfRay;
			this.intersected = intersected;
			this.side = side;
		}
	}
}

