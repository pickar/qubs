package xyz.qubs.demo;

import xyz.qubs.game.world.generation.noise.OpenSimplexNoise;

public class TerrainNoise {
	public static final int octaves = 6;
	public static final double lacunarity = 1.6;
	public static final double gain = 0.6;

	public static final OpenSimplexNoise noise = new OpenSimplexNoise(1123456789L);
	public static final double FEATURE_SIZE = 64;

	public static double fBM(double x, double y, double z) {
		x /= FEATURE_SIZE;
		y /= FEATURE_SIZE;
		z /= FEATURE_SIZE;

		float maxamp = 0;

		double amplitude = 0.8;
		double frequency = 1.0;
		double sum = 0.0;
		for (int i = 0; i < octaves; ++i) {
			sum += amplitude * noise.evaluate(x * frequency, y * frequency, z * frequency);
			maxamp += amplitude;
			amplitude *= gain;
			frequency *= lacunarity;
		}
		return sum * 1.15470d / maxamp;
//		return noise.evaluate(x, y, z)*1.15470d;
	}

	public static void main(String[] args) {
		double max = 0;
		double min = 0;
		for (int i = 0; i < 10000000; i++) {
			double value = fBM(i, i, i);
			if (value > max) {
				max = value;
			}
			if (value < min) {
				min = value;
			}
		}
		System.out.println(max);
		System.out.println(min);
//		double sum = 0;
//		for (int i = 0; i < 1000; i++)
//			sum += noise.Evaluate(i, i);
//
//		sum = 0;
//		long time1 = System.nanoTime();
//		for (int i = 0; i < 1_000_0*32*32; i++)
//			sum += noise.Evaluate(i, i);

//		System.out.println((double)(System.nanoTime()-time1)/1_000_000_000.0);

//		int size = 1024;
//		double featureSize = 32;
//		int arraySize = 8;
//
//		byte[][][] array = new byte[arraySize][arraySize][arraySize];
//
//		for (int x = 0; x < array.length; x++) {
//			for (int y = 0; y < array.length; y++) {
//				for (int z = 0; z < array.length; z++) {
//					array[x][y][z] = (byte) Math.max(0, Math.min(127 + (int) (fBM(x / featureSize, y / featureSize, z / featureSize) * 126), 255));
//				}
//			}
//		}
//
//		System.out.println(Arrays.deepToString(array));

//		for (int i = 0; i < 100; i++)
//			System.out.print(fBM(i/featureSize, i/featureSize)+", ");
//		for (int i = 1; i <= 8; i++) {
//			BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
//			for (int x = 0; x < size; x++) {
//				for (int y = 0; y < size; y++) {
//					int color = Math.max(0, Math.min(127 + (int) (fBM(x / featureSize, y / featureSize, i) * 126), 255));
//					image.setRGB(x, y, toPackedRGBGray(color));
//				}
//			}
//
//			try {
//				ImageIO.write(image, "png", new File("noise_" + i + ".png"));
//			} catch (IOException ignored) {
//			}
//		}
	}

	public static int toPackedRGB(int red, int green, int blue) {
		int c = red;
		c = c << 8;
		c = c | green;
		c = c << 8;
		c = c | blue;

		return c;
	}

	public static int toPackedRGBGray(int gray) {
		return toPackedRGB(gray, gray, gray);
	}
}
