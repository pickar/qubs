package xyz.qubs.demo;

import java.util.Random;

public class PseudoRandomTest {

	public static void main(String[] args) {
		PseudoRandom random = new PseudoRandom();
		float sum = 0;
		int samples = 100000;
		for (int i = 0; i < samples; i++) {
			boolean res = random.pseudoBoolean(0.1f);
			System.out.println(res + ", " + random.countHappened);
			if (res) {
				sum++;
			}
		}
		System.out.println(sum / samples);
	}
}

class PseudoRandom {
	private static Random rng;
	public float countHappened = 0;

	public PseudoRandom(long seed) {
		rng = new Random(seed);
	}

	public PseudoRandom() {
		rng = new Random();
	}

	public boolean pseudoBoolean(float chance) {
		boolean ret = rng.nextFloat() <= chance * (1 - 1.5 * countHappened * countHappened * countHappened);
		if (ret) {
			countHappened += 1 - chance;
		} else {
			countHappened -= chance;
		}
		return ret;
	}
}
