//package pickar.voxelgame.demo;
//
//import it.unimi.dsi.fastutil.objects.ObjectArrayList;
//import it.unimi.dsi.fastutil.objects.ObjectList;
//import pickar.voxelgame.game.block.OperationalBlock;
//import pickar.voxelgame.game.item.ItemStack;
//import pickar.voxelgame.game.item.filter.IItemFilter;
//import pickar.voxelgame.game.item.filter.WhitelistItemFilter;
//import pickar.voxelgame.game.transport.energy.EnergyNet;
//
//import java.util.Random;
//
//public class aaa {
//	public static void main(String[] args) {
//		testEnergyNet();
//	}
//
//	private static void testItemFilters() {
//		ObjectList<ItemStack> items = new ObjectArrayList<>();
//
//		items.add(new ItemStack(1));
//		items.add(new ItemStack(2));
//		items.add(new ItemStack(3));
//		items.add(new ItemStack(4));
//
//		IItemFilter blacklistItemFilter = new WhitelistItemFilter();
//		blacklistItemFilter.addToFilter(new ItemStack(1));
//		blacklistItemFilter.addToFilter(new ItemStack(3));
//
//		System.out.println(blacklistItemFilter.filter(items));
//	}
//
//	private static void testEnergyNet() {
//		Random rand = new Random();
//
//		ObjectList<EnergyNet<OperationalBlock>> energyNets = new ObjectArrayList<>();
//
//		long time0 = System.nanoTime();
//		int testCount = 1000;
//		for (int e = 0; e < 100; e++) {
//			ObjectList<OperationalBlock> producers = new ObjectArrayList<>();
//			for (int i = 0; i < testCount; i++) {
//				OperationalBlock op = new OperationalBlock();
//				op.getPosition().set(i, i, i);
//				op.maxEnergy = rand.nextInt(1000) * 1000;
//				op.operationsNeeded = rand.nextInt(9000) + 1000;
//				producers.add(op);
//			}
//
//			ObjectList<OperationalBlock> energyGivers = new ObjectArrayList<>();
//			for (int i = 0; i < testCount / 5; i++) {
//				OperationalBlock op = new OperationalBlock();
//				op.getPosition().set(i * 3, i * 3 + 3, i * 3);
//				op.maxEnergy = rand.nextInt(1000) * 10000;
//				op.receiveEnergy(op.maxEnergy);
//				op.outcomeRate = rand.nextInt(500) + 100;
//				op.incomeRate = 0;
//				op.operationsNeeded = 0;
//				energyGivers.add(op);
//			}
//
//			EnergyNet<OperationalBlock> energyNet = new EnergyNet<>();
//
//			producers.forEach(energyNet::addConsumer);
//			energyGivers.forEach(energyNet::addProducer);
//
//			energyNets.add(energyNet);
//		}
//		System.out.println((System.nanoTime() - time0) / 1000000000.0);
//
//		System.out.println("STARTED");
//		double iterations = 10;
//		long sum = 0;
//		for (int k = 0; k < iterations; k++) {
//			long time1 = System.nanoTime();
//			for (EnergyNet<OperationalBlock> energyNet : energyNets) {
//				for (int i = 0; i < 20; i++) {
//					energyNet.tick();
//				}
//			}
//			sum += System.nanoTime() - time1;
//		}
////		System.out.println(energyNet);
////		System.out.println(producers.get(testCount-1));
//
//		System.out.println((sum / iterations) / 1000000000.0);
//	}
//}
