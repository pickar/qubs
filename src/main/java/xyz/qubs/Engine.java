package xyz.qubs;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector3i;
import org.lwjgl.system.Configuration;
import xyz.qubs.engine.Vulkan;
import xyz.qubs.engine.Window;
import xyz.qubs.engine.data.ExtendedDataRegistry;
import xyz.qubs.engine.input.Camera;
import xyz.qubs.engine.input.MouseInput;
import xyz.qubs.engine.logger.Logger;
import xyz.qubs.engine.model.Side;
import xyz.qubs.engine.ui.UI;
import xyz.qubs.engine.utils.Timer;
import xyz.qubs.engine.vulkan.renderers.WorldRenderer;
import xyz.qubs.game.block.Block;
import xyz.qubs.game.block.BlockPos;
import xyz.qubs.game.block.BlockTypeRegistry;
import xyz.qubs.game.block.types.SlabBlockType;
import xyz.qubs.game.world.World;
import xyz.qubs.game.world.chunk.IChunk;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static org.lwjgl.glfw.GLFW.*;
import static xyz.qubs.engine.settings.SettingsManager.debugMode;

public class Engine {
	public static final Camera camera = new Camera();
	public static final Random random = new Random();
	public static final AtomicInteger fpsToShow = new AtomicInteger();
	public static final int UPDATES_PER_SECOND = 60;
	private static final float CAMERA_POS_STEP = 0.1f;
	private static final float MOUSE_SENSITIVITY = 1;
	private static final int MAX_BLOCK_TICK = 8;
	private static final Map<Integer, List<Supplier<Runnable>>> globalHooks = new HashMap<>();
	public static World world;
	public static ExecutorService threadPoolGeneration = Executors.newFixedThreadPool(4);
	public static ExecutorService threadPoolLighting = Executors.newFixedThreadPool(2);
	public static ExecutorService mainThreadPool = Executors.newSingleThreadExecutor();
	private static float cameraSpeedMultiplier = 1;
	private static int blockTick = 0;

	static {
		addHook(GLFW_KEY_F, () -> () -> System.out.println(camera.getChunk().getChunkPos() + ": " + ExtendedDataRegistry.BLOCK_COUNT_DATA_HANDLER.getData(camera.getChunk()).blockCount));
		addHook(GLFW_KEY_G, () -> () -> cameraSpeedMultiplier = 100000);
		addHook(GLFW_KEY_R, () -> () -> {
			Vector3i pos = camera.selectedBlockPos;
			if (pos != null) {
				world.setBlockType(pos.x, pos.y, pos.z, BlockTypeRegistry.AIR);
				camera.matrixChanged = true;
			}
		});

		addHook(GLFW_KEY_LEFT_CONTROL, () -> () -> cameraSpeedMultiplier = 10);
	}

	private final double MS_PER_UPDATE = 1000.0 / UPDATES_PER_SECOND;
	private final double MS_PER_UPDATE_INV = 1 / MS_PER_UPDATE;
	private final Vector3f change = new Vector3f();
	private final Vector3f cameraShift = new Vector3f();
	public Vector3f cameraInc = new Vector3f();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
//		EventManager.registerListeners(Engine.class);
		if (debugMode) {
//			Configuration.DEBUG.set(true);
//			Configuration.DEBUG_FUNCTIONS.set(true);
			Configuration.DEBUG_MEMORY_ALLOCATOR.set(true);
			Configuration.DEBUG_STACK.set(true);
		}

		Engine engine = new Engine();

		try {
			engine.run();
		} catch (Throwable e) {
			// hack to turn off memory leaks print if exception was caught, bc they are useless in this scenario
			Class<?> clazz = Class.forName("org.lwjgl.system.MemoryManage$DebugAllocator");
			Field field1 = clazz.getDeclaredField("ALLOCATIONS");
			field1.setAccessible(true);
			((ConcurrentMap<Long, ?>) field1.get(null)).clear();
			throw e;
		}
		dispose();
//		if (isDebugging && !EventManager.getRegisteredListeners().isEmpty()) {
//			EventManager.getRegisteredListeners().forEach(logger::error);
//		}

		Logger.instance.info("END");
	}

	private static void dispose() {
		Vulkan.disposed = true;
		Window.dispose();
		MouseInput.dispose();
		threadPoolGeneration.shutdown();
		threadPoolLighting.shutdown();
		try {
			threadPoolGeneration.awaitTermination(10, TimeUnit.SECONDS);
			threadPoolLighting.awaitTermination(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		UI.dispose();
		Vulkan.dispose();
	}

	public static void addHook(int key, Supplier<Runnable> function) {
		if (!globalHooks.containsKey(key)) {
			globalHooks.put(key, new ArrayList<>());
		}
		globalHooks.get(key).add(function);
	}

	public void run() {
		Logger.instance.info("START");
		if (debugMode) {
			Logger.instance.warn("DEBUG MODE IS ON");
		}

		Window.init();
		Vulkan.init();
		MouseInput.init();
		UI.init();
		world.generate();

		Vulkan.fillUBO();
		mainThreadPool.execute(() -> {
			Thread.currentThread().setPriority(1);
			double lag = 0.0;
			Timer timer = new Timer();
			while (!Window.shouldClose) {
				double elapsed = timer.getElapsedTime();
				lag += elapsed;

				if (lag >= MS_PER_UPDATE) {
					update();
					lag = 0;
				}
			}
		});
		inputDrawLoop();
		mainThreadPool.shutdown();
	}

	private void inputDrawLoop() {
		double lag = 0.0;
		Timer timer = new Timer();
		while (!Window.shouldClose) {
			double elapsed = timer.getElapsedTime();
			lag += elapsed;

			while (lag >= MS_PER_UPDATE) {
				glfwPollEvents();
				input();
				lag -= MS_PER_UPDATE;
			}

			Vulkan.drawFrame(lag * MS_PER_UPDATE_INV);
			fpsToShow.incrementAndGet();
		}
		Vulkan.waitIdle();
	}

	private void update() {
		Window.update();
		world.update();
		if (WorldRenderer.INSTANCE.frustumTicks > 0) {
			WorldRenderer.INSTANCE.frustumTicks--;
		}
	}

	private void input() {
		MouseInput.input();

		globalHooks.forEach((key, hooks) -> {
			if (Window.isKeyPressed(key)) {
				hooks.forEach(func -> {
					if (func.get() != null) {
						func.get().run();
					}
				});
			}
		});

		cameraInc.set(0, 0, 0);
		if (Window.isKeyPressed(GLFW_KEY_W)) {
			cameraInc.z = -1;
		} else if (Window.isKeyPressed(GLFW_KEY_S)) {
			cameraInc.z = 1;
		}
		if (Window.isKeyPressed(GLFW_KEY_A)) {
			cameraInc.x = -1;
		} else if (Window.isKeyPressed(GLFW_KEY_D)) {
			cameraInc.x = 1;
		}
		if (Window.isKeyPressed(GLFW_KEY_Z)) {
			cameraInc.y = -1;
		} else if (Window.isKeyPressed(GLFW_KEY_X)) {
			cameraInc.y = 1;
		}
		float offsetX = cameraInc.x * CAMERA_POS_STEP * cameraSpeedMultiplier;
		float offsetY = cameraInc.y * CAMERA_POS_STEP * cameraSpeedMultiplier;
		float offsetZ = cameraInc.z * CAMERA_POS_STEP * cameraSpeedMultiplier;

		cameraShift.add(camera.movePosition(offsetX, offsetY, offsetZ));
		Vector3i moveWorld = new Vector3i();
		for (int i = 0; i < 3; i++) {
			if (Math.abs(cameraShift.get(i)) >= IChunk.CHUNK_SIZE) {
				moveWorld.setComponent(i, (int) cameraShift.get(i) >> IChunk.LOG2_CHUNK_SIZE);
			}
		}
		cameraShift.x -= moveWorld.x << IChunk.LOG2_CHUNK_SIZE;
		cameraShift.y -= moveWorld.y << IChunk.LOG2_CHUNK_SIZE;
		cameraShift.z -= moveWorld.z << IChunk.LOG2_CHUNK_SIZE;

		if (!moveWorld.equals(0, 0, 0)) {
			world.shiftChunks(moveWorld.x, moveWorld.y, moveWorld.z);
		}

		boolean cameraUpdated = false;

		if (Window.cursorGrabbed && blockTick == 0) {
			if (MouseInput.isLeftButtonPressed()) {
				Vector3i pos = camera.selectedBlockPos;
				if (pos != null) {
					world.setBlockType(pos.x, pos.y, pos.z, BlockTypeRegistry.AIR);
					camera.matrixChanged = true;
					blockTick = MAX_BLOCK_TICK;
				}
			}
			if (MouseInput.isRightButtonPressed()) {
				Vector3i pos = camera.selectedBlockPos;
				if (pos != null && camera.selectedBlockSide != null) {
					Side side = camera.selectedBlockSide;
					pos = new Vector3i(pos).add(side.x, side.y, side.z);

					BlockPos blockPos = BlockPos.get(pos.x & (IChunk.CHUNK_SIZE - 1), pos.y & (IChunk.CHUNK_SIZE - 1), pos.z & (IChunk.CHUNK_SIZE - 1));
					SlabBlockType.Data data = (SlabBlockType.Data) BlockTypeRegistry.OAK_SLAB.createData();
					data.rotation = random.nextInt(2);
					Block block = new Block(blockPos, BlockTypeRegistry.OAK_SLAB, data);
					world.setBlock(pos.x, pos.y, pos.z, block);

					world.setBlockType(pos.x, pos.y, pos.z, BlockTypeRegistry.OAK_SLAB);
					((SlabBlockType.Data) world.getBlock(pos.x, pos.y, pos.z).data).rotation = random.nextInt(2);

					camera.matrixChanged = true;
					blockTick = MAX_BLOCK_TICK;
				}
			}
		}
		if (blockTick > 0) { blockTick--;}

		if (Window.cursorGrabbed) {
			Vector2f rotVec = MouseInput.getDisplVec();
			if (!rotVec.equals(0, 0)) {
				cameraUpdated = true;
				camera.moveRotation(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY);
			}
		}
		if (!cameraInc.equals(0, 0, 0) || cameraUpdated) {
			WorldRenderer.updateFrustum(false);
		}
		Vulkan.fillUBO();
		cameraSpeedMultiplier = 1;
		camera.rayCast();
	}
}
