#version 460

layout(location = 0) in vec2 fragTexCoord;
layout(location = 1) in vec4 fragColor;
layout(location = 2) in flat int texIndex;

layout(binding = 1) uniform sampler samp;
layout(binding = 2) uniform texture2D[2] tex;

layout(location = 0) out vec4 outColor;

void main() {
	vec4 texture = texture(sampler2D(tex[texIndex], samp), fragTexCoord);
	if (texture.a == 0) {
		discard;
	}
	if (texIndex == 0) {
		outColor = fragColor*texture;
	} else {
		outColor = vec4(1, 0, 0, texture.a);
	}
}