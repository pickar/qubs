#version 460

layout(location = 0) in int data;
layout(location = 1) in int data1;
layout(location = 2) in int data2;
layout(location = 3) in int data3;

layout(binding = 0) uniform viewproj {
	mat4 proj;
	mat4 view;
};

layout(binding = 3) uniform models {
	vec4 model[16];
};

layout(location = 0) out vec2 fragTexCoord;
layout(location = 1) out vec3 fragColor;
layout(location = 2) out int texIndex;

const float[4] ao = { 0, 0.25, 0.45, 0.75 };

vec3 getPosition(int data) {
	return vec3((data >> 27) & 31, (data >> 22) & 31, (data >> 17) & 31);
}

vec2 getTexCoord(int data) {
	return vec2((data >> 14) & 1, (data >> 13) & 1);
}

float getAo(int data) {
	return ao[(data >> 15) & 3];
}

int getTextureId(int data) {
	return data & 8191;
}

vec3 getColor(int data) {
	return vec3(((data >> 24) & 0xff), ((data >> 16) & 0xff), ((data >> 8) & 0xff)) / 255.0;
}

vec3 getChunkPos(int index) {
	int indy = (index+1);
	int indz = (index+2);

	return vec3(model[index >> 2][index & 3], model[indy >> 2][indy & 3], model[indz >> 2][indz & 3]);
}

void main() {
	gl_Position = proj * view  * vec4(getChunkPos(gl_InstanceIndex)+getPosition(data), 1.0);
	fragTexCoord = getTexCoord(data);
	fragColor = vec3(1-getAo(data))*getColor(data1);
	texIndex = getTextureId(data);
}