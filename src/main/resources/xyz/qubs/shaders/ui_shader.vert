#version 460

struct Vec1 {
	int pos;
	int ztex;
	int size;
	int clipPos;
};

struct Vec2 {
	int clipSize;
	int color;
	int rounding;
	int none2;
};

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;
layout(location = 2) in int data;

layout(location = 0) out int index;
layout(location = 1) out vec2 fragTexCoord;

layout(set = 0, binding = 0) uniform Matrix {
	mat4 proj;
};
layout(set = 0, binding = 1) uniform Vectors {
	Vec1[16] data1;
};
layout(set = 0, binding = 2) uniform Vectors2 {
	Vec2[16] data2;
};

mat4 scale = mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
mat4 position = mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

int getData(int compressed, int i) {
	if (i == 0) {
		return compressed & 0x0000FFFF;
	} else {
		return (compressed >> 16) & 0x0000FFFF;
	}
}

void main() {
	scale[0][0] = getData(data1[gl_InstanceIndex].size, 0);
	scale[1][1] = getData(data1[gl_InstanceIndex].size, 1);

	position[3][0] = getData(data1[gl_InstanceIndex].pos, 0);
	position[3][1] = getData(data1[gl_InstanceIndex].pos, 1);
	position[3][2] = float(getData(data1[gl_InstanceIndex].ztex, 0)/65536.0)-0.01;

	gl_Position = proj * position * scale * vec4(inPosition, 1.0);
	index = gl_InstanceIndex;

	fragTexCoord = inTexCoord;
}