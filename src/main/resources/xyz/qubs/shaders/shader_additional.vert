#version 460

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;
layout(location = 2) in int data;

layout(binding = 0) uniform UBO {
	mat4 viewproj;
	mat4 selectedBlockPos;
	mat4 crossPos;
	int info;
};

layout(location = 0) out vec2 fragTexCoord;
layout(location = 1) out vec4 fragColor;
layout(location = 2) out int texIndex;

void main() {
	if (gl_InstanceIndex == 0) {
		gl_Position = viewproj * selectedBlockPos * vec4(inPosition, 1.0);
		vec4 color = vec4(0, 0, 0, 1);
		color.a = 0.4;
		if (info == -1) {
			color.a = 0;
		} else if (floor(gl_VertexIndex / 4) == info) {
			//		color.r = 1;
			color.a = 0.8;
		}
		fragColor = color;
	} else if (gl_InstanceIndex == 1) {
		gl_Position = crossPos * vec4(inPosition.xy, 0.5, 1.0);
		//		fragColor = vec4(0, 0, 0, 0);
	}
	fragTexCoord = inTexCoord;
	texIndex = data & 0x3fffffff;
}