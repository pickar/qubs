#version 460

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;
layout(location = 2) in int data;

layout(binding = 0) uniform viewproj {
	mat4 proj;
	mat4 view;
};

layout(binding = 3) uniform models {
	vec4 model[16];
};

layout(location = 0) out vec2 fragTexCoord;
layout(location = 1) out vec3 fragColor;
layout(location = 2) out int texIndex;

const float[4] ao = { 0, 0.25, 0.45, 0.75 };

vec3 getPos(int index) {
	int indy = (index+1);
	int indz = (index+2);

	return vec3(model[index >> 2][index & 3], model[indy >> 2][indy & 3], model[indz >> 2][indz & 3]);
}

void main() {
	gl_Position = proj * view  * vec4(getPos(gl_InstanceIndex)+inPosition, 1.0);
	fragTexCoord = inTexCoord;
	vec3 color = vec3(1-ao[((data >> 30) & 0x3)]);
	fragColor = color;
	texIndex = data & 0x3fffffff;
}