#version 460

struct Vec1 {
	int pos;
	int ztex;
	int size;
	int clipPos;
};

struct Vec2 {
	int clipSize;
	int color;
	int rounding;
	int none2;
};

layout(location = 0) in flat int index;
layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 1) uniform Vectors {
	Vec1[16] data1;
};
layout(set = 0, binding = 2) uniform Vectors2 {
	Vec2[16] data2;
};

layout(set = 1, binding = 0) uniform sampler samp;
layout(set = 1, binding = 1) uniform texture2D textures[texture_count];

int getData(int compressed, int i) {
	if (i == 0) {
		return compressed & 0x0000FFFF;
	} else {
		return (compressed >> 16) & 0x0000FFFF;
	}
}

vec4 intToColor(int value) {
	float b = value & 0xFF;
	float g = (value >> 8) & 0xFF;
	float r = (value >> 16) & 0xFF;
	float a = (value >> 24) & 0xFF;

	return vec4(r/255.0, g/255.0, b/255.0, a/255.0);
}

void main() {
	int tex = getData(data1[index].ztex, 1)-1;
	outColor = intToColor(data2[index].color);
	if (tex != -1) {
		outColor *= texture(sampler2D(textures[tex], samp), fragTexCoord);
	}

	vec2 pos = vec2(getData(data1[index].pos, 0), getData(data1[index].pos, 1));
	vec2 size = vec2(getData(data1[index].size, 0), getData(data1[index].size, 1));

	vec2 clipPos = vec2(getData(data1[index].clipPos, 0), getData(data1[index].clipPos, 1));
	vec2 clipSize = vec2(getData(data2[index].clipSize, 0), getData(data2[index].clipSize, 1));

	vec2 texCoord = fragTexCoord;
	texCoord *= size;
	texCoord.y *= -1;
	texCoord += pos;

	if (texCoord.x < clipPos.x || texCoord.y < clipPos.y || texCoord.x >= clipPos.x+clipSize.x || texCoord.y >= clipPos.y+clipSize.y) {
		outColor.a = 0;
	}

	if (data2[index].rounding > 0 && outColor.a != 0) {
		float f = length(max(abs(texCoord - (pos+size/2)) - size/2 + data2[index].rounding/2, 0.0)) - data2[index].rounding/2;
		if (f > 0.0) {
			outColor = vec4(0);
		}
	}
}